var bm = {prefix: ''};
// biz player
bm.Player = (function() {
  var instance = null;
  return {
    init: function(player_id) {
      // player id
      if (player_id === undefined)
        player_id = "bm_wav_player";

      if (jQuery('#' + player_id).length === 0) {
        jQuery('body').append('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="' + player_id + '" align="middle"><embed src="/wavplayer.swf?gui=none&h=1&w=1" bgcolor="#ffffff" width="1" height="1" allowScriptAccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" /></object>');
      }
      // player instance
      instance = document.getElementById(player_id);
      if (!instance.doPlay) {
        for (var i = 0; i < instance.childNodes.length; i++) {
          var child = instance.childNodes[i];
          if (child.tagName === "EMBED")
            instance = child;
          break;
        }
      }
    },
    play: function(name) {
      try {
        if (instance)
          instance.play(bm.prefix + '/sound/' + name + '.wav');
      } catch (err) {

      }
    }
  };
})();

// nodejs
bm.Sockjs = (function() {
  var _initialized = false;
  var _socket = false;
  var _ready = false;
  var _queue_emit = [];
  var _listener = {};
  var _dev = false;
  return {
    init: function(_options) {
      if (typeof SockJS === 'undefined') {
        if (_dev)
          console.log('Warning : SockJS IO error');
        return false;
      }
      if (_initialized) {
        if (_dev)
          console.log('Warning : Already initialized');
        return false;
      }

      _initialized = true;

      if (_dev)
        console.log('Info : Initialized Success');

      _socket = new SockJS('http://' + _options['domain'] + ':' + _options['port'] + '/' + _options['pref']);
      // socket connection

      // on opened
      _socket.onopen = function() {
        if (_dev)
          console.log('Info : Connect Success');
        _ready = true;
        // auth bolsoni daraa queud eventuudee duudna
        for (var j = 0, jj = _queue_emit.length; j < jj; j++) {
          _socket.send(_queue_emit[j]);
        }
      };

      _socket.onclose = function() {
        if (_dev)
          console.log('Info : connection closed:');
      };
      // on message
      _socket.onmessage = function(e) {
        var data = JSON.parse(e.data);
        // listener blah blah
        if (_listener.hasOwnProperty(data.type)) {
          var fn = _listener[data.type];
          fn(data);
        } else {
          if (_dev)
            console.log('Info : unknown event received: ' + data.type);
        }
      };
      return true;
    },
    emit: function() {
      var data = arguments.length > 1 ? arguments[1] : {};
      var params = JSON.stringify(jQuery.extend({}, data, {
        type: arguments[0]
      }));
      _ready ? _socket.send(params) : _queue_emit.push(params);
    },
    on: function() {
      if (arguments.length === 2) {
        _listener[arguments[0]] = arguments[1];
      }
    }
  };
})();

// biz chat
bm.Notification = (function() {
  var initialized = false,
          cid = 0,
          counter = 0,
          pids = [];
  return {
    init: function() {
      if (initialized) {
        return;
      }

      $('.ad-notification-link').click(function() {
        // TODO CHECK BROWSE OR BLAH BLAH
        if (cid > 0) {
          document.location.href = bm.prefix + '/c/' + cid + '?pids=' + pids.join(',');
        } else {
          document.location.href = bm.prefix + '/n?pids=' + pids.join(',');
        }
        return false;
      });

      initialized = true;
      // event listeners
      bm.Sockjs.on('notification', function(data) {
        // msg listen client
        var _pids = data.pids.split(",");

        // array push blah blah
        for (var i = 0, len = _pids.length; i < len; i++) {
          pids.push(_pids[i]);
        }
        counter += parseInt(data.count, 10);

        //
        $('.ad-notification-count').html(counter);

        // 
        $('.ad-notification').show().addClass("active").delay(600).queue(function() {
          $(this).removeClass("active");
          // $(this).hide();
          $(this).dequeue();
        });

        // play notification sound
        bm.Player.play('notice');
      });
    },
    joinChannel: function(_cid) {
      if (_cid) {
        $('#button-add-product').attr('href', bm.prefix + '/add?cid=' + _cid);
      } else {
        $('#button-add-product').attr('href', bm.prefix + '/add');
      }
      $('.ad-notification').hide();

      // before joining leave room
      bm.Sockjs.emit('leave_channel', {
        cid: cid
      });
      // reset counter
      counter = 0;
      pids = [];

      cid = _cid;
      bm.Sockjs.emit('join_channel', {
        cid: _cid
      });
    }
  };
})();



bm.Notification1 = (function() {
  var initialized = false,
          counter = 0,
          pids = [],
          timestamp = Math.round(new Date().getTime() / 1000),
          intervalId = null,
          cid = 0;

  return {
    init: function() {
      if (initialized) {
        return;
      }
      $('.ad-notification-link').click(function() {
        // TODO CHECK BROWSE OR BLAH BLAH
        if (cid > 0) {
          document.location.href = bm.prefix + '/c/' + cid + '?pids=' + pids.join(',');
        } else {
          document.location.href = bm.prefix + '/n?pids=' + pids.join(',');
        }
        return false;
      });
      initialized = true;
    },
    joinChannel: function(_cid) {
      if (_cid) {
        $('#button-add-product').attr('href', bm.prefix + '/add?cid=' + _cid);
      } else {
        $('#button-add-product').attr('href', bm.prefix + '/add');
      }

      $('.ad-notification').hide();

      // reset data counter blah blah
      counter = 0;
      pids = [];

      cid = _cid;
      if (intervalId !== null) {
        window.clearInterval(intervalId);
      }
      // 10 second tutamd
      intervalId = window.setInterval(function() {
        // ajax
        $.ajax({
          url: bm.prefix + '/product/ajaxNotification',
          data: {cid: _cid, timestamp: timestamp},
          dataType: 'json',
          success: function(data) {
            // results
            $.each(data['results'], function(key, value) {
              var _pids = value['pids'].split(",");

              // array push blah blah
              for (var i = 0, len = _pids.length; i < len; i++) {
                pids.push(_pids[i]);
              }
              counter += parseInt(value.count, 10);
            });

            // is product added
            if (data['results'].length > 0) {
              //
              $('.ad-notification-count').html(counter);

              // 
              $('.ad-notification').show().addClass("active").delay(600).queue(function() {
                $(this).removeClass("active");
                // $(this).hide();
                $(this).dequeue();
              });

              // play notification sound
              bm.Player.play('notice');
            }

            // timestamp
            timestamp = data['timestamp'];
          }
        });
      }, 10 * 1000);
    }
  };
})();
// ========================
// category selector form
// ========================


$(document).ready(function() {
  $("#product-form").on("click", "#product-submit", function() {

    $('#duplicate').val('1');

    var _this = $(this);
    var _isLoading = _this.data("loading");
    if (_isLoading) {
      return false;
    }
    _isLoading = true;
    var _loader = $('<img src="' + bm.prefix + '/images/loading.gif" />');
    _this.after(_loader);
    $.ajax({
      url: bm.prefix + '/product/isValid',
      data: $('#product-form').serialize(),
      type: 'POST',
      dataType: 'json',
      beforeSend: function() {
        $('#product-submit').attr('disabled','disabled');
      },
      success: function(data) {
        $('#product-submit').removeAttr('disabled');
        $('#product-form .input-error').removeClass('input-error');

        _loader.remove();
        _isLoading = false;
        // remove error classes

        $.each(['category_id','category_id_leaf', 'name', 'description', 'password', 'contact', 'price'], function(key, value) {
          if (value === 'category_id_leaf') {
            $('#' + value).parent('div').removeClass('input-error');
          } else {
            $('#' + value).removeClass('input-error');
          }
        });

        if (data.errors.length > 0) {
          var error_text = '';

          // 
          $('html,body').animate({scrollTop: $('.product_category:first').offset().top}, 500);
          $.each(data.errors, function(key, value) {

            $('#' + value).addClass('input-error');
            
             if (value ==='category_id') {
              $('#' + value).parent('div').addClass('input-error');
              error_text += ' - Та категороо сонгоогүй байна \n';
            }
            else if (value ==='category_id_leaf') {
              
              error_text += ' - Та категороо гүйцэт сонгоогүй байна \n';
            } else if (value === 'name') {
              error_text += ' - Та гарчигаа оруулаагүй байна \n';
            } else if (value === 'description') {
              error_text += ' - Та зарын тайлбараа оруулаагүй байна \n';
            } else if (value === 'contact') {
              error_text += ' - Та утасны дугаараа оруулаагүй байна \n';
            } else if (value === 'password') {
              error_text += ' - Та зарын нууц кодоо оруулаагүй байна \n';
            } else if (value === 'duplicate') {
              error_text += ' - Та өдөрт 1 зарыг 1 л удаа оруулна \n';
            } else if (value === 'vnagree') {
              error_text += ' - Та үйлчилгээний нөхцөл зөвшөөрөөгүй байна';
            } else {
              //$('#' + value).addClass('input-error');
            }
          });

          if(error_text.length > 0) {
            alert('ТА ӨГӨГДӨЛ ДУТУУ ОРУУЛСАН БАЙНА. \n\n' + error_text);
          }
        } else {
          // submit form
          $('#product-submit').attr('disabled','disabled');
          $('#product-form').submit();
        }
      }
    });
    return false;
  });
  $("#product-form").on("change", ".product_category", function() {
    var _this = $(this);
    // $('#product_category_id').val('');
    $('#category_id').val(_this.val());
    // remove next siblings
    _this.parent().nextAll('.product-category-main').remove();
    // value
    if (_this.val()) {
// ajax loader
      var _loader = $('<div class="form-col"><img style="padding-top: 5px" src="' + bm.prefix + '/images/loading.gif" /></div>');
      _this.parent().after(_loader);
      $('#attributeform').html('');
      // ajax request
      $.ajax({
        url: bm.prefix + '/category/ajaxList',
        dataType: 'json',
        data: {id: _this.val()},
        type: 'POST',
        success: function(data) {
          _loader.remove();

          if (data.length > 0) {
            // content
            var _content = '<div class="col-md-4 mr-b-15 product-category-main"><label for="angilal"><span class="red">#</span> АНГИЛАЛ СОНГОХ</label><select class="product_category form-control"><option value="">Сонгоно уу</option>';
            var _raquo = '';
            // data
            $.each(data, function(key, value) {
              _raquo = (value.leaf === 'true' ? '' : " &raquo;");
              _content += '<option value="' + value.id + '" data-leaf="' + value.leaf + '">' + value.name + _raquo + '</option>';
            });
            _content += '</select></div>';
            // append
            _this.parent().after(_content);
          } else {
            // location id
            _this.parent().after($('<div class="col-md-4 mr-b-15 product-category-main"><img style="padding-top: 5px" src="/images/icons/tick.png"></div>'));
          }
          //load attribute
          $.ajax({
            url: bm.prefix + '/category/attribute',
            data: {id: _this.val()},
            type: 'POST',
            success: function(data) {
              $('#attributeform').html(data);
            }
          });
          //end load attribute
        }
      });
    }
  });


  // =======================
  //  loading bar begin
  // =======================
  var _loadingbarOptions = {
    replaceURL: true,
    async: true,
    complete: function(xhr, text) {
    },
    cache: true,
    error: function(xhr, text, e) {
    },
    global: true,
    dataType: 'json',
    statusCode: {},
    success: function(data, text, xhr) {
    },
    done: function(data) {
      $(document).attr('title', data.title);
      $('#content').html(data.content);

      loaderBarContentInit();
    }
  };

  function loaderBarContentInit() {
    $('[data-toggle="tooltip"]').tooltip({placement: 'auto'});
    $("[rel=tooltip]").tooltip({placement: 'right'});

    $("#content .loading-bar").loadingbar(_loadingbarOptions);
  }
  function loaderBarHeaderInit() {
    $("#header .loading-bar").loadingbar(_loadingbarOptions);
  }
  // check history pushStata is blah blah
  if (Modernizr.history) {
    //
    loaderBarHeaderInit();

    loaderBarContentInit();
    //if (history.replaceState && history.length == 0)
    //    history.pushState({}, document.title, document.location);

    if (window.addEventListener) {
      window.addEventListener('load', function() {
        setTimeout(function() {
          window.addEventListener('popstate', function(e) {
            $.ajax({
              url: document.location,
              beforeSend: function(xhr) {
                xhr.setRequestHeader('X-Requested-With', {
                  toString: function() {
                    return '';
                  }
                });
                xhr.setRequestHeader('X-Push-State', 'PushState');
                // 
                if ($("#loadingbar").length === 0) {
                  $("body").append("<div id='loadingbar'></div>");
                  $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
                  $("#loadingbar").width((50 + Math.random() * 30) + "%");
                }
              },
              dataType: 'json'
            }).always(function() {
              $("#loadingbar").width("101%").delay(200).fadeOut(400, function() {
              });
            }).done(function(data) {
              $(document).attr('title', data.title);
              $('#content').html(data.content);

              loaderBarContentInit();
              $(this).remove();
            });
          });
        }, 0);
      });
    }
  }// end of history loading blah blah
});

$(document).ready(function() {
  //show search suggestion
  var searchTimer = null;
  var _value = '';


  /*
   - ene function-iig ajilahgui bolgoloo. word table hereglegdehgui bolson.
   - listen hiij bgaa classin neriig oorchilow
  */
  $('.ad-search-txt_DISABLED').keyup(function() {
    
    if (_value === $(this).val().trim()) {
      return;
    }
    if (searchTimer !== null) {
      clearTimeout(searchTimer);
    }

    $('.search-suggestion').hide();

    // value
    _value = $(this).val().trim();

    searchTimer = setTimeout(function() {
      $.ajax({
        url: bm.prefix + '/product/suggest',
        data: {keyword: _value},
        dataType: 'json',
        success: function(data) {
          // TODO FIX for exploit
          var _content = '';
          $.each(data, function(key, value) {
            _content += '<li>' + '<a href="/search?keyword=' + value + '">' + value + '</a>' + '</li>';
          });

          $('#suggestion-list').html(_content);

          if (_content !== '') {
            $('.search-suggestion').show();
          }
        }
      });
    }, 800);
  });



  //hide search suggestion
  $(window).click(function(e) {
    if ($(e.target).parents('.ad-search').length === 0) {
      $('.search-suggestion').hide();
    }
  });

  // modal box
  $("#main").on("click", "#openBtn", function() {
    
    var _this = $(this);
    $('.modal-body').load(_this.attr('href'), function(result) {
      $('#myModal').modal({
        backdrop: 'static',
        show: true
      });
    });
    return false;
  });



  // modal box
  $("#myModal").on("click", "#subscribe-form-button", function() {
    var _form = $('#subscribe-form');
    $.ajax({
      url: _form.attr('action'),
      data: _form.serialize(),
      type: 'POST',
      dataType: 'json',
      success: function(data) {
        $('#subscribe-form .input-error').removeClass('input-error');

        if (data.errors.length > 0) {
          $.each(data.errors, function(key, value) {
            $('#subscribe_' + value).addClass('input-error');
          });
        } else {
          // close blah blah 
          $('#myModal').modal('hide');
          $('.alert-email-subscribe').hide();
        }
      }
    });
    return false;
  });

  //zar duguilah
  $('body').on('click', '.toggle-check-trigger', function(event) {
    var _e = $(this);
    _e.toggleClass('saved');

    $.ajax({
      url: bm.prefix + '/i/toggle',
      data: {pid: _e.data('pid')},
      success: function(data) {
        $('.checked-ads-count').text(data);
        $('.my-checked-ads').addClass("active").delay(600).queue(function() {
          $(this).removeClass("active");
          $(this).dequeue();
        });
      }
    });
  });
});


$(document).ready(function() {
  //-----------Auto validation on submit
  $("#automain").on("click", "#autoproduct-submit", function() {
    $('#autoproduct-submit').removeAttr('disabled');
    $('#duplicate').val('1');
    if($('#price').val()==0)
    {
      $('#price').val($('#priceother').val());
    }
    var _this = $(this);
    var _isLoading = _this.data("loading");
    if (_isLoading) {
      return false;
    }
    
    _isLoading = true;
    var _loader = $('<img src="' + bm.prefix + '/images/loading.gif" />');
    _this.after(_loader);
    $.ajax({
      url: bm.prefix + '/product/autoIsValid',
      data: $('#autoproduct-form').serialize(),
      type: 'POST',
      dataType: 'json',
      beforeSend: function() {
        $('#autoproduct-submit').attr('disabled','disabled');
      },
      success: function(data) {
        $('#autoproduct-submit').removeAttr('disabled');
        $('#autoproduct-form .input-error').removeClass('input-error');

        _loader.remove();
        _isLoading = false;
        // remove error classes

        $.each(['category_id', 'name', 'description', 'password', 'contact', 'price', 'vnagree'], function(key, value) {
          if (value === 'category_id') {
            $('#' + value).parent('div').removeClass('input-error');
          } else {
            $('#' + value).removeClass('input-error');
          }
        });

        if (data.errors.length > 0) {
          var error_text = '';

          // 
          $('html,body').animate({scrollTop: $('#automain:first').offset().top}, 500);
          $.each(data.errors, function(key, value) {

            $('#' + value).addClass('input-error');

            if (value === 'category_id') {
              //$('#' + value).parent('div').addClass('input-error');
              error_text += ' - Та маркаа гүйцэт сонгоогүй байна \n';
            } else if (value === 'name') {
              error_text += ' - Та гарчигаа оруулаагүй байна \n';
            } else if (value === 'description') {
              error_text += ' - Та зарын тайлбараа оруулаагүй байна \n';
            } else if (value === 'contact') {
              error_text += ' - Та утасны дугаараа оруулаагүй байна \n';
            } else if (value === 'password') {
              error_text += ' - Та зарын нууц кодоо оруулаагүй байна \n';
            } else if (value === 'duplicate') {
              error_text += ' - Та өдөрт 1 зарыг 1 л удаа оруулна \n';
            } else if (value === 'vnagree') {
              error_text += ' - Та үйлчилгээний нөхцөл зөвшөөрөөгүй байна';
            } else {
              //$('#' + value).addClass('input-error');
            }
          });

          if(error_text.length > 0) {
            alert('ТА ӨГӨГДӨЛ ДУТУУ ОРУУЛСАН БАЙНА. \n\n' + error_text);
          }
        } else {
          // submit form
          $('#autoproduct-submit').attr('disabled','disabled');
          $('#autoproduct-form').submit();
        }
      }
    });
    return false;
  });


  //---------Property validation 
  $("#propertymain").on("click", "#propertyproduct-submit", function() {
    $('#propertyproduct-submit').removeAttr('disabled');
    $('#duplicate').val('1');
    
    var _this = $(this);
    var _isLoading = _this.data("loading");
    if (_isLoading) {
      return false;
    }
    
    _isLoading = true;
    var _loader = $('<img src="' + bm.prefix + '/images/loading.gif" />');
    _this.after(_loader);
    $.ajax({
      url: bm.prefix + '/product/propertyIsValid',
      data: $('#propertyproduct-form').serialize(),
      type: 'POST',
      dataType: 'json',
      beforeSend: function() {
        $('#propertyproduct-submit').attr('disabled','disabled');
      },
      success: function(data) {
        $('#propertyproduct-submit').removeAttr('disabled');
        $('#propertyproduct-form .input-error').removeClass('input-error');

        _loader.remove();
        _isLoading = false;
        // remove error classes

        $.each(['category_id', 'name', 'description', 'password', 'contact', 'price','location_id'], function(key, value) {
          if (value === 'category_id') {
            $('#' + value).parent('div').removeClass('input-error');
          } else {
            $('#' + value).removeClass('input-error');
          }
        });

        if (data.errors.length > 0) {
          var error_text = '';

          // 
          $('html,body').animate({scrollTop: $('#propertymain:first').offset().top}, 500);
          $.each(data.errors, function(key, value) {
            console.log(value);
            $('#' + value).addClass('input-error');

            if (value === 'category_id') {
              //$('#' + value).parent('div').addClass('input-error');
              error_text += ' - Та маркаа гүйцэт сонгоогүй байна \n';
            } else if (value === 'name') {
              error_text += ' - Та гарчигаа оруулаагүй байна \n';
            } else if (value === 'description') {
              error_text += ' - Та зарын тайлбараа оруулаагүй байна \n';
            } else if (value === 'contact') {
              error_text += ' - Та утасны дугаараа оруулаагүй байна \n';
            } else if (value === 'password') {
              error_text += ' - Та зарын нууц кодоо оруулаагүй байна \n';
            } else if (value === 'duplicate') {
              error_text += ' - Та өдөрт 1 зарыг 1 л удаа оруулна \n';
            } else if (value === 'location_id') {
              error_text += ' - Та байршилаа сонгоогүй байна';
            } else {
              //$('#' + value).addClass('input-error');
            }
          });

          if(error_text.length > 0) {
            alert('ТА ӨГӨГДӨЛ ДУТУУ ОРУУЛСАН БАЙНА. \n\n' + error_text);
          }
        } else {
          // submit form
          $('#propertyproduct-submit').attr('disabled','disabled');
          $('#propertyproduct-form').submit();
        }
      }
    });
    return false;
  });
}); 
