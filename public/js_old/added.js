function changeBg(athis, bg)
{
  $('#list_banners').find('li').each(function(){
      var current = $(this);
        current.children().children().removeAttr('style');
        current.children().children().html(current.children().children().attr('my_text'));
  });
  athis.css('background-image', 'url(' + bg + ')');
  athis.attr('my_text',athis.html());
  athis.html('');
}

function autoChangeBanners()
{
    var too = $('#list_banners').children().length;
    var i = 1;
    for(j = 0; j< too * 2; j++){
          var time = 5000 * j;
          setTimeout(function() {
              if(i == too)
              {
                 i = 1;
              }
              else
              {
                i++;
              }
              
              $("#tab_click_"+i).trigger("click");
          },time);
    }
    
}

jQuery(document).ready(function() {
  if($('#various3').length)
  { 
      console.log('hello');
      $("#various3").fancybox();
  }
  
  $('.form-row input[type="text"]').tooltip();


  if($('.banner-top').length)
  {
        $( "#tabs" ).tabs({
           activate: function( event, ui ) {
              var result = $( "#result-2" ).empty();
              result.append( "activated" );
           },
           create: function( event, ui ) {
              var result = $( "#result-1" ).empty();
              result.append( "created" );
           }
           
           
        });

        autoChangeBanners();
  }
  

// if(Arg("keyword")){
// 	$('.container').highlight(Arg("keyword"), { wordsOnly: true });
// }

// $(".banner").click(function () {
//     // slider.stopAuto();
//     // slider.startAuto();
// });
// $(".bx-pager-link").click(function () {
//     slider.stopAuto();
//     slider.startAuto();
// });
// $(".bx-pager-item").click(function () {
//     slider.stopAuto();
//     slider.startAuto();
// });

// page show, next prev product
$(document).keydown(function(e){
  if (typeof pnext != 'undefined') {
		if (e.keyCode == 37) {
			$(".overlay-arrows-previous-anchor-image").css("background-color", 'yellow');
			window.location.href = "/p/" + pnext;
			return false;
		}
	}
  if (typeof pprev != 'undefined') {
		if (e.keyCode == 39) {
			$(".overlay-arrows-next-anchor-image").css("background-color", 'yellow');
			window.location.href = "/p/" + pprev;
			return false;
		}
	}
});


//product add: photo upload
if($('#image_container .image-select').length > 0) {

    // number formatter
    if($('#price').length)
    {
      $('#price').number(true, 0);
    } 
    
    // images
    $('#image_container .image-select').each(function() {
      var __element = $(this);
      var __parentDiv = __element.parents('div:first');
      var __imageUploaded = __parentDiv.find('.image-uploaded');
      var __imageDelete = __parentDiv.find('.image-delete');
      var __inputHidden = __parentDiv.find('.image-filename');

      // delete click blah blah
      __imageDelete.click(function() {
        // jquery ajax delete blah2
        $.ajax({
          url: '/image/delete',
          data: {filename: __inputHidden.val(), pid: $('#product_id').val()},
          type: 'POST'
        });

        __parentDiv.removeClass('uploaded');
        __imageUploaded.html('');
        __inputHidden.val('');
      });

      //upload blah blah
      new AjaxUpload(__element, {
        action: '/image/upload',
        responseType: 'json',
        onSubmit: function(file, ext) {
          if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)) {
            var randwid = randomIntFromInterval(10, 60);
            $('#prog-bars').append('<div class="progress" rel="'+file+'"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'+randwid+'" aria-valuemin="0" aria-valuemax="100" style="width:'+randwid+'%">'+file+'</div></div>');
            __parentDiv.addClass('uploading');
            setTimeout(function(){ 
            $('div[rel^="'+file+'"]').children().css('width', '70%');
            }, 300);
            setTimeout(function(){ 
            $('div[rel^="'+file+'"]').children().css('width', '80%');
            }, 300);
            setTimeout(function(){ 
            $('div[rel^="'+file+'"]').children().css('width', '90%');
            }, 300);

          } else {
            alert('Та зөвxөн (png, jpg, gif) өргөтгөлтэй файл оруулна уу.');
            return false;
          }
        },
        onComplete: function(file, response) {
          __parentDiv.removeClass('uploading');
          if (response.error === true) {
            alert(response.msg);
          } else {
            $('div[rel^="'+file+'"]').children().css('width', '100%');
            __inputHidden.val(response.filename);
            __imageUploaded.html('<img src="/uploads/tmp/' + response.filename + '" />');
            // add uploaded class
            __parentDiv.addClass('uploaded');
            $('div[rel^="'+file+'"]').children().css('width', '100%');
            // $('#prog-bars').last().children('.progress-bar').attr('aria-valuenow', 100);
            // $('#prog-bars').last().children('.progress-bar').css('width', 100%);
          }
        }
      });
    });
}

});

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}
//product show.action: checkedit
function chdt(pid) {
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: {pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/e/' + pid;
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    return false;
}

//product show.action: checkrenew
function chrenew(pid) {
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: {pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/renew/' + pid;
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    return false;
}

//product autoshow.action: checkedit
function achdt(pid) {
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: {pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/autoEdit/' + pid;
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    return false;
}

function pochdt(pid) {
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: {pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/propertyEdit/' + pid;
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    return false;
}

//product show.action: checkdelete
function chdl(pid) {
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: { pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/del/' + pid + '&s=1';
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    return false;
}

function chds(pid) {
    if($('#product_password').val()){
    $.ajax({
      url: '/product/checkCode',
      dataType: 'json',
      data: { pid: $('#product_id').val(), password: $('#product_password').val()},
      success: function(data) {
        if (data.isValid) {
          document.location.href = '/addBasket/' + pid;
        } else {
          $('#product_password').addClass('input-error');
        }
      }
    });
    }else{
      $('#product_password').addClass('input-error');
      $( "#dialog" ).dialog();
    }
    return false;
}

function submitForm() {
  document.getElementById("login_form").submit();
}

function submitRegisterForm() {
  document.getElementById("register_form").submit();
}

// search function nemsen
dropdown();
function dropdown() {
      var widget = $(this);
      var label = widget.find('span.valueOfButton');
      var list = $( "ul#suggestion-list" );
      var selected;
      var highlighted;
      var select = function(i) {selected = $(i);label.text(selected.text());$("#keyword").val(selected.text());};
      var highlight = function(i) {highlighted = $(i);highlighted.addClass('selected').siblings('.selected').removeClass('selected');};
      var hover = function(event) {highlight(this);};
      var rebind = function(event) {bind();};
      var bind = function() {list.on('mouseover', 'li', hover);widget.off('mousemove', rebind);};
      var unbind = function() {list.off('mouseover', 'li', hover);widget.on('mousemove', rebind);};
      list.on('click', 'li', function(event) {select(this);});
      widget.keydown(function(event) {unbind();switch(event.keyCode) {case 38:highlight((highlighted && highlighted.prev().length > 0) ? highlighted.prev() : list.children().last());break;case 40:highlight((highlighted && highlighted.next().length > 0) ? highlighted.next() : list.children().first());break;case 13:if(highlighted) {select(highlighted);}break;}});bind();
}

//zar duguilah
  $('body').on('click', '.toggle-check-controller', function(event) {
    var _e = $(this);
    _e.parents('.btn-haritsuulah-circle').toggleClass('duguilsan');

    $.ajax({
      url: bm.prefix + '/i/basket',
      data: {pid: _e.data('pid')},
      success: function(data) {
        // $('.my-checked-ads').addClass("active").delay(600).queue(function() {
        //   $(this).removeClass("active");
        //   //$(this).dequeue();
        // });
      }
    });
  });

//autozar duguilah
  function autoToggle(id, _this) {
    var _e = $(_this);
    _e.children('span').toggleClass('activeround');

    $.ajax({
      url: bm.prefix + '/i/autotoggle',
      data: {pid: id},
      success: function(data) {
        $('.count_square').html(data);

        $('.search_content_right').addClass("active").delay(600).queue(function() {
          $(this).removeClass("active");
          $(this).dequeue();
        });
      }
    });
  }

  //autozar duguilah
  function propertyToggle(id, _this) {
    var _e = $(_this);
    _e.children('span').toggleClass('activeroundProperty');

    $.ajax({
      url: bm.prefix + '/i/propertytoggle',
      data: {pid: id},
      success: function(data) {
        $('.count_square').html(data);

        $('.search_content_right').addClass("active").delay(600).queue(function() {
          $(this).removeClass("active");
          $(this).dequeue();
        });
      }
    });
  }


$( document ).ready(function() {
  // if($('.auto_more_heading').length) 
  //   {
  //     if($(window).width() > 950)
  //       {
  //         $('.auto_more_heading').css("width",'648px');
  //         $('.auto_more_heading h2').css('font-size','16px');
  //         $('.sorting').removeAttr('width');
  //         $('.auto_more_heading h2').next().css('width','287px');

  //       }
  //       if($(window).width() < 950 && $(window).width() > 750 )
  //       {
  //         $('.auto_more_heading').css("width",$('#autolist').width());
  //         $('.auto_more_heading h2').css('font-size','12px');
  //         $('.auto_more_heading h2').css('padding','0px 4px 0px 12px');
  //         $('.auto_more_heading h2').next().css('margin-left','-27px');
  //         // $('#sortType').css('width','50%');

  //       }
  //       else if( $(window).width() < 750 )
  //       {
  //         $('#auto_banner_fixed_div').css("display","none");
  //         // $('.row auto_more_row').css('width','80%');
  //         $('.auto_more_heading').css("display","block");
  //         $('.auto_more_heading').css("width",$(window).width() - 40);
  //         $('.auto_more_heading h2').css('font-size','12px');
  //         $('.auto_more_heading h2').next().css('width','100%');
  //         $('.auto_more_heading h2').next().css('margin-top','-16px');
          
          
  //         $('.sorting').css('width','70%');
  //         $('.sorting').css('padding','13px 0px 7px 50px');
  //         $('.sorting').css('float','left');
          
  //   }
  // }

    //if($('.product_category').length)
    if(false)
    {

      if($('.product_category').val() == 19)
      {
          window.location.href = 'http://auto.zar.mn/addauto';
      }
      $( ".product_category" ).change(function() {
        if($('.product_category').val() == 19)
        {
          window.location.href = 'http://auto.zar.mn/addauto';
        }
      });

      if($('.product_category').val() == 26)
      {
          window.location.href = 'http://property.zar.mn/addproperty';
      }
      $( ".product_category" ).change(function() {
        if($('.product_category').val() == 26)
        {
          window.location.href = 'http://property.zar.mn/addproperty';
        }
      });
    }

    if($('.auto_more_heading').length)
    {
      if($('.auto_more_heading').attr("turul") == 'category' )
      {
        $('.auto_more_heading').css("top",389);
        $('.auto_more_heading').css("display","block");  
      } 
      else 
      {
        $('.auto_more_heading').css("top",200);
        $('.auto_more_heading').css("display","block");  
      }
    }

    if($('#auto_banner_fixed_div').length) 
    {
      $('#auto_banner_fixed_div').css("left",($(window).width() - 1000)/2 + 850);
      if($(window).width() > 1200)
      $('#auto_banner_fixed_div').css("display","block");
    }

    if($('#banner2_fixed_div').length) 
    {
      $('#banner2_fixed_div').css("left",($(window).width() - 970)/2 + 990);
      $('#banner2_fixed_div').css("display","block");

    }

    if($('#banner3_fixed_div').length) 
    {
      $('#banner3_fixed_div').css("right",($(window).width() - 970)/2 + 990);
      $('#banner3_fixed_div').css("display","block");
    }
    sumTotal();
  });

  $(window).resize(function(){

    if($('#banner2_fixed_div').length)
    {
      $('#banner2_fixed_div').css("left",($(window).width() - 970)/2 + 990);
      $('#banner2_fixed_div').css("display","block");
    }

    if($('#banner3_fixed_div').length)
    {
      $('#banner3_fixed_div').css("right",($(window).width() - 970)/2 + 990);
      $('#banner3_fixed_div').css("display","block");
    }

    if($('#auto_banner_fixed_div').length) 
    {
      $('#auto_banner_fixed_div').css("left",($(window).width() - 1000)/2 + 850);
      if($(window).width() > 1200)
        $('#auto_banner_fixed_div').css("display","block");
      else
        $('#auto_banner_fixed_div').css("display","none");  
    }

  });

  $(window).scroll(function() {
    if($(document).scrollTop() > 389)
    {
        if($('.auto_more_heading').attr("turul") == 'category' )
        $('.auto_more_heading').css("top",0);
        
        $('#auto_banner_fixed_div').css("top",0);
    }
    else
    {
        if($('.auto_more_heading').attr("turul") == 'category' )
        $('.auto_more_heading').css("top",389-$(document).scrollTop()); 
        
        $('#auto_banner_fixed_div').css("left",($(window).width() - 1000)/2 + 850);
        $('#auto_banner_fixed_div').css("top",389-$(document).scrollTop());
      
    }

    if($(document).scrollTop() > 200)
    {
        if($('.auto_more_heading').attr("turul") == 'search' )
        $('.auto_more_heading').css("top",0);
    }
    else
    {
        if($('.auto_more_heading').attr("turul") == 'search' )
        $('.auto_more_heading').css("top",200-$(document).scrollTop()); 
    }

    if($(document).scrollTop() > 173)
    {
      $('#banner2_fixed_div').css("top",0);
      $('#banner3_fixed_div').css("top",0);
      
    }
    else
    {
      $('#banner2_fixed_div').css("left",($(window).width() - 970)/2 + 989);
      $('#banner3_fixed_div').css("right",($(window).width() - 970)/2 + 989);
      $('#banner2_fixed_div').css("top",172-$(document).scrollTop());
      $('#banner3_fixed_div').css("top",172-$(document).scrollTop());
    
    }
      
  });

  $('body').on('change', '.weekprice', function(event) {
    sumTotal();
  });

  function sumTotal(){
    var _total = 0;
    $( ".weekprice" ).each(function( index ) {
      _total+=parseInt($( this ).val());
    });
    $('#totalprice').html(_total);
  }

jQuery(function($){

    // How easy is this??
    // $('#profileImage').Jcrop({
    //   minSize:   [ 150, 150],
    //   onSelect: updateCoords,
    //   aspectRatio: 1
    // });

  });
  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function jcrop_target(pos) {
        return function (c) { updateCoordsBanners(pos, c); };
  };

  function updateCoordsBanners(pos,c)
  {
    $('#x'+pos).val(c.x);
    $('#y'+pos).val(c.y);
    $('#w'+pos).val(c.w);
    $('#h'+pos).val(c.h);
  };
  function updateCoordsBanner(c)
  {
    $('#bx').val(c.x);
    $('#by').val(c.y);
    $('#bw').val(c.w);
    $('#bh').val(c.h);
  };
  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Зураг дээр хулганаар хэмжээг тохируулна уу.');
    return false;
  };
  function checkCoordsBanner()
  {
    if (parseInt($('#bw').val())) return true;
    alert('Зураг дээр хулганаар хэмжээг тохируулна уу.');
    return false;
  };
  function checkCoordsBanners(pos)
  {
    if (parseInt($('#w'+pos).val())) return true;
    alert('Зураг дээр хулганаар хэмжээг тохируулна уу.');
    return false;
  };
  
function calcCompany()
{
  if($('#is_company').prop("checked"))
     $('#company_div').show();
  else
    $('#company_div').hide();
}

$(document).ready(createUploader);
  
    function createUploader(){
      var button = $('#upload');           
        var uploader = new qq.FileUploaderBasic({
            button: document.getElementById('file-uploader'),
            action: '/fileupload/upload.php',
            allowedExtensions: ['jpg', 'gif', 'png', 'jpeg'],
            onSubmit: function(id, fileName) {
              $('#loading_bar').show();
              $('#profileLoader').attr('src', '/images/uploading.gif');
        interval = window.setInterval(function(){
        }, 200);
      },
            onComplete: function(id, fileName, responseJSON){
              $('#profileLoader').attr('src', '/images/camera-icon.png');
              $('#loading_bar').hide();
        window.clearInterval(interval);
        
              if(responseJSON['success'])
              {
                load_original(responseJSON['filename']);
          }},
                debug: true
            });           
    }
        
    function load_original(filename){
          $('#thumbnail').attr('src', "/fileupload/uploads/"+filename);
          $('#filename').val(filename);
          $('#thumbnail').Jcrop({
            minSize:   [ 150, 150],
            onSelect: updateCoords,
            aspectRatio: 1
          });
          $('#myMiga').modal({});
  }

    $(document).ready(loadBannersUploader);
  
    function loadBannersUploader()
    {
      for (var i = 1; i < 7; i++) {
        if($('#file-uploader'+i).length)
        {
           bannersUploader(i,$('#file-uploader'+i).attr('data-w'),$('#file-uploader'+i).attr('data-h'));
        }
      }
    }
    // function bannersUploader(pos,w,h){
    //    var button = $('#upload'+pos);
       
    //     var uploader = new qq.FileUploaderBasic({
    //         button: document.getElementById('file-uploader'+pos),
    //         action: '/fileupload/upload.php',
    //         params: { w: w,
    //                   h: h,
    //                   pos: pos
    //         },
    //         allowedExtensions: ['jpg', 'gif', 'png', 'jpeg'],
    //         onSubmit: function(id, fileName) {
    //           console.log('oncomplete'+w+ '---'+h+'----'+pos);
    //           $('#profileLoader'+pos).attr('src', '/images/uploading.gif');
    //           interval = window.setInterval(function(){
    //           }, 200);
              
    //         },
    //         onComplete: function(id, fileName, responseJSON){
    //           $('#profileLoader'+pos).attr('src', '/images/camera-icon.png');
    //           window.clearInterval(interval);
        
    //           if(responseJSON['success'])
    //           {
    //             load_original_banners(responseJSON['filename'],h,w,pos);
    //         }},
    //         debug: true
    //     });           
    // }
        
    function load_original_banners(filename,h,w,pos){
          $('#thumbnail'+pos).attr('src', "/fileupload/uploads/"+filename);
          $('#filename'+pos).val(filename);
          $('#thumbnail'+pos).Jcrop({
            minSize:   [ h, w ],
            onSelect: jcrop_target(pos),
            aspectRatio: w / h
          });
          $('#banner_modal'+pos).modal({});
    }

  // $(document).ready(bannerUploader);
  
  //   function bannerUploader(){
  //     var button = $('#upload_banner');           
  //       var uploader = new qq.FileUploaderBasic({
  //           button: document.getElementById('banner-uploader'),
  //           action: '/fileupload/upload.php',
  //           allowedExtensions: ['jpg', 'gif', 'png', 'jpeg'],
  //           onSubmit: function(id, fileName) {
  //             $('#loading_bar').show();
  //       interval = window.setInterval(function(){
  //       }, 200);
  //     },
  //           onComplete: function(id, fileName, responseJSON){
  //             $('#loading_bar').hide();
  //       window.clearInterval(interval);
        
  //             if(responseJSON['success'])
  //             {
  //               load_original_banner(responseJSON['filename']);
  //         }},
  //               debug: true
  //           });           
  //   }
        
    function load_original_banner(filename){
          $('#thumbnailbanner').attr('src', "/fileupload/uploads/"+filename);
          $('#bannername').val(filename);
          $('#thumbnailbanner').Jcrop({
            minSize:   [300, 750],
            onSelect: updateCoordsBanner,
            aspectRatio: 750 / 300
          });
          $('#myBanner').modal({});
  }

  $( document ).ready(function() {
      var pathArray = window.location.pathname.split( '/' );
        if(pathArray[1] == 'profile'){
        window.scrollTo(0, 175);
        }
    });

  $(document).ready(function() {

      if($('#category_id').val() == 28 || $('#category_id').val() == 36 )
      {
        if($('#price_type').length)
        {

            $('#price').css('width','70%');
      
            $('#price_type').css('width','30%');
            $('#price_type').css('float','right');
            $('#price_type').css('margin-top','-35px');
            $('#price_type').css('display','block');
        } 
        else
        {
            $('#price_type').css('display','none');
            $('#price_type').val(0);
        }  
      }  


      if($('#type_category').length)
      {
        $('#type_category').val(20); 
      }  
      if($('#price').length)
      {
        $('#price').keyup(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which !=8) {
            event.preventDefault();
          }
         
          if(parseFloat($(this).val()) < parseFloat("1"))
          {
              $(this).val('');
          }
        });
      }
      
      $('#priceother').keyup(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which !=8) {
          event.preventDefault();
        }

       
        if(parseFloat($(this).val()) < parseFloat("1"))
        {
            $(this).val('');
        }
      });
    
       
      $('#textbox102').keyup(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which !=8) {
          event.preventDefault();
        }
        if(parseFloat($(this).val()) >= parseFloat("10"))
        {
            $(this).val('');
        }
        if(parseFloat($(this).val()) < parseFloat("0.1"))
        {
            $(this).val('');
        }
        
        
      });

      $('#priceauto').keyup(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which !=8) {
          event.preventDefault();
        }
        if(parseFloat($(this).val()) >= parseFloat("300"))
        {
            $(this).val('');
        }
        if(parseFloat($(this).val()) < parseFloat("1"))
        {
            $(this).val('');
        }
        
        
      });

      $('#contact').keypress(function(event) {
        if (event.which !=32 && event.which !=59 && event.which !=44 && (event.which < 48 || event.which > 57 ) && event.which !=8 ) { // && event.ctrlKey && (event.which == 86 || event.which==118) ctrl + v zuvshuuruh
          if((event.ctrlKey || event.metaKey )&& (event.which == 86 || event.which==118))
          {
            
          } 
          else
          {
            event.preventDefault();  
          } 
          

        }
      });
    
      var searchTimer = null;
      var _value = '';


  $('.user-search-txt').keyup(function() {
    if (_value === $(this).val().trim()) {
      return;
    }
    if (searchTimer !== null) {
      clearTimeout(searchTimer);
    }

    $('.user-suggestion').hide();

    // value
    _value = $(this).val().trim();

    searchTimer = setTimeout(function() {
      $.ajax({
        url: bm.prefix + '/profile/suggest',
        data: {keyword: _value},
        dataType: 'json',
        success: function(data) {
          // TODO FIX for exploit
          var _content = '';
          $.each(data, function(key, value) {
            _content += '<li>' + '<a href="#" onclick="selectMail(\'' + value + '\');return false;">' + value + '</a>' + '</li>';
          });

          $('#user-suggestion-list').html(_content);

          if (_content !== '') {
            $('.user-suggestion').show();
          }
        }
      });
    }, 800);
  });

  //hide search suggestion
  $(window).click(function(e) {
    if ($(e.target).parents('.user-search').length === 0) {
      $('.user-suggestion').hide();
    }
  });
  });

function selectMail(mail){
  $('#usermail').val(mail);
  $('.user-suggestion').hide();
}

function companyUserSubmit(){
  $.ajax({
        url: bm.prefix + '/profile/adduser',
        data: {email: $('#usermail').val()},
        type: "POST",
        success: function(data) {
          // TODO FIX for exploit
          $('#comUserList').append(data);
          $('#usermail').val('');
        }
      });
}
function drawReplay(product_id, user_id, parent_id){

  if($('#replay').length)
  {
    if($('#replay').parent().attr('id') != "commentid"+parent_id)
    {
      $("#commentid"+parent_id).append('<div id="replay" class="com-post" style="padding-left:20px"><form id="commentForm" action="/product/savefeed" method="POST" style="display:flex"><input name="_token" value="'+$("input[name=_token]").val()+'" type="hidden"><input type="hidden" value="'+product_id+'" class="objId" name="objId"><input type="hidden" value="'+user_id+'" name="product_user_id"><input type="hidden" value="'+parent_id+'" name="parent_id"/><textarea style="float:left;  height: 34px; width: 100%;" id="maxcommentbody" name="content" class="commentbox"></textarea><input type="submit" value="Илгээх" class="btn btn-red" style=" float:right; height: 34px;"></form></div>');          
    }  
    $('#replay').remove();
  } 
  else
  {
    $("#commentid"+parent_id).append('<div id="replay" class="com-post" style="padding-left:20px"><form id="commentForm" action="/product/savefeed" method="POST" style="display:flex"><input name="_token" value="'+$("input[name=_token]").val()+'" type="hidden"><input type="hidden" value="'+product_id+'" class="objId" name="objId"><input type="hidden" value="'+user_id+'" name="product_user_id"><input type="hidden" value="'+parent_id+'" name="parent_id"/><textarea style="float:left;  height: 34px; width: 100%;" id="maxcommentbody" name="content" class="commentbox"></textarea><input type="submit" value="Илгээх" class="btn btn-red" style=" float:right; height: 34px;"></form></div>');      
  }

}

function deleteFeedback(id){
  $.ajax({
        url: bm.prefix + '/profile/deletefeed',
        data: {id: id},
        type: "POST",
        success: function(data) {
          // TODO FIX for exploit
          $('#commentid'+id).remove();
        }
      });
}
function deleteProductComment(id){
  $.ajax({
        url: bm.prefix + '/product/deletefeed',
        data: {id: id},
        type: "POST",
        success: function(data) {
          // TODO FIX for exploit
          $("li").each(function(){
              if($(this).attr("child") == id)
              $(this).remove();
          });
          $('#commentid'+id).remove();

        }
      });
}

function follow(com_id){
  $.ajax({
        url: bm.prefix + '/profile/follow',
        data: {company_id: com_id},
        type: "POST",
        success: function(data) {
          $('#follow').html(data);
        }
      });
}
function loadLocation(cat_id,select_html_id)
{
  if(cat_id != 0)
  {
    $.ajax({
        url: bm.prefix + '/product/locations',
        dataType: 'json',
        data: {id: cat_id},
        type: 'POST',
        success: function(data) {
          if (data.length > 0) {
            // content
            $('#location_id').val(cat_id);
            var _content = '<select id="'+select_html_id+'" name="'+select_html_id+'">';
            var _raquo = '';
            // data
            _content += '<option value="0">Сонгоно уу</option>';
            $.each(data, function(key, value) {
              _content += '<option value="' + value.id + '">' + value.name + '</option>';

            });
            _content += '</select>';

            $('#'+select_html_id).replaceWith(_content);
            
            $('#'+select_html_id).attr('onchange',"setLocation(this.value)");
            $('#'+select_html_id).attr('class','form-control input');
          }
        }
      });
  }  
  
}

function propertyCategory(cat_id,select_html_id)
{
  if(cat_id != 0)
  {
    $.ajax({
        url: bm.prefix + '/category/ajaxList',
        dataType: 'json',
        data: {id: cat_id},
        type: 'POST',
        success: function(data) {
          propertyAttributesShow(cat_id);
          showPriceType(cat_id);
          if (data.length > 0) {
            // content
            $('#category_id').val(cat_id);
            var _content = '<select id="'+select_html_id+'" name="'+select_html_id+'">';
            var _raquo = '';
            // data
            _content += '<option value="0">Сонгоно уу</option>';
            $.each(data, function(key, value) {
              _content += '<option value="' + value.id + '">' + value.name + '</option>';

            });
            _content += '</select>';

            $('#'+select_html_id).replaceWith(_content);
            
            $('#'+select_html_id).attr('onchange',"setMarkCategory(this.value)");
            $('#'+select_html_id).attr('class','form-control input');
          }
          else
          {
            $('#'+select_html_id).css('display','none');
          }  
        }
      });
  }  
  
}

function autoFactoryLoad(cat_id,select_html_id,is_home)
{
  if(cat_id != 0)
  {
    $.ajax({
        url: bm.prefix + '/category/ajaxList',
        dataType: 'json',
        data: {id: cat_id},
        type: 'POST',
        success: function(data) {
          if (data.length > 0) {
            // content
            $('#category_id').val(cat_id);
            var _content = '<select id="'+select_html_id+'" name="'+select_html_id+'">';
            var _raquo = '';
            // data
            _content += '<option value="0">Сонгоно уу</option>';
            $.each(data, function(key, value) {
              _content += '<option value="' + value.id + '">' + value.name + '</option>';

            });
            _content += '</select>';

            $('#'+select_html_id).replaceWith(_content);
            if(is_home == 1)
            {
              $('#'+select_html_id).attr('onchange',"autoMarkLoad(this.value,'mark_category',1)");  
            }
            else
            {
              $('#'+select_html_id).attr('onchange',"autoMarkLoad(this.value,'mark_category',0)");  
            }
            
            if(is_home == 1)
                $('#'+select_html_id).attr('class','form-control-home');
            else
                $('#'+select_html_id).attr('class','form-control input');
            
            
          }
        }
      });
  }  
  
}

function autoMarkLoad(cat_id,select_html_id,is_home)
{
  if(cat_id != 0)
  {
    $.ajax({
          url: bm.prefix + '/category/ajaxList',
          dataType: 'json',
          data: {id: cat_id},
          type: 'POST',
          success: function(data) {
            if (data.length > 0) {
              // content
              $('#category_id').val(cat_id);
              var _content = '<select id="'+select_html_id+'" name="'+select_html_id+'">';
              var _raquo = '';
              // data
              _content += '<option value="0">Сонгоно уу</option>';
              $.each(data, function(key, value) {
                  _content += '<option value="' + value.id + '">' + value.name + '</option>';
              });
              _content += '</select>';

              $('#'+select_html_id).replaceWith(_content);
              $('#'+select_html_id).attr('onchange',"setMarkCategory(this.value)");

            }
            else
            {
              $('#'+select_html_id).html('<option value="0">---</option>');
            }
            if(is_home == 1)
                  $('#'+select_html_id).attr('class','form-control-home');
              else
                  $('#'+select_html_id).attr('class','form-control input');
          }
        });
  }
}

function setMarkCategory(cat_id)
{
  if($('#propertymain').length)
  {
       propertyAttributesShow(cat_id);
       showPriceType(cat_id);
  } 
  if(cat_id != 0)
  {
    $('#category_id').val(cat_id);
  }
}

function setLocation(loc_id)
{
  if(loc_id != 0)
  {
    $('#location_id').val(loc_id);
  }
}

$(document).ready( function() {
  $(".compare-link").click(function(){
    if ($('#compare_ids').val().split("-", 5).length < 2) {
      alert('Та 2-оос болон түүнээс дээш бүтээгдxүүн сонгоно уу');
    } else {
      var url ='/productCompare?ids='+$('#compare_ids').val();
      window.location.href = url;
    }    
    return false;
  });
//filter PAGE compare
    productCompare.categoryId = 20;
    eval("var compareValues = "+(cookieManager.getCookie("compareValues") || '{categoryId:0,items:{}}')+";");

    //var compareItems = new compareProductItem();
    if (productCompare.categoryId == compareValues.categoryId){
      productCompare.compareItems.setItems(compareValues.items);
    } else {
      cookieManager.setCookie('compareValues', "{categoryId:0,items:{}}");
    }

    //binding
    $('.compare-select').click(function(){
      
      var element = $(this);
      if (element.attr('checked')) {
        element.removeAttr('checked');
      }else{
        element.attr('checked', 'checked');
      }
      if (element.attr('checked')) {
        
        if (productCompare.compareItems.getLength() < 4){
          productCompare.compareItems.addItem(element.val(), element.parent().parent().parent().find( "img" ).attr('src'));
          element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
        } else {
          alert('Та дээд тал нь 4 бүтээгдэxүүн харьцуулж чадна');
          element.attr('checked', false);
        }
      } else { //removing
        element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -3px');
        productCompare.compareItems.deleteItem(element.val());
      }
      cookieManager.setCookie('compareValues', "{categoryId:"+productCompare.categoryId+",items:"+productCompare.compareItems.serialize()+"}");
      productCompare.bindCompareItems();
    });
    productCompare.bindCompareItems();
});

function compareSelect(_this){
      var element = $(_this);
      if (element.attr('checked')) {
        element.removeAttr('checked');
      }else{
        element.attr('checked', 'checked');
      }
      if (element.attr('checked')) {
        
        if (productCompare.compareItems.getLength() < 4){
          productCompare.compareItems.addItem(element.val(), element.siblings('img').attr('src'));
          element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
        } else {
          alert('Та дээд тал нь 4 бүтээгдэxүүн харьцуулж чадна');
          element.attr('checked', false);
        }
      } else { //removing
        element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -3px');
        productCompare.compareItems.deleteItem(element.val());
      }
      cookieManager.setCookie('compareValues', "{categoryId:"+productCompare.categoryId+",items:"+productCompare.compareItems.serialize()+"}");
      productCompare.bindCompareItems();
    }

var productCompare = {
  categoryId   : 0,
  compareItems : new compareProductItem(),
  gotoCompare : function(){
    if ($('#compare_ids').val().split("-", 5).length < 2) {
      alert('Та 2-оос болон түүнээс дээш бүтээгдxүүн сонгоно уу');
    } else {
      window.location = "/productCompare?ids=" + $('#compare_ids').val();
    }
  } ,
  bindCompareItems : function (){
    $('.compare-select').attr('checked', false).each(function(){
      var productId = $(this).val();
      
      if (productCompare.compareItems.hasProductId(productId)){
        $('#compare_id_'+productId).attr('checked', true);
        $('#compare_id_'+productId).parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
      }
    });
    var nbProductItems = productCompare.compareItems.getLength();
    var productIds     = productCompare.compareItems.getProductIds();
    $('.compare-li').html('').each(function(index){
      if (index < nbProductItems) {
        $(this).html('<img src="'+productCompare.compareItems.getImgSrc(productIds[index])+'" class="compare-img" /><span><img src="/images/icons/remove.png" /></span>');
        $(this).find('span > img').click(function(){
          productCompare.compareItems.deleteItem(productIds[index]);
          cookieManager.setCookie('compareValues', "{categoryId:"+productCompare.categoryId+",items:"+productCompare.compareItems.serialize()+"}");
          productCompare.bindCompareItems();
        });
      } else {
        return false;
      }
    });
    $("#compare_ids").val(productCompare.compareItems.getProductIds().join('-'));
  }
}

var cookieManager = {
  getCookie : function(c_name){
    if (document.cookie.length > 0){
      c_start=document.cookie.indexOf(c_name + "=");
      if (c_start != -1) {
        c_start = c_start + c_name.length+1;
        c_end = document.cookie.indexOf(";",c_start);
        if (c_end==-1) c_end=document.cookie.length;
        return unescape(document.cookie.substring(c_start,c_end));
      }
    }
    return "";
  },
  setCookie : function(c_name,value,expiredays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie = c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
  }
}

function compareProductItem(items) {
  this.items = items || {};
  this.addItem = function(productId, imgSrc){
    this.items[productId] = imgSrc
  };
  this.getImgSrc = function(productId) {
    return this.items[productId];
  }
  this.deleteItem = function(productId){
    if (this.hasProductId(productId)) delete this.items[productId];
  };
  this.getItems = function(){
    return this.items;
  };
  this.setItems = function(items){
    this.items = items
  };
  this.hasProductId = function(productId){
    if (typeof this.items[productId] == 'undefined'){
      return false;
    }
    return true;
  }
  this.getProductIds = function(){
    var productIds = [];
    for(productId in this.items) productIds.push(productId);
    return productIds;
  };
  this.serialize = function(){
    var itemArray = [];
    for(productId in this.items) itemArray.push(productId+':"'+this.items[productId]+'"');
    return "{"+itemArray.join(",")+"}";
  };
  this.getLength = function(){
    var counter = 0;
    for(productId in this.items) counter++;
    return counter;
  };
}


function gotoCategory(cid) {
  window.location.href = "/c/" + cid;    
}

function resetAll(cid){
  $.ajax({
        url: bm.prefix + '/resetall',
        data: {cid: cid},
        dataType: "json",
        type: "POST",
        success: function(data) {
          $('.auto_more_chk_lab').css('background-position', '-4px -3px');
          $('.auto_more_chk_lab2').css('background-position', '-4px -3px');
            // 
            $.ajax({
              url: bm.prefix + '/c/'+cid,
              data: {cid: cid},
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
            // 

          jQuery.each($('.autoselecterselect'), function(i, val) {
            var element = $(val);
            if (element.attr('checked')) {
              element.removeAttr('checked');
            }
            element.parent().css('display', 'block');
          });
          $('#resetAll').css('display', 'none');
        }
      });
}

function resetSearchFilter(keyword){
  $.ajax({
        url: bm.prefix + '/resetall',
        data: {keyword: keyword},
        dataType: "json",
        type: "POST",
        success: function(data) {
          $('.auto_more_chk_lab').css('background-position', '-4px -3px');
          $('.auto_more_chk_lab2').css('background-position', '-4px -3px');
            // 
            $.ajax({
              url: bm.prefix + 'search?keyword='+keyword,
              data: {keyword: keyword},
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
            // 

          jQuery.each($('.autoselecterselect'), function(i, val) {
            var element = $(val);
            if (element.attr('checked')) {
              element.removeAttr('checked');
            }
            element.parent().css('display', 'block');
          });
          $('#resetSearchFilter').css('display', 'none');
        }
      });
}

function searchAttribute(keyword, aid) {
    //window.location.href = "/c/" + cid + '?aid=' + aid;  
     isChecked($('#checkbox'+aid));
     $.ajax({
        url: bm.prefix + '/autolist',
        data: {keyword: keyword, aid: aid},
        dataType: "json",
        type: "POST",
        success: function(data) {
          $('.auto_more_chk_lab').css('background-position', '-4px -3px');
            // 
            $.ajax({
              url: bm.prefix + 'search?keyword='+keyword,
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
            // 

          jQuery.each(data, function(i, val) {
            setChecked($('#checkbox'+val));
          });
        }
      });
}

function searchAttributeText(keyword, aid, name) {
     var element = $('#'+name+aid);
     if (element.attr('checked')) {
      element.removeAttr('checked');
      var auto_more_box = element.parent().parent();
      auto_more_box.find('span').each(function(i, val) {
        $(val).css('display', 'block');
      });
    }else{
      element.attr('checked', 'checked');
      var auto_more_box = element.parent().parent();
      auto_more_box.find('span').each(function(i, val) {
        $(val).css('display', 'none');
      });
      element.parent().css('display', 'block');
    }
    if (element.attr('checked')) {
      element.parent().find('.auto_more_chk_lab2').css('background-position', '-4px -47px');
    }else {
      element.parent().find('.auto_more_chk_lab2').css('background-position', '-4px -3px');
    }
    jQuery.each($('.autoselecterselect'), function(i, val) {
              var element = $(val);
              if (element.attr('checked')) {
                $('#resetSearchFilter').css('display', 'block');
              }
            });
     $.ajax({
        url: bm.prefix + '/autolistText',
        data: {keyword: keyword, aid: aid, name:name},
        dataType: "json",
        type: "POST",
        success: function(data) {
          //$('.auto_more_chk_lab').css('background-position', '-4px -3px');
            // 
            $.ajax({
              url: bm.prefix + 'search?keyword='+keyword,
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
            // 

          // jQuery.each(data, function(i, val) {
          //   setChecked($('#'+name+val));
          // });
        }
      });
}
function gotoAttribute(cid, aid) {
    //window.location.href = "/c/" + cid + '?aid=' + aid;  
     isChecked($('#checkbox'+aid));
     $.ajax({
        url: bm.prefix + '/autolist',
        data: {cid: cid, aid: aid},
        dataType: "json",
        type: "POST",
        success: function(data) {
          $('.auto_more_chk_lab').css('background-position', '-4px -3px');
            // 
            $.ajax({
              url: bm.prefix + '/c/'+cid,
              data: {cid: cid},
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
            // 

          jQuery.each(data, function(i, val) {
            setChecked($('#checkbox'+val));
          });
        }
      });
}

function gotoPriceFilter(cid)
{
     var price_max = parseInt($('#price_max').val());
     var price_min = parseInt($('#price_min').val());
     
     $.ajax({
        url: bm.prefix + '/product/priceFilter',
        data: {max: price_max, min: price_min},
        dataType: "json",
        type: "POST",
        success: function(data) {
            $.ajax({
              url: bm.prefix + '/c/'+cid,
              data: {cid: cid},
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
        }
      });
}

// function gotoAttributeText(cid, aid, name) {
//      var element = $('#'+name+aid);
//      if (element.attr('checked')) {
//       element.removeAttr('checked');
//       var auto_more_box = element.parent().parent();
//       auto_more_box.find('span').each(function(i, val) {
//         $(val).css('display', 'block');
//       });
//     }else{
//       element.attr('checked', 'checked');
//       var auto_more_box = element.parent().parent();
//       auto_more_box.find('span').each(function(i, val) {
//         $(val).css('display', 'none');
//       });
//       element.parent().css('display', 'block');
//     }
//     if (element.attr('checked')) {
//       element.parent().find('.auto_more_chk_lab2').css('background-position', '-4px -47px');
//     }else {
//       element.parent().find('.auto_more_chk_lab2').css('background-position', '-4px -3px');
//     }
//     jQuery.each($('.autoselecterselect'), function(i, val) {
//               var element = $(val);
//               if (element.attr('checked')) {
//                 $('#resetAll').css('display', 'block');
//               }
//             });
//      $.ajax({
//         url: bm.prefix + '/autolistText',
//         data: {cid: cid, aid: aid, name:name},
//         dataType: "json",
//         type: "POST",
//         success: function(data) {
//           //$('.auto_more_chk_lab').css('background-position', '-4px -3px');
//             // 
//             $.ajax({
//               url: bm.prefix + '/c/'+cid,
//               data: {cid: cid},
//               type: "POST",
//               beforeSend: function(data){
//                 $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
//               },
//               success: function(data) {
//                 $('#autolist').html(data);
//               }
//             });
//             // 

//           // jQuery.each(data, function(i, val) {
//           //   setChecked($('#'+name+val));
//           // });
//         }
//       });
// }

function setPriceRange(cid, minval, maxval){
  $.ajax({
        url: bm.prefix + '/pricerange',
        data: {maxval: maxval, minval: minval},
        dataType: "json",
        type: "POST",
        success: function(data) {
          
            $.ajax({
              url: bm.prefix + '/c/'+cid,
              data: {cid: cid},
              type: "POST",
              beforeSend: function(data){
                $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
              },
              success: function(data) {
                $('#autolist').html(data);
              }
            });
        }
      });
}

function orderList(cid) {
    // 
    $.ajax({
      url: bm.prefix + '/c/'+cid,
      data: {cid: cid, sortType: $('#sortType').val()},
      type: "POST",
      beforeSend: function(data){
        $('#autolist').html('<div style="padding: 20px; text-align: center;"><img src="/images/loading_bar.gif"></div>');
      },
      success: function(data) {
        $('#autolist').html(data);
      }
    });
    //
}

function isChecked(element){

  if (element.attr('checked')) {
    element.removeAttr('checked');
    var auto_more_box = element.parent().parent();
    auto_more_box.find('span').each(function(i, val) {
      $(val).css('display', 'block');
    });
  }else{
    element.attr('checked', 'checked');
    var auto_more_box = element.parent().parent();
    auto_more_box.find('span').each(function(i, val) {
      $(val).css('display', 'none');
    });
    element.parent().css('display', 'block');
  }
  if (element.attr('checked')) {
    element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
  }else {
    element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -3px');
  }
  jQuery.each($('.autoselecterselect'), function(i, val) {
            var element = $(val);
            if (element.attr('checked')) {
              if($('#resetAll').length)
              $('#resetAll').css('display', 'block');
              else
              $('#resetSearchFilter').css('display', 'block');
            }
          });
}

function setChecked(element){
  element.attr('checked', 'checked');
  element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
}

function createSubMenu(container,id){
    var input = document.createElement("input");
    input.type = "text";
    input.className = "input_created";
    input.id        = "enterSubMenu";
    

    container.after(input);

    $("#newSubMenu").attr("onclick","removeSubMenuInput("+id+")");
    $("#enterSubMenu").attr("onKeyPress","saveSubMenu(event,"+id+")");

}

function createVideo(container,id,oldVideo){
    var input = document.createElement("input");
    input.type = "text";
    input.className = "input_created_video";
    input.id        = "enterVideo";
    input.value     = oldVideo;

    container.after(input);
    $("#create_video").attr("onclick","removeVideoInput("+id+",'"+oldVideo+"')");
    $("#enterVideo").attr("onKeyPress","saveVideo(event,"+id+")");

}

function saveVideo(e,id)
{
  if(e.keyCode == 13){
      $.ajax({
      url: bm.prefix + '/profile/changeVideo',
      type: 'POST',
      dataType: 'json',
      data: {pid: id, name:$("#enterVideo").val()},
      success: function(data) {
        if(data.error == '')
        {
          $("#newSubMenu").attr("onclick","createSubMenu($(this),"+id+")");
          $("#enterVideo").remove();
          if(data.same == '')
          window.location = bm.prefix + '/profile/' + data.id;
        }
        else
        {
          $("#enterVideo").attr("class","input-error");
          alert('Зөвхөн youtube URL оруулна уу');
        }  
      }
    });
  }
}

function removeVideoInput(id,oldVideo)
{
  $("#create_video").attr("onclick","createVideo($(this),"+id+",'"+oldVideo+"')");
  $("#enterVideo").remove();
}

function removeSubMenuInput(id)
{
  $("#newSubMenu").attr("onclick","createSubMenu($(this),"+id+")");
  $("#enterSubMenu").remove();
}

function saveSubMenu(e,id)
{
  if(e.keyCode == 13){
      $.ajax({
      url: bm.prefix + '/profile/createsub',
      type: 'POST',
      dataType: 'json',
      data: {pid: id, name:$("#enterSubMenu").val()},
      success: function(data) {
        $("#newSubMenu").attr("onclick","createSubMenu($(this),"+id+")");
        $("#enterSubMenu").remove();
        $("#newSubMenu").next().find("ul").append('<li id ="sub_id'+data.id+'"><a href="/profile/content/'+data.id+'">'+data.name+'</a><i class="fa fa-remove" style="position: inherit; float:right; margin-top:-20px; cursor:pointer; color:#666; margin-right:5px" onclick ="removeSubMenu('+data.id+')"></i></li>');
      }
    });
  }
}

function removeSubMenu(id)
{
    var r = confirm("Та итгэлтэй байна уу?");
    if (r == true) {
        $.ajax({
          url: bm.prefix + '/profile/removesub',
          type: 'POST',
          dataType: 'json',
          data: {id: id},
          success: function(data) {
            $("#sub_id"+id).remove();
            var s = document.URL;
            if(s.indexOf('/content/'+id) > -1){
              window.location = bm.prefix + '/profile/' + data.pid;  
            }
          }
        });
    } 
}

$(document).ready(function() {
  if($('#propertyCategoryPage').length)
  {

    var cats = ['27','29','30','108','113','120','122','115','118','217'];
    var a = $.inArray($('#category_id').val(), cats);
    
    if(a != -1)
    {
      $('#turees').css('display','block');
      $('#oronsuuts1').css('display','none');
      $('#oronsuuts2').css('display','none');
    }
    else if($('#category_id').val() == 28 || $('#category_id').val() == 36)
    {
      $('#turees').css('display','none');
      $('#oronsuuts1').css('display','block');
      $('#oronsuuts2').css('display','block');
    }
    else
    {
      $('#turees').css('display','none');
      $('#oronsuuts1').css('display','block');
      $('#oronsuuts2').css('display','none'); 
    }  
  }  

  if($(".alert").length)
  {
    var timeOut = ($(".alert").has( "a" ).length)? 30000 : 5000;

    setTimeout(function(){ $(".alert").remove(); }, timeOut);
  } 
  
  $(".notify_detail").click(function (){
      // if($('.badge').html() == '')
      // {
      //   var count = 0;
      // }
      // else
      // {
      //   var count = parseInt($('.badge').html());
      // }
      
      // var badge_count=count - 1;
      // if($(this).attr('not_id').length)
      // {
      //   $.ajax({
      //           url: bm.prefix +'/notify/readed/'+$(this).attr('not_id'),
      //           type: 'GET',
      //           success: function(data) {
      //               $('.badge').html(badge_count);
      //               // alert('blah');
      //               window.location.href = $(".notify_detail").children().attr("href");
      //               $(this).remove();         

      //           }
      //       });
      // }
      // else
      // {
      //   $(this).remove();            
      // }  


      
  });

  $(".notify_taga").click(function (){
      if($('.badge').html() == '')
      {
        var count = 0;
      }
      else
      {
        var count = parseInt($('.badge').html());
      }
      
      var badge_count=count - 1;
      if($(this).parent().attr('not_id').length)
      {
        $.ajax({
                url: bm.prefix +'/notify/readed/'+$(this).parent().attr('not_id'),
                type: 'GET',
                success: function(data) {
                    $('.badge').html(badge_count);
                    // alert('blah');
                    window.location.href = $(".notify_detail").children().attr("href");
                    $(this).parent().remove();         

                }
            });
      }
      else
      {
        $(this).parent().remove();            
      }  

        return false;
    });

  if($(".msg-list").length)
    {
        $(".msg-list").perfectScrollbar();
    }
  $("#not-li>a").click(function (){
        $("#not-container").fadeToggle(300);
        $("#not-li>a>.badge").fadeOut("slow");
        return false;
    });

    $("#editprofile-li>a").click(function (){
        $("#editprofile-container").fadeToggle(300);
        $("#editprofile-li>a>.badge").fadeOut("slow");
        return false;
    });

    //Document Click
    $(document).click(function (){
        $("#not-container").hide();
    });
   
    $(document).click(function (){
        $("#editprofile-container").hide();
    });
    //Popup Click
    $("#editprofile-container").click(function (){
        return false;
    });
    //scroll
   
    if($('.chat_body').length)
    {    
      $(".msg-scroll, .chat-scroll").perfectScrollbar();
      
      $("#search").click(function(){
          //$( "#sqry" ).toggle("slow");
          $( "#sqry" ).animate({
              width: "toggle"
            }, {
              duration: 800,
              specialEasing: {
                width: "linear"
              }
          });
      });
    }  

   $(".fancy").fancybox({
      'hideOnContentClick': true
    });  

  if($('.chat_body').length)
  {
    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
  }

  if($('#changePassFancy').length)
  {
     $(".fancy").fancybox({
      'hideOnContentClick': true
    });
  }

  if($('#categoryAddInLeft').length)
  {
    $('#categoryAddInLeft').on('click', function(){
        $.fancybox({
            width: 160,
            height: 400,
            autoSize: false,
            href: '/profile/setCategory',
            type: 'ajax'
        });
    });
  }

  if($('#forgotPassword').length)
  {
     $(".fancy").fancybox({
      'hideOnContentClick': true
    });
  }
  
  if($('#dialogCategory').length)
  {
    $( "#dialogCategory" ).dialog({
      width: 200,
      height:400,
      autoOpen: false,
      draggable: true,
      modal:  true,
      buttons: {
          "Сонгох": function () {
              $.ajax({
                  url: bm.prefix + '/profile/setCategory',
                  type: 'POST',
                  dataType: 'json',
                  data: {id: $('#category_id').val()},
                  success: function(data) {
                    if(data.same == ''){
                      $('#categoryCompanyList').append('<li id ="comcat'+data.id+'">'+data.name+'<i class="fa fa-remove" style="float:right; padding-top:4px; cursor:pointer" onclick ="removeCompanyCategory('+data.id+')"></i></li>');
                    }
                  }
              });
              $(this).dialog("close");
          }
        }
    });

  }
  if($('.color-box').length)
  {
      $( "#cover_style_chooser" ).click(function() {
        $( "#cover_style_changer" ).toggle( "blind", 500 );
      });

      $('.color-box').colpick({
        colorScheme:'dark',
        layout:'rgbhex',
        color:'ff8800',
        onSubmit:function(hsb,hex,rgb,el) {
          $(el).css('background-color', '#'+hex);
          $(el).colpickHide();

          $.ajax({
          url: bm.prefix + '/profile/colorPick',
          type: 'POST',
          dataType: 'json',
          data: {color_code: hex},
          success: function(data) {
            var s = document.URL;
            window.location = bm.prefix + '/profile/' + data.pid;  
            }
          });
        }
      });
      
      $('.edit').editable('/edit/title', {
        submitdata : {id: $('.edit').attr('detail-id')}
      });
      
      $('.editarea').editable('/edit/body', {
        type : 'wysiwyg',
        height : '200px',
        width  : '90%',
        onblur    : 'ignore',
        submit    : 'Хадгалах',
        cancel    : 'Буцах',
        submitdata : {id: $('.editarea').attr('detail-id')},
        wysiwyg   : {   
                        controls : {
                                bold          : { visible : true },
                                italic        : { visible : true },
                                underline     : { visible : true },
                                strikeThrough : { visible : true },
                                
                                justifyLeft   : { visible : true },
                                justifyCenter : { visible : true },
                                justifyRight  : { visible : true },
                                justifyFull   : { visible : true },

                                indent  : { visible : true },
                                outdent : { visible : true },

                                subscript   : { visible : true },
                                superscript : { visible : true },
                                
                                undo : { visible : true },
                                redo : { visible : true },
                                
                                insertOrderedList    : { visible : true },
                                insertUnorderedList  : { visible : true },
                                insertHorizontalRule : { visible : true },

                                h4: {
                                  visible: true,
                                  className: 'h4',
                                  command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                                  arguments: ($.browser.msie || $.browser.safari) ? '<h4>' : 'h4',
                                  tags: ['h4'],
                                  tooltip: 'Header 4'
                                },
                                h5: {
                                  visible: true,
                                  className: 'h5',
                                  command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                                  arguments: ($.browser.msie || $.browser.safari) ? '<h5>' : 'h5',
                                  tags: ['h5'],
                                  tooltip: 'Header 5'
                                },
                                h6: {
                                  visible: true,
                                  className: 'h6',
                                  command: ($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                                  arguments: ($.browser.msie || $.browser.safari) ? '<h6>' : 'h6',
                                  tags: ['h6'],
                                  tooltip: 'Header 6'
                                },
                                
                                cut   : { visible : true },
                                copy  : { visible : true },
                                paste : { visible : true },
                                html  : { visible: true },
                                increaseFontSize : { visible : true },
                                decreaseFontSize : { visible : true },
                                exam_html: {
                                  exec: function() {
                                    this.insertHtml('<abbr title="exam">Jam</abbr>');
                                    return true;
                                  },
                                  visible: true
                                }
            }
        },
      });
  }

  $('#vnagree').click(function(){
      var element = $(this);
      if (element.attr('checked')) {
        element.removeAttr('checked');
      }else{
        element.attr('checked', 'checked');
      }
      if (element.attr('checked')) {
          element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -24px');
        }
       else { //removing
        element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -3px');
      
      }
    });

jQuery.each($('.autoselecterselect'), function(i, val) {
  var element = $(val);
  if (element.attr('checked')) {
          element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -47px');
          element.parent().find('.auto_more_chk_lab2').css('background-position', '-4px -47px');
        }
});

});



function removeCompanyCategory(id)
{
  $.ajax({
      url: bm.prefix + '/profile/removeCategory',
      type: 'POST',
      dataType: 'json',
      data: {id: id},
      success: function(data) {
        $("#comcat"+id).remove();
        if ($('#categoryCompanyList li').length == 0)
        {
          $('#categoryCompanyList').parent().append('<span id="non_category" style="color:gray">категори сонгоогүй байна</span>');
        }  

      }
  });
}

function autoCategory(id)
{
    $('#category_id').val(id);
    if(id == 20)
    {
      $('#attributesForCar').css('display','block');
      $('#priceauto').css('display','block');
      $('#priceother').css('display','none');
      $('#priceauto').attr('value','');
      $('#forPrice').html('');
      $('#forPrice').append('Үнэ <sup>*</sup>(сая)');
    }
    else
    {
      $('#attributesForCar').css('display','none'); 
      $('#priceauto').css('display','none');
      $('#priceother').css('display','block');
      $('#priceother').attr('value','');
      $('#forPrice').html('');
      $('#forPrice').append('Үнэ <sup>*</sup>');
    }
}

function propertyAttributesShow(id)
{
    var cats = ['28','29','36','108'];
    var a = $.inArray(id, cats);
    
    if(a == -1)
    {
      $('#attributesForProperty').css('display','none');
    }
    else
    {
      $('#attributesForProperty').css('display','block'); 
    }
}

function showPriceType(id)
{
    var cats = ['28','36'];
    var a = $.inArray(id, cats);
    
    if(a == -1)
    {
      $('#price_type').css('display','none');
    }
    else
    {
      $('#price').css('width','70%');
      
      $('#price_type').css('width','30%');
      $('#price_type').css('float','right');
      $('#price_type').css('margin-top','-35px');
      $('#price_type').css('display','block');
      if(id == '28')
      {
         $('#price_type').val(1);  
      } 
      else if(id == '36')
      {
        $('#price_type').val(0); 
      }

    }
}

function clickLink(atag,div)
{
    div.hide();
    console.log(atag.attr("href"));
    window.location = bm.prefix + atag.attr("href");
}

function chatLoadingMessages(id,me){
  $('#to_user_id').val(id);
  $.ajax({
        url: bm.prefix + '/chat/loadingMessages',
        data: {user_id: id},
        type: "POST",
        success: function(data) {
          $('#chats_body').html(data);
          $('#badge'+id).html('');
            


          me.parent().parent().find('li').each(function(){
              var current = $(this);
              current.removeAttr('class');
          });

          me.parent().addClass('active');
          $(".msg-scroll, .chat-scroll").perfectScrollbar();
          $(".chat-scroll").scrollTop( $( ".chat-scroll" ).prop( "scrollHeight" ) );
          $(".chat-scroll").perfectScrollbar('update');

          $('#chat_input').keypress(function(event) {
              if(event.keyCode == 13){
                 var image = '';
                  if($('#my_profile_pic').val() == '')
                    image = '/images/profile.png';
                  else
                    image = '/uploads/profile/'+$('#my_profile_pic').val();
                  app.BrainSocket.message('generic.event',
                    {
                        'message':$(this).val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':$('#my_profile_name').val(),
                        'created_at':getDateTime()
                    }
                  );

                  app.BrainSocket.message('event.notify.chat',
                    {
                        'message':$('#chat_input').val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':$('#my_profile_name').val(),
                        'created_at':getDateTime()
                    }
                  );

                  chating($('#user_id').val(), $('#to_user_id').val(),$(this).val());
                  $(".chat-scroll").scrollTop( $( ".chat-scroll" ).prop( "scrollHeight" ) );
                  $(".chat-scroll").perfectScrollbar('update');
                  $(this).val('');
              }
              return event.keyCode != 13; 
          });

          $('#chat_button').click(function() {
                  var image = '';
                  if($('#my_profile_pic').val() == '')
                    image = '/images/profile.png';
                  else
                    image = '/uploads/profile/'+$('#my_profile_pic').val();
                  app.BrainSocket.message('generic.event',
                    {
                        'message':$('#chat_input').val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':$('#my_profile_name').val(),
                        'created_at':getDateTime()
                    }
                  );

                  app.BrainSocket.message('event.notify.chat',
                    {
                        'message':$('#chat_input').val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':$('#my_profile_name').val(),
                        'created_at':getDateTime()
                    }
                  );
                  chating($('#user_id').val(), $('#to_user_id').val(),$('#chat_input').val());
                  $('#chat_input').val('');

                  
          });


        }
      });
}

function chating(user_id, to_user_id,text){
   $.ajax({
        url: bm.prefix + '/chat/save',
        data: {user_id: user_id, to_user_id:to_user_id, message: text},
        type: "POST",
        success: function(data) {
          $(".chat-scroll").scrollTop( $( ".chat-scroll" ).prop( "scrollHeight" ) );
          $(".chat-scroll").perfectScrollbar('update');
        }
   });       

}

function checkNotifyChat(user_id){
   $.ajax({
        url: bm.prefix + '/notify/checkReadedNotify/'+user_id,
        type: "GET",
        success: function(data) {
        }
   });       

}

 function getDateTime() {
    var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds(); 
    if(month.toString().length == 1) {
        var month = '0'+month;
    }
    if(day.toString().length == 1) {
        var day = '0'+day;
    }   
    if(hour.toString().length == 1) {
        var hour = '0'+hour;
    }
    if(minute.toString().length == 1) {
        var minute = '0'+minute;
    }
    if(second.toString().length == 1) {
        var second = '0'+second;
    }   
    var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;   
     return dateTime;
}

function doRate(adId, star)
{
  jQuery.ajax({
    url: '/rate/rate',
    type: 'POST',
    data: {'product_id': adId, 'star': star},
    success: function(html){
      jQuery("#rate_cont").replaceWith(html);
    }
  });
}




