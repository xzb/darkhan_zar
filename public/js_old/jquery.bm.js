$(document).ready(function() {
    var menu_max_scroll = 110;

    jQuery(document).scroll(function() {
        if (jQuery(this).scrollTop() > menu_max_scroll) {
            jQuery('.fixed-user-links').addClass('visible');
        }
        else {
            jQuery('.fixed-user-links').removeClass('visible');
        }
    });


    //zar duguilah
    $('body').on('click', '.toggle-check-trigger', function(event) {

        $(this).parents('.ad').toggleClass('duguilsan');

        if ($(this).parents('.ad').hasClass('duguilsan')) {
            //duguilah
            $('.checked-ads-count').html(parseInt($('.checked-ads-count').html()) + 1)
        }
        else {
            //bolih
            $('.checked-ads-count').html(parseInt($('.checked-ads-count').html()) - 1)
        }

        $('.my-checked-ads').addClass("active").delay(600).queue(function() {
            $(this).removeClass("active");
            $(this).dequeue();
        });

    });

    $('body').on('click', 'a.fancybox', function(event) {

        event.preventDefault();

        $.fancybox.open({
            href: $(this).attr('href'),
            type: 'ajax',
            padding: 0,
            closeBtn: false,
            //maxWidth: 1000,
            width: $(window).width(),
            //minWidth: 1000,
            autoSize: true,
            margin: 0,
            autoResize: true,
            fitToView: false,
            aspectRatio: false,
            minHeight: $(window).height(),
            height: $(window).height(),
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                overlay: {
                    opacity: 1,
                    locked: true
                }
            }
        });
    });





});
