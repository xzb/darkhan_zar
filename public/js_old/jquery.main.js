$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip({placement: 'auto'});
  $("[rel=tooltip]").tooltip({placement: 'right'});
  
  var menu_max_scroll = 110;

  jQuery(document).scroll(function() {
    if (jQuery(this).scrollTop() > menu_max_scroll) {
      jQuery('.fixed-user-links').addClass('visible');
    }
    else {
      jQuery('.fixed-user-links').removeClass('visible');
    }
  });

  $('body').on('click', '.btn-popup-close', function(event) {
    event.preventDefault();
    // close pop up
    closePopUp();
    // 
    return false;
  });

  $('body').on('click', 'a.fancybox', function(event) {
    event.preventDefault();
    openPopUpUrl($(this).attr('href'));
    return false;
  });

  //scroll
    "use strict";

    if($(".msg-scroll").length)
    {
        $(".msg-scroll").perfectScrollbar();
    }

    if($(".chat-scroll").length)
    {
        $(".chat-scroll").perfectScrollbar();
    }

    
    
    $("#search").click(function(){
        //$( "#sqry" ).toggle("slow");
        $( "#sqry" ).animate({
            width: "toggle"
          }, {
            duration: 800,
            specialEasing: {
              width: "linear"
            }
        });
    });
});

function closePopUp() {
  $.fancybox.close();
}

function openPopUpUrl(url) {
  $.fancybox.open({
    href: url,
    type: 'ajax',
    padding: 0,
    closeBtn: false,
    width: $(window).width(),
    autoSize: true,
    margin: 0,
    autoResize: true,
    fitToView: false,
    aspectRatio: false,
    minHeight: $(window).height(),
    height: $(window).height(),
    openEffect: 'none',
    closeEffect: 'none',
    helpers: {
      overlay: {
        opacity: 1,
        locked: true
      }
    },
    onStart: function() {
      //alert('adfasdf');
      $("body").css("overflow", "hidden");
    }
  });
}
