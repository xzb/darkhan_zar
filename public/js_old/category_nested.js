  function drawLeafs(id){

    if($("#b"+id).attr('data-action') == 'expand')
    {
        $("#b"+id).attr('data-action','collapse'); 
    }
    else
    {
        $("#b"+id).attr('data-action','expand'); 
    }
    if($("#b"+id).attr('drawed') == 'true')
    {
        $("#data"+id).parent().toggleClass('dd-collapsed');
    }   
    else
    {
        $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/admin/category/getLeafs/"+id,
          dataType: 'json',
          success: function(data) {
            $('<ol />', {
                 "class" : 'dd-list',
                 "id": "ol"+id

              }).insertAfter( $( "#data"+id ) );
            
            $.each(data, function(key, value) {
                if(value['is_leaf'] == 1)
                {
                    $("#ol"+id).append($('<li />',{"class" : 'dd-item dd-collapsed', "data-id":value['id']})
                                  .append($('<button />',{"data-action" : 'expand',"drawed":"false","type":"button", text:'Expand', "onclick":"drawLeafs("+value['id']+")","id":"b"+value['id']}))
                                    .append($('<div />',{"class" : 'dd-handle', text:value['name'],"id":"data"+value['id']})
                                      .append($('<div/>',{"class":"pull-right hidden-phone"})

                                        .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/category/add/"+value['id']}).append($('<button/>',{"class":"btn btn-success btn-xs icon-plus"})))
                                        .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/category/"+value['id']+"/edit"}).append($('<button/>',{"class":"btn btn-primary btn-xs icon-pencil"})))
                                        .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/categorybanner/add/"+value['id']}).append($('<button/>',{"class":"btn btn-xs icon-plus"})))
                                        .append($('<form/>',{"style":"display:inline","data-confirm":"Are you sure?", "accept-charset":"UTF-8", "action":window.location.protocol +"//"+window.location.host+"/admin/category/"+value['id'], "method":"POST"})
                                          .append($('<input/>',{"type":"hidden", "value":"DELETE", "name":"_method"}))
                                          .append($('<button/>',{"class":"btn btn-danger btn-xs icon-trash","href":window.location.protocol +'//'+window.location.host+"/admin/category/"+value['id'], "type":"submit","onclick":"if(confirm('Sure')) { return true; } else { return false; }"}))
                                          )
                                        
                                              )  
                                            )
                                        );

                }
                else{
                    $("#ol"+id).append($('<li />',{"class" : 'dd-item', "data-id":value['id']})
                                .append($('<div />',{"class" : 'dd-handle', text:value['name']})
                                  .append($('<div/>',{"class":"pull-right hidden-phone"})
                                    .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/category/add/"+value['id']}).append($('<button/>',{"class":"btn btn-success btn-xs icon-plus"})))                                                                 
                                    .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/category/"+value['id']+"/edit"}).append($('<button/>',{"class":"btn btn-primary btn-xs icon-pencil"})))
                                    .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/categorybanner/add/"+value['id']}).append($('<button/>',{"class":"btn btn-xs icon-plus"})))
                                    .append($('<form/>',{"style":"display:inline","data-confirm":"Are you sure?", "accept-charset":"UTF-8", "action":window.location.protocol +"//"+window.location.host+"/admin/category/"+value['id'], "method":"POST"})
                                          .append($('<input/>',{"type":"hidden", "value":"DELETE", "name":"_method"}))
                                          .append($('<button/>',{"class":"btn btn-danger btn-xs icon-trash","href":window.location.protocol +'//'+window.location.host+"/admin/category/"+value['id'], "type":"submit","onclick":"if(confirm('Sure')) { return true; } else { return false; } "}))
                                           )
                                         )
                                        )
                                      );
                }    
            });

            $("#data"+id).parent().toggleClass('dd-collapsed');
          }
        });
        $("#b"+id).attr("drawed","true");
    }
    $('.col-lg-12').height("86%");

  }

  
      
      

  function drawCategoryCombo(id,parent,is_leaf){
      if($('#category'+parent).next())
      {
          $('#category'+parent).next() .remove();
      }
      if(is_leaf == 1)
      {
         $('#categories'+parent).append($('<div />', {
                     "id": "categories"+id,
                     "class":"form-group",
                     "style":"padding-left:20px"
                    }));
        if(id != 0)
        {
          $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/admin/category/getLeafs/"+id,
          dataType: 'json',
          success: function(data) {
            $('#categories'+id).append($('<select/>',{"class":"form-control","id":"category"+id,"width":"120px","onchange":"drawCategoryCombo($(this).val(),"+id+",$('#category"+id+"').find('option:selected').attr('is_leaf'))"}));
            $('#category'+id).append($('<option/>',{"class":"form-control","value":0, text:"Сонгоно уу"}));
              $.each(data, function(key, value) {
                  $('#category'+id).append($('<option/>',{"value":value['id'], text:value['name'],"is_leaf":value['is_leaf']}));
              });
            }
          });
        }

      }

      if(id == 0)
      {
        $("#category_id").val($('#categories'+parent).prev().val());  
      }
      else
      {
        $("#category_id").val(id);  
      } 
      
        
  

  }

  function drawLeafsZK(id){

    if($("#b"+id).attr('data-action') == 'expand')
    {
        $("#b"+id).attr('data-action','collapse'); 
    }
    else
    {
        $("#b"+id).attr('data-action','expand'); 
    }
    if($("#b"+id).attr('drawed') == 'true')
    {
        $("#data"+id).parent().toggleClass('dd-collapsed');
    }   
    else
    {
        $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/admin/zk_category/getLeafs/"+id,
          dataType: 'json',
          success: function(data) {
            $('<ol />', {
                 "class" : 'dd-list',
                 "id": "ol"+id

              }).insertAfter( $( "#data"+id ) );
            
            $.each(data, function(key, value) {
                if(value['is_leaf'] == 1)
                {
                    $("#ol"+id).append($('<li />',{"class" : 'dd-item dd-collapsed', "data-id":value['id']})
                                  .append($('<button />',{"data-action" : 'expand',"drawed":"false","type":"button", text:'Expand', "onclick":"drawLeafsZK("+value['id']+")","id":"b"+value['id']}))
                                    .append($('<div />',{"class" : 'dd-handle', text:value['name'],"id":"data"+value['id']})
                                      .append($('<div/>',{"class":"pull-right hidden-phone"})

                                        .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/add/"+value['id']}).append($('<button/>',{"class":"btn btn-success btn-xs icon-plus"})))
                                        .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/"+value['id']+"/edit"}).append($('<button/>',{"class":"btn btn-primary btn-xs icon-pencil"})))
                                        .append($('<form/>',{"style":"display:inline","data-confirm":"Are you sure?", "accept-charset":"UTF-8", "action":window.location.protocol +"//"+window.location.host+"/admin/zk_category/"+value['id'], "method":"POST"})
                                          .append($('<input/>',{"type":"hidden", "value":"DELETE", "name":"_method"}))
                                          .append($('<button/>',{"class":"btn btn-danger btn-xs icon-trash","href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/"+value['id'], "type":"submit","onclick":"if(confirm('Sure')) { return true; } else { return false; }"}))
                                          )
                                        
                                              )  
                                            )
                                        );

                }
                else{
                    $("#ol"+id).append($('<li />',{"class" : 'dd-item', "data-id":value['id']})
                                .append($('<div />',{"class" : 'dd-handle', text:value['name']})
                                  .append($('<div/>',{"class":"pull-right hidden-phone"})
                                    .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/add/"+value['id']}).append($('<button/>',{"class":"btn btn-success btn-xs icon-plus"})))                                                                 
                                    .append($('<a/>',{"href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/"+value['id']+"/edit"}).append($('<button/>',{"class":"btn btn-primary btn-xs icon-pencil"})))
                                    .append($('<form/>',{"style":"display:inline","data-confirm":"Are you sure?", "accept-charset":"UTF-8", "action":window.location.protocol +"//"+window.location.host+"/admin/zk_category/"+value['id'], "method":"POST"})
                                          .append($('<input/>',{"type":"hidden", "value":"DELETE", "name":"_method"}))
                                          .append($('<button/>',{"class":"btn btn-danger btn-xs icon-trash","href":window.location.protocol +'//'+window.location.host+"/admin/zk_category/"+value['id'], "type":"submit","onclick":"if(confirm('Sure')) { return true; } else { return false; } "}))
                                           )
                                         )
                                        )
                                      );
                }    
            });

            $("#data"+id).parent().toggleClass('dd-collapsed');
          }
        });
        $("#b"+id).attr("drawed","true");
    }
    $('.col-lg-12').height("86%");

  }

  
      
      

  function drawCategoryComboZK(id,parent,is_leaf){
      if($('#category'+parent).next())
      {
          $('#category'+parent).next() .remove();
      }
      if(is_leaf == 1)
      {
         $('#categories'+parent).append($('<div />', {
                     "id": "categories"+id,
                     "class":"form-group",
                     "style":"padding-left:20px"
                    }));
        if(id != 0)
        {
          $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/admin/zk_category/getLeafs/"+id,
          dataType: 'json',
          success: function(data) {
            $('#categories'+id).append($('<select/>',{"class":"form-control","id":"category"+id,"width":"120px","onchange":"drawCategoryComboZK($(this).val(),"+id+",$('#category"+id+"').find('option:selected').attr('is_leaf'))"}));
            $('#category'+id).append($('<option/>',{"class":"form-control","value":0, text:"Сонгоно уу"}));
              $.each(data, function(key, value) {
                  $('#category'+id).append($('<option/>',{"value":value['id'], text:value['name'],"is_leaf":value['is_leaf']}));
              });
            }
          });
        }

      }

      if(id == 0)
      {
        $("#category_id").val($('#categories'+parent).prev().val()).trigger('change');  
      }
      else
      {
        $("#category_id").val(id).trigger('change');
      } 
      
  }

  function drawAttributeInputs(id)
  {
    $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/admin/zk_product/drawAttributeInputs",
          data: {category_id: id},
          type: "POST",
          success: function(data) {
            $('#with_attribute').html(data);
          }
    });
  }

  function drawCategoryComboInFront(id,parent,is_leaf){
      if($('#category'+parent).next())
      {
          $('#category'+parent).next() .remove();
      }
      if(is_leaf == 1)
      {
         $('#categories'+parent).append($('<div />', {
                     "id": "categories"+id,
                     "class":"form-group",
                     "style":""
                    }));
        if(id != 0)
        {
          $.ajax({
          url: window.location.protocol +'//'+window.location.host+"/profile/getLeafs/"+id,
          dataType: 'json',
          success: function(data) {
            $('#categories'+id).append($('<select/>',{"class":"form-control","id":"category"+id,"width":"120px","onchange":"drawCategoryComboInFront($(this).val(),"+id+",$('#category"+id+"').find('option:selected').attr('is_leaf'))"}));
            $('#category'+id).append($('<option/>',{"class":"form-control","value":0, text:"Сонгоно уу"}));
              $.each(data, function(key, value) {
                  $('#category'+id).append($('<option/>',{"value":value['id'], text:value['name'],"is_leaf":value['is_leaf']}));
              });
            }
          });
        }

      }

      if(id == 0)
      {
        $("#category_id").val($('#categories'+parent).prev().val());  
      }
      else
      {
        $("#category_id").val(id);  
      } 
      
        
  

  }

