$(function() {
	var timer = setInterval( "slideSwitch()", 5000 );
	
	$("#newsSliderNav li").click(function(){
		
		//var $current = $($(this).attr('tabIndex'));
		var $current = $($(this).attr('newsId'));

		$("#newsSliderNav li").removeClass('active-nav');
		$(this).addClass('active-nav');

		$('#newsSlider .newsSlider').removeClass('active-slider');
		$current.addClass('active-slider');
		
		$current.css({opacity: 0.0})
			.addClass('active-slider')
			.animate({opacity: 1.0}, 200, function() {
        });
				
		clearInterval(timer);
		timer = setInterval( "slideSwitch()", 5000);
		return false;
	});
});

function slideSwitch() {
	
	
   var $active = $('#newsSlider div.active-slider');

    if ( $active.length == 0 ) $active = $('#newsSlider div.newsSlider:last');

    var $next =  $active.next().length ? $active.next() : $('#newsSlider div.newsSlider:first');

    $active.addClass('last-active');	
	
	var $activeLI = $('#newsSliderNav li.active-nav');
	var $nextLI = $activeLI.next().length ? $activeLI.next() : $('#newsSliderNav li:first');	
	
	
		$next.css({opacity: 0})
			.addClass('active-slider')
			.animate({opacity: 1}, 1000, function() {
				$active.removeClass('active-slider last-active');
        });
		
		$nextLI.addClass('active-nav');
		$activeLI.removeClass('active-nav');					

}			