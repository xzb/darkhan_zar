$(document).ready(function() {

    var news_fixed_left = 210;

    $(document).scroll(function() {
        if ($(this).scrollTop() > news_fixed_left) {
            $('.news-fixed-left').addClass('fixed');
        }
        else {
            $('.news-fixed-left').removeClass('fixed');
        }
    });

    $('.mobile-menu').click(function() {
        $('html').toggleClass('showMenu');
        if ($('html').hasClass('showMenu')) {

            var w = $('#main').width();
            var h = $('.menu-left').height();

            $('#site').width(w);
            $('#site').height(h);

        } else {
            $('#site').width('auto');
            $('#site').height('auto');
        }
    });
    if ($('#site').find('.category-tab-1 .tab-1 .scroll-pane').length) {

        if(!detectmob()) {
            $('.category-tab-1 .tab-1 .scroll-pane').slimScroll({
                wheelStep : 1,
                height: '935px',
                alwaysVisible: true,
                railVisible: true,
                railColor: '#e4e4e4',
                color: 'gray',
                railOpacity: 1,
                opacity: 1
            });
        }
    }
    if ($('#site').find('.tab-1 .scroll-pane').length) {

        if(!detectmob()) {
            $('.tab-1 .scroll-pane').slimScroll({
                wheelStep : 1,
                height: '372px',
                alwaysVisible: true,
                railVisible: true,
                railColor: '#e4e4e4',
                color: 'gray',
                railOpacity: 1,
                opacity: 1
            });
        }

    }


    if ($('#site').find('.column-half .scroll-pane').length) {

        if(!detectmob()) {
            $('.column-half .scroll-pane').slimScroll({
                wheelStep : 1,
                height: '120px',
                alwaysVisible: true,
                railVisible: true,
                railColor: '#e4e4e4',
                color: 'gray',
                railOpacity: 1,
                opacity: 1
            });
        }
    }

    if ($('#site').find('.poll_list .scroll-pane1').length) {

        if(!detectmob()) {
            $('.poll_list .scroll-pane1').slimScroll({
                wheelStep : 1,
                height: '260px',
                alwaysVisible: true,
                railVisible: true,
                railColor: '#e4e4e4',
                color: 'gray',
                railOpacity: 1,
                opacity: 1
            });
        }
    }

    function tickExchageRate() {
        $('#tickerExchageRate li:first').slideUp(function() {
            $(this).appendTo($('#tickerExchageRate')).slideDown();
        });
    }
    window.setInterval(function() {
        tickExchageRate();
    }, 5000);

    function tickMarketInfo() {
        $('#tickerMarketInfo li:first').slideUp(function() {
            $(this).appendTo($('#tickerMarketInfo')).slideDown();
        });
    }
    window.setInterval(function() {
        tickMarketInfo();
    }, 6000);

    function tickWeather() {
        $('#tickerWeather li:first').slideUp(function() {
            $(this).appendTo($('#tickerWeather')).slideDown();
        });
    }
    setInterval(function() {
        tickWeather();
    }, 7000);

    $('.fancybox').fancybox({});

    $('#search_submit').click(function() {
         window.location = "/search/" + $('#search_input').val();
         return false;
    });

    $("#search_input").keyup(function(event){
        if(event.keyCode == 13){
         window.location = "/search/" + $('#search_input').val();
         return false;
        }
    });

    function isValidDate(str){
        if(str=="" || str==null){return false;}
        var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);
        if( m === null || typeof m !== 'object'){return false;}
        if (typeof m !== 'object' && m !== null && m.size!==3){return false;}
        var ret = true;
        var thisYear = new Date().getFullYear();
        var minYear = 1999;
        if( (m[1].length < 4) || m[1] < minYear || m[1] > thisYear){ret = false;}
        if( (m[1].length < 2) || m[2] < 1 || m[2] > 12){ret = false;}
        if( (m[1].length < 2) || m[3] < 1 || m[3] > 31){ret = false;}
        return ret;
    }  

    // for homepage archive. changing class for current day.
    var url  = document.URL;
    var last = url.split("/").pop();
    if(isValidDate(last)) {

        $( ".days-wrap" ).html('');
        $.get( "/archive/show/nav", function( data ) {
            $( ".days-wrap" ).html( data );
            $( "li[rel='" + last +  "']" ).addClass('active');
        });
        
    }


});


function detectmob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}