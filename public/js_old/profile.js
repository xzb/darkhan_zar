function homeMore(pid,index) {
    document.location.href = '/homeMore/'+pid+'?ind='+index;
}
$( document ).ready(function() {
    $('.event-slider').bxSlider({
          controls: false
    });
        
    if($('#myProducts').length)
    {
      moreMyProducts($('#myProducts').attr('my_id'),1,$('#myProducts'));
    }

    if($('#myFollowedCompany').length)
    {
      moreFollowedCompanies($('#myFollowedCompany').attr('user_id'),1,$('#myFollowedCompany'));
    }

    if($('#myEvents').length)
    {
      moreEvents($('#myEvents').attr('user_id'),1,$('#myEvents'));
    }
});

function moreMyProducts(user_id,page,div){
  $.ajax({
        url: bm.prefix + '/myProductAjax',
        data: {id: user_id, page: page},
        type: "POST",
        success: function(data) {
          div.after(data);
          div.remove();
        }
      });
}

function moreFollowedCompanies(user_id,page,div){
  $.ajax({
        url: bm.prefix + '/followedCompaniesAjax',
        data: {id: user_id, page: page},
        type: "POST",
        success: function(data) {
          div.after(data);
          div.remove();
        }
      });
}

function moreEvents(user_id,page,div){
  $.ajax({
        url: bm.prefix + '/eventsAjax',
        data: {id: user_id, page: page},
        type: "POST",
        success: function(data) {
          div.after(data);
          div.remove();
        }
      });
}