<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => false,


	// New add. 4.2
	'cipher' => MCRYPT_RIJNDAEL_256,

	// Oauth
    'Facebook' => array(
        'client_id'     => 974263029343389,
        'client_secret' => '290100ea49ba40feaadde5d1bc896979',
        'scope'         => array(
				            	'email',
				            	'publish_actions',
				            	//'read_friendlists',
				            	//'read_stream',
				            	//'user_groups'
				            	),
    ),
	'Twitter' => array(
	    'client_id'     => 'yHEqRFpTOqHm1ivd38uL8Si5R',
	    'client_secret' => 'HIUV5wtZxG1hqdjqjZgp7WyqXO4QhRgBYi1mFNbx1TjCStQuzS',
	),

	// PRICE
	'price' => array('1' => 5000, '19'=>15000, '26'=>30000, '2182'=> 15000, '2158'=>10000, '2157'=>10000, '2204' => 5000, '2212' => 5000, '2282' => 5000),
	'attr_types' => array('textbox'=>'textbox', 'checkbox'=>'checkbox', 'selectbox'=>'selectbox', 'textarea'=>'textarea','radio'=>'radio'),
	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'http://localhost',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'Asia/Ulaanbaatar',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'mn',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => 'zD1iMo7NiYQZtUaSBuSbwi5LPPxHTXVL',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => array(

		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Session\CommandsServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Html\HtmlServiceProvider',
		'Illuminate\Log\LogServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Database\MigrationServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Remote\RemoteServiceProvider',
		'Illuminate\Auth\Reminders\ReminderServiceProvider',
		'Illuminate\Database\SeedServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Workbench\WorkbenchServiceProvider',
		'Cartalyst\Sentry\SentryServiceProvider',
		'Krucas\Notification\NotificationServiceProvider',
		'Barryvdh\Elfinder\ElfinderServiceProvider',
		'Intervention\Image\ImageServiceProvider',
		'Barryvdh\Debugbar\ServiceProvider',
		'Vinicius73\SEO\Providers\SEOServiceProvider',
		'Fitztrev\LaravelHtmlMinify\LaravelHtmlMinifyServiceProvider',
	    'CeesVanEgmond\Minify\MinifyServiceProvider',
	    'Watson\Active\ActiveServiceProvider',
	    'Baum\BaumServiceProvider',
	    //'Sairiz\Mandrill\MandrillServiceProvider',
        'Artdarek\OAuth\OAuthServiceProvider',
		'BrainSocket\BrainSocketServiceProvider',
		'Bkwld\Croppa\ServiceProvider',
		'Artisaninweb\SoapWrapper\ServiceProvider',
	    'Maatwebsite\Excel\ExcelServiceProvider'

	),

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => storage_path().'/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => array(

		'App'             => 'Illuminate\Support\Facades\App',
		'Artisan'         => 'Illuminate\Support\Facades\Artisan',
		'Auth'            => 'Illuminate\Support\Facades\Auth',
		'Blade'           => 'Illuminate\Support\Facades\Blade',
		'Cache'           => 'Illuminate\Support\Facades\Cache',
		'ClassLoader'     => 'Illuminate\Support\ClassLoader',
		'Config'          => 'Illuminate\Support\Facades\Config',
		'Controller'      => 'Illuminate\Routing\Controller',
		'Cookie'          => 'Illuminate\Support\Facades\Cookie',
		'Crypt'           => 'Illuminate\Support\Facades\Crypt',
		'DB'              => 'Illuminate\Support\Facades\DB',
		'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
		'Event'           => 'Illuminate\Support\Facades\Event',
		'File'            => 'Illuminate\Support\Facades\File',
		'Form'            => 'Illuminate\Support\Facades\Form',
		'Hash'            => 'Illuminate\Support\Facades\Hash',
		'HTML'            => 'Illuminate\Support\Facades\HTML',
		'Input'           => 'Illuminate\Support\Facades\Input',
		'Lang'            => 'Illuminate\Support\Facades\Lang',
		'Log'             => 'Illuminate\Support\Facades\Log',
		'Mail'            => 'Illuminate\Support\Facades\Mail',
		'Paginator'       => 'Illuminate\Support\Facades\Paginator',
		'Password'        => 'Illuminate\Support\Facades\Password',
		'Queue'           => 'Illuminate\Support\Facades\Queue',
		'Redirect'        => 'Illuminate\Support\Facades\Redirect',
		'Redis'           => 'Illuminate\Support\Facades\Redis',
		'Request'         => 'Illuminate\Support\Facades\Request',
		'Response'        => 'Illuminate\Support\Facades\Response',
		'Route'           => 'Illuminate\Support\Facades\Route',
		'Schema'          => 'Illuminate\Support\Facades\Schema',
		'Seeder'          => 'Illuminate\Database\Seeder',
		'Session'         => 'Illuminate\Support\Facades\Session',
		'SSH'             => 'Illuminate\Support\Facades\SSH',
		'Str'             => 'Illuminate\Support\Str',
		'URL'             => 'Illuminate\Support\Facades\URL',
		'Validator'       => 'Illuminate\Support\Facades\Validator',
		'View'            => 'Illuminate\Support\Facades\View',
		'Sentry'      	  => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
		'Notification'    => 'Krucas\Notification\Facades\Notification',
		'Image' 		  => 'Intervention\Image\Facades\Image',
		'Article'     	  => 'App\Models\Article',
		'Page'        	  => 'App\Models\Page',			
		'Users'      	  => 'App\Models\User',			
		'Banner'      	  => 'App\Models\Banner',
		'Debugbar'    	  => 'Barryvdh\Debugbar\Facade',
		'SEOMeta'    	  => 'Vinicius73\SEO\Facades\Meta',
		'SEOSitemap' 	  => 'Vinicius73\SEO\Facades\Sitemap',
		'OpenGraph'   	  => 'Vinicius73\SEO\Facades\OpenGraphHelper',
		'Active' 		  => 'Watson\Active\Facades\Active',
	    'OAuth'			  => 'Artdarek\OAuth\Facade\OAuth',
	    'Mycache'      	  => 'App\Models\Mycache',
	    'sfUser'      	  => 'App\Models\sfUser',
	    'BrainSocket'     => 'BrainSocket\BrainSocketFacade',
	    'Croppa' 		  => 'Bkwld\Croppa\Facade',
	    'SoapWrapper' => 'Artisaninweb\SoapWrapper\Facades\SoapWrapper',
        'Excel' => 'Maatwebsite\Excel\Facades\Excel'
	),

);
