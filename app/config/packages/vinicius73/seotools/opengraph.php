<?php

return array(
	'defaults' => array(
		/**
		 * default title and description
		 * Can be null|string|seotools
		 * If 'seotools' the title will be awarded by MetaGenerator->getTitle()
		 */
		'title'       => 'Дархан Зар',
		'description' => 'Дархан Зар',
		/**
		 * default url
		 * Can be null|string|url
		 * If 'url' the title will be awarded by Input::url()
		 */
		'url'         => true,
		'type'        => true,
		'image'       => array(),
		'site_name'   => 'dazar.mn'
	)
);
