<?php

    return array(

		'defaults' => array(
			'title'       => \DB::table('settings')->remember(600, 'frontend_title')->first()->frontend_title,
			'description' => \DB::table('settings')->remember(600, 'frontend_description')->first()->frontend_description,
			'separator'   => ' | ',
			'keywords'    => array(\DB::table('settings')->remember(600, 'frontend_keywords')->first()->frontend_keywords),
		),	
	);


