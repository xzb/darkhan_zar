<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default class runner
	|--------------------------------------------------------------------------
	|
	| Class that will be performed in the sitemap class
	|
	*/

	'classrun'  => 'App\Models\SitemapRun',
	/**
	 * enable the sitemap cache
	 */
	'cache'     => true,
	/**
	 * Cache time in minutes
	 */
	'cachetime' => 600,
	/**
	 * enable the sitemap
	 */
	'enabled' => true,

);