<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => Config::get('app.Facebook.client_id'),
            'client_secret' => Config::get('app.Facebook.client_secret'),
            'scope'         => Config::get('app.Facebook.scope'),
        ),

        'FacebookKet' => array(
            'client_id'     => Config::get('app.FacebookKet.client_id'),
            'client_secret' => Config::get('app.FacebookKet.client_secret'),
            'scope'         => Config::get('app.FacebookKet.scope'),
        ),

		'Twitter' => array(
		    'client_id'     => Config::get('app.Twitter.client_id'),
		    'client_secret' => Config::get('app.Twitter.client_secret'),   
		),  

		'Google' => array(
		    'client_id'     => '896936063756-u0cihb4cugu7bh7rqq6bj0l8d3p2gto5.apps.googleusercontent.com',
		    'client_secret' => 'QE0GvbotfIh0-Vd36qnqemzP',
		    'scope'         => array('userinfo_email', 'userinfo_profile'),
		),

	)

);