<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('/autohomesearch', array('as' => 'front.autohome.search', 'uses' => 'App\Controllers\Front\AutoHomeController@autoHomeSearch'));
// Route::group(array('before' => 'cache', 'after' => 'cache'), function()
// {
// });
Route::get('/', array('before' => 'cache', 'after' => 'cache', 'as' => 'front.homepage', 'uses' => 'App\Controllers\Front\HomeController@index'));

Route::get('/p/{id}/{name?}', array('before' => 'cache', 'after' => 'cache', 'as' => 'front.product.show', 'uses' => 'App\Controllers\Front\ProductController@show'), function($id) { })->where('id', '[0-9]+');

Route::any('/c/{id}/{name?}', array('before' => 'cache', 'after' => 'cache', 'as' => 'category.list', 'uses' => 'App\Controllers\Front\ProductController@category'), function($id) { })->where('id', '[0-9]+');
Route::get('/add', array('as'=>'product_add','uses' => 'App\Controllers\Front\ProductController@add'), function($cid = null) { })->where('cid', '[0-9]+');
Route::get('/addauto', array('uses' => 'App\Controllers\Front\ProductController@addAuto'), function($cid = null) { })->where('cid', '[0-9]+');
Route::get('/addproperty', array('uses' => 'App\Controllers\Front\ProductController@addProperty'), function($cid = null) { })->where('cid', '[0-9]+');
Route::post('/category/ajaxList', array('uses' => 'App\Controllers\Front\CategoryController@ajaxList'));
Route::post('/category/ajaxLocation', array('uses' => 'App\Controllers\Front\CategoryController@ajaxLocation'));
Route::post('/category/attribute', array('uses' => 'App\Controllers\Front\CategoryController@attribute'));

Route::post('/product/locations', array('uses' => 'App\Controllers\Front\ProductController@loadLocation'));
Route::get('/e/{pid}', array('as'=>'product_edit','uses' => 'App\Controllers\Front\ProductController@edit'), function($pid) { })->where('pid', '[0-9]+');
Route::get('/autoEdit/{pid}', array('uses' => 'App\Controllers\Front\ProductController@autoEdit'), function($pid) { })->where('pid', '[0-9]+');
Route::get('/propertyEdit/{pid}', array('uses' => 'App\Controllers\Front\ProductController@propertyEdit'), function($pid) { })->where('pid', '[0-9]+');
Route::put('/product/store', array('as' => 'front.product.store','uses' => 'App\Controllers\Front\ProductController@store'));
Route::put('/product/storeauto', array('as' => 'front.product.storeauto','uses' => 'App\Controllers\Front\ProductController@storeAuto'));
Route::put('/product/storeproperty', array('as' => 'front.product.storeproperty','uses' => 'App\Controllers\Front\ProductController@storeProperty'));
Route::put('/product/isValid', array('uses' => 'App\Controllers\Front\ProductController@isValid'));
Route::put('/product/autoIsValid', array('uses' => 'App\Controllers\Front\ProductController@autoIsValid'));
Route::put('/product/propertyIsValid', array('uses' => 'App\Controllers\Front\ProductController@propertyIsValid'));
Route::put('/images/{pid}', array('uses' => 'App\Controllers\Front\ImageController@imageList'));
Route::get('/productSale/{pid}', array('uses' => 'App\Controllers\Front\ProductController@sale'));
Route::post('/product/salesave', array('as' => 'profile.salesave', 'uses' => 'App\Controllers\Front\ProductController@salesave'));
Route::post('/product/priceFilter', array('as' => 'product.priceFilter', 'uses' => 'App\Controllers\Front\ProductController@priceFilter'));
Route::get('/profile/{id}', array('as' => 'profile.show', 'uses' => 'App\Controllers\Front\ProductController@profile'), function($id) { })->where('id', '[0-9]+');
Route::get('/profileNew/{id}', array('as' => 'profile.show1', 'uses' => 'App\Controllers\Front\ProfileController@businessProfile'), function($id) { })->where('id', '[0-9]+');
Route::get('/saw/{id}', array('as' => 'profile.saw', 'uses' => 'App\Controllers\Front\ProfileController@saw'), function($id) { })->where('id', '[0-9]+');
Route::get('/profile/info/{id}', array('as' => 'profile.info', 'uses' => 'App\Controllers\Front\ProfileController@info'), function($id) { })->where('id', '[0-9]+');
Route::post('/profile/createsub', array('as' => 'profile.createsub', 'uses' => 'App\Controllers\Front\ProfileController@createsub'));
Route::post('/profile/removesub', array('as' => 'profile.removesub', 'uses' => 'App\Controllers\Front\ProfileController@removesub'));
Route::post('/profile/colorPick', array('as' => 'profile.colorPick', 'uses' => 'App\Controllers\Front\ProfileController@colorPick'));
Route::get('/profile/content/{id}', array('as' => 'profile.content', 'uses' => 'App\Controllers\Front\ProfileController@content'), function($id) { })->where('id', '[0-9]+');
Route::get('/profile/news/{id}', array('as' => 'profile.news', 'uses' => 'App\Controllers\Front\ProfileController@news'), function($id) { })->where('id', '[0-9]+');
Route::get('/profile/feedback/{id}', array('as' => 'profile.feedback', 'uses' => 'App\Controllers\Front\ProfileController@feedback'), function($id) { })->where('id', '[0-9]+');
Route::get('/profile/getLeafs/{root_id}', array('as' => 'profile.getleafs', 'uses' => 'App\Controllers\Front\ProfileController@getLeafs'));
Route::post('/profile/setCat', array('as' => 'profile.setCat', 'uses' => 'App\Controllers\Front\ProfileController@setCat'));
Route::get('/profile/setCategory', array('as' => 'profile.setCategory', 'uses' => 'App\Controllers\Front\ProfileController@setCategory'));
Route::post('/profile/removeCategory', array('as' => 'profile.removeCategory', 'uses' => 'App\Controllers\Front\ProfileController@removeCategory'));
Route::get('/profile/setCoverStyle/{id}', array('as' => 'profile.setCoverStyle', 'uses' => 'App\Controllers\Front\ProfileController@setCoverStyle'), function($id) { })->where('id', '[0-9]+');
Route::post('/edit/title', array('as' => 'profile.edit.title', 'uses' => 'App\Controllers\Front\ProfileController@editTitle'));
Route::post('/edit/body', array('as' => 'profile.edit.body', 'uses' => 'App\Controllers\Front\ProfileController@editBody'));
Route::get('/company/{slug}', array('uses' => 'App\Controllers\Front\ProfileController@showcompany'), function($slug) { })->where('slug1', '[a-z]+');
Route::get('/productCompare', array('uses' => 'App\Controllers\Front\CategoryController@compare'));
Route::get('/profile/companyRemove', array('uses' => 'App\Controllers\Front\ProfileController@companyRemove'));
Route::post('/autolist', array('uses' => 'App\Controllers\Front\AutoHomeController@catlist'));
Route::post('/autolistText', array('uses' => 'App\Controllers\Front\AutoHomeController@autolistText'));
Route::post('/carlist', array('uses' => 'App\Controllers\Front\AutoHomeController@carlist'));
Route::post('/resetall', array('uses' => 'App\Controllers\Front\AutoHomeController@resetall'));
Route::post('/pricerange', array('uses' => 'App\Controllers\Front\PropertyHomeController@pricerange'));
Route::get('/coupon', array('as' => 'profile.couponList','uses' => 'App\Controllers\Front\ProfileController@coupon'));
Route::get('/companies', array('as' => 'front.companies', 'uses' => 'App\Controllers\Front\HomeController@company'));




Route::group(array('before' => 'csrf'), function()
{
    Route::post('/profile/savefeed', array('uses' => 'App\Controllers\Front\ProfileController@savefeed'));
    Route::post('/product/savefeed', array('uses' => 'App\Controllers\Front\ProductController@savefeed'));
    Route::post('/image/upload', 'App\Controllers\Front\ImageController@upload');
});

Route::get('/testingMail', array('uses' => 'App\Controllers\Front\MailController@sendMail'));

Route::get('/cron/profileRenewProducts', array('uses' => 'App\Controllers\Front\CronController@profileRenewProducts'));
Route::get('/cron/randomRenew', array('uses' => 'App\Controllers\Front\CronController@randomRenew'));
Route::get('/cron/searchExpired', array('uses' => 'App\Controllers\Front\CronController@searchExpired'));
Route::get('/cron/productExpired', array('uses' => 'App\Controllers\Front\CronController@productExpired'));
Route::get('/cron/tmp', array('uses' => 'App\Controllers\Front\CronController@tmp'));
Route::get('/cron/productDaily', array('uses' => 'App\Controllers\Front\CronController@productDaily'));
Route::get('/cron/productInstant', array('uses' => 'App\Controllers\Front\CronController@productInstant'));
Route::get('/cron/dashboard', 'App\Controllers\Front\CronController@dashboard');
Route::get('/cron/productGetFeaturedExpire', 'App\Controllers\Front\CronController@productGetFeaturedExpire');
Route::get('/cron/productInActive', 'App\Controllers\Front\CronController@productInActive');
Route::get('/cron/removeStockImagesNonFiles', 'App\Controllers\Front\CronController@removeStockImagesNonFiles');
Route::get('/cron/autoRenewProcessing', 'App\Controllers\Front\CronController@autoRenewProcessing');

Route::get('/profile/forgotPass', 'App\Controllers\Front\OauthController@forgotPass');
Route::post('/profile/sentPass', 'App\Controllers\Front\OauthController@sentPass');
Route::any('/resetPass/{code}', array('as' => 'front.profile.resetpass', 'uses' => 'App\Controllers\Front\OauthController@resetPass'));
Route::get('/userActive/{code}', array('as' => 'front.profile.useractive', 'uses' => 'App\Controllers\Front\OauthController@userActive'));
Route::post('/rate/rate', 'App\Controllers\Front\HomeController@rate');
Route::get('/help/page', 'App\Controllers\Front\PageController@help');
Route::get('/profile/comingsoon', 'App\Controllers\Front\ProfileController@comingsoon');
Route::get('/renew/{id}', array('as' => 'product.renew', 'uses' => 'App\Controllers\Front\ProductController@renew'), function($id) { })->where('id', '[0-9]+');
Route::get('/renewajax/{id}', array('as' => 'product.renewajax', 'uses' => 'App\Controllers\Front\ProductController@renewajax'), function($id) { })->where('id', '[0-9]+');
Route::get('/tellAdmin/{id}',array('as' => 'product.telladmin', 'uses' => 'App\Controllers\Front\ProductController@tellAdmin'), function($id) { })->where('id', '[0-9]+');
Route::post('/tellAdmin/{id}',array('as' => 'product.telladmin', 'uses' => 'App\Controllers\Front\ProductController@tellAdmin'), function($id) { })->where('id', '[0-9]+');
Route::group(array('before' => 'auth.front'), function()
{

    Route::get('/home/{id}', array('as' => 'profile.home', 'uses' => 'App\Controllers\Front\ProfileController@home'), function($id) { })->where('id', '[0-9]+');
    
    Route::get('/othersProduct/{id}', array('as' => 'profile.othersProduct', 'uses' => 'App\Controllers\Front\ProfileController@othersProduct'), function($id) { })->where('id', '[0-9]+');
    Route::get('/myProduct/{id}', array('as' => 'profile.myProduct', 'uses' => 'App\Controllers\Front\ProfileController@myProduct'), function($id) { })->where('id', '[0-9]+');
    Route::post('/myProductAjax', array('as' => 'profile.myProductAjax', 'uses' => 'App\Controllers\Front\ProfileController@myProductAjax'));
    
    Route::get('/followedCompanies/{id}', array('as' => 'profile.followedCompanies', 'uses' => 'App\Controllers\Front\ProfileController@followedCompanies'), function($id) { })->where('id', '[0-9]+');
    Route::post('/followedCompaniesAjax', array('as' => 'profile.followedCompaniesAjax', 'uses' => 'App\Controllers\Front\ProfileController@followedCompaniesAjax'));
    
    Route::get('/events/{id}', array('as' => 'profile.events', 'uses' => 'App\Controllers\Front\ProfileController@events'), function($id) { })->where('id', '[0-9]+');
    Route::post('/eventsAjax', array('as' => 'profile.eventsAjax', 'uses' => 'App\Controllers\Front\ProfileController@eventsAjax'));

    Route::get('/my', array('as' => 'product.my', 'uses' => 'App\Controllers\Front\ProductController@my'));
    Route::get('/logout', array('uses' => 'App\Controllers\Front\OauthController@logout'));
    Route::any('/profile/edit', array('as' => 'profile.edit', 'uses' => 'App\Controllers\Front\ProfileController@edit'));
    // Route::get('/profile/banner', array('as' => 'profile.banner', 'uses' => 'App\Controllers\Front\ProfileController@banner'));
    // Route::any('/profile/banner/new', array('as' => 'profile.bannernew', 'uses' => 'App\Controllers\Front\ProfileController@bannernew'));
    // Route::get('/profile/banner/delete/{id}', array('as' => 'profile.bannerdelete', 'uses' => 'App\Controllers\Front\ProfileController@bannerdelete'));
    Route::any('/profile/crop', array('as' => 'profile.crop', 'uses' => 'App\Controllers\Front\ProfileController@crop'));
    Route::any('/profile/banners', array('as' => 'profile.banners', 'uses' => 'App\Controllers\Front\ProfileController@banners'));
    Route::any('/profile/company', array('as' => 'profile.company', 'uses' => 'App\Controllers\Front\ProfileController@company'));
    Route::any('/profile/user', array('as' => 'profile.user', 'uses' => 'App\Controllers\Front\ProfileController@companyuser'));
    Route::get('/profile/suggest', 'App\Controllers\Front\ProfileController@suggest');
    Route::post('/profile/adduser', 'App\Controllers\Front\ProfileController@adduser');
    Route::post('/profile/changeVideo', 'App\Controllers\Front\ProfileController@changeVideo');
    Route::get('/profile/user/delete/{id}', array('as' => 'profile.userdelete', 'uses' => 'App\Controllers\Front\ProfileController@deleteuser'));
    Route::post('/profile/deletefeed', 'App\Controllers\Front\ProfileController@deletefeed');
    Route::post('/product/deletefeed', 'App\Controllers\Front\ProductController@deletefeed');
    Route::get('/profile/expired', 'App\Controllers\Front\ProfileController@expired');
    Route::post('/profile/follow', 'App\Controllers\Front\ProfileController@follow');
    Route::get('/f', 'App\Controllers\Front\ProductController@followList');
    Route::post('/product/salesearch', 'App\Controllers\Front\ProductController@salesearch');
    Route::get('/saledelete/{sid}', array('uses' => 'App\Controllers\Front\ProductController@saledelete'), function($sid) { })->where('pid', '[0-9]+');
    Route::get('/profile/changePass', 'App\Controllers\Front\ProfileController@changePass');
    Route::post('/profile/setPass', 'App\Controllers\Front\ProfileController@setPass');
    Route::get('/chats/{chat_user_id}','App\Controllers\Front\ChatController@chat');
    Route::post('/chat/loadingMessages','App\Controllers\Front\ChatController@loadingMessages');
    Route::post('/chat/save','App\Controllers\Front\ChatController@saveConversation');
    Route::get('/notify/readed/{id}', array('uses'=>'App\Controllers\Front\NotifyController@readed'), function($id) { })->where('id', '[0-9]+');
    Route::get('/notify/checkReadedNotify/{user_id}', array('uses'=>'App\Controllers\Front\NotifyController@checkReadedNotify'), function($id) { })->where('id', '[0-9]+');
});

Route::get('/login', array('uses' => 'App\Controllers\Front\OauthController@login'));
Route::get('/oauth/facebook', array('uses' => 'App\Controllers\Front\OauthController@facebook'));
Route::get('/oauth/twitter', array('uses' => 'App\Controllers\Front\OauthController@twitter'));
Route::post('/user/login', array('as'=>'front.user.login' ,'uses'=>'App\Controllers\Front\OauthController@postLogin'));
Route::post('/user/register', array('as'=>'front.user.register' ,'uses'=>'App\Controllers\Front\OauthController@register'));

Route::get('/page/{slug}', array('uses' => 'App\Controllers\Front\ProductController@page'), function($slug) { })->where('slug1', '[a-z]+');
Route::get('/product/suggest', 'App\Controllers\Front\ProductController@suggest');
Route::any('/search', 'App\Controllers\Front\ProductController@search');
Route::get('/i/toggle', 'App\Controllers\Front\ProductController@toggle');
Route::get('/i/autotoggle', 'App\Controllers\Front\AutoHomeController@toggle');
Route::get('/i/propertytoggle', 'App\Controllers\Front\PropertyHomeController@toggle');
Route::get('/i/basket', 'App\Controllers\Front\ProductController@setbasket');
Route::get('/i', 'App\Controllers\Front\ProductController@interestList');
Route::get('/product/checkCode', array( /* 'before' => 'csrf', */ 'uses' => 'App\Controllers\Front\ProductController@checkCode', function(){}));
Route::get('/del/{id}', 'App\Controllers\Front\ProductController@delete');
Route::get('/images/{pid}', 'App\Controllers\Front\ImageController@imageList');
Route::get('/image/load/{pid}', 'App\Controllers\Front\ImageController@load');
Route::post('/image/delete', 'App\Controllers\Front\ImageController@delete');
Route::get('/addBasket/{id}', 'App\Controllers\Front\ProductController@addBasket');
Route::get('/basket', array('as' => 'front.basket', 'uses' => 'App\Controllers\Front\ProductController@basket'));
Route::post('/basket', array('as' => 'front.basket', 'uses' => 'App\Controllers\Front\ProductController@basket'));
Route::get('/buyProfile', array('as' => 'front.buyProfile', 'uses' => 'App\Controllers\Front\ProductController@buyProfile'));
Route::post('/buyProfile', array('as' => 'front.buyProfile', 'uses' => 'App\Controllers\Front\ProductController@buyProfile'));
Route::get('/removeBasket/{id}', array('as' => 'front.removeBasket', 'uses' => 'App\Controllers\Front\ProductController@removeBasket'), function($id) { })->where('id', '[0-9]+');
Route::get('/shopping/golomtApproved', array('as' => 'shopping.golomt', 'uses' => 'App\Controllers\Front\ShoppingController@golomtApproved'));
Route::post('/shopping/postsaving', array('uses' => 'App\Controllers\Front\ShoppingController@postsaving'));
Route::post('/shopping/saveApproved', array('as' => 'shopping.saveapproved', 'uses' => 'App\Controllers\Front\ShoppingController@saveApproved'));
Route::any('/shopping/saveDeclined', array('as' => 'shopping.savedeclined', 'uses' => 'App\Controllers\Front\ShoppingController@saveDeclined'));
Route::get('/subscribe/popupCategory/{id}', array('as' => 'subscribe.category', 'uses' => 'App\Controllers\Front\HomeController@subCategory'), function($id) { })->where('id', '[0-9]+');
Route::get('/subscribe/popupKeyword', array('as' => 'subscribe.keyword', 'uses' => 'App\Controllers\Front\HomeController@subKeyword'));
Route::post('/subscribe/popupCategory/{id}', array('as' => 'subscribe.category', 'uses' => 'App\Controllers\Front\HomeController@subCategory'), function($id) { })->where('id', '[0-9]+');
Route::post('/subscribe/popupKeyword', array('as' => 'subscribe.keyword', 'uses' => 'App\Controllers\Front\HomeController@subKeyword'));
Route::get('/subscribe/c', array('as' => 'subscribe.uncategory', 'uses' => 'App\Controllers\Front\HomeController@category'));
Route::get('/subscribe/k', array('as' => 'subscribe.unkeyword', 'uses' => 'App\Controllers\Front\HomeController@keyword'));
Route::post('/subscribe/c', array('as' => 'subscribe.uncategory', 'uses' => 'App\Controllers\Front\HomeController@category'));
Route::post('/subscribe/k', array('as' => 'subscribe.unkeyword', 'uses' => 'App\Controllers\Front\HomeController@keyword'));


Route::get('/api/favoriteCategories', 'App\Controllers\Front\ApiController@favoriteCategories');
Route::get('/api/categories', 'App\Controllers\Front\ApiController@categories');
Route::get('/api/search', 'App\Controllers\Front\ApiController@search');
Route::get('/api/productList', 'App\Controllers\Front\ApiController@productList');
Route::get('/api/productDetail', 'App\Controllers\Front\ApiController@productDetail');
Route::get('/api/other', 'App\Controllers\Front\ApiController@other');
Route::get('/api/featured', 'App\Controllers\Front\ApiController@featured');
Route::post('/api/submitAd', 'App\Controllers\Front\ApiController@submitAd');
Route::post('/api/uploadImage', 'App\Controllers\Front\ApiController@uploadImage');
Route::get('/api/delete', 'App\Controllers\Front\ApiController@delete');

Route::get('admin/logout', array('as' => 'admin.logout', 'uses' => '\Admin\AuthController@getLogout'));
Route::get('admin/login', array('as' => 'admin.login', 'uses' => '\Admin\AuthController@getLogin'));
Route::post('admin/login', array('as' => 'admin.login.post', 'uses' => '\Admin\AuthController@postLogin'));

Route::post('fileUpload', 'App\Controllers\Admin\PagesController@fileUpload');
Route::get('banner', array('as' => 'banner.ind', 'uses' => 'BannersController@index'));

Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function()
{
        Route::any('/', 'App\Controllers\Admin\ProductController@index');
        Route::resource('pages', 'App\Controllers\Admin\PagesController',array('except' => array('show')));
        Route::resource('location', 'App\Controllers\Admin\LocationController',array('except' => array('show')));
        Route::resource('events', 'App\Controllers\Admin\EventsController',array('except' => array('show')));
        Route::resource('banner', 'App\Controllers\Admin\BannerController');
        Route::resource('attribute', 'App\Controllers\Admin\AttributeController');
        Route::resource('prices', 'App\Controllers\Admin\PricesController');
        Route::resource('users', 'App\Controllers\Admin\UsersController');
        Route::resource('category', 'App\Controllers\Admin\CategoryController', array('except' => array('show','store')));
        Route::resource('polls', 'App\Controllers\Admin\PollsQuestionsController');
        Route::post('pages/upload','App\Controllers\Admin\PagesController@upload');
        Route::get('pages/loadUploads/{obj_id}', 'App\Controllers\Admin\PagesController@loadUploads');
        Route::get('pages/loadPicAfterUpload', 'App\Controllers\Admin\PagesController@loadPicAfterUpload');
        
        Route::resource('answer', 'App\Controllers\Admin\PollsAnswersController',array('except' => array('create','show','index','store')));
        Route::resource('bannerloc', 'App\Controllers\Admin\BannerLocationController',array('except' => array('show')));
        
        Route::get('pages/removeUpload/{obj_id}', 'App\Controllers\Admin\PagesController@removeUpload');

        Route::get('dashboard', 'App\Controllers\Admin\DashboardController@index');        
        Route::any('settings', 'App\Controllers\Admin\DashboardController@settings');        
        
        Route::resource('comment','App\Controllers\Admin\CommentController',array('except' => array('create','show','store','edit','update')));
        Route::put('comment', array('as' => 'admin.comment.index', 'uses'=>'App\Controllers\Admin\CommentController@index'));
        Route::get('pages/getNewsTags/{page_id}', 'App\Controllers\Admin\PagesController@getPageTags');
        Route::get('pages/tags', 'App\Controllers\Admin\PagesController@getTags');
        Route::get('pages/tagsQuery/{query}', 'App\Controllers\Admin\PagesController@getTagsByQuery');

        Route::get('category/getLeafs/{root_id}', 'App\Controllers\Admin\CategoryController@getLeafs');
        Route::get('category/add/{root_id}', array('as' => 'admin.category.add', 'uses'=>'App\Controllers\Admin\CategoryController@add'));
        Route::put('category/store/{root_id}', array('as' => 'admin.category.store', 'uses'=>'App\Controllers\Admin\CategoryController@store'));
        
        Route::put('category/storeAdd', array('as' => 'admin.category.storeAdd', 'uses'=>'App\Controllers\Admin\CategoryController@storeAdd'));

        Route::resource('product', 'App\Controllers\Admin\ProductController',array('except' => array('create','show','store','index')));
        Route::get('productPager/{category_id?}/{title?}/{categories?}/{category0?}/{is_featured?}/{from?}/{to?}', array('as' => 'admin.product.index', 'uses'=>'App\Controllers\Admin\ProductController@index'));
        Route::post('product/upload','App\Controllers\Admin\ProductController@upload');
        Route::get('product/loadUploads/{obj_id}', 'App\Controllers\Admin\ProductController@loadUploads');
        Route::get('product/loadPicAfterUpload/{product_id}', 'App\Controllers\Admin\ProductController@loadPicAfterUpload');
        Route::get('product/categorystats', array('as'=>'admin.product.categorystats', 'uses'=>'App\Controllers\Admin\ProductController@categoryStats'));
        Route::get('product/users_search', array('as'=>'admin.users.search', 'uses'=>'App\Controllers\Admin\UsersController@searchKeyword'));
        Route::get('product/removeUpload/{obj_id}', 'App\Controllers\Admin\ProductController@removeUpload');
        Route::get('payment/index', array('as' => 'admin.payment.index', 'uses'=>'App\Controllers\Admin\PaymentController@index'));
        Route::get('payment/destroy/{id}', array('as' => 'admin.payment.destroy', 'uses'=>'App\Controllers\Admin\PaymentController@destroy'));
        Route::get('payment/active/{id}', array('as' => 'admin.payment.active', 'uses'=>'App\Controllers\Admin\PaymentController@active'));
        Route::get('payment/profile/{id}', array('as' => 'admin.payment.profile', 'uses'=>'App\Controllers\Admin\PaymentController@profileActive'));

        Route::resource('blacklist', 'App\Controllers\Admin\BlacklistController', array('except' => array('show')));
        Route::put('blacklist/store', array('as' => 'admin.blacklist.store', 'uses'=>'App\Controllers\Admin\BlacklistController@store'));

        Route::get('category/rebuildCategory', array('as' => 'admin.category.rebuild', 'uses'=>'App\Controllers\Admin\CategoryController@rebuildCategory'));
        Route::any('product/setfeatured/{id}', array('as' => 'admin.product.featured', 'uses'=>'App\Controllers\Admin\ProductController@setfeatured'));
        Route::any('product/setAutoRenew/{id}', array('as' => 'admin.product.setAutoRenew', 'uses'=>'App\Controllers\Admin\ProductController@setAutoRenew'));
        Route::any('product/setCoupon/{id}', array('as' => 'admin.product.setCoupon', 'uses'=>'App\Controllers\Admin\ProductController@setCoupon'));

        Route::post('category_attribute/create', 'App\Controllers\Admin\CategoryAttributeController@create');
        Route::post('category_attribute/delete', 'App\Controllers\Admin\CategoryAttributeController@delete');
        Route::post('attribute_values/create', 'App\Controllers\Admin\AttributeValuesController@create');
        Route::post('attribute_values/delete', 'App\Controllers\Admin\AttributeValuesController@delete');
        Route::post('attribute_values/sort', 'App\Controllers\Admin\AttributeValuesController@sort');

        Route::any('toExcel', 'App\Controllers\Admin\DashboardController@exportToExcel');

        Route::resource('categorybanner', 'App\Controllers\Admin\CategoryBannerController', array('except' => array('show','store')));
        Route::get('categorybanner/add/{category_id}', array('as' => 'admin.categorybanner.add', 'uses'=>'App\Controllers\Admin\CategoryBannerController@add'));
        Route::put('categorybanner/store/{category_id}', array('as' => 'admin.categorybanner.store', 'uses'=>'App\Controllers\Admin\CategoryBannerController@store'));
        Route::put('categorybanner/storeAdd', array('as' => 'admin.categorybanner.storeAdd', 'uses'=>'App\Controllers\Admin\CategoryBannerController@storeAdd'));
});
    Route::get('/auto', array('as' => 'front.autohomepage', 'uses' => 'App\Controllers\Front\AutoHomeController@index'));

Route::group(array('before' => 'auth.admin'), function()
{
    \Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
    \Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
    \Route::get('elfinder/tinymce', 'Barryvdh\Elfinder\ElfinderController@showTinyMCE4');
    \Route::get('elfinder/ckeditor4', 'Barryvdh\Elfinder\ElfinderController@showCKeditor4');
});