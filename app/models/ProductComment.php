<?php namespace App\Models;
use Cache;
 
class ProductComment extends \Eloquent {
 
    protected $table = 'product_comment';
    public    $timestamps = false;
    
}