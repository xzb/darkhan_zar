<?php namespace App\Models;
use App\Models\UserInterest;
use App\Models\ZKBasket;
use App\Models\ZKBasketItem;
use Cookie, Response, Session, Sentry;
 
class sfUser extends \Eloquent {

	protected $ns = '_fuser';

	public static function addKeyword($keyword)
	{

		// keyword
		$keyword = mb_strtolower($keyword, 'UTF-8');

		// pids 
		if(isset($_COOKIE['zrla_keywords'])) {
			$keywords = unserialize($_COOKIE['zrla_keywords']);
		} else {
			$keywords = array();
		}
		

		// keywords
		$keywords[$keyword] = $keyword;

		// saving in session
		setcookie('zrla_keywords', serialize($keywords), time() + 604800);
	}

	public static function hasKeyword($keyword, $normalize = true) {
	  $keyword = mb_strtolower($keyword, 'UTF-8');

	  if ($normalize) {
	    $terms = array_keys(textAnalyze($word));
	    // sort words
	    sort($terms);
	    // keyword
	    $keyword = join(' ', $terms);
	  }

	  // pids 
	  if(isset($_COOKIE['zrla_keywords'])) {
  	  	$keywords = unserialize($_COOKIE['zrla_keywords']);
  	  } else {
  	  	$keywords = array();
  	  }
	  return isset($keywords[$keyword]);
	}

	public static function hasInterest($pid) {
	    $pids = Session::get('iids', array());
	    return isset($pids[$pid]);
	}
	public static function hasWish($pid) {
	    $pids = Session::get('wids', array());
		return isset($pids[$pid]);
	}

	public static function getInterest() {
	  if(Sentry::check())
	  {
	  	$interest = UserInterest::where('user_id','=',Sentry::getUser()->id)->where('type','=',1)->get();
	  	$products = array();
        foreach($interest as $i)
        {
          $products[] = $i->product_id;
        }
        Session::put('iids', $products);
      }	
      if(count(Session::get('iids')) == 1)
      {  
      	 if(array_key_exists(0,Session::get('iids')) && Session::get('iids')[0]== 0)
      	 {
      	 	Session::put('iids',array());	
      	 }
      }

	  return Session::get('iids', array());
	}

	public static function getWish() {
	  if(Sentry::check())
	  {
	  	$interest = UserInterest::where('user_id','=',Sentry::getUser()->id)->where('type','=',4)->get();
	  	$products = array();
        foreach($interest as $i)
        {
          $products[] = $i->product_id;
        }
        Session::put('wids', $products);
      }
	  return Session::get('wids', array());
	}

	public static function addInterest($pid) {
	    $pids       = Session::get('iids', array());
	    $pids[$pid] = $pid;
	    Session::put('iids', $pids);

	    return $pids;
    }

    public static function addWish($pid) {
	    $pids       = Session::get('wids', array());
	    $pids[$pid] = $pid;
	    Session::put('wids', $pids);
	    return $pids;
    }

    public static function addAuto($pid) {
	    $pids       = Session::get('autoids', array());
	    $pids[$pid] = $pid;
	    Session::put('autoids', $pids);
	    return $pids;
    }

    public static function addProperty($pid) {
	    $pids       = Session::get('properids', array());
	    $pids[$pid] = $pid;
	    Session::put('properids', $pids);
	    return $pids;
    }

	public static function deleteInterest($pid) {
		$pids       = Session::get('iids', array());
		if (isset($pids[$pid])) {
		  unset($pids[$pid]);
		}
		Session::put('iids', $pids);
		return $pids;
	}

	public static function deleteWish($pid) {
		$pids       = Session::get('wids', array());
		if (isset($pids[$pid])) {
		  unset($pids[$pid]);
		}
		Session::put('wids', $pids);
		return $pids;
	}

	public static function deleteAuto($pid) {
		$pids       = Session::get('autoids', array());
		if (isset($pids[$pid])) {
		  unset($pids[$pid]);
		}
		Session::put('autoids', $pids);
		return $pids;
	}

	public static function deleteProperty($pid) {
		$pids       = Session::get('properids', array());
		if (isset($pids[$pid])) {
		  unset($pids[$pid]);
		}
		Session::put('properids', $pids);
		return $pids;
	}

	public static function hasBasket($baid) {
	    $baids = Session::get('baids', array());
		return isset($baids[$baid]);
	}

	public static function hasZkBasket($baid) {
	    $baids = Session::get('zkbaids', array());
		return isset($baids[$baid]);
	}

	public static function getBasket() {
	  return Session::get('baids', array());
	}

	public static function getZkBasket() {
	  // $sessionId = Session::get('session_id');
	  // if(!$sessionId){
	  // 	$sessionId = md5(uniqid(rand(), true));
	  // 	Session::put('session_id', $sessionId);
	  	
	  // 	$user_id = 0;
	  // 	if(Sentry::check()){
	  // 		$user_id = Sentry::getUser()->id;
	  // 	}
	  // 	$basket = ZKBasket::createSession($sessionId, $user_id);
	  // }
	  // Session::forget('session_id');
	  return Session::get('zkbaids', array());
	}

	public static function addBasket($baid) {
	    $baids       = Session::get('baids', array());
	    $baids[$baid] = $baid;
	    Session::put('baids', $baids);
	    return $baids;
    }

    public static function addZkBasket($baid, $amount = 1, $color = 0, $size = 0) {
	    $baids       = Session::get('zkbaids', array());
	    $array = array('zkpId'=>$baid, 'amount'=>$amount, 'color'=>$color, 'size'=>$size);
	    $baids[$baid] = $array;
	    Session::put('zkbaids', $baids);
	    if(Sentry::check())
		  {
		  	$basket = ZKBasket::getUserBasket(Sentry::getUser()->id);
		  	$basketItem = ZKBasketItem::getItem($basket->id, $baid, $amount, $color, $size);
	      }
	    return $baids;
    }

    public static function addCouponToBasket($coupon_id) {
	    $coupon_ids       = Session::get('coup_ids', array());
	    $coupon_ids[$coupon_id] = $coupon_id;
	    Session::put('coup_ids', $coupon_ids);
	    return $coupon_ids;
    }

    public static function delCouponBasket($coupon_id) {
	    $coupon_ids       = Session::get('coup_ids', array());
	    if (isset($coupon_ids[$coupon_id])) {
		  unset($coupon_ids[$coupon_id]);
		}
		Session::put('coup_ids', $coupon_ids);
		return $coupon_ids;
	}

	public static function getCouponBasket() {
	  return Session::get('coup_ids', array());
	}

	public static function hasCouponBasket($coupon_id) {
	    $coupon_ids = Session::get('coup_ids', array());
		return isset($coupon_ids[$coupon_id]);
	}

    public static function deleteBasket($baid) {
		$baids       = Session::get('baids', array());
		if (isset($baids[$baid])) {
		  unset($baids[$baid]);
		}
		Session::put('baids', $baids);
		return $baids;
	}

	public static function deleteZkBasket($baid, $color, $size) {
		$baids       = Session::get('zkbaids', array());
		if (isset($baids[$baid])) {
		  unset($baids[$baid]);
		}
		if(Sentry::check())
		  {
		  	$basket = ZKBasket::getUserBasket(Sentry::getUser()->id);
		  	$sub_total = ZKBasketItem::removeItem($basket->id, $baid, $color, $size);
		  	$basket->total = $basket->total - $sub_total;
		  	$basket->save();
	      }
		Session::put('zkbaids', $baids);
		return $baids;
	}

	public static function addPid($pid) {
		$pids       = Session::get('pids', array());
		$pids[$pid] = $pid;
		Session::put('pids', $pids);
	}

	public static function hasPid($pid) {
		$pids = Session::get('pids', array());
		return isset($pids[$pid]);
	}

	public static function deletePid($pid) {
		$pids = Session::get('pids', array());
		if (isset($pids[$pid])) {
		  unset($pids[$pid]);
		}
		Session::put('pids', $pids);
	}

	public static function getPids() {
		$pids = Session::get('pids', array());
		return $pids;
	}

	public static function addPids() {
        $products = Product::where('user_id', '=', Sentry::getUser()->id)->get();
        foreach($products as $product) {
            sfUser::addPid($product->id);
        }

        $comFol = CompanyFollow::where('user_id', '=', Sentry::getUser()->id)->first();
        if($comFol){
        	Session::put('isFollow', true);
        }
	}

	public static function isFollow(){
		$isFol = Session::get('isFollow', false);
		return $isFol;
	}
 
}