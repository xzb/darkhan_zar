<?php namespace App\Models;

use App\Models\Category;
use App\Models\Product;
use App\Models\Page;
use DB;

class SitemapRun
{

	/**
	 * @var \Vinicius73\SEO\Generators\SitemapGenerator
	 */
	public $generator;

	public function __construct($generator)
	{
		$this->generator = $generator;
	}

	/**
	 * Run generator commands
	 */
	public function run()
	{
		// 20 days before
		$date = date('Y-m-d', time() - 86400 * 20);

		$cats  = DB::table('category')->whereBetween('level', array(0, 3))->orderBy('level', 'ASC')->get();
		$prods = DB::table('product')->where('created_at', '>', $date)->take(5000)->orderBy('created_at','DESC')->get();
		$pages = Page::all();

		$this->generator->addRaw(
			array(
				  'location'         => $_ENV['baseurl'],
				  'last_modified'    => '2014-10-10',
				  'change_frequency' => 'yearly',
				  'priority'         => '1.0'
			)
		);

		foreach($cats as $cat) {
			$this->generator->addRaw(
				array(
					  'location'         => $_ENV['baseurl'].'/c/'.$cat->id,
					  'last_modified'    => '2014-10-10',
					  'change_frequency' => 'yearly',
					  'priority'         => '0.9'
				)
			);
		}

		foreach($pages as $page) {
			$this->generator->addRaw(
				array(
					  'location'         => $_ENV['baseurl'].'/page/'.$page->slug,
					  'last_modified'    => '2014-10-10',
					  'change_frequency' => 'monthly',
					  'priority'         => '0.8'
				)
			);
		}

		foreach($prods as $prod) {
			$this->generator->addRaw(
				array(
					  'location'         => $_ENV['baseurl'].'/p/'.$prod->id,
					  'last_modified'    => date('Y-m-d', strtotime($prod->updated_at)),
					  'change_frequency' => 'monthly',
					  'priority'         => '0.5'
				)
			);
		}

		return $this->generator->generate();
	}
}