<?php namespace App\Models;
use Cache;
 
class ProductAttribute extends \Eloquent {
 
    protected $table = 'product_attribute';
    public    $timestamps = false;
    
    public static function getAttrValues($product_id)
    {
    	return ProductAttribute::where('product_id','=',$product_id)->groupBy('attribute_id')->get();
    }

    public static function getProductAttrValues($product_id, $attribute_id, $type)
    {
    	if($type == 'textbox' || $type == 'textarea'){
    		$pa = ProductAttribute::where('product_id','=',$product_id)->where('attribute_id', '=', $attribute_id)->first();
    		if($pa){
    		return $pa->attribute_value;
    		}else{
    		return null;
    		}
    	}else{
	    	$pa = ProductAttribute::where('product_id','=',$product_id)->where('attribute_id', '=', $attribute_id)->first();
	    	if($pa){
	    	$value = ProductAttributeValue::getAttrValue($pa->id);
            if(is_null($value))
              return null;
            else
	    	  return $value->value;
            
	    	}else{
	    		return null;
	    	}
    	}
    }

    public static function getProductAttributeOne($product_id, $attribute_id){
    	return ProductAttribute::where('product_id', '=', $product_id)->where('attribute_id', '=', $attribute_id)->first();
    }

    public static function getProductIdsByAttrVal($id)
    {
        $returnArr = array();
        $pa = ProductAttribute::join('product_attribute_value', function($join) {
            $join->on('product_attribute.id', '=', 'product_attribute_value.product_attribute_id');
        })
        ->where('product_attribute_value.attribute_value_id',$id)
        ->select('product_attribute.product_id')
        ->get();

        foreach($pa as $p)
        {
            array_push($returnArr, $p->product_id);
        }    
        return $returnArr;
    }

}