<?php namespace App\Models;
use Cache;
 
class CategoryAttribute extends \Eloquent {
 
    protected $table = 'category_attribute';
    public    $timestamps = false;

    public static function getAttributesByCategory($category_id){

        
        $attributes = CategoryAttribute::join('attribute', function($join) {
            $join->on('category_attribute.attribute_id', '=', 'attribute.id');
        })
        ->where('category_attribute.category_id','=',$category_id)
        ->select('attribute.*')
        ->orderBy('attribute.sort_order', 'asc')
        // ->orderBy('attribute.type', 'asc')
        

        ->get();

        return $attributes;

    }

    public static function getAttributesByCategoryFilter($category_id){

        $filterArray = array('selectbox', 'checkbox', 'radio');
        $attributes = CategoryAttribute::join('attribute', function($join) {
            $join->on('category_attribute.attribute_id', '=', 'attribute.id');
        })
        ->where('category_attribute.category_id','=',$category_id)
        ->where('is_filterable', '=', 1)
        ->whereIn('type', $filterArray)
        ->select('attribute.*')
        ->orderBy('attribute.sort_order', 'asc')
        ->orderBy('attribute.type', 'asc')
        

        ->get();

        return $attributes;

    }
    
}