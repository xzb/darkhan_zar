<?php namespace App\Models;

use App\Models\Category;
use Cache, View, Session;
 
class Mycache {


    public static function homepage(){
        $roots = Category::where('parent_id','=',null)->where('homepage_visible','=',1)->orderBy('rank')->get();
        
        // if (!Cache::has('view_homepage')) {
        //     $html = \View::make('front._partials.homepage')
        //                 ->with('colsWithRoots', $colsWithRoots)
        //                 ->with('cols', $cols)->render();
        //     Cache::add('view_homepage', $html, 1440);
        // }

        // return Cache::get('view_homepage');

        return            $html = \View::make('front._partials.homepage')
                        ->with('roots', $roots)
                        ->render();

    }

    public static function autohomebanner(){
        if (!Cache::has('view_autohome_banner')) {
            $banners = Banner::getBannersByLocation(6);
            $html = View::make('front.banner.autohome', array('banners'=>$banners))->render();
                Cache::add('view_autohome_banner', $html, 1440);
        }
        return Cache::get('view_autohome_banner');
    }

    public static function footer(){
        // if (!Cache::has('view_footer')) {
        //     $html = View::make('front._partials.footer')->render();
        //     Cache::add('view_footer', $html, 1440);
        // }
        // return Cache::get('view_footer');

        return $html = View::make('front._partials.footer')->render();
    }

    public static function autofooter(){
    	if (!Cache::has('view_autofooter')) {
    		$html = View::make('front._partials.autofooter')->render();
    	    Cache::add('view_autofooter', $html, 1440);
    	}
    	return Cache::get('view_autofooter');
	}

    public static function banner($loc, $category_id = 0){
    	
        if (!Cache::has('view_banner_'.$loc.$category_id)) {
            

            if($loc == 2)
    		{
                $html = View::make('front.banner.show', array('loc' => $loc, 'category_id' => $category_id))->render();
	            Cache::add('view_banner_'.$loc.'0', $html, 1440);
            }
            elseif($loc == 1 || $loc == 3 || $loc == 4)
            {
                $banner = Mycache::getBanner($loc, $category_id);
                $html = View::make('front.banner.show2', array('loc' => $loc, 'banner1' => $banner))->render();
                Cache::add('view_banner_'.$loc.$category_id, $html, 1440);

            }
            else if($loc == 7)
            {
                $banner = Banner::where('page_id','=',$loc)->orderBy('sort','asc')->get();
                $html = View::make('front.banner.show3', array('loc' => $loc, 'banner1' => $banner))->render();
                Cache::add('view_banner_'.$loc.$category_id, $html, 1440);                
            }  

    	}
    	return Cache::get('view_banner_'.$loc.$category_id);
	}

    public static function banner2($loc, $category_id = 0, $banner_id){
        
        // if (!Cache::has('view_banner_'.$loc.$category_id)) {
            // $categoryBanner = '';
            // if($loc == 1)
            // {
            //     $categoryBanner = CategoryBanner::where('category_id', '=', $category_id)->orderByRaw("RAND()")->first(); 
            //     Session::put('categoryBannerId', $categoryBanner->id);
            // }elseif( $loc == 3 || $loc == 4){
            //     $categoryBannerId = Session::get('categoryBannerId');
            //     if($categoryBannerId){
            //         $categoryBanner = CategoryBanner::find($categoryBannerId);
            //     }else{
            //         $categoryBanner = CategoryBanner::where('category_id', '=', $category_id)->orderByRaw("RAND()")->first();
            //     }
            // }
            $banner = Banner::where('id', '=', $banner_id)
                    ->where('start_date', '<=', date('Y-m-d', time()))
                    ->where('end_date', '>=', date('Y-m-d', time()))
                    ->where('is_active','=',1)
                    // ->where('page_id','=',$loc)
                    ->first();
            // $banner = $banner_id Mycache::getBanner2($loc, $category_id, $categoryBanner);
            $html = View::make('front.banner.show2', array('loc' => $loc, 'banner1' => $banner))->render();
            return $html;
            // Cache::add('view_banner_'.$loc.$category_id, $html, 1440);
        // }
        // return Cache::get('view_banner_'.$loc.$category_id);
    }

    public static function bannerOnProduct($loc, $product_id = 0){
        $product = Product::find($product_id);
        $category_id = $product->category_id;

        if (!Cache::has('view_banner_'.$loc.$category_id)) {
            if($loc == 2)
            {
                $html = View::make('front.banner.show', array('loc' => $loc, 'category_id' => $category_id))->render();
                Cache::add('view_banner_'.$loc.$category_id, $html, 1440);
            }
            elseif($loc == 1 || $loc == 3 || $loc == 4)
            {
                $banner = Mycache::getBanner($loc, $category_id);
                $html = View::make('front.banner.show2', array('loc' => $loc, 'banner1' => $banner))->render();
                Cache::add('view_banner_'.$loc.$category_id, $html, 1440);

            }

        }
        return Cache::get('view_banner_'.$loc.$category_id);
    }

    public static function getBanner($loc,$category_id)
    {
        $category = null;
        $category = Category::find($category_id); 
        $banner1 = null;

        switch ($loc) {
            case 1:
                $cat_banner = $category->banner_id;        
                break;
            case 3:
                $cat_banner = $category->banner2_id;        
                break;
            case 4:
                $cat_banner = $category->banner3_id;        
                break;
            default:
                $cat_banner = 0;
                break;    
        }
        
        if($cat_banner != 0)
        {
            $banner1 = Banner::where('id', '=', $cat_banner)
                    ->where('start_date', '<=', date('Y-m-d', time()))
                    ->where('end_date', '>=', date('Y-m-d', time()))
                    ->where('is_active','=',1)
                    ->where('page_id','=',$loc)
                    ->first();
        }
        else 
        {
            $ancestorsArr = array();
            $ancestors = $category->ancestors()->get();
            foreach($ancestors as $a)
            {
                switch ($loc) {
                    case 1:
                        $cat_ban = $a->banner_id;        
                        break;
                    case 3:
                        $cat_ban = $a->banner2_id;        
                        break;
                    case 4:
                        $cat_ban = $a->banner3_id;        
                        break;
                    default:
                        $cat_ban = 0;
                        break;    
                }
                if($cat_ban !=0)
                {
                    array_push($ancestorsArr, $cat_ban);
                }
            }

            if(end($ancestorsArr) != 0)
            {
                $banner1 = Banner::where('id', '=', end($ancestorsArr))
                    ->where('start_date', '<=', date('Y-m-d', time()))
                    ->where('end_date', '>=', date('Y-m-d', time()))
                    ->where('is_active','=',1)
                    ->where('page_id','=',$loc)
                    ->first();       
            }    
        }

        return $banner1;
    }
 
    public static function getBanner2($loc,$category_id, $categoryBanner)
    {
        $category = null;
        $category = Category::find($category_id);

        $banner1 = null;

        switch ($loc) {
            case 1:
                $cat_banner = $categoryBanner->banner_id;        
                break;
            case 3:
                $cat_banner = $categoryBanner->banner2_id;        
                break;
            case 4:
                $cat_banner = $categoryBanner->banner3_id;        
                break;
            default:
                $cat_banner = 0;
                break;    
        }
        
        if($cat_banner != 0)
        {
            $banner1 = Banner::where('id', '=', $cat_banner)
                    ->where('start_date', '<=', date('Y-m-d', time()))
                    ->where('end_date', '>=', date('Y-m-d', time()))
                    ->where('is_active','=',1)
                    // ->where('page_id','=',$loc)
                    ->first();
        }

        return $banner1;
    }

}