<?php namespace App\Models;
use Cache;
 
class ZKCategoryAttribute extends \Eloquent {
 
    protected $table = 'zk_category_attribute';
    public    $timestamps = false;

    public static function getAttributesByCategory($category_id){

        
        $attributes = ZKCategoryAttribute::join('zk_attribute', function($join) {
            $join->on('zk_category_attribute.attribute_id', '=', 'zk_attribute.id');
        })
        ->where('zk_category_attribute.category_id','=',$category_id)
        ->select('zk_attribute.*')
        ->orderBy('zk_attribute.sort_order', 'asc')
        ->orderBy('zk_attribute.type', 'asc')
        ->get();

        return $attributes;

    }
    
}