<?php namespace App\Models;

use DB;

class UserMail extends \Eloquent {
 
    protected $table      = 'user';
    public    $timestamps = false;
    
    public static function getUserInstance($email) {
    // user instance
    $user = UserMail::where('email', '=', $email)->first();

    // user is exist
    if (!$user) {
      $user = new UserMail;
      $user->email = $email;
      $user->save();
    }

    return $user;
  }
}