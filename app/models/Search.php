<?php namespace App\Models;

class Search extends \Eloquent {
 
    protected $table = 'search';
    public    $timestamps = false;

    public static function insert($keyword){

        $search = new Search();
        $search->keyword    = $keyword;
        $search->created_at = date('Y-m-d H:m:i');
        $search->save();

	    // terms
	   //  $terms = textAnalyze($keyword);

	   //  foreach ($terms as $term => $count) {
	   //    // blah blah
		  // $word = Word::firstOrCreate(array('name' => $term));

	   //    // search word blah blah
	   //    $searchWord = new SearchWord();
	   //    $searchWord->word_id   = $word->id;
	   //    $searchWord->search_id = $search->id;
	   //    $searchWord->created_at = date('Y-m-d H:m:i');
	   //    $searchWord->save();
	   //  }

	    
    }
    
}