<?php namespace App\Models;
 
class BannerLocation extends \Eloquent {
 
    protected $table = 'banner_location';
 
    public function author()
    {
        return $this->belongsTo('User');
    }

    public static function getLocationArr(){
        $returnArray = array();
        $locations = BannerLocation::get();
        
        foreach($locations as $l)
        {   
            $returnArray[$l->id] = $l->location;
        }
        return $returnArray;
    }
 }