<?php namespace App\Models;
use Event, DB;
class State extends \Eloquent {
 
    protected $table = 'state';
    public    $timestamps = false;
 	
 	public static function getListDaysCount($day, $category_id){
        $state = State::where('created_at', '=', $day)->where('category_id', '=', $category_id)->first();
        if($state){
        	return $state->numb;	
        }else{
        	return 0;
        }
        
    }

    public static function getListMountCount($from, $to, $category_id){

        $between  = array($from, $to);
        $states = DB::table('state')->select(DB::raw('SUM(numb) as results'))->where('category_id', '=', $category_id)->whereBetween('created_at', $between)->first();
        
        return $states->results;
    }

    public static function getDayCat($day, $category_id){
        $state = State::where('created_at', '=', $day)->where('category_id', '=', $category_id)->first();
        return $state;
    }

    public static function getList($from, $to, $category_id){

        $between  = array($from, $to);
        $states = DB::table('state')->where('category_id', '=', $category_id)->whereBetween('created_at', $between)->get();
        
        return $states;
    }

    public static function getCountDay(){
        $day = date('Y-m-d', strtotime('-1 days'));
        $co = 0;
        $states = State::where('created_at', '=', $day)->get();
        foreach ($states as $key => $state) {
            $co += $state->numb;
        }
        return $co;
    }

}