<?php namespace App\Models;
use Cache;
 
class Location extends \Eloquent {

    protected $table = 'location';
    public    $timestamps = false;
    
    public static function getChilds($pid = null) {
      if($pid) {
          $cats = Location::where('parent_id','=', $pid)->orderBy('id', 'ASC')->get();
      } else {
          $cats = Location::where('parent_id','!=',0)->orderBy('id', 'ASC')->get();
      }
      return $cats;
    }
}