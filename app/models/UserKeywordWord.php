<?php namespace App\Models;

use DB;

class UserKeywordWord extends \Eloquent {
 
    protected $table      = 'user_keyword_word';
    public    $timestamps = false;
    
    
    public static function pushToUserKeyword($keyword_id, $word_id) {
    $instance = new UserKeywordWord;
    $instance->keyword_id = $keyword_id;
    $instance->word_id = $word_id;
    $instance->save();

    return $instance;
  }
}