<?php namespace App\Models;

use DB;

class SearchWord extends \Eloquent {
 
    protected $table      = 'search_word';
    public    $timestamps = false;

  public static function getSearchQuery($keyword) {
     $terms = textAnalyze($keyword);

     $originalTerms = array_keys($terms);
     // all terms
     $allTerms = $originalTerms;

     foreach ($originalTerms as $term) {
       $allTerms[] = translateText($term);
     }

     // removing duplicated words
     $allTerms = array_unique($allTerms);
     
     // preventing blah blah
     $allTerms[] = "_";

    $query =  DB::table('product_word')
              ->join('word', 'product_word.word_id', '=', 'word.id')
              ->select('product_word.product_id')
              ->groupBy('product_word.product_id')
     //->orderBy('product_word.product_id', 'desc')
     ->havingRaw('COUNT(product_word.product_id)>='.count($terms))
     ->whereIn('word.name', $allTerms);

     return $query;
  }

    public static function getSearchQueryV2($keyword, $page = 1, $limit = 50) {

      $start = ($page * $limit) - $limit;
      $end   = $start + $limit;

      $terms = textAnalyze($keyword);

      $originalTerms = array_keys($terms);
      // all terms
      $allTerms = $originalTerms;

      foreach ($originalTerms as $term) {
        $allTerms[] = translateText($term);
      }

      // removing duplicated words
      $allTerms = array_unique($allTerms);
      
      // preventing blah blah
      $allTerms[] = "_";
      
      $result = DB::table('product_word as p')
            ->join('word as w', 'p.word_id', '=', 'w.id')
            ->whereIn('w.name',$allTerms)
            ->groupBy('p.product_id')
            ->having('count','>=',count($terms))
            ->orderBy('p.product_id','desc')
            ->select('p.product_id',DB::raw("COUNT('p.product_id') as count"))

            ->paginate($limit);
      return $result;
    }

    public static function getSearchCount($keyword) {

      $terms = textAnalyze($keyword);

      $originalTerms = array_keys($terms);
      // all terms
      $allTerms = $originalTerms;

      foreach ($originalTerms as $term) {
        $allTerms[] = translateText($term);
      }

      // removing duplicated words
      $allTerms = array_unique($allTerms);
      
      // preventing blah blah
      $allTerms[] = "_";
      
      $allTerms2 = array();
      foreach($allTerms as $allTerm) {
        $allTerms2[] = "'".$allTerm."'";
      }

      $query =
      "
    SELECT COUNT( * ) AS num_results
    FROM (

    SELECT p.id
    FROM product_word p
    INNER JOIN word w ON p.word_id = w.id
    WHERE w.name IN (".implode(',', $allTerms2).")
    GROUP BY p.product_id
    HAVING COUNT( p.product_id ) >= ".count($terms)."
    )dctrn_count_query
      ";

      $res = DB::select(DB::raw($query));

      if($res) {
        return $res[0]->num_results;
      } else {
        return 0;
      }
    }
    
}