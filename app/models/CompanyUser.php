<?php namespace App\Models;
use Cache;
 
class CompanyUser extends \Eloquent {
 
    protected $table = 'company_user';
    public    $timestamps = false;
    
}