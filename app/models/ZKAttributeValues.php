<?php namespace App\Models;
use Cache;
 
class ZKAttributeValues extends \Eloquent {
 
    protected $table = 'zk_attribute_values';
    public    $timestamps = false;
	
	public static function getValuesByAtt($attr_id){
		$values = ZKAttributeValues::where('attribute_id','=',$attr_id)->get();

        return $values;
	}
}