<?php namespace App\Models;

use Cache, DB;
 
class ZKBasket extends \Eloquent {
 
    protected $table = 'zk_basket';
    public    $timestamps = false;
 	
 	public static function createSession($session_id, $user_id){
		$b = ZKBasket::where('session_id', '=', $session_id)->first();
		if(!$b){
			$b = new ZKBasket;
			$b->created_at = date('Y-m-d H:i:s', time());
		}
		$b->user_id = $user_id;
		$b->session_id = $session_id;
		$b->save();
		return $b;
	}

	public static function getUserBasket($user_id){
		$b = ZKBasket::where('user_id', '=', $user_id)->where('status', '=', 0)->orderBy('created_at','desc')->first();
		if(!$b){
			$b = new ZKBasket;
			$b->created_at = date('Y-m-d H:i:s', time());
			$b->user_id = $user_id;
			$b->status = 0;
			$b->save();
		}
		return $b;
	}
}