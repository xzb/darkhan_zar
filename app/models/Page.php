<?php namespace App\Models;

use Cache, DB;
 
class Page extends \Eloquent {
 
    protected $table = 'pages';
    protected $fillable = array(
        'id',
        'type',
        'page_id',
        'image',
        'author',
        'title',
        'description',
        'body',
        'is_featured',
        'is_emeregency',
        'user_id',
        'created_at',
        'albom_id',
        'image_old',
        'youtube_link'
        );
 
    public function author()
    {
        return $this->belongsTo('User');
    }

    public function pagestatus()
    {
        return $this->hasOne('App\Models\Pagestatus', 'page_id');
    }    

    public function uploads()
    {
        return $this->hasMany('Uploads', 'obj_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('Comment', 'page_id', 'id');
    }

    public function getCategory() {
        $category = Category::find($this->page_id);
        return $category ? $category->name : '';
    }    
    
    public function getNewsList() {
        $pages = Page::where('type', '=', 1)->take(50)->get();
        return $pages;
    }

    public static function allPagesToArray($page_id,$level){
    	if($page_id == 0) $returnArray = array();
        $pages = Page::where('page_id', '=', $page_id)->where('type','=',0)-> get();
        
        foreach($pages as $p)
        {   
            $returnArray[$p->id] = str_repeat(' &nbsp; &nbsp;', $level).' &raquo; '.$p->title;
            $cntChild = count(Page::where('page_id','=',$p->id)->where('type','=',0)->get());

            if($cntChild !=0){
                 $returnArray = $returnArray + Page::allPagesToArray($p->id,$level+1);
            }
        }
        return $returnArray;
    }

    public static function getFeaturedCachedNewss($minutes = 180){
        if (!Cache::has('is_featured')) {
            $newss = Page::where('type', '=', 1)
                         ->where('is_featured', '=', 1)
                         ->where('is_active', '=', 1)
                         ->orderBy('created_at', 'DESC')
                         ->take(4)->get();

            Cache::add('is_featured', $newss, $minutes);
        }
        return Cache::get('is_featured');
    }

    public static function getPagesByCategory($category_id, $limit = 3){
        if (!Cache::has('category_featured_'.$category_id)) {
            $pages = Page::where('type', '=', 1)
                         ->where('is_featured', '=', 1)
                         ->where('is_active', '=', 1)
                         ->where('page_id', '=', $category_id)
                         ->orderBy('created_at', 'DESC')
                         ->take($limit)->get();
             Cache::add('category_featured_'.$category_id, $pages,180);
        }

        return Cache::get('category_featured_'.$category_id);
    }    

    public static function setPageStatus($page_id) {
        $page_status = Pagestatus::getPageStatus($page_id);
        $page_status->view_count  = $page_status->view_count + 1;
        $page_status->total       = $page_status->facebook + $page_status->twitter + $page_status->google;
        $page_status->save();
    }

    public static function countComment($page_id) {
        $count = Comment::where('page_id', '=', $page_id)->count();
        return $count;
    }    

    // most resent pages
    public static function mpages1() {
        if (!Cache::has('most_pages_1')) {
            $pages = Page::where('type', '=', 1)
                         ->where('is_active', '=', 1)
                         ->orderBy('created_at', 'DESC')
                         ->take(20)->get();

            foreach($pages as $page) {
                $page_status = Pagestatus::firstOrCreate(array('page_id' => $page->id));
                $page['comment_count'] = $page_status->comment_count;
                $page['total']         = $page_status->total;
            }

            Cache::add('most_pages_1', $pages, 10);
        }
        return Cache::get('most_pages_1');
    }

    public static function mpages2() {
        if (!Cache::has('most_pages_2')) {

            $query =
            '
            SELECT ps.comment_count, ps.total, pt.id as pid, pt.title as ptitle, pt.image as pimage, pt.image_old as pimageold, pt.is_emergency as pis_emergency, pt.created_at as pcreated_at, ps.view_count as view_count, pt.youtube_link, pt.is_uploads FROM 

            (
            select id, page_id, view_count, comment_count, total from `page_status` order by view_count desc LIMIT 40
            ) as ps, pages as pt

            WHERE

            ps.page_id    = pt.id AND 
            pt.is_active  = 1 AND
            pt.created_at >= DATE_ADD(CURDATE(), INTERVAL -2 DAY)

            group by pt.id
            order by ps.view_count DESC, pt.created_at DESC
            LIMIT 20
            ';
            $pages = DB::select(DB::raw($query));

            Cache::add('most_pages_2', $pages, 182);
        }
        return Cache::get('most_pages_2');
    }

    public static function mpages3() {
        if (!Cache::has('most_pages_3')) {

            $query =
            '
            SELECT ps.comment_count, ps.total, pt.id as pid, pt.title as ptitle, pt.image as pimage, pt.image_old as pimageold, pt.is_emergency as pis_emergency, pt.created_at as pcreated_at, ps.comment_count as comment_count, pt.youtube_link, pt.is_uploads FROM 

            (
            select id, page_id, comment_count, total from `page_status` order by comment_count desc LIMIT 40
            ) as ps, pages as pt

            WHERE

            ps.page_id    = pt.id AND 
            pt.is_active  = 1 AND 
            pt.created_at >= DATE_ADD(CURDATE(), INTERVAL -2 DAY)

            group by pt.id
            order by ps.comment_count DESC, pt.created_at DESC
            LIMIT 20
            ';
            $pages = DB::select(DB::raw($query));            

            Cache::add('most_pages_3', $pages, 188);
        }
        return Cache::get('most_pages_3');
    }     

    public static function getEmergencyNewsCached() {
        if (!Cache::has('is_emergency')) {
            $news1 = Page::where('is_emergency', '=', 1)->where('is_active', '=', 1)->orderBy('created_at', 'DESC')->first();
            Cache::add('is_emergency', $news1, 180);
        }
        return Cache::get('is_emergency');
    }

    public static function getPublishAndConvCached() {
        if (!Cache::has('is_publish_and_conv')) {

            $query =
            '
            SELECT p.id, p.title, p.image, p.image_old, p.is_conversation, p.is_publish, p.created_at
            FROM pages p
            WHERE
            p.is_active  = 1 AND
            ( p.is_conversation = 1 OR p.is_publish = 1 ) 
            order by p.created_at DESC
            LIMIT 1
            ';
            $pages = DB::select(DB::raw($query));  

            // $pages = Page::where('is_publish', '=', 1)
            //              ->where('is_active', '=', 1)
            //              ->whereIn('is_active', '=', 1)
            //              ->orderBy('created_at', 'DESC')
            //              ->first();
            Cache::add('is_publish_and_conv', $pages, 190);
        }
        return Cache::get('is_publish_and_conv');
    }

    public static function getPreNewsCached() {
        if (!Cache::has('is_pre_news')) {

            $categories = Category::where('parent_id', '=', 47)->where('status', '=', 1)->take(10)->get();
            $pages      = array();
            foreach($categories as $category) {
                $tmpPage = Page::where('page_id', '=', $category->id)->where('is_active', '=', 1)->orderBy('created_at', 'DESC')->first();
                if(!$tmpPage)
                    continue;

                $pages[] = $tmpPage;
            }
            Cache::add('is_pre_news', $pages, 300);
        }
        return Cache::get('is_pre_news');
    }

    public static function getRelatedNews($page_id, $limit = 2) {
        if (!Cache::has('related_news_'.$page_id)) {

            $page_id = intval($page_id);
            $query =
            '
                SELECT DISTINCT n.* FROM pages n 
                INNER JOIN page_tag n2 ON n.id = n2.page_id 
                INNER JOIN page_tag n3 ON ((n3.page_id = '.$page_id.' AND n2.tag_id = n3.tag_id)) 
                WHERE (n.is_active = 1 AND n.id <> '.$page_id.') 
                ORDER BY n.created_at DESC LIMIT '.$limit.'
            ';
            $pages = DB::select(DB::raw($query));

            Cache::add('related_news_'.$page_id, $pages, 100);
        }
        return Cache::get('related_news_'.$page_id);
    }

    public static function getFooterPages() {
        if (!Cache::has('footer_pages')) {
            $pages = Page::where('type', '=', 0)
                         ->where('is_active', '=', 1)
                         ->where('is_footer', '=', 1)
                         ->orderBy('created_at', 'DESC')
                         ->take(4)
                         ->get();
            Cache::add('footer_pages', $pages, 1000);
        }
        return Cache::get('footer_pages');
    }

    public static function getJobs() {
        if (!Cache::has('jobs')) {

            $url = 'http://biznetwork.mn/job/rssFeaturedJobs';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            $result = $response;

            $xml = @simplexml_load_string($result);
            if ($xml) {

                $i = 0;
                foreach($xml->children() as $state)
                {
                    if($i >= 10) {
                        continue;
                    }

                    $states[$i]['title']        = (string)$state->title;
                    $states[$i]['link']         = (string)$state->link;
                    $states[$i]['created_at']   = (string)$state->created_at;
                    $states[$i]['company_name'] = (string)$state->company_name;
                    $states[$i]['company_link'] = (string)$state->company_link;
                    $states[$i]['company_logo'] = (string)$state->company_logo;
                    $i++;
                }

                Cache::add('jobs', $states, 184);

            } else {
                Cache::add('jobs', null, 184);
            }

        }

        return Cache::get('jobs');
    }

    public static function getTendersForHomepage() {
        if (!Cache::has('homepage_tenders')) {
            $page = Page::where('page_id', '=', 13)->where('is_active', '=', 1)->orderBy('created_at', 'DESC')->take(10)->get();
            Cache::add('homepage_tenders', $page, 186);
        }
        return Cache::get('homepage_tenders');
    }    
 
}