<?php namespace App\Models;
use DB;
 
class Blacklist extends \Eloquent {
 
    protected $table = 'blacklist';

    public static function isBlacklisted($name, $description, $contact) {

		$terms = $name.' '.$description.' '.$contact;
		$terms = textAnalyze2($terms);
		$terms2 = $name.$description.$contact;
		$terms = array_keys($terms);
		$terms = array_unique($terms);
		$blacklists = DB::table('blacklist')->lists('name');

		$result = array_intersect($terms, $blacklists);

		if(sizeof($result)) {
			$return = true;
		} else {
			$return = false;
		}

		$phones = str_split($terms2);
		$realPhoneNumber = '';
		$intValues = array('1','2','3','4','5','6','7','8','9','0');
		
		for($i=0; $i < count($phones);$i++)
		{	
			if (in_array($phones[$i], $intValues))
			{
				$realPhoneNumber .= $phones[$i];
			}
		}
		
		foreach($blacklists as $b)
		{
			if (strpos($realPhoneNumber, $b) !== FALSE)
			{
		        return true;
		    }
		}
		
		return $return;

    }

 }