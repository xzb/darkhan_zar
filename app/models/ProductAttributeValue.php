<?php namespace App\Models;
use Cache;
 
class ProductAttributeValue extends \Eloquent {
 
    protected $table = 'product_attribute_value';
    public    $timestamps = false;
    

    public static function getProductAttrValues($product_attribute_id)
    {
    	return ProductAttributeValue::where('product_attribute_id','=',$product_attribute_id)->get();
    }
    	
	public static function getAttrValue($id)
    {
    	$pav = ProductAttributeValue::where('product_attribute_id','=',$id)->first();
        if($pav)
    	   $attrVal = AttributeValues::find($pav->attribute_value_id);
        else
           $attrVal = null; 
    	return $attrVal;
    }
}