<?php namespace App\Models;
use Event, DB, Cache,Croppa;
 
class ZKProduct extends \Eloquent {
 
    protected $table = 'zk_product';

    public function delete(array $options = array())
	{
	    //deleting product_words. this product's related
	    if($this->id) {
	      DB::table('zk_product_stock')->where('product_id', '=', $this->id)->delete();
	      DB::table('zk_product_attribute')->where('product_id', '=', $this->id)->delete();
	    }

	    //deleting images.
	      @unlink(public_path() . '/files/uploads/zarket/products/' . $this->image_filename);
	      // Croppa::delete('/files/uploads/zarket/products/' . $this->image_filename);
	    // before save code 
	    parent::delete();
	    
	}

	public static function getProductsByType($type, $limit)
	{
		$query = DB::table('zk_product');
		if($type == 'is_new')
			$query ->where('is_new', '=', 1);
		if($type == 'is_hot')
			$query ->where('is_hot', '=', 1);
		if($type == 'is_sale')
			$query ->where('is_sale', '=', 1);
		
		$ret = $query->orderBy('created_at','DESC')->remember(10)->get();

		return $ret;

		
	}
}