<?php namespace App\Models;
use Cache;
 
class CompanyBanner extends \Eloquent {
 
    protected $table = 'company_banner';
    public    $timestamps = false;

    public static function getBannerInPosition($company_id, $cover_type){
    	return $banner = CompanyBanner::where('company_id','=', $company_id)
    		->where('image_type_id','=', $cover_type)->first();
    }
    
}