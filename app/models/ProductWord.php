<?php namespace App\Models;

use DB;

class ProductWord extends \Eloquent {
 
    protected $table = 'product_word';
    protected $fillable = array(
          'product_id',
          'word_id',
          'relevance',
        );
    public    $timestamps = false;
}