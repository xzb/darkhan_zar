<?php namespace App\Models;
use Cache, DB, Baum\Node;


 
class Category extends  \Baum\Node {
 
    protected $table = 'category';
    public    $timestamps = false;

    // 'parent_id' column name
    protected $parentColumn = 'parent_id';

    // 'lft' column name
    protected $leftColumn = 'lft';

    // 'rgt' column name
    protected $rightColumn = 'rgt';

    // 'depth' column name
    protected $depthColumn = 'level';

    // guard attributes from mass-assignment
    protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'level');

    public static $cat_options = array('----');
    // public static function isLeaf($right = 0, $left = 0){
    //     return $right - $left == 1 ? true : false;
    // }

    public static function getParentByChild($child_id,$parents)
    {   

        $category = Category::find($child_id);
        if($category->parent_id != null)
        {
            array_push($parents, $category->parent_id);
            $parents = Category::getParentByChild($category->parent_id,$parents);
        }

        return $parents;
        
    }

    public static function recover()
    {
      // is function is defined
      if (!function_exists('doRecover')) {

        function doRecover($parent_id, &$count, $level) {
          $list = CategoryTable::getInstance()->createQuery('c')
                  ->orderBy('c.name')
                  ->where('c.parent_id' . ($parent_id !== null ? ' = ' . $parent_id : ' IS NULL'))
                  ->execute();
          $list = Category::where('parent_id',($parent_id !== null ? '=' : 'IS'), $parent_id)->orderBy('name', 'ASC')->get();
          foreach ($list as $c) {
            $lft = ++$count;
            doRecover($c->id, $count, $level + 1);
            $rgt = $count;

            $c->lft = $lft;
            $c->rgt = $rgt;
            $c->level = $level;

            // save and free up resource
            $c->save();
            // $c->free();
          }

          $count++;
        }

        // end of function
      }

      $count = 0;
      // do recover
      doRecover(null, $count, 0);
    }

    public static function getParents($cid, $c = null) {
      // self
      if ($c === null) {
        $c = Category::find($cid);
      }

      $q = 
      "
      SELECT * FROM category c 
      WHERE (".($c ? $c->lft : -1)." BETWEEN c.lft AND c.rgt) 
      ORDER BY c.lft ASC
      ";
      $pages = DB::select(DB::raw($q));
      return $pages;
    }

    public static function getParentsName($cid, $c = null) {
      // self
      if ($c === null) {
        $c = Category::find($cid);
      }

      $q = 
      "
      SELECT * FROM category c 
      WHERE (".($c ? $c->lft : -1)." BETWEEN c.lft AND c.rgt) 
      ORDER BY c.lft ASC
      ";
      $cats  = DB::select(DB::raw($q));
      $names = array();

      foreach($cats as $cat) {
        $names[] = $cat->name;
      }

      return $names;
    }

    public static function getChildren($pid = null) {
      if ($pid) {
        $pid = (int) $pid;
      }
      if($pid) {
          $cats = Category::where('parent_id','=', $pid)->where('is_visible','=',1)->orderBy('rank', 'ASC')->get();
      } else {
          $cats = Category::whereNull('parent_id')->where('is_visible','=',1)->orderBy('rank', 'ASC')->get();
      }
      return $cats;
    }

    public static function getAncestorsId($category_id = 0) {

      $category = Category::find($category_id);
      if($category)
      {
        $ans = $category->ancestorsAndSelf()->lists('id');

        $res = '';
        if(sizeOf($ans)) {
          $res = implode(',', $ans);
        }
        return $res;
      }
      else
      return null;  
    }

    public static function getGrandParentId($category_id){
      $parent = Category::find($category_id);
      if($parent->parent_id){
        return Category::getGrandParentId($parent->parent_id);
      }else{
        return $category_id;
      }
    }

    public static function formatCategory($level, $parent_id , $nested_category, $cat_options)
    {
      foreach($nested_category[$parent_id] as $id => $name)
      {
        $cat_options[$id] = str_repeat('&nbsp;', $level * 8) . $name;
        if (isset($nested_category[$id]))
        {
          $cat_options = Category::formatCategory($level + 1, $id, $nested_category, $cat_options, $cat_options);
        }
      }
      return $cat_options;
    }

    public static function getParentCategoryOptions($self_ids = 19)
    {
      $cat_options = array('----');
      $cat = Category::find($self_ids);
      $categories = Category::orderBy('rank', 'ASC')
            ->where('lft', '>=', $cat->lft)
            ->where('rgt', '<=', $cat->rgt)
            ->get();

      $nested_category = array();
      $nested_category[$cat['parent_id']][$cat['id']] = $cat['name'];    
      
      foreach($categories as $category)
      {
        //echo $category->id; die;
        //$nested_category[$category->getParentId()][$category->getId()] = $category->getName();
        $nested_category[$category['parent_id']][$category['id']] = $category['name'];
      }
      
      $cat_options = Category::formatCategory(0, $cat->parent_id, $nested_category, $cat_options);
      
      return $cat_options;
    }

    public static function getCategoryOptions()
    {
      $cat_options = array('----');
      $categories = Category::orderBy('rank', 'ASC')->get();

      $nested_category = array();
      
      foreach($categories as $category)
      {
        $nested_category[$category['parent_id']][$category['id']] = $category['name'];
      }
      
      $cat_options = Category::formatCategory(0, 0, $nested_category, $cat_options);
      
      return $cat_options;
    }

     // public function delete(array $options = array())
     // {
     //  $lft = $this->lft;
     //  $rgt = $this->rgt;
     //  $width = $rgt - $lft + 1;

     //  // delete subchildren
     //  DB::statement("DELETE FROM category WHERE lft BETWEEN {$lft} AND {$rgt}");

     //  // update left right
     //  DB::statement("UPDATE category SET rgt = rgt - {$width} WHERE rgt > {$rgt}");
     //  DB::statement("UPDATE category SET lft = lft - {$width} WHERE lft > {$rgt}");

     //  return parent::delete($conn);
     // }

   public function save(array $options = array())
   {
        Cache::forget('catgory_list_'.$this->id);
        Cache::forget('front.homepage');

        // before save code 
        parent::save();
        // after save code
   }

   public function delete(array $options = array())
   {
        Cache::forget('catgory_list_'.$this->id);
        Cache::forget('front.homepage');

        // before save code 
        parent::delete();
        // after save code
   }

   public function getParentIdsArray(array $options = array())
   {
      $parent_ids = str_replace('-', '', $this->parent_ids);
      return explode(',', $parent_ids);
   }

   public function getSeoUrl(array $options = array())
   {
      return $this->seo_url ? $this->seo_url : $this->name;
   }

}