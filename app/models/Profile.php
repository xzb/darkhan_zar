<?php namespace App\Models;
use Cache;
 
class Profile extends \Eloquent {
 
    protected $table = 'profile';
    public    $timestamps = false;
    
}