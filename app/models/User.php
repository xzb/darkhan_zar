<?php namespace App\Models;

use Eloquent;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Cartalyst\Sentry\Users\Eloquent\User implements UserInterface, RemindableInterface {
/**
* The database table used by the model.
*
* @var string
*/
protected $table = 'users';
protected $fillable = array(
	'email',
	'password',
	'first_name',
	'last_name',
	'social_id',
	'activated',
	'image',
	'type',
	'token',
	'token_secret'
);

public static function getOnlineUsers()
{
	$users = User::where('is_online','=',1)->get();
	$onlineUsers = array();
	foreach($users as $u){
		$onlineUsers[] = $u->id;
	}
	return $onlineUsers;
}

public function getRememberToken()
{
    return $this->remember_token;
}

public function setRememberToken($value)
{
    $this->remember_token = $value;
}

public function getRememberTokenName()
{
    return 'remember_token';
}

/**
* The attributes excluded from the model's JSON form.
*
* @var array
*/
protected $hidden = array('password');

/**
* Get the unique identifier for the user.
*
* @return mixed
*/
public function getAuthIdentifier()
{
return $this->getKey();
}

/**
* Get the password for the user.
*
* @return string
*/
public function getAuthPassword()
{
return $this->password;
}

/**
* Get the e-mail address where password reminders are sent.
*
* @return string
*/
public function getReminderEmail()
{
return $this->email;
}

// product count for logged user.
public function myProductsCount(){
return Product::where('user_id', '=', $this->id)->count();
}

public static function searchByEmail($email){
return User::where('email', '=', $email)->first();
}

}