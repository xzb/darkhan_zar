<?php namespace App\Models;

use Cache, DB;
 
class ZKBasketItem extends \Eloquent {
 
    protected $table = 'zk_basket_item';
    public    $timestamps = false;
    
 	public static function getItem($basket_id, $product_id, $amount, $color, $size){
		$b = ZKBasketItem::where('basket_id', '=', $basket_id)->where('product_id', '=', $product_id)->where('color_id', '=', $color)->where('size_id', '=', $size)->first();
		if(!$b){
			$zkProduct = ZKProduct::find($product_id);
			$price = $zkProduct->price;
			if($zkProduct->is_sale){$price = $zkProduct->sale_price;}
			if($color == 0){$color = $zkProduct->stock_color_id;}
			if($size == 0){$size = $zkProduct->stock_size_id;}
			$b = new ZKBasketItem;
			$b->basket_id = $basket_id;
			$b->product_id = $product_id;
			$b->amount = $amount;
			$b->color_id = $color;
			$b->size_id = $size;
			$b->price = $price;
			$b->sub_total = $price*$amount;
			$b->save();
		}
		return $b;
	}

	public static function removeItem($basket_id, $product_id, $color, $size){
		$b = ZKBasketItem::where('basket_id', '=', $basket_id)->where('product_id', '=', $product_id)->where('color_id', '=', $color)->where('size_id', '=', $size)->first();
		$sub_total = $b->sub_total;
		if($b){
			$b->delete();
		}
		return $sub_total;
	}

	public static function listItems($basket_id){
		$b = ZKBasketItem::where('basket_id', '=', $basket_id)->get();
		return $b;
	}
}