<?php namespace App\Models;
 
class Productimage extends \Eloquent {
 
    protected $table = 'image';
    public    $timestamps = false;

	public static function getPhotosByProductId($product_id) {
        return Productimage::where('product_id', '=', $product_id)->get();
    }

    public static function getLastTempUploads($product_id) {
        $upload = Productimage::where('product_id','=',$product_id)->orderBy('id','DESC')->first();
        return $upload;
    }
 }