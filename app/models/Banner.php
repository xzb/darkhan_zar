<?php namespace App\Models;
use Cache;
 
class Banner extends \Eloquent {
 
    protected $table = 'banner';
 
    public function author()
    {
        return $this->belongsTo('User');
    }

    public static function getBannerByLocation($location_id){

        if (!Cache::has('one_banner_'.$location_id)) {
            $banner = Banner::where('page_id','=',$location_id)
                             ->where('end_date','>=',date('Y-m-d',time()))
                             ->where('is_active','=',1)
                             ->orderBy('sort', 'ASC')
                             ->first();
             Cache::add('one_banner_'.$location_id, $banner, 1440);
        }
        return Cache::get('one_banner_'.$location_id);
    }

    public static function getBannersByLocation($location_id){

        if (!Cache::has('banner_'.$location_id)) {
            $banners = Banner::where('page_id','=',$location_id)
                             ->where('end_date','>=',date('Y-m-d',time()))
                             ->where('is_active','=',1)
                             ->orderBy('sort', 'ASC')
                             ->get();
             Cache::add('banner_'.$location_id, $banners, 1440);
        }
        return Cache::get('banner_'.$location_id);
    }

    public static function getCategoryArr(){
        $returnArray = array();
        $locations = Banner::where('page_id', '=', 1)
                    ->where('end_date','>=',date('Y-m-d',time()))
                    ->where('is_active','=',1)
                    ->get();
        
        foreach($locations as $l)
        {   
            $returnArray[$l->id] = $l->name;
        }
        return $returnArray;
    }

    public static function getBannerByType($parent_id){
        $returnArray = array();
        $locations = Banner::where('page_id', '=', $parent_id)->where('end_date','>=',date('Y-m-d',time()))->get();
        
        foreach($locations as $l)
        {   
            $returnArray[$l->id] = $l->name;
        }
        return $returnArray;
    }

    public static function getBannersByLocationLimit($location_id, $limit){

        if (!Cache::has('banner_'.$location_id)) {
            $banners = Banner::where('page_id','=',$location_id)
                             ->where('end_date','>=',date('Y-m-d',time()))
                             ->where('is_active','=',1)
                             ->orderBy('sort', 'ASC')
                             ->take($limit)
                             ->get();
             Cache::add('banner_'.$location_id, $banners, 1440);
        }
        return Cache::get('banner_'.$location_id);
    }

    
    public static function getCategoryBanners($category, $location_id){
        
        $ancestorsArr = array();
        $ancestors = $category->ancestorsAndSelf()->get();
        foreach($ancestors as $a)
        {
            {
                array_push($ancestorsArr, $a->id);
            }
        }

        $banners = Banner::join('category', function($join) {
            $join->on('banner.category_id', '=', 'category.id');
        })
        ->whereIn('banner.category_id', $ancestorsArr)
        ->select('banner.*')
        ->where('banner.is_active', '=', 1)
        ->where('banner.page_id','=',$location_id)
        ->orderBy('banner.sort', 'desc')->get();

        return $banners;
    }

    public static function getCategoryBanner($category, $location_id){
        
        $ancestorsArr = array();
        $ancestors = $category->ancestorsAndSelf()->get();
        foreach($ancestors as $a)
        {
            {
                array_push($ancestorsArr, $a->id);
            }
        }

        $banner = Banner::join('category', function($join) {
            $join->on('banner.category_id', '=', 'category.id');
        })
        ->whereIn('banner.category_id', $ancestorsArr)
        ->select('banner.*')
        ->where('banner.is_active', '=', 1)
        ->where('banner.page_id','=',$location_id)
        ->orderBy('banner.sort', 'desc')->first();

        return $banner;
    }
 }