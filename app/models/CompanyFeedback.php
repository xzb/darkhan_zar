<?php namespace App\Models;
use Cache;
 
class CompanyFeedback extends \Eloquent {
 
    protected $table = 'company_feedback';
    public    $timestamps = false;
    
}