<?php namespace App\Models;
use Cache, Event, DB;
 
class CategoryStats extends \Eloquent {
 
    protected $table = 'category_stats';
    public    $timestamps = false;

    public static function getListMountCount($from, $to){
    	$between  = array($from, $to);
        $category_stats = DB::table('category_stats')->select('category_stats.category_name as category_name', DB::raw('SUM(count) as count'))->whereBetween('created_at', $between)->groupBy('category_id')->paginate(25);

        return $category_stats;
    }

    public static function getList($from, $to){
    	$between  = array($from, $to);
        $category_stats = DB::table('category_stats')->select('category_stats.category_name as category_name', DB::raw('SUM(count) as count'))->whereBetween('created_at', $between)->groupBy('category_id')->get();

        return $category_stats;
    }
}