<?php namespace App\Models;
 
class Uploads extends \Eloquent {
 
    protected $table = 'uploads';
 
    public function author()
    {
        return $this->belongsTo('User');
    }

    public function page()
    {
        return $this->belongsTo('Page');
    }

    public static function getTempUploads($user_id) {
        $uploads = Uploads::where('user_id', '=', $user_id)->where('obj_id','=',0)->orderBy('created_at','DESC')->get();
        return $uploads;
    }

    public static function getLastTempUploads($user_id) {
        $upload = Uploads::where('user_id', '=', $user_id)->where('obj_id','=',0)->where('parent_id','=',0)->orderBy('created_at','DESC')->first();
        return $upload;
    }

    public static function getUploadsByObj($obj_id) {
        
        $uploads = Uploads::where('obj_id', '=',$obj_id)->where('parent_id','=',0)->orderBy('created_at','DESC')->get();
        return $uploads;
    }

    public static function getFilesWithChild($file_id) {
        $uploads = Uploads::where('id', '=', $file_id)->orWhere('parent_id','=',$file_id)->orderBy('parent_id','DESC')->get();
        return $uploads;
    }

    public static function getPhotosByPageId($page_id) {
        return Uploads::where('obj_id', '=', $page_id)->where('parent_id','=',0)->orderBy('created_at','DESC')->take(30)->get();
    }

}