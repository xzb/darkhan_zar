<?php namespace App\Models;

use DB;

class UserCategory extends \Eloquent {
 
    protected $table      = 'user_category';
    public    $timestamps = false;
    
    public static function subscribe($email, $cid, $is_daily = true) {
    // user blah blah
    $user = UserMail::getUserInstance($email);

    $userCategory = UserCategory::where('user_id', '=', $user->id)->where('category_id', '=', $cid)->first();

    if (!$userCategory) {
      // user category
      $userCategory = new UserCategory;
      $userCategory->user_id = $user->id;
      $userCategory->category_id = $cid;
    }

    $userCategory->is_daily = $is_daily;
    $userCategory->save();
  }

  public static function unsubscribe($email, $cid) {
    // user blah blah
    $user = UserMail::getUserInstance($email);
    // unsubscribe
    $userCategory = UserCategory::where('user_id', '=', $user->id)->where('category_id', '=', $cid)->first();
    // delete blah blah
    if ($userCategory) {
      $userCategory->delete();
    }
  }

  public static function listus($user_id){
    $userCategory = UserCategory::where('user_id', '=', $user_id)->get();
    return $userCategory;
  }


}