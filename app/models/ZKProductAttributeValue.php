<?php namespace App\Models;
use Cache;
 
class ZKProductAttributeValue extends \Eloquent {
 
    protected $table = 'zk_product_attribute_value';
    public    $timestamps = false;
    

    public static function getProductAttrValues($product_attribute_id)
    {
    	return ZKProductAttributeValue::where('product_attribute_id','=',$product_attribute_id)->get();
    }
    	
	public static function getAttrValue($id)
    {
    	$pav = ZKProductAttributeValue::where('product_attribute_id','=',$id)->first();
        if($pav)
    	   $attrVal = ZKAttributeValues::find($pav->attribute_value_id);
        else
           $attrVal = null; 
    	return $attrVal;
    }
}