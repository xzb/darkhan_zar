<?php namespace App\Models;

use Cache, DB;
 
class ZKStockImages extends \Eloquent {
 
    protected $table = 'zk_stock_images';
    public    $timestamps = false;

	public static function getPhotosByStockId($stock_id) {
        return ZKStockImages::where('stock_id', '=', $stock_id)->get();
    }

    public static function getLastTempUploads($user_id) {
        $upload = ZKStockImages::where('user_id', '=', $user_id)->orderBy('id','DESC')->first();
        return $upload;
    }

    public static function getTempUploadsByUser($user_id) {
        $uploads = ZKStockImages::where('user_id', '=', $user_id)->where('stock_id','=',0)->orderBy('id','DESC')->get();
        return $uploads;
    }
    
}