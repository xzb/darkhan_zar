<?php namespace App\Models;
use Cache;
 
class UserState extends \Eloquent {
 
    protected $table = 'user_state';
    public    $timestamps = false;
    
}