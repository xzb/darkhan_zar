<?php namespace App\Models;
use Cache;
 
class Group extends \Eloquent {
 
    protected $table = 'groups';
    public    $timestamps = true;
    

    public static function getGroupsArr(){
        $returnArray = array();
        $group = Group::get();
        
        foreach($group as $g)
        {   
            $returnArray[$g->id] = $g->name;
        }
        return $returnArray;
    }
}