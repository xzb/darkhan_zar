<?php namespace App\Models;
use Cache;
 
class CompanyDetail extends \Eloquent {
 
    protected $table = 'company_detail';
    public    $timestamps = false;
    
}