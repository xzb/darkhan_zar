<?php namespace App\Models;
 
class Word extends \Eloquent {
 
    protected $table = 'word';
    protected $fillable = array(
        'name',
        );
    public    $timestamps = false;
    
    public static function pushToWord($name) {
    //$word = $this->findOneBy('name', $name);
    $word = Word::where('name', '=', $name)->first();
    if (!$word) {
      $word = new Word;
      $word->name = $name;
      $word->save();
    }
    return $word;
  }

  public function save(array $options = array())
  {
    
    parent::save();
  }
}