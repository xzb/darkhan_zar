<?php namespace App\Models;
use Cache;
 
class ZKComment extends \Eloquent {
 
    protected $table = 'zk_comment';
    public    $timestamps = false;

    public static function getComment($product_id){
		$b = ZKComment::where('product_id', '=', $product_id)->orderBy('created_at','ASC')->remember(5)->get();
		return $b;
	}
}