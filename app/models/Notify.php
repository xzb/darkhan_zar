<?php namespace App\Models;
use Cache;
 
class Notify extends \Eloquent {
 
    protected $table = 'notification';
    
    public static function getNotifyForUser($user_id, $type, $is_read){
        $notify = Notify::where('object_type','=',$type)->where('is_read','=',$is_read)->where('user_id','=',$user_id)->get();
        return $notify;
    }
}