<?php namespace App\Models;
use Cache;
 
class ZKProductAttribute extends \Eloquent {
 
    protected $table = 'zk_product_attribute';
    public    $timestamps = false;
    
    public static function getAttrValues($product_id)
    {
    	return ZKProductAttribute::where('product_id','=',$product_id)->groupBy('attribute_id')->get();
    }

    public static function getProductAttrValues($product_id, $attribute_id, $type)
    {
    	if($type == 'textbox' || $type == 'textarea'){
    		$pa = ZKProductAttribute::where('product_id','=',$product_id)->where('attribute_id', '=', $attribute_id)->first();
    		if($pa){
    		return $pa->attribute_value;
    		}else{
    		return null;
    		}
    	}else{
	    	$pa = ZKProductAttribute::where('product_id','=',$product_id)->where('attribute_id', '=', $attribute_id)->first();
	    	if($pa){
	    	$value = ZKProductAttributeValue::getAttrValue($pa->id);
            if(is_null($value))
              return null;
            else
	    	  return $value->value;
            
	    	}else{
	    		return null;
	    	}
    	}
    }

    public static function getProductAttributeOne($product_id, $attribute_id){
    	return ZKProductAttribute::where('product_id', '=', $product_id)->where('attribute_id', '=', $attribute_id)->first();
    }

    public static function getProductIdsByAttrVal($id)
    {
        $returnArr = array();
        $pa = ZKProductAttribute::join('zk_product_attribute_value', function($join) {
            $join->on('zk_product_attribute.id', '=', 'zk_product_attribute_value.product_attribute_id');
        })
        ->where('zk_product_attribute_value.attribute_value_id',$id)
        ->select('zk_product_attribute.product_id')
        ->get();

        foreach($pa as $p)
        {
            array_push($returnArr, $p->product_id);
        }    
        return $returnArr;
    }

}