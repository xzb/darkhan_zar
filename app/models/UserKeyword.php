<?php namespace App\Models;

use DB;

class UserKeyword extends \Eloquent {
 
    protected $table      = 'user_keyword';
    public    $timestamps = false;
    
    public static function subscribe($email, $text, $is_daily = true) {
    // terms
    $terms = textAnalyze($text);

    // number of words
    $nb_word = count($terms);

    // blah blah
    if ($nb_word === 0) {
      return;
    }

    // words
    $words = array_keys($terms);

    // sort words
    sort($words);

    // User
    $user = UserMail::getUserInstance($email);

    //$keyword = UserKeyword::getInstance()->findOneByUserIdAndKeyword($user->id, join(' ', $words));
    $keyword = UserKeyword::where('user_id', '=', $user->id)->where('keyword', '=', join(' ', $words))->first();

    if (!$keyword) {
      // user keyword
      $keyword = new UserKeyword;
      $keyword->user_id = $user->id;
      $keyword->keyword = $text;
      $keyword->is_daily = $is_daily;
      $keyword->nb_word = $nb_word;
      $keyword->save();

      // terms
      foreach ($words as $term) {
        // blah blah
        $word = Word::pushToWord($term);
        // blah blah
        UserKeywordWord::pushToUserKeyword($keyword->id, $word->id);
      }
    } else {
      $keyword->is_daily = $is_daily;
      $keyword->save();
    }
  }

  public static function updates($id, $is_daily = true) {
    $keyword = UserKeyword::find($id);
    if (!$keyword) {
      return;
    }

    $keyword->is_daily = $is_daily;
    $keyword->save();
    
    return $keyword->keyword;
  }

  public static function unsubscribe($id) {
    $keyword = UserKeyword::find($id);
    $text = null;
    if ($keyword) {
      $text = $keyword->keyword;
      // delete
      $keyword->delete();
    }
    return $text;
  }
}