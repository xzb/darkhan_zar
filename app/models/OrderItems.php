<?php namespace App\Models;
use Cache;
 
class OrderItems extends \Eloquent {
 
    protected $table = 'order_items';
    public    $timestamps = false;
 	
 	public static function getListOfPaymentId($payment_id){
 		$q = OrderItems::where('payment_order_id', '=', $payment_id)->get();
 		return $q;
 	}   
}