<?php namespace App\Models;
use Cache;
 
class AttributeValues extends \Eloquent {
 
    protected $table = 'attribute_values';
    public    $timestamps = false;
	
	public static function getValuesByAtt($attr_id){
		$values = AttributeValues::where('attribute_id','=',$attr_id)->get();

        return $values;
	}
}