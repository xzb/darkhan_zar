<?php namespace App\Models;

use App\Models\ProductWord;
use App\Models\Category;
use App\Models\Blacklist;
use Event, DB, Cache, Notification, Redirect, Sentry, Croppa, Cookie;
 
class Product extends \Eloquent {
 
    protected $table = 'product';

    public static function getBrowseQuery($category, $column = null){
        // $query =
        // '
        //     SELECT p.id, p.name, p.image_filename, p.contact, p.price, p.created_at FROM product p 
        //     INNER JOIN 
        //         category c ON p.category_id = c.id AND (c.lft BETWEEN '.$category->lft.' AND '.$category->rgt.') 
        //     WHERE 
        //     ORDER BY p.is_featured DESC, p.created_at DESC
        // ';
        // return $query;


        $between  = array($category->lft, $category->rgt);
        $products = Product::join('category', function($join) {
            $join->on('product.category_id', '=', 'category.id');
        })
        ->whereBetween('category.lft', $between)
        ->select('product.id', 'product.name', 'product.image_filename', 'product.contact','product.description','product.price', 'product.created_at','product.is_featured')
        ->orderBy('product.is_featured', 'desc')
        ->orderBy('product.created_at', 'desc');

        return $products;
    }

    public static function getImage($product_id){
        $images = Page::where('product_id', '=', $product_id)->get();
    }

   public function save(array $options = array('from' => 'default'))
   {
        $images_count = Productimage::where('product_id', '=', $this->id)->count();

        if(!$images_count) {
            $this->image_filename = null;
        }

        //checking blacklist
        if(Blacklist::isBlacklisted($this->name, $this->description, $this->contact)) {
            Notification::error('Та зар нэмэх боломжгүй байна.');
            die('Ta zar nemeh bolomjgui baina');
            return Redirect::to('/add');
        }

        //checking spam
        if(array_key_exists('from', $options)) {
            if($options['from'] == 'front_save') {
                $count = DB::table('product')
                    ->where('created_at', '>', date('Y-m-d h:i:s', time() - 28800))
                    ->where(function($query)
                    {
                        $query->where('name', '=', sanitize($this->name))
                              ->where('contact', '=', sanitize($this->contact));
                    })->count();
                if($count) {
                    Notification::error('Та зар нэмэх боломжгүй байна.');
                    die('Ta zar nemeh bolomjgui baina');
                    return Redirect::to('/add');
                }
            }            
        }

        // before save code 
        parent::save();
        // after save code

        

        // ===============
        //      SEARCH Version 2
        // ===============
        $this->search_text = $this->name.' '.$this->description.' '.$this->contact.' '.translateText($this->name.' '.$this->description);

        // ===============
        //      SET CIDs 
        // ===============
        $ancestors_ids = Category::getAncestorsId($this->category_id);
        $this->cids    = $ancestors_ids;

        //deleting cache. category list's first page
        if($options['from'] != 'front_show') {
            $this->doDeleteCache($this->id, $this->cids);
        }
    
        parent::save();
   }

   public function delete(array $options = array())
   {
        //deleting product_words. this product's related
        if($this->id) {
          DB::table('product_word')->where('product_id', '=', $this->id)->delete();
          DB::table('user_state')->where('product_id', '=', $this->id)->delete();
          DB::table('product_attribute')->where('product_id', '=', $this->id)->delete();
        }

        //deleting cache. category list's first page
        $this->doDeleteCache($this->id, $this->cids);

        //deleting images.
        $images = Productimage::where('product_id', '=', $this->id)->get();
        foreach($images as $image) {

          if(file_exists(public_path() . '/uploads/thumb/' . $image->filename))
            @unlink(public_path() . '/uploads/thumb/' . $image->filename);

          if(file_exists(public_path() . '/uploads/thumb/s_' . $image->filename))
            @unlink(public_path() . '/uploads/thumb/s_' . $image->filename);
            
          if(file_exists('/uploads/thumb/s_' . $image->filename))
            Croppa::delete('/uploads/thumb/s_' . $image->filename);

          if(file_exists(public_path() . '/uploads/orig/' . $image->filename))
            @unlink(public_path() . '/uploads/orig/' . $image->filename);

          $image->delete();
        }

        // remove product  from session
        sfUser::deletePid($this->id);

        // before save code 
        parent::delete();
        // after save code
    }

    public static function getListDaysCount($day){
        $between  = array($day.' 00:00:00', $day.' 23:59:59');
        $products = Product::whereBetween('created_at', $between)->count();
        
        return $products;
    }

    public static function getListMountCount($from, $to){
        $between  = array($from.' 00:00:00', $to.' 23:59:59');
        $products = Product::whereBetween('created_at', $between)->count();
        
        return $products;
    }

    public static function renew($array_products)
    {
        $products = Product::whereIn('id',$array_products)->get();
        foreach($products as $product)
        {
          $product->created_at = date('Y-m-d H:i:s');
          $product->is_active = 1;
          $product->save();
        }
        return true;
    }

   public function getStockStr()
   {
        if(!$this->is_stock_str) {
            return '';
        }
        $attrs = explode("|", $this->is_stock_str);
        $str   = '';
        foreach($attrs as $attr) {
            $str .= '<span><i class="fa fa-tag"></i>'.$attr.'</span> ';
        }
        return $str;
   }

   public function doDeleteCache($id, $category_ids)
   {
        if($category_ids) {
            $ancestors_ids = explode(',', $category_ids);
            foreach($ancestors_ids as $ancestors_id)
            {
                Cache::forget('catgory_list_'.$ancestors_id);
            }
        }
        Cache::forget('product_show_'.$id);
   }

    public function getCategoryFullName() {
        $category1 = Category::find($this->category_id);
        if($category1) {
            $category2 = Category::find($category1->parent_id);
            return $category2 ? $category2->name.'/'.$category1->name : $category1->name;
        } else {
            return '';
        }
    }

    public function getLocationName() {
        $location1 = \App\Models\Location::find($this->location_id);
        if($location1) {
            $location2 = \App\Models\Location::find($location1->parent_id);
            return $location2 ? $location2->name.'/'.$location1->name : $location1->name;
        } else {
            return '';
        }
    }

    public function getUser() {
        $user =  User::find($this->user_id);
        if($user) {
            return $user;
        } else {
            return null;
        }
    }
 
 }