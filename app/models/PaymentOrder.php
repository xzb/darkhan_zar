<?php namespace App\Models;
use Cache;
 
class PaymentOrder extends \Eloquent {
 
    protected $table = 'payment_order';
    public    $timestamps = false;
    
}