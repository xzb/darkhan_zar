<?php namespace App\Models;
use Cache;
 
class CoverImageType extends \Eloquent {
 
    protected $table = 'cover_image_type';
    public    $timestamps = false;
    
}