<?php namespace App\Models;
use Cache;
 
class CompanyCategory extends \Eloquent {
 
    protected $table = 'company_category';
    public    $timestamps = false;
    
}