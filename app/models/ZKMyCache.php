<?php namespace App\Models;

use App\Models\Banner;
use App\Models\ZKProduct;
use Cache, View, Session;
 
class ZKMyCache {


    public static function homepage(){
        
        $bannerSlide = Banner::getBannersByLocationLimit(13,5);
        $bannerAds1 = Banner::getBannersByLocationLimit(11,2);
        $bannerAds2 = Banner::getBannersByLocationLimit(12,2);
        $newProducts = ZKProduct::getProductsByType('is_new',10);
        $hotProducts = ZKProduct::getProductsByType('is_hot',10);
        $saleProducts = ZKProduct::getProductsByType('is_sale',10);
        
        if (!Cache::has('zarket_view_homepage')) {
            $html = \View::make('zarket.home.cachedhome')
                        ->with('bannerSlide',$bannerSlide)
                        ->with('bannerAds1',$bannerAds1)
                        ->with('bannerAds2',$bannerAds2)
                        ->with('newProducts',$newProducts)
                        ->with('hotProducts',$hotProducts)
                        ->with('saleProducts',$saleProducts)
                        ->render();
            Cache::add('zarket_view_homepage', $html, 1440);
        }

        return Cache::get('zarket_view_homepage');
    }

}