<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//$_ENV['baseurl'] = URL::to('/');
	$_ENV['baseurl'] = 'http://www.dzar.mn';
	$_ENV['tmpurl']  = 'http://www.dzar.mn';
	View::share('base_img_url', '');
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
   $token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');
   if (Session::token() != $token) {
      throw new Illuminate\Session\TokenMismatchException;
   }
});


//
Route::filter('auth.admin', function()
{
    if ( ! Sentry::check())
    {
        return Redirect::to('/admin/login');
    } else {
    	$user   = Sentry::getUser();
    	$groups = $user->getGroups();
    	$permissions = array();
    	foreach($groups as $group) {
    		if($group->permissions)
    			$permissions[] = $group->permissions;
    	}

    	if(!sizeOf($permissions)) {
    		Sentry::logout();
    		return Redirect::to('/login');
    	}

    }

});

//
Route::filter('auth.front', function()
{
    if ( ! Sentry::check())
    {
        return Redirect::to('/login');
    }
});


Route::filter('pushstate', function($route, $request, $response = null)
{
	// if(isset($_SERVER['HTTP_X_PUSH_STATE']) == 'PushState') {
 //      echo json_encode(array(
 //          'content' => $response->getContent()
 //      ));
 //      exit();
	// }
});

// Cache
Route::filter('cache', function($route, $request, $response = null)
{
/*

	$minutes = 60;
	$url     = Request::url();
	$noti_count = Notification::count();
	$interest_count = count(sfUser::getInterest());

	if (!Sentry::check() && !$noti_count && !$interest_count && Request::isMethod('get')) {

		// Category list
		if(Active::route('category.list')) {

			preg_match_all('!\d+!', $url, $matches);

			if(isset($matches[0][0])){
				$cat_id = $matches[0][0];
				$page   = isset($_GET['page']);
				$key    = 'catgory_list_'.$cat_id;

				if(!$page && is_null($response) && Cache::has($key))
				{
				    $cache = Cache::get($key);
				    return $cache;
				}
				elseif(!is_null($response) && !Cache::has($key))
				{
				    Cache::put($key, $response->getContent(), $minutes);
				}
			}

		}
		// Homepage 
		else if(Active::route('front.homepage')) {

		    //$key = 'route-'.Str::slug($url);
		    $key = 'front.homepage';
		    $cache = Cache::get($key);

		    if(is_null($response) && $cache)
		    {
		        return Cache::get($key);
		    }
		    elseif(!is_null($response) && !$cache)
		    {
		        Cache::put($key, $response->getContent(), $minutes);
		    }

		}
	    // Product Show
		else if(Active::route('front.product.show')) {

			preg_match_all('!\d+!', $url, $matches);

			if(isset($matches[0][0])){

				$id  = $matches[0][0];
				$key = 'product_show_'.$id;
				$cache = Cache::get($key);

				if(is_null($response) && $cache)
				{
				    return $cache;
				}
				elseif(!is_null($response) && !$cache)
				{
				    Cache::put($key, $response->getContent(), 10080); //7 days
				}

			}

		}

    }

*/

});