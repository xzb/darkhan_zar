<?php namespace App\Controllers\Admin;
 
use App\Models\Category;
use App\Models\CategoryBanner;
use App\Models\Banner;
use App\Models\Page;
use App\Services\Validators\CategoryBannerValidator;
use Input, Notification, Redirect, Sentry, Str, Cache, Response;
use App\Helpers\myHelpers;
 
class CategoryBannerController extends \BaseController {
 
    public function getLeafs($root_id)
    {
        $data = array();
        $leaves = Category::find($root_id)->descendants()->limitDepth(1)->get();
        foreach($leaves as $l){
            $data[] = array("id"=>$l->id, "name"=>$l->name,"is_leaf"=>(!$l->isLeaf())?"1":"0","parent"=>$l->parent_id);
        }
        return Response::json($data);
    }

    public function index()
    {
        $banners = CategoryBanner::get();
        return \View::make('admin.categorybanner.index')->with('banners', $banners);
    }
 
    public function add($category_id)
    {
        $no_edit = true;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        $banner2 = $arr + Banner::getBannerByType(3);
        $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5);

        return \View::make('admin.categorybanner.create')->with('root_id', $category_id)->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        ->with('banner2',$banner2)
                        ->with('banner3',$banner3)
                        ->with('category_col',$category_col);
    }

    public function create()
    {
        $no_edit = true;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        $banner2 = $arr + Banner::getBannerByType(3);
        $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5);

        return \View::make('admin.category.create')->with('root_id', 0)->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        ->with('banner2',$banner2)
                        ->with('banner3',$banner3)
                        ->with('category_col',$category_col)
                        ;
    }    

    public function storeCreate()
    {
        $validation = new CategoryValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0, 0);
            return Redirect::to('admin/category');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }   
    }
 
    public function store($root_id)
    {

        $validation = new CategoryBannerValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0, $root_id);
            return Redirect::to('admin/categorybanner');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $no_edit = false;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        $banner2 = $arr + Banner::getBannerByType(3);
        $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5);
        // $categoriesCombobox = array();
        // $categoriesAll = Category::select('name','level','id')->orderBy('lft')->orderBy('level')->get();
        // foreach($categoriesAll as $c){
        //     $categoriesCombobox[$c->id] = /*str_repeat("-->",$c->level).*/$c->id.'---'.$c->name;
        // }

        $category = CategoryBanner::find($id);
        return \View::make('admin.categorybanner.edit')
                        ->with('category',$category)
                        ->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        ->with('banner2',$banner2)
                        ->with('banner3',$banner3);
    }
 
    public function update($id)
    {
        $validation = new CategoryBannerValidator;

        if ($validation->passes())
        {
            $this->insertInputs($id, Input::get('category_id'));
            return Redirect::to('admin/categorybanner');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function destroy($id)
    {
        $category = CategoryBanner::find($id);
        // for($i = 1; $i <=4; $i++)
        // {
        //     if (Cache::has('view_banner_'.$i.$category->id)) {
        //         Cache::forget('view_banner_'.$i.$category->id);
        //     }
        // }
        $category->delete();
        // Cache::forget('front.homepage');
        // Cache::forget('view_homepage');
        Notification::success('Ангилал устгагдлаа.');
        return Redirect::to('admin/categorybanner');
    }

    public function rebuildCategory(){
        $category = Category::rebuild();
        
        //Notification::success('Ангилал дахин эрэмбэлэгдлээ.');
        return Redirect::to('admin/category');
    }

    private function insertInputs($id,$root_id)  {

            if($id != 0)
            $category = CategoryBanner::find($id);
            else 
            $category = new CategoryBanner;
        
            $category->category_id = $root_id;
            $category->banner_id    = Input::get('banner_id');
            $category->banner2_id    = Input::get('banner2_id');
            $category->banner3_id    = Input::get('banner3_id');
        
            $category->save();
            Notification::success('Ангилал амжилттай хадгалагдлаа.');
            // for($i = 1; $i <=4; $i++)
            // {
            //     if (Cache::has('view_banner_'.$i.$category->id)) {
            //         Cache::forget('view_banner_'.$i.$category->id);
            //     }
            // }
            
            // Cache::forget('categories');
            // Cache::forget('categories_menu');
            // Cache::forget('front.homepage');
            // Cache::forget('view_homepage');
    }


 
}