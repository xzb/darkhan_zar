<?php namespace App\Controllers\Admin;
 
use App\Models\Product;
use App\Models\Uploads;
use App\Models\Category;
use App\Models\Productimage;
use App\Models\CategoryStats;
use App\Services\Validators\ProductEditValidator;
use Input, Notification, Redirect, Sentry, Str, Image, Cache, DB, Response, Request, Session, Excel;
 
class ProductController extends \BaseController {
 
    public function index()
    {
        $urlArray = array();
        $roots = Category::roots()->get();
        $cArray = array(array('id'=>0,'name'=>'--Ангилал--'));
        
        foreach($roots as $id=>$r){

            $cArray[$id+1]['id']= $r->id;
            $cArray[$id+1]['name']= $r->name;
            $cArray[$id+1]['leaf']= (!$r->isLeaf())?true:false;
        }
        
        
        $query = Product::orderBy('created_at', 'DESC');
        
        $category_id = Input::get('category_id',0);
        $categories = array();
        if($category_id != 0)
        {
            $is_leaf = Category::find($category_id)->isLeaf();
            
            if(!$is_leaf)
            {
                $cLeaves = Category::find($category_id)->leaves()->get();

                foreach($cLeaves as $c)
                {
                    array_push($categories,$c->id);
                }
            }
            else
            {
                array_push($categories, $category_id);
            }    
        }    
        
        $title        = Input::get('title','');
        $product_id   = Input::get('product_id','');
        $is_featured  = Input::get('is_featured',3);
        $from         = Input::get('from','');
        $to           = Input::get('to','');

        if($product_id != '')
        {
             $query = $query->where('id','=', $product_id);
             $urlArray['product_id'] = $product_id;
        }
        if($title != '')
        {
             $query = $query->where('name','like','%'.$title.'%');
             $urlArray['title'] = $title;
        }

        if($category_id != 0)
        {
             $query = $query->whereIn('category_id',$categories);
             $urlArray['category_id'] = $category_id;
        }
        
        if($is_featured != 3)
        {
             $query = $query->where('is_featured','=',$is_featured);
             $urlArray['is_featured'] = $is_featured;
        }
        
        if($from != '')
        {
           $date = date("Y-m-d",strtotime($from));
           $query = $query->where('created_at','>=',$date);
           $urlArray['from'] = $from;
        }
        if($to != '')
        {
            $date = date("Y-m-d",strtotime($to));
            $query = $query->where('created_at','<=',$date);
            $urlArray['to'] = $to;
        }

        $pages = $query->paginate(20);


        return \View::make('admin.product.index')
                    ->with('pages', $pages)
                    ->with('cArray',$cArray)
                    ->with('url',$urlArray)
                    ->with('product_id',$product_id)
                    ->with('title',$title)
                    ->with('category_id',$category_id)
                    ->with('is_featured',$is_featured)
                    ->with('from',$from)
                    ->with('to',$to)
                    ;
    }
 
    public function show($id)
    {
        $news = Page::find($id);
        $newsmore = PageMore::where('page_id','=',$id)->orderBy('created_at', 'DESC')->get();
        return \View::make('admin.news.show')->with('news', $news)->with('newsmore', $newsmore);
    }
 
    public function create()
    {
        $roots = Category::roots()->get();
        $cArray = array(array('id'=>0,'name'=>'--Ангилал--'));
        
        foreach($roots as $id=>$r){

            $cArray[$id+1]['id']= $r->id;
            $cArray[$id+1]['name']= $r->name;
            $cArray[$id+1]['leaf']= (!$r->isLeaf())?true:false;
        }
        $product = null;
        
        return \View::make('admin.product.create')->with('product', $product)->with('combobox',$cArray);
    }
 
    public function store()
    {
        $validation = new PageValidator;

        if ($validation->passes())
        {
            $this->insertInputs(0);
            return Redirect::to('admin/productPager');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $roots = Category::roots()->get();
        $cArray = array(array('id'=>0,'name'=>'--Ангилал--'));
        
        foreach($roots as $cid=>$r){

            $cArray[$cid+1]['id']= $r->id;
            $cArray[$cid+1]['name']= $r->name;
            $cArray[$cid+1]['leaf']= (!$r->isLeaf())?true:false;
        }

        $product = Product::find($id);
        $category = Category::find($product->category_id);
        $parents = array();
        $parentcats = $category->getAncestors();
        $parentcats[]=$category;
        foreach ($parentcats as $key => $value) {
            $parents[] = $value->id;
        }
        $parents[] = $product->category_id;
        return \View::make('admin.product.edit')->with('product',$product)->with('cArray',$cArray)->with('parents',$parents)->with('parentcats',$parentcats);
    }
 
    public function update($id)
    {
        $validation = new ProductEditValidator;
 
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/productPager');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        Notification::success('Зар устгагдлаа.');
        
        return Redirect::to('admin/productPager');
    }

    private function insertInputs($id)  {
            if($id != 0) {
                $product  = Product::find($id);
            }
            else {
                $product = new Product;
                $product->save();
            }

            $product->name           = Input::get('name');
            $product->category_id     = Input::get('category_id');

            $product->description     = Input::get('description');
            $product->is_featured     = Input::get('is_featured',0);
            $product->price           = Input::get('price',0);
            $product->contact     = Input::get('contact','');
            $product->latlng         = sanitize(Input::get('latlng'));
            
            
            $product->save();

            Notification::success('Зар амжилттай засагдлаа');
    }

    public function upload(){
        $id = Input::get('product_id');
        $product = Product::find($id);

        $file = Input::file('file');

        $ext = $file->getClientOriginalExtension();
        $mimetype = $file->getMimeType();
        
        $origName = md5(time()).'.'.$ext;

        $product_image = new Productimage();
        $product_image->filename = $origName;
        $product_image->hashcode = md5_file($file);
        $product_image->product_id = $product->id;
        $product_image->save();

        //create orig image
        $image = Image::make($file);
        $image->resize(800, 600, function ($constraint) {
                   $constraint->aspectRatio();
                   $constraint->upsize();
              });
        $image->insert(public_path().'/images/watermark.png', 'bottom-right');

        //thumbnail
        $thumb = Image::make($file);
        $thumb->fit(313, 214);
        $thumb->save(public_path().'/uploads/thumb/'.$origName);

        $firstimage = Productimage::where('product_id',$product->id)->orderBy('id','ASC')->first();
        if($firstimage) {
            $product->image_filename = $firstimage->filename;
            $product->save();            
        }
         
        return $image->save(public_path().'/uploads/orig/'.$origName);
    }

    public function loadPicAfterUpload($product_id){
        $result  = array();
        $file = Productimage::getLastTempUploads($product_id);
        
        
        $obj['name'] = $file->filename;
        $obj['size'] = $file->size;
        $obj['id'] = $file->id;
        $result[] = $obj;
        
        return Response::json($result);
    }

    public function removeUpload($id){
        $f = Productimage::find($id);
    
        if(file_exists (public_path().'/uploads/orig/'.$f->filename))
        {
            unlink(public_path().'/uploads/orig/'.$f->filename);
        }
        if(file_exists (public_path().'/uploads/thumb/'.$f->filename))
        {
            unlink(public_path().'/uploads/thumb/'.$f->filename);
        }

        $product_id = $f->product_id;

        $f->delete();

        $product = Product::find($product_id);
        $firstimage = Productimage::where('product_id',$product_id)->orderBy('id','ASC')->first();
        if($firstimage) {
            $product->image_filename = $firstimage->filename;
            $product->save();            
        }
        unset($product_id);

    }

    public function loadUploads($obj_id){
        $result  = array();
        $files = Productimage::getPhotosByProductId($obj_id);

        foreach ( $files as $file ) {
                $obj['name'] = $file->filename;
                $obj['size'] = $file->size;
                $obj['id'] = $file->id;
                $result[] = $obj;
        }
    
         
        return Response::json($result);
    }

    public function drawCategory($cids)
    {
        function drawCat($category_id){

            for($i = 0; $i <= count($cids); $i++){

            }

            $category = Category::find($category_id);
            $is_leaf = $category->isLeaf();

            $html = '<div id="categories'.$category->parent_id.'" class="form-group" style="padding-left:16px">
                            <select class="form-control" name="category'.$category->parent_id.'" id="category'.$category->parent_id.'" style="width:150px" onchange="drawCategoryCombo($(this).val(),'.$category->parent_id.','.$is_leaf.')">';
                          $leaves = Category::find($category->parent_id)->descendants()->limitDepth(1)->get();
                          
                             foreach($leaves as $i)
                             {
                                $html .= "<option value=".$i->id.">".$i->name."</option>";   
                             }
                             
            $html .= '</select>';

            
            
            $html .=' </div>';

            return $html;
        }
    }

    public function setfeatured($id)
    {
        $product = Product::find($id);
        
        if (Request::isMethod('post')){
            
            $is_featured = $product->is_featured;
            $set = 0;
            $html = 'btn-error';
            $start_date = Input::get('featured_start_date') ? Input::get('featured_start_date') : date("Y-m-d", time());
            $due_date = Input::get('featured_due_date') ? Input::get('featured_due_date') : date("Y-m-d", time() + 604800);
            
            $product->is_featured = 1;
            $product->is_active = 1;
            $product->featured_start_date = $start_date;
            $product->featured_due_date = $due_date;
            // $product->video = Input::get('video');
            $product->save();    
            return Redirect::to('/admin/productPager');
        }else{
            $is_featured = $product->is_featured;
            if($is_featured == 1){
                $html = 'btn-error';
                $product->is_featured = 0;
                $product->save();
                return "Амжилттай хадгаллаа.";
            }
        }
        return \View::make('admin.product.popup')->with('product', $product);
    }

    public function setAutoRenew($id)
    {
        $product = Product::find($id);
        
        if (Request::isMethod('post')){
            
            $is_autorenew = $product->auto_renew_time;
            $set = 0;
            $html = 'btn-error';
            $product->auto_renew_time = Input::get('renew_time');
            $product->created_at = date("Y-m-d", time());
            $product->is_active = 1;
            $product->save();    

            return Redirect::to('/admin/productPager');
        }else{
            $is_autorenew = $product->auto_renew_time;
            if($is_autorenew != 0){
                $html = 'btn-error';
                $product->auto_renew_time = 0;
                $product->save();
                return "Амжилттай хадгаллаа.";
            }
        }
        return \View::make('admin.product.autorenew')->with('product', $product);
    }

    public function setCoupon($id)
    {
        $product = Product::find($id);
        
        if (Request::isMethod('post')){
            $html = 'btn-error';
        
            $product->is_coupon = 1;
            $product->coupon_price = sanitize(Input::get('price'));
            $product->coupon_percent = sanitize(Input::get('discount'));
            
            
            $product->is_active = 1;
            $product->save();    
            
            return Redirect::to('/admin/productPager');
        }else{
            if($product->is_coupon == 1){
                $html = 'btn-error';
                $product->is_coupon = 0;
                $product->save();
                return 'is not coupon';
            }
            // else 
            // {
            //     $product->is_coupon = 1;
            //     $product->save();
            //     return 'success';
            // }

        }
        return \View::make('admin.product.coupon')->with('product', $product);
    }

    public function categoryStats()
    {
        $from = Input::get('from');
        $date = '';
        if($from != '')
        {
           $date = date("Y-m-d",strtotime($from));
        }
        if(!$date){
            $date = Session::get('stateFrom');
        }
        if(!$date) $date = date('Y-m-d', time());
        $from = date('m/d/Y', strtotime($date));
        Session::put('stateFrom', $date);

        $to = Input::get('to');
        $dateto = '';
        if($to != '')
        {
           $dateto = date("Y-m-d",strtotime($to));
        }
        if(!$dateto){
            $dateto = Session::get('stateTo');
        }
        if(!$dateto) $dateto = date('Y-m-d', time());
        $to = date('m/d/Y', strtotime($dateto));
        Session::put('stateTo', $dateto);

        //$category_stats = CategoryStats::orderBy('count','desc')->where('created_at', '=', $date)->paginate(25);
        $between  = array($from, $to);
        $category_stats = CategoryStats::getListMountCount($date, $dateto);
        // DB::table('category_states')->select('category_states.category_name',DB::raw('SUM(count) as results'))->whereBetween('created_at', $between)->paginate(25);
        if(Input::get('excel') == 1){

            Excel::create('Dashboard', function($excel) use($date, $dateto){

            // Set the title
            $excel->setTitle('Тайлан');

            // Chain the setters
            $excel->setCreator('Myagmar-Ochir')
                  ->setCompany('Singleton LLC');

            // Call them separately
            $excel->setDescription('lol');

            $excel->sheet('Sheet1', function($sheet) use($date, $dateto){
                $sheet->row(1, array(
                     $date.'-ээс '.$dateto.' хүртэл категорийн хандалт' 
                ));

                $sheet->row(2, array(
                     'Категори нэр', 'хандсан тоо'
                ));
                $category_stats = CategoryStats::getList($date, $dateto);
                
                $counter = 3;
                foreach($category_stats as $state){
                    $sheet->row($counter, array(
                         $state->category_name, $state->count
                    ));
                    $counter++;
                }

        });

    })->export('xlsx');

        }

        return \View::make('admin.product.categorystats')->with('category_stats', $category_stats)->with('from', $from)->with('to', $to);   
    }
 
}