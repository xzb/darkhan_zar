<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	
	public function __construct()
	{
		$user = Sentry::getUser();
		View::share('logged_user', $user);
		if($user)
		{
			$groups = $user->getGroups();
			View::share('logged_group', $groups);	
		}
		
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}