<?php namespace App\Controllers\Admin;

use App\Models\Blacklist; 
use App\Services\Validators\BlacklistValidator;
use Input, Notification, Redirect, Sentry, Str, Image;
 
class BlacklistController extends \BaseController {
    
    public function index()
    {
        $blacklists = Blacklist::all();
        return \View::make('admin.blacklist.index')->with('blacklists', $blacklists);
    }
 
    public function create()
    {
        $blacklist = null;
        return \View::make('admin.blacklist.create')->with('blacklist', $blacklist);
    }
 
    public function store()
    {
        die('store');
        $validation = new BlacklistValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0);
            return Redirect::to('admin/blacklist');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $blacklist = Blacklist::find($id);
        return \View::make('admin.blacklist.edit')->with('blacklist', $blacklist);
        
    }
 
    public function update($id)
    {
        $validation = new BlacklistValidator;
 
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/blacklist');            
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
    

    public function destroy($id)
    {
        $blacklist = Blacklist::find($id);
        $blacklist->delete();
        Notification::success('Устгагдлаа.');
        return Redirect::to('admin/blacklist');
    }


    private function insertInputs($id)  {
        if($id != 0)
        $blacklist = Blacklist::find($id);
        else 
        $blacklist = new Blacklist;

        
        $blacklist->name       = mb_strtolower(Input::get('name'));
        $blacklist->type       = Input::get('type');
        $blacklist->save();
        Notification::success('Амжилттай хадгалагдлаа.');
    }


 
}