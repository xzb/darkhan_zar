<?php namespace App\Controllers\Admin;
 
use App\Models\Attribute;
use App\Models\Category;
use App\Models\AttributeValues;
use App\Services\Validators\AttributeValidator;
use Input, Notification, Redirect, Sentry, Str, DB, Config;
 
class AttributeController extends \BaseController {
  public function index()
  {
      $category_id = Input::get('category_id', 0);
      $attributes = null;
      if ($category_id)
      {
        $attributes = DB::table('attribute')
            ->join('category_attribute', 'attribute.id', '=', 'category_attribute.attribute_id')
            ->select('attribute.*')
            ->where('category_attribute.category_id', '=', $category_id)
            ->orderBy('name', 'DESC')
            ->get();
      }
      else
      {
        $attributes = Attribute::orderBy('name', 'DESC')->get();
      }
      $categorys = Category::orderBy('lft')->get();
      
      return \View::make('admin.attribute.index')->with('attributes', $attributes)->with('categorys', $categorys)->with('category_id', $category_id);
  }
  
  public function create()
  {
      $attribute = null;
      $selectBox = Config::get('app.attr_types');
      return \View::make('admin.attribute.create')->with('attribute', $attribute)->with('combobox',$selectBox);
  }

  public function store()
  {
      $validation = new AttributeValidator;

      if ($validation->passes('default'))
      {
          $this->insertInputs(0);
      }

      return Redirect::back()->withInput()->withErrors($validation->errors);
  }

  public function edit($id)
  {
      $attribute = Attribute::find($id);
      $selectBox = Config::get('app.attr_types');
      // $category1s = Category::getParentCategoryOptions(21);
      // $category2s = Category::getParentCategoryOptions(151);
      $ids = array(20, 21, 151);
      $ids2 = array(29, 108, 28, 36);
      // $category0s = array('20'=>'Зарна');
      // $category1s = array('21'=>'Суудлын машин');
      // $category2s = array('151'=>'Жиип');
      // $category3s = array_merge( $category0s, $category1s );
      // $categorys = array_merge($category3s, $category2s);
      // $categorys = Category::whereIn('id', $ids)->get();
      // $categorys2 = Category::whereIn('id', $ids2)->get();
      $categorys = Category::select('*')->orderBy('lft')->orderBy('level')->get();
      $categorys2 = array();
      $category_attributes = DB::table('category_attribute')
            ->join('category', 'category_attribute.category_id', '=', 'category.id')
            //->select('category.name AS category_name')
            ->where('category_attribute.attribute_id', '=', $id)
            ->get();
      //$category_attributes = CategoryAttribute::where('attribute_id', '=', $id)->get();
      $attribute_list = null;
      if($attribute->type != 'textarea' && $attribute->type != 'textbox'){
        $attribute_list = AttributeValues::where('attribute_id', '=', $id)->orderBy('sort_order', 'ASC')->get();
      }
      return \View::make('admin.attribute.edit')->with('attribute',$attribute)->with('combobox',$selectBox)->with('categorys',$categorys)->with('categorys2',$categorys2)->with('category_attributes',$category_attributes)->with('attribute_list',$attribute_list);
  }

  public function update($id)
  {
      $validation = new AttributeValidator;

      if ($validation->passes('update'))
      {
          return $this->insertInputs($id);
      }

      return Redirect::back()->withInput()->withErrors($validation->errors);
  }

  public function destroy($id)
  {
      $Attribute = Attribute::find($id);
      $Attribute->delete();

      Notification::success('Амжилттай устгагдлаа.');

      return Redirect::to('admin/attribute');
  }

  private function insertInputs($id)  {
          if($id != 0)
          $Attribute = Attribute::find($id);
          else 
          $Attribute = new Attribute;  

          $Attribute->name       = Input::get('name');
          $Attribute->type       = Input::get('type');
          $is_main = Input::get('is_main',0);
          $is_required = 0;
          if($is_main){
            $is_required = 1;
          }
          $Attribute->is_main         = Input::get('is_main',0);
          $Attribute->is_column       = Input::get('is_column',0);
          $Attribute->is_filterable    = Input::get('is_filterable',0);
          $Attribute->sort_order    = Input::get('sort_order',0);
          $Attribute->is_required    = $is_required;
          $Attribute->is_stock       = Input::get('is_stock',0);
          $Attribute->for_title       = Input::get('for_title',0);
          // $Attribute->for_url       = Input::get('for_url',0);

          $Attribute->save();
          Notification::success('Амжилттай хадгалагдлаа.');    
          return Redirect::to('admin/attribute/'.$Attribute->id.'/edit');
  }



  
  
}



