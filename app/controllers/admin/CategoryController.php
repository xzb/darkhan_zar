<?php namespace App\Controllers\Admin;
 
use App\Models\Category;
use App\Models\Cat;
use App\Models\Banner;
use App\Models\Page;
use App\Services\Validators\CategoryValidator;
use Input, Notification, Redirect, Sentry, Str, Cache, Response;
use App\Helpers\myHelpers;
 
class CategoryController extends \BaseController {
 
    public function getLeafs($root_id)
    {
        $data = array();
        $leaves = Category::find($root_id)->descendants()->limitDepth(1)->get();
        foreach($leaves as $l){
            $data[] = array("id"=>$l->id, "name"=>$l->name,"is_leaf"=>(!$l->isLeaf())?"1":"0","parent"=>$l->parent_id);
        }
        return Response::json($data);
    }

    public function index()
    {
        $categories = Category::roots()->orderBy('rank')->get();
        return \View::make('admin.category.index')->with('categories', $categories);
    }
 
    public function add($root_id)
    {
        $no_edit = true;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        // $banner2 = $arr + Banner::getBannerByType(3);
        // $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5,6,7);

        return \View::make('admin.category.create')->with('root_id', $root_id)->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        // ->with('banner2',$banner2)
                        // ->with('banner3',$banner3)
                        ->with('category_col',$category_col)
                        ;
    }

    public function create()
    {
        $no_edit = true;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        // $banner2 = $arr + Banner::getBannerByType(3);
        // $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5,6,7);

        return \View::make('admin.category.create')->with('root_id', 0)->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        // ->with('banner2',$banner2)
                        // ->with('banner3',$banner3)
                        ->with('category_col',$category_col)
                        ;
    }    

    public function storeCreate()
    {
        $validation = new CategoryValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0, 0);
            return Redirect::to('admin/category');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }   
    }
 
    public function store($root_id)
    {
        $validation = new CategoryValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0, $root_id);
            return Redirect::to('admin/category');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $no_edit = false;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Banner::getCategoryArr();
        // $banner2 = $arr + Banner::getBannerByType(3);
        // $banner3 = $arr + Banner::getBannerByType(4);
        $category_col = array(1,2,3,4,5,6,7);
        $categoriesCombobox = array();
        $categoriesAll = Category::select('name','level','id')->orderBy('lft')->orderBy('level')->get();
        foreach($categoriesAll as $c){
            $categoriesCombobox[$c->id] = /*str_repeat("-->",$c->level).*/$c->id.'---'.$c->name;
        }

        $category = Category::find($id);
        return \View::make('admin.category.edit')
                        ->with('category',$category)
                        ->with('combobox',$selectBox)
                        ->with('no_edit',$no_edit)
                        // ->with('banner2',$banner2)
                        // ->with('banner3',$banner3)
                        ->with('category_col',$category_col)
                        ->with('categoriesCombobox',$arr + $categoriesCombobox)
                        ;
    }
 
    public function update($id)
    {
        $validation = new CategoryValidator;

        if ($validation->passes())
        {
            $this->insertInputs($id,0);
            return Redirect::to('admin/category');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function destroy($id)
    {
        $category = Category::find($id);
        for($i = 1; $i <=4; $i++)
        {
            if (Cache::has('view_banner_'.$i.$category->id)) {
                Cache::forget('view_banner_'.$i.$category->id);
            }
        }
        $category->delete();
        Cache::forget('front.homepage');
        Cache::forget('view_homepage');
        Notification::success('Ангилал устгагдлаа.');
        return Redirect::to('admin/category');
    }

    public function rebuildCategory(){
        $category = Category::rebuild();
        
        //Notification::success('Ангилал дахин эрэмбэлэгдлээ.');
        return Redirect::to('admin/category');
    }

    private function insertInputs($id,$root_id)  {

            if($id != 0)
            $category = Category::find($id);
            else 
            $category = new Category;
        
            $category->name    = Input::get('name');
            $category->seo_title    = Input::get('seo_title');
            $category->seo_url      = Input::get('seo_url');
            $category->seo_description    = Input::get('seo_description');
            $category->rank    = Input::get('rank');
            $category->banner_id    = Input::get('banner_id');
            
            if($root_id != 0)
            {
                $category->parent_id = $root_id;
            }
            elseif(Input::get('parent_id') != 0 && Input::get('parent_id') != $category->parent_id)
            {
                $category->parent_id = Input::get('parent_id');
            }

            $category->root_css    = Input::get('root_css');
            $category->is_visible    = intval(Input::get('is_visible'));
            $category->homepage_visible    = intval(Input::get('homepage_visible'));
            // $category->hompage_col_no    = Input::get('homepage_col_no');
            $category->link    = Input::get('link');
            // $category->banner2_id    = Input::get('banner2_id');
            // $category->banner3_id    = Input::get('banner3_id');
            // $category->root_color    = Input::get('root_color');
            $category->for_title    = Input::get('for_title',0);


            $category->save();
            // Notification::success('Ангилал амжилттай хадгалагдлаа.');
            for($i = 1; $i <=4; $i++)
            {
                if (Cache::has('view_banner_'.$i.$category->id)) {
                    Cache::forget('view_banner_'.$i.$category->id);
                }
            }
            
            Cache::forget('categories');
            Cache::forget('categories_menu');
            Cache::forget('front.homepage');
            Cache::forget('view_homepage');
    }


 
}