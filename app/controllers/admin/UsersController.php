<?php namespace App\Controllers\Admin;
 
use App\Models\User;
use App\Models\Group;
use App\Models\UsersGroup;
use App\Models\Search;
use App\Services\Validators\UsersValidator;
use App\Services\Validators\UsersEditValidator;
use Input, Notification, Redirect, Sentry, Str, Image, DB;
use App\Helpers\myHelpers;
 
class UsersController extends \BaseController {
    
    public function index()
    {
        $me = Sentry::getUser();
        $group = $me->getGroups();
        if($group[0]['name']== 'Admin')
        {
            $users = User::paginate(20);
        }
        else
        {

            $users = User::join('users_groups', function($join) {
                            $join->on('users.id', '=', 'users_groups.user_id');
                          })
                    ->where('users_groups.group_id', '<>', 8)->orderBy('users.created_at','DESC')->paginate(20);
        }
        
        return \View::make('admin.users.index')->with('users', $users);
    }
 
    public function create()
    {
        $me = Sentry::getUser();
        $group = $me->getGroups();
        if($group[0]['name']== 'Admin')
        {
            $user = null;
            $groups = Group::getGroupsArr();
            return \View::make('admin.users.create')->with('user', $user)->with('groups', $groups)->with('users_group', null);
        }
        else
        {
            Notification::success('Танд энэ үйлдлийг хийх эрхгүй байна');
            return Redirect::to('admin/users');
        }
    }
 
    public function store()
    {
        $validation = new UsersValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0);
            return Redirect::to('admin/users');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $me = Sentry::getUser();
        $group = $me->getGroups();
        if($group[0]['name']== 'Admin' || $me->getId() == $id)
        {
            $user = User::find($id);

            $groups = Group::getGroupsArr();
            $users_group = UsersGroup::where('user_id','=',$id)->first();
            return \View::make('admin.users.edit')->with('user',$user)->with('groups', $groups)->with('users_group', $users_group);
        }
        else
        {
            Notification::success('Танд энэ үйлдлийг хийх эрхгүй байна');
            return Redirect::to('admin/users');
        }
    }
 
    public function update($id)
    {
        $validation = new UsersEditValidator;
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/users');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }    
    }
 
    public function destroy($id)
    {
        try
        {
            $me = Sentry::getUser();
            $group = $me->getGroups();
            if($group[0]['name']== 'Admin')
            {
                $user = Sentry::findUserById($id);
                $user->activated = 0;
                $user->save();
            }   
            else
            {
                Notification::success('Танд энэ үйлдлийг хийх эрхгүй байна');
                return Redirect::to('admin/users');
            }
            
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            echo 'User was not found.';
        }
 
        Notification::success('Админ хэрэглэгч идэвхгүй боллоо.');
 
        return Redirect::to('admin/users');
    }

    
    private function insertInputs($id)  {
            if($id != 0)
            {
                // sentry user update
                try
                {
                    // Find the user using the user id
                    $user = Sentry::findUserById($id);
                
                    $user->email        = Input::get('email');
                    $user->last_name    = Input::get('last_name');
                    $user->first_name   = Input::get('first_name');
                    $user->name         = substr(Input::get('last_name'), 0, 1).'. '.Input::get('first_name');
                    
                    if(Input::get('password'))
                    $user->password     = Input::get('password');
                    
                    $user->activated    = Input::get('activated',0);

                    if (Input::hasFile('image'))
                    {
                        $file = Input::file('image');
                        $destinationPath = 'files/uploads/users';
                        $filename = $file->getClientOriginalName();
                        $extension =$file->getClientOriginalExtension(); //if you need extension of the file
                        if(!in_array($extension,\myHelpers::$imageExt))
                        {
                          Notification::error('Та зөвхөн зураг оруулна уу.');
                          return Redirect::to('admin/users/create');
                        }
                        else
                        {
                          if($user->image) {
                             @unlink($destinationPath.'/'.$user->image);
                             $thumb = pathinfo($destinationPath.'/'.$user->image);
                             @unlink($destinationPath.'/'.$thumb['filename'].'_s.'.$thumb['extension']);
                          }
                          $image = $this->upload(Input::file('image'));
                          $user->image = $image;
                        }
                    }
                    
                    // Update the user
                    if ($user->save())
                    {
                        DB::table('users_groups')->where('user_id', $id)->delete();
                        
                        $adminGroup = Sentry::findGroupById(Input::get('user_type'));
                        $user->addGroup($adminGroup);
                        // User information was updated
                    }
                    else
                    {
                        // User information was not updated
                    }
                }
                catch (Cartalyst\Sentry\Users\UserExistsException $e)
                {
                    echo 'User with this login already exists.';
                }
                catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
                {
                    echo 'User was not found.';
                }

            }
            else 
            {
                try
                {

                    // Create the user
                    $user = Sentry::createUser(array(
                        'email'      => Input::get('email'),
                        'password'   => Input::get('password'),
                        'first_name' => Input::get('first_name'),
                        'last_name'  => Input::get('last_name'),
                        'name'       => substr(Input::get('last_name'), 0, 1).'. '.Input::get('first_name'),
                        'activated'  => Input::get('activated',1),
                    ));


                    if (Input::hasFile('image'))
                    {
                        $file = Input::file('image');
                        $destinationPath = 'files/uploads/users';
                        $filename = $file->getClientOriginalName();
                        $extension =$file->getClientOriginalExtension(); //if you need extension of the file
                        if(!in_array($extension,\myHelpers::$imageExt))
                        {
                          Notification::error('Та зөвхөн зураг оруулна уу.');
                          return Redirect::to('admin/users/create');
                        }
                        else
                        {
                          $image = $this->upload(Input::file('image'));
                          $user->image = $image;
                          $user->save();
                        }
                    }

                    $user->save();
                    // Find the group using the group id
                    $adminGroup = Sentry::findGroupById(Input::get('user_type'));

                    // Assign the group to the user
                    $user->addGroup($adminGroup);
                }
                catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
                {
                    echo 'Login field is required.';
                }
                catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
                {
                    echo 'Password field is required.';
                }
                catch (Cartalyst\Sentry\Users\UserExistsException $e)
                {
                    echo 'User with this login already exists.';
                }
                catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
                {
                    echo 'Group was not found.';
                }
            }
            

            Notification::success('Хэрэглэгч амжилттай хадгалагдлаа.');    
            
    }

    private function upload($file){

        $ext = $file->getClientOriginalExtension();
        $path = 'files/uploads/users';
        $mimetype = $file->getMimeType();
        $name     = time();
        
        //create orig image
        $image = Image::make($file->getRealPath());
        $image->resize(null, 400, function ($constraint) {
                     $constraint->aspectRatio();
                     $constraint->upsize();
                });        
        $origName = $name.'.'.$ext;
        $image->save($path.'/'.$origName);

        //thumbnail
        $thumb = Image::make($file->getRealPath());
        $thumb->resize(null, 100, function ($constraint) {
                     $constraint->aspectRatio();
                     $constraint->upsize();
                });
        $thumbName = $name.'_s.'.$ext;
        $thumb->save($path.'/'.$thumbName);

        return $origName;
    }    

    public function searchKeyword(){
        $keyword = Input::get('keyword');
        if($keyword){
            $searchs = Search::where('keyword','LIKE', '%'.$keyword.'%')->paginate(20);
        }else{
            $searchs = Search::paginate(20);
        }
        
        return \View::make('admin.users.searchkeyword')->with('searchs',$searchs)->with('keyword',$keyword);
    }


 
}