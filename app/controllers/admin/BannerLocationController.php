<?php namespace App\Controllers\Admin;
 
use App\Models\BannerLocation;
use App\Services\Validators\BannerLocationValidator;
use Input, Notification, Redirect, Sentry, Str, Cache;
use App\Helpers\myHelpers;
 
class BannerLocationController extends \BaseController {
 
    public function index()
    {
        $bannerlocs = BannerLocation::take(50)->get();
        return \View::make('admin.bannerloc.index')->with('bannerlocs', $bannerlocs);
    }
 
    public function create()
    {
        $bannerloc = null;
        return \View::make('admin.bannerloc.create')->with('bannerloc', $bannerloc);
    }
 
    public function store()
    {
        $validation = new BannerLocationValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/bannerloc');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $bannerloc = BannerLocation::find($id);
        return \View::make('admin.bannerloc.edit')->with('bannerloc',$bannerloc);
    }
 
    public function update($id)
    {
        $validation = new BannerLocationValidator;

        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/bannerloc');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function destroy($id)
    {
        $bannerloc = BannerLocation::find($id);
        $bannerloc->delete();
 
        Notification::success('Баннер төрөл устгагдлаа.');
 
        return Redirect::to('admin/bannerloc');
    }

    
    private function insertInputs($id)  {
            if($id != 0)
            $bannerloc = BannerLocation::find($id);
            else 
            $bannerloc = new BannerLocation;  
            
            $bannerloc->location     = Input::get('location');
            $bannerloc->size         = Input::get('size');
            $bannerloc->price        = Input::get('price');
            $bannerloc->file_type    = Input::get('file_type');
            $bannerloc->changes      = Input::get('changes');
            $bannerloc->save();
            Notification::success('Баннер төрөл амжилттай хадгалагдлаа.');

            // Deleting cache
            Cache::forget('banner_'.Input::get('location'));
    }


 
}