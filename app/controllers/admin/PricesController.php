<?php namespace App\Controllers\Admin;
 
use App\Models\Prices;
use App\Models\Category;
use App\Services\Validators\PricesValidator;
use Input, Notification, Redirect, Sentry, Str, Cache;
use App\Helpers\myHelpers;
 
class PricesController extends \BaseController {
 
    public function index()
    {
        $prices = Prices::take(50)->get();
        return \View::make('admin.prices.index')->with('pricess', $prices);
    }
 
    public function edit($id)
    {
        $prices = Prices::find($id);
        $category = Category::find($prices->category_id);
        return \View::make('admin.prices.edit')->with('prices',$prices)->with('category',$category);
    }
 
    public function update($id)
    {
        $validation = new PricesValidator;

        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/prices');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }

    
    private function insertInputs($id)  {
            if($id != 0)
            $prices = Prices::find($id);
            
            $prices->price        = Input::get('price');
            $prices->save();
            Notification::success('Амжилттай хадгалагдлаа.');
    }


 
}