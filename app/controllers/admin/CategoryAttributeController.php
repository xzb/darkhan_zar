<?php namespace App\Controllers\Admin;
 
use App\Models\Attribute;
use App\Models\Category;
use App\Models\CategoryAttribute;
use App\Services\Validators\AttributeValidator;
use Input, Notification, Redirect, Sentry, Str, DB, Config;
 
class CategoryAttributeController extends \BaseController {
  
  public function create()
  {
      $attribute_id = Input::get('attribute_id');
      $category_id = Input::get('category_id');
      $category = Category::find($category_id);
      $attribute = Attribute::find($attribute_id);
      $categories = Category::orderBy('rank', 'ASC')
            ->where('lft', '>', $category->lft)
            ->where('rgt', '<', $category->rgt)
            ->get();
      if($category && $attribute){
        $cateAttr = CategoryAttribute::where('category_id', '=', $category_id)->where('attribute_id', '=', $attribute_id)->first();
        if(!$cateAttr){
          $cateAttr = new CategoryAttribute;
          $cateAttr->category_id = $category_id;
          $cateAttr->attribute_id = $attribute_id;
          $cateAttr->save();
        }
        foreach ($categories as $child) {
          $cateAttrchild = CategoryAttribute::where('category_id', '=', $child->id)->where('attribute_id', '=', $attribute_id)->first();
            if(!$cateAttrchild){
            $cateAttrchild = new CategoryAttribute;
            $cateAttrchild->category_id = $child->id;
            $cateAttrchild->attribute_id = $attribute_id;
            $cateAttrchild->save();
          }
          $cateAttrchild = null;
        }
        return \View::make('admin.attribute.list')->with('category_attribute', $cateAttr)->with('category', $category)->with('categories', array());
      }
      return false;
  }
  
  public function delete()
  {
    $attribute_id = Input::get('attribute_id');
    $category_id = Input::get('category_id');
    $cateAttr = CategoryAttribute::where('category_id', '=', $category_id)->where('attribute_id', '=', $attribute_id)->delete();
    
    return '.';
  }
  
}



