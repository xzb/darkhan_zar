<?php 
namespace Admin;
use Auth, BaseController, Form, Input, Redirect, Sentry, View;
 
class AuthController extends BaseController {
 
        /**
         * Display the login page
         * @return View
         */
        public function getLogin()
        {
                
                return View::make('admin.auth.login');
        }
 
        /**
         * Login action
         * @return Redirect
         */
        public function postLogin()
        {

                $credentials = array(
                        'email' => Input::get('email'),
                        'password' => Input::get('password')
                );
        
                try
                {
                        $user = Sentry::authenticate($credentials, false);
                        if ($user)
                        {
                            return Redirect::to('admin/pages');
                        }
                }
                catch(\Exception $e)
                {
                        return Redirect::to('admin/login')->withErrors(array('login' => $e->getMessage()));
                }
        }
 
        /**
         * Logout action
         * @return Redirect
         */
        public function getLogout()
        {
                Sentry::logout();
                return Redirect::to('admin/login');
        }
 
}