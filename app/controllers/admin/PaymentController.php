<?php 
namespace App\Controllers\Admin;
use App\Models\User;
use App\Models\PaymentOrder;
use App\Models\OrderItems;
use App\Models\Product;
use App\Models\Profile;
use App\Models\Prices;
use App\Models\Category;
use App\Models\sfUser;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;
 
class PaymentController extends BaseController {
 
        /**
         * Display the login page
         * @return View
         */
        public function index()
        {
              $query = PaymentOrder::orderBy('created_at', 'DESC'); 
              $id = Input::get('id');
              $payment_type = Input::get('payment_type', 0);
              $status = Input::get('status', 0);
              if($id){
                $query = $query->where('id','=', $id);
              }
              if($status){
               $query = $query->where('status','=', $status); 
              }
              if($payment_type){
                $query = $query->where('payment_type', '=', $payment_type); 
              }
              $pages = $query->paginate(20);
              
              return \View::make('admin.payment.index')
                    ->with('pages', $pages)
                    ->with('id', $id)
                    ->with('status', $status)
                    ->with('payment_type', $payment_type);  
        }

        public function destroy($id)
        {
          $payment = PaymentOrder::find($id);
          $orderItems = OrderItems::getListOfPaymentId($id);
          foreach ($orderItems as $key => $value) {
            $value->delete();
          }
          $payment->delete();
          Notification::success('Амжилттай устгалаа.');
          return Redirect::to('/admin/payment/index');
        }

        public function active($id)
        {
          $allprices = Prices::get();
          $prices = array();
          foreach ($allprices as $price) {
            $prices[$price->category_id] = $price->price;
          }
          $payment = PaymentOrder::find($id);
          $orderItems = OrderItems::getListOfPaymentId($id);
          $start_date = date("Y-m-d", time());
          $week = 1;
          foreach ($orderItems as $key => $orderItem) {
            $product = Product::find($orderItem->product_id);
            if($product){
              $grand_id = Category::getGrandParentId($product->category_id);
              $thisPrice = $prices[$grand_id];
              if($thisPrice>0){
              $week = $orderItem->price/$thisPrice;
              }
              $times = $week*604800;
              $due_date = date("Y-m-d", time() + $times);
              $product->is_featured = 1;
              $product->featured_start_date = $start_date;
              $product->featured_due_date = $due_date;
              $product->save();
            }
          }
          $payment->status = 2;
          $payment->save();
          Notification::success('Амжилттай идэвхжүүллээ.');
          return Redirect::to('/admin/payment/index');
        }

        public function profileActive($id)
        {
          $payment = PaymentOrder::find($id);
          $payment->status = 2;
          $payment->save();
          $profile = Profile::where('user_id','=',$payment->user_id)->first();
          $profile->is_company = 1;
          $profile->is_paid = 1;
          $profile->paid_until = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
          $profile->save();
          Notification::success('Амжилттай идэвхжүүллээ.');
          return Redirect::to('/admin/payment/index');
        }
 
}