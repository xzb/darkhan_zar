<?php namespace App\Controllers\Admin;
 
use App\Models\Location;
use App\Services\Validators\LocationValidator;
use Input, Notification, Redirect, Sentry, Str, DB, Config;
 
class LocationController extends \BaseController {
  public function index()
  {
       
      $locations = Location::where('parent_id','=',0)->orderBy('sort', 'ASC')->get();
      return \View::make('admin.location.index')->with('locations', $locations);
  }
  
  public function create()
  {
      $location = null;
      $selectBox = array_merge(array(0=>'------------'),$this->getLocation(0));
      
      return \View::make('admin.location.create')->with('selectbox', $selectBox)->with('location',$location);
  }

  public function store()
  {
      $validation = new LocationValidator;

      if ($validation->passes('default'))
      {
          $this->insertInputs(0);
          return Redirect::to('admin/location');
      }
      else
      {
          return Redirect::back()->withInput()->withErrors($validation->errors);  
      }  
      
  }

  public function edit($id)
  {
      $location = Location::find($id);
      $selectBox = array_merge(array(0=>'------------'),$this->getLocation(0));

      return \View::make('admin.location.edit')->with('location', $location)->with('selectbox',$selectBox);
  }

  public function update($id)
  {
      $validation = new LocationValidator;

      if ($validation->passes('update'))
      {
          $this->insertInputs($id);
          return Redirect::to('admin/location');
      }
      else
      {
          return Redirect::back()->withInput()->withErrors($validation->errors);  
      }  

      
  }

  public function destroy($id)
  {
      $childs = Location::getChilds($id);
      if(count($childs)> 0)
      {
        foreach($childs as $c)
        {
           $c->delete();
        }  
      }
      $location = Location::find($id);
      $location->delete();

      Notification::success('Амжилттай устгагдлаа.');

      return Redirect::to('admin/location');
  }

  private function insertInputs($id)  {
          if($id != 0)
          $location = Location::find($id);
          else 
          $location = new Location;  
          $parent_id = Input::get('parent_id',0);
          $parent = Location::find($parent_id);
          $location->name       = Input::get('name');
          $location->parent_id  = $parent_id;
          $location->sort  = Input::get('sort',0);
          if($parent_id > 0)
          $location->level      = $parent->level + 1;

          $location->save();
          Notification::success('Амжилттай хадгалагдлаа.');    
  }


  public static function getLocation($parent_id)
  {
    //$retArray = array();
    $selectBox = Location::where('parent_id','=',$parent_id)->orderBy('sort','asc')->get();
      foreach($selectBox as $sel)
      {
        $retArray[$sel->id] = str_repeat('---', $sel->level).$sel->name;
        if(Location::where('parent_id','=',$sel->id)->count()>0)
        {
           $retArray = array_merge($retArray, self::getLocation($sel->id));
        }
      }

    return $retArray;
  }
  
  
}



