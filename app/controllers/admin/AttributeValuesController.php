<?php namespace App\Controllers\Admin;
 
use App\Models\Attribute;
use App\Models\Category;
use App\Models\AttributeValues;
use App\Services\Validators\AttributeValidator;
use Input, Notification, Redirect, Sentry, Str, DB, Config;
 
class AttributeValuesController extends \BaseController {

  public function create()
  {
      $attribute_id = Input::get('attribute_id');
      $attribute_value = Input::get('attribute_value');
      $attribute = Attribute::find($attribute_id);
      if($attribute_value && $attribute){
        
          $attrVal = new AttributeValues;
          $attrVal->attribute_id = $attribute_id;
          $attrVal->value = $attribute_value;
          $attrVal->save();
        
        return \View::make('admin.attribute.values')->with('attribute_value', $attrVal);
      }
      return false;
  }
  
  public function delete()
  {
    $_id = Input::get('id');
    $aVal = AttributeValues::find($_id);
    if($aVal){
      $aVal->delete();
    }
    return '.';
  }

  public function sort(){
    $ids = explode(",", Input::get('ids'));
    $counter = 1;
    foreach($ids as $id)
    {
      $attributeValue = AttributeValues::find($id);
      if ($attributeValue)
      {
        $attributeValue->sort_order = $counter;
        $attributeValue->save();
        $counter++;
      }
    }
    return '.';
  }
  
}



