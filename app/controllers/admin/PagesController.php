<?php namespace App\Controllers\Admin;
 
use App\Models\Page;
use App\Models\Tags;
use App\Models\PageTag;
use App\Models\Uploads;
use App\Services\Validators\PageValidator;
use Input, Notification, Redirect, Sentry, Str, Image;
use App\Helpers\myHelpers;
 
class PagesController extends \BaseController {
    
    public function index()
    {
        $pages = Page::paginate(15);
        return \View::make('admin.pages.index')->with('pages', $pages);
    }
 
    public function create()
    {
        $files = Uploads::getTempUploads(Sentry::getUser()->id);
        foreach($files as $f){
            if(file_exists ($f->path.'/'.$f->filename))
                unlink($f->path.'/'.$f->filename);
            $f->delete();
        }

        $page = null;
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Page::allPagesToArray(0,0);
        return \View::make('admin.pages.create')->with('page', $page)->with('combobox',$selectBox);
    }
 
    public function store()
    {
        $validation = new PageValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0);
            return Redirect::to('admin/pages');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $page = Page::find($id);
        $arr = array(0=>'--------------------');
        $selectBox = $arr + Page::allPagesToArray(0,0);
        return \View::make('admin.pages.edit')->with('page',$page)->with('combobox',$selectBox);
        
    }
 
    public function update($id)
    {
        $validation = new PageValidator;
 
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/pages');            
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
    

    public function destroy($id)
    {
        $page = Page::find($id);
        $destinationPath = 'files/uploads';
        if($page->image) {
            unlink($destinationPath.'/'.$page->image);
        }

        $uploads = Uploads::getUploadsByObj($id);
        foreach ($uploads as $u) {
            $files = Uploads::getFilesWithChild($u->id);
            foreach($files as $f){
                unlink($f->path.'/'.$f->filename);
                $f->delete();
            }
        }
        $page->delete();

        Notification::success('Хуудас устгагдлаа.');
 
        return Redirect::to('admin/pages');
    }

    public function upload(){

        $file = Input::file('file');
        $ext = $file->getClientOriginalExtension();
        $path = 'files/uploads/page/pictures';
        $mimetype = $file->getMimeType();
        
        $uploads = new Uploads;
        $uploads->path = $path;
        $uploads->extension = $ext;
        $uploads->mimetype = $mimetype;
        $uploads->size   = $file->getSize();

        // //create orig image
        $image = Image::make($file->getRealPath());
        $image->insert(public_path().'/uploads/logo.png', 'bottom-right');
        $origName = time().'_orig.'.$ext;

        $uploads->filename = $origName; 
        $uploads->user_id = Sentry::getUser()->id;
        $uploads->save();
        
        $thumbUpload = new Uploads;
        $thumbUpload->path = $path;
        $thumbUpload->mimetype = $mimetype;
        $thumbUpload->extension = $ext;

        //thumbnail
        $thumb = Image::make($file->getRealPath());
        $thumb->resize(null, 100, function ($constraint) {
                     $constraint->aspectRatio();
                });
        $thumbName = time().'_thumb.'.$ext;
        $thumb->save($path.'/'.$thumbName);

        $thumbUpload->filename = $thumbName;
        $thumbUpload->parent_id = $uploads->id;
        $thumbUpload->user_id = Sentry::getUser()->id;
        $thumbUpload->save();
        
        return $image->save($path.'/'.$origName);
    
    }

    public function loadPicAfterUpload(){
        $result  = array();
        $file = Uploads::getLastTempUploads(Sentry::getUser()->id);
    
        
        $obj['name'] = $file->filename;
        $obj['size'] = $file->size;
        $obj['id'] = $file->id;
        $result[] = $obj;
        
    
         
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function removeUpload($id){
        $files = Uploads::getFilesWithChild($id);
        foreach($files as $f){
            unlink($f->path.'/'.$f->filename);
            $f->delete();
        }

    }

    public function loadUploads($obj_id){
        $result  = array();
        $files = Uploads::getUploadsByObj($obj_id);

        foreach ( $files as $file ) {
                $obj['name'] = $file->filename;
                $obj['size'] = $file->size;
                $obj['id'] = $file->id;
                $result[] = $obj;
        }
    
         
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function getTags(){
        $result  = array();
        $tags = Tags::get();

        foreach ( $tags as $t ) {
            $obj['name'] = $t->name;
            $result[] = $obj;
        }
        
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function getTagsByQuery($query){
        $result  = array();
        $tags = Tags::where('name','like','%'.$query.'%')->get();

        foreach ( $tags as $t ) {
            $obj['name'] = $t->name;
            $result[] = $obj;
        }
        
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function getPageTags($page_id){
        $result  = array();
        $tags = Tags::getTagsByNews($page_id);

        foreach ( $tags as $t ) {
                $obj['id'] = $t->id;
                $obj['name'] = $t->name;
                $result[] = $obj;
        }
    
         
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }

    private function insertInputs($id)  {
            if($id != 0)
            $page = Page::find($id);
            else 
            $page = new Page;
            
            $page->type      = Input::get('type');
            $page->title      = Input::get('title');
            //$page->title_en   = Input::get('title_en');
            //$page->slug       = Str::slug(Input::get('title'));
            $page->slug       = Input::get('slug');
            $page->body       = Input::get('body');
            //$page->body_en    = Input::get('body_en');
            $page->is_header  = Input::get('is_header',0);
            $page->is_footer  = Input::get('is_footer',0);
            $page->is_homepopup  = Input::get('is_homepopup',0);
            $page->is_active  = Input::get('is_active',0);
            //$page->is_right   = Input::get('is_right',0);
            //$page->is_left    = Input::get('is_left',0);
            $page->page_id    = Input::get('page_id',0);
            //$page->meta_title          = Input::get('meta_title');
            //$page->meta_title_en       = Input::get('meta_title_en');
            //$page->meta_keywords       = Input::get('meta_keywords');
            //$page->meta_keywords_en    = Input::get('meta_keywords_en');
            //$page->meta_description    = Input::get('meta_description');
            //$page->meta_description_en = Input::get('meta_description_en');
            $page->user_id = Sentry::getUser()->id;



            // IMAGE
            if (Input::hasFile('image'))
            {
                $file = Input::file('image');
                $destinationPath = 'files/uploads';
                $filename = $file->getClientOriginalName();
                $extension =$file->getClientOriginalExtension(); //if you need extension of the file
                if(!in_array($extension,\myHelpers::$imageExt))
                {
                  Notification::error('Та зөвхөн зураг оруулна уу.');
                  return Redirect::to('admin/pages/create');
                }
                else
                {
                  $new_file = value(function() use ($file){
                        $filename = time() . '.' . $file->getClientOriginalExtension();
                        return strtolower($filename);
                    });

                  $uploadSuccess = $file->move($destinationPath, $new_file);
                  $page->image = $new_file;
                }
            }

            $page->save();
            
            $uploadedFiles = Uploads::getTempUploads(Sentry::getUser()->id);

            foreach($uploadedFiles as $upload)
            {
                $page->is_uploads = 1;
                $page->save();
                $upload->obj_id = $page->id;
                $upload->save();
            }
            Notification::success('Хуудас амжилттай хадгалагдлаа.');
    }


 
}