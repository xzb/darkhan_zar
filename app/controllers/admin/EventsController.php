<?php namespace App\Controllers\Admin;
 
use App\Models\Events;
use App\Models\Profile;
use App\Models\Notify;
use App\Models\Uploads;
use App\Services\Validators\EventsValidator;
use Input, Notification, Redirect, Sentry, Str, Image;
use App\Helpers\myHelpers;
 
class EventsController extends \BaseController {
    
    public function index()
    {
        $events = Events::paginate(15);
        return \View::make('admin.events.index')->with('events', $events);
    }
 
    public function create()
    {
        $event = null;
        $typeArr = array('1'=>'Үйл явдал', '2'=>'Шинэ байгууллага');
        $companyArr = array();
        $companyLists = Profile::where('is_company','=',1)->orderBy('id','desc')->get();
        foreach($companyLists as $c)
        {
            $companyArr[$c->user_id] = $c->company_name;
        }
        return \View::make('admin.events.create')
                ->with('event', $event)
                ->with('companyArr', $companyArr)
                ->with('typeArr', $typeArr)
                            ;
    }

    public function store()
    {
        $validation = new EventsValidator;
        
        if ($validation->passes())
        {
            $this->insertInputs(0);
            return Redirect::to('admin/events');
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
 
    public function edit($id)
    {
        $event = Events::find($id);
        $typeArr = array('1'=>'Үйл явдал', '2'=>'Шинэ байгууллага');
        $companyArr = array();
        $companyLists = Profile::where('is_company','=',1)->orderBy('id','desc')->get();
        foreach($companyLists as $c)
        {
            $companyArr[$c->user_id] = $c->company_name;
        }

        return \View::make('admin.events.edit')->with('event',$event)
                        ->with('companyArr', $companyArr)
                        ->with('typeArr', $typeArr);
        
    }
 
    public function update($id)
    {
        $validation = new EventsValidator;
 
        if ($validation->passes())
        {
            $this->insertInputs($id);
            return Redirect::to('admin/events');            
        } else {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }
    

    public function destroy($id)
    {
        $event = Events::find($id);
        $destinationPath = 'files/uploads/events';
        if($event->image) {
            unlink($destinationPath.'/'.$event->image);
        }

        if($event->type == 1)
        {
            $notifies = Notify::where('object_id','=',$event->id)->where('object_type','=',3)->get();
            foreach ($notifies as $value) {
                $value->delete();
            }
        }   
        else
        {
            $notifies = Notify::where('object_id','=',$event->company_id)->where('object_type','=',3)->get();
            foreach ($notifies as $value) {
                $value->delete();
            }
        }

        $event->delete();

        Notification::success('Үйл явдал устгагдлаа.');
 
        return Redirect::to('admin/events');
    }

    private function insertInputs($id)  {
            if($id != 0)
            $event = Events::find($id);
            else 
            $event = new Events;
            
            $event->title      = Input::get('title');
            $event->body       = Input::get('body');
            $event->type       = Input::get('type');
            $event->company_id       = Input::get('company_id');
            
            // IMAGE
            if (Input::hasFile('image'))
            {
                $file = Input::file('image');
                $destinationPath = 'files/uploads/events';
                $filename = $file->getClientOriginalName();
                $extension =$file->getClientOriginalExtension(); //if you need extension of the file
                if(!in_array($extension,\myHelpers::$imageExt))
                {
                  Notification::error('Та зөвхөн зураг оруулна уу.');
                  return Redirect::to('admin/events/create');
                }
                else
                {
                  $new_file = value(function() use ($file){
                        $filename = time() . '.' . $file->getClientOriginalExtension();
                        return strtolower($filename);
                    });

                  $uploadSuccess = $file->move($destinationPath, $new_file);
                  $event->image = $new_file;
                }
            }

            $event->save();
            if($id == 0)
            {
                $profiles = Profile::select('user_id')->get();
                foreach($profiles as $p){
                    $not = new Notify;
                    $not->user_id =  $p->user_id;
                    $not->from_id = $event->type;

                    if($event->type == 1)
                    {
                        $not->object_id = $event->id;
                    }   
                    else
                    {
                        $not->object_id = $event->company_id;
                    }
                    
                    $not->object_type = 3;
                    $not->is_read = 0;
                    $not->save();    
                }
                
            }    
            

            Notification::success('Үйл явдал амжилттай хадгалагдлаа.');
    }


 
}