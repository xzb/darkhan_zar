<?php namespace App\Controllers\Admin;
 
use App\Models\Banner;
use App\Models\Page;
use App\Models\Category;
use App\Models\BannerLocation;
use App\Services\Validators\BannerValidator;
use Input, Notification, Redirect, Sentry, Str, Cache;
 
class BannerController extends \BaseController {
  public function index()
  {
      $selectBox = BannerLocation::getLocationArr();
      return \View::make('admin.banner.index')->with('banners', Banner::orderBy('created_at', 'DESC')->paginate(15))->with('page',$selectBox);
  }
  
  public function create()
  {
      $banner = null;
      $categorys = Category::get();
      $arr = array(0=>'--------------------');
      $selectBox = $arr + BannerLocation::getLocationArr();
      return \View::make('admin.banner.create')->with('banner', $banner)->with('combobox',$selectBox)->with('categorys',$categorys);
  }

  public function store()
  {
      $validation = new BannerValidator;

      if ($validation->passes('default'))
      {
          $this->insertInputs(0);
      }

      return Redirect::back()->withInput()->withErrors($validation->errors);
  }

  public function edit($id)
  {
      $banner = Banner::find($id);
      $categorys = Category::get();
      $arr = array(0=>'--------------------');
      $selectBox = $arr + BannerLocation::getLocationArr();
      return \View::make('admin.banner.edit')->with('banner',$banner)->with('combobox',$selectBox)->with('categorys',$categorys);
  }

  public function update($id)
  {
      $validation = new BannerValidator;

      if ($validation->passes('update'))
      {
          return $this->insertInputs($id);
      }

      return Redirect::back()->withInput()->withErrors($validation->errors);
  }

  public function destroy($id)
  {
      $banner = Banner::find($id);
      if($banner->path != '')
        @unlink(public_path() . '/files/uploads/banner/' . $banner->path);
      $category = Category::where('banner_id','=',$banner->id)->orWhere('banner2_id','=',$banner->id)->orWhere('banner3_id','=',$banner->id)->first();

      if($category)
      {
        if (Cache::has('view_banner_'.$banner->page_id.$category->id)) {
            Cache::forget('view_banner_'.$banner->page_id.$category->id);
        }
      
      }
      else
      {
          if (Cache::has('view_banner_'.$banner->page_id.'0')) {
              Cache::forget('view_banner_'.$banner->page_id.'0');
          }
          if (Cache::has('view_homepage')) {
              Cache::forget('view_homepage');
          }
      }
      if (Cache::has('view_autohome_banner')){

        Cache::forget('view_autohome_banner');
        Cache::forget('banner_6');
      }
      Cache::flush();
      $banner->delete();

      Notification::success('Баннер устгагдлаа.');

      return Redirect::to('admin/banner');
  }

  private function insertInputs($id)  {
          if($id != 0)
          $banner = Banner::find($id);
          else 
          $banner = new Banner;  

          $banner->page_id    = Input::get('page_id',0);
          $banner->name       = Input::get('name');
          $banner->name_en       = Input::get('name_en');
          $banner->link       = Input::get('link');

          $banner->is_recursive  = Input::get('is_recursive',0);
          $banner->blank_window  = Input::get('blank_window',0);
          $banner->start_date    = date ( 'Y-m-d',strtotime(Input::get('from')) );
          $banner->end_date      = date ( 'Y-m-d',strtotime(Input::get('to')) );
          
          $banner->sort         = Input::get('sort',0);
          $banner->second       = Input::get('second',0);
          $banner->is_active    = Input::get('is_active',0);
          $banner->category_id  = Input::get('category_id',0);

          $locaction = BannerLocation::find(Input::get('page_id',0));

          if(Input::get('width') == 0 || Input::get('width') == '') {
            $banner->width = $locaction->width;
          }else {
            $banner->width = Input::get('width');
          }

          if(Input::get('height')== 0 || Input::get('height')== '') {
            $banner->height = $locaction->height;
          }
          else
          {
            $banner->height = Input::get('height'); 
          }
            

          // IMAGE
          if (Input::hasFile('path'))
            {
                $file = Input::file('path');
                $destinationPath = 'files/uploads/banner';
                $filename = $file->getClientOriginalName();
                $extension =$file->getClientOriginalExtension(); 
                if(!in_array($extension,\myHelpers::$bannerExt))
                {
                  Notification::error('Та зөвхөн зураг болон flash файл оруулна уу.');
                  return Redirect::to('admin/banner/create');
                }
                else
                {
                  if($banner->path != '')
                  @unlink(public_path() . '/files/uploads/banner/' . $banner->path);

                  $new_file = value(function() use ($file){
                        $filename = time() . '.' . $file->getClientOriginalExtension();
                        return strtolower($filename);
                    });
                  //get mime type
                  $finfo = finfo_open(FILEINFO_MIME_TYPE);
                  $mime=finfo_file($finfo, $file);
                  $banner->mime_type = $mime;
                  finfo_close($finfo);

                  $uploadSuccess = $file->move($destinationPath, $new_file);
                  $banner->path = $new_file;
                }
            }

            if (Input::hasFile('path_en'))
            {
                $file = Input::file('path_en');
                $destinationPath = 'files/uploads/banner';
                $filename = $file->getClientOriginalName();
                $extension =$file->getClientOriginalExtension(); 
                if(!in_array($extension,\myHelpers::$bannerExt))
                {
                  Notification::error('Та зөвхөн зураг болон flash файл оруулна уу.');
                  return Redirect::to('admin/banner/create');
                }
                else
                {
                  if($banner->path_en != '')
                  @unlink(public_path() . '/files/uploads/banner/' . $banner->path_en);

                  $new_file = value(function() use ($file){
                        $filename = 'en_'.time() . '.' . $file->getClientOriginalExtension();
                        return strtolower($filename);
                    });
                  //get mime type
                  $finfo = finfo_open(FILEINFO_MIME_TYPE);
                  $mime=finfo_file($finfo, $file);
                  $banner->mime_type = $mime;
                  finfo_close($finfo);

                  $uploadSuccess = $file->move($destinationPath, $new_file);
                  $banner->path_en = $new_file;
                }
            }
            

          $banner->save();
          $category = Category::where('banner_id','=',$banner->id)->orWhere('banner2_id','=',$banner->id)->orWhere('banner3_id','=',$banner->id)->first();

          if($category)
          {
              if (Cache::has('view_banner_'.$banner->page_id.$category->id)) {
                  Cache::forget('view_banner_'.$banner->page_id.$category->id);
              }
          } 
          else
          {
              if (Cache::has('view_banner_'.$banner->page_id.'0')) {
                  Cache::forget('view_banner_'.$banner->page_id.'0');
              }
              if (Cache::has('view_homepage')) {
                  Cache::forget('view_homepage');
              }
          }

          if($banner->page_id == 6){
            if (Cache::has('view_autohome_banner')){
              Cache::forget('view_autohome_banner');
              Cache::forget('banner_6');
            }
          }
          Cache::flush();

          Notification::success('Баннер амжилттай хадгалагдлаа.');    
          return Redirect::to('admin/banner');
  }



  
  
}



