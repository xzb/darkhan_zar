<?php namespace App\Controllers\Admin;
 
use App\Models\State;
use App\Models\Product;
use App\Models\Category;
use Input, Notification, Redirect, Sentry, Str, Image, DB, Response, Excel, Cache;
use App\Services\Validators\SettingsValidator;
use App\Helpers\myHelpers;
 
class DashboardController extends \BaseController {
    
    public function index()
    {
        //$prods = State::getListMountCount(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y"))), date("Y-m-d", mktime(0, 0, 0, date("m")+1, 0, date("Y"))), 0);
        
        $topCategorys = [0=>2158, 1=>1, 2=>19, 3=>26, 4=>2182];
        $topKeys = [0=>'job', 1=>'prod', 2=>'car', 3=>'apart', 4=>'serve'];
        $json = array();
        for($i = 1; $i<=7; $i++){
            $line = array();
            $line['period'] = date('Y-m-d', strtotime('-'.$i.' days'));
            foreach ($topCategorys as $key => $value) {
                $cstate = State::getListDaysCount(date('Y-m-d', strtotime('-'.$i.' days')), $value);
                $line[$topKeys[$key]] = $cstate;
            }
            $json[] = $line;
        }
        $jsonArray = json_encode($json);
        $ison = array();

        $counts = array();
        
        for($i = 1; $i<=12; $i++){
            $state = State::getListDaysCount(date('Y-m-d', strtotime('-'.$i.' days')), 0);
            $counts[] = $state;
        }
        //print_r($counts); die;
        $max = max($counts);

        $months = array();
        for($d = -1; $d<10; $d++){
            $de = $d+1;
            $prods = State::getListMountCount(date("Y-m-d", mktime(0, 0, 0, date("m")-$de, 1, date("Y"))), date("Y-m-d", mktime(0, 0, 0, date("m")-$d, 0, date("Y"))), 0);
            $months[] = $prods;

            $iline = array();
            $iline['period'] = date('Y-m', strtotime('-'.$de.' month'));
            foreach ($topCategorys as $key => $value) {
                $cstate = State::getListMountCount(date("Y-m-d", mktime(0, 0, 0, date("m")-$de, 1, date("Y"))), date("Y-m-d", mktime(0, 0, 0, date("m")-$d, 0, date("Y"))), $value);
                $iline[$topKeys[$key]] = $cstate;
            }
            $ison[] = $iline;
        }
        $isonArray = json_encode($ison);

        $mmax = max($months);
        return \View::make('admin.dashboard.index')->with('counts', $counts)->with('max', $max)->with('months', $months)->with('mmax', $mmax)->with('jsonArray', $jsonArray)->with('isonArray', $isonArray);
    }  

    public function exportToExcel(){
        $from = Input::get('from');
        
        Excel::create('Dashboard', function($excel) {

        // Set the title
        $excel->setTitle('Тайлан');

        // Chain the setters
        $excel->setCreator('Myagmar-Ochir')
              ->setCompany('Singleton LLC');

        // Call them separately
        $excel->setDescription('lol');

        $excel->sheet('Sheet1', function($sheet) {
            $sheet->row(1, array(
                 'Огноо', 'Зарын тоо', 'Ажилд авна', 'Бараа зарна', 'Авто машин', 'Үл хөдлөх', 'Үйлчилгээ' 
            ));
            $from = Input::get('from');
            if(!intval($from)){
                $from = '01/01/1970';
            }
            $to = Input::get('to');
            if(!intval($to)){
                $to = '01/01/2050';
            }
            $datefrom = date("Y-m-d",strtotime($from));
            $dateto = date("Y-m-d",strtotime($to));
            $states = State::getList($datefrom, $dateto, 0);
            $counter = 2;
            foreach($states as $state){
                $sheet->row($counter, array(
                     $state->created_at, $state->numb
                ));
                $counter++;
            }

            $topCategorys = ['C'=>2158, 'D'=>1, 'E'=>19, 'F'=>26, 'G'=>2182];
            foreach ($topCategorys as $key => $value) {
                $cstates = State::getList($datefrom, $dateto, $value);
                $co = 2;
                foreach($cstates as $cstate){
                $sheet->cell($key.$co, function($cell) use($cstate){
                    $cell->setValue($cstate->numb);
                });
                $co++;
                }
            }

        });

    })->export('xlsx');
    }

    public function settings()
    {
        $settings = DB::table('settings')->first();

        if (\Request::isMethod('post'))
        {

            $validation = new SettingsValidator;

            if ($validation->passes())
            {
                $backend_title = strip_tags(str_replace('"', '', Input::get('backend_title')));
                $frontend_title = strip_tags(str_replace('"', '', Input::get('frontend_title')));
                $frontend_description = strip_tags(str_replace('"', '', Input::get('frontend_description')));
                $frontend_keywords = strip_tags(str_replace('"', '', Input::get('frontend_keywords')));

                DB::table('settings')->update(array('backend_title' => $backend_title,'frontend_title' => $frontend_title,'frontend_description' => $frontend_description, 'frontend_keywords' => $frontend_keywords));

                Cache::forget('backend_title');
                Cache::forget('frontend_title');
                Cache::forget('frontend_description');
                Cache::forget('frontend_keywords');

                return Redirect::to('admin/settings');
            } else {
                return Redirect::back()->withInput()->withErrors($validation->errors);
            }
        }

        return \View::make('admin.dashboard.settings')->with('settings', $settings);
    }
 
}