<?php namespace App\Controllers\Front;
 
use App\Models\Page;
use App\Models\Pagestatus;
use App\Models\Category;
use Cookie, Response, App, SEOMeta, OpenGraph;
use App\Models\Uploads;
use App\Models\User;
use App\Models\PageMore;

class PageController extends \FrontBaseController {

    public function show($id)
    {
        $page = Page::find($id);
        if(!$page)
            App::abort(404);

        if(!$page->is_active)
            App::abort(404);

        SEOMeta::setTitle($page->title);
        SEOMeta::setDescription($page->description);
        SEOMeta::addMeta('article:published_time', $page->created_at->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', 'news', 'property');
        if($page->image_old) {
            OpenGraph::addImage($_ENV['baseurl'].'/pic/'.$page->image_old);
        } else {
            OpenGraph::addImage($_ENV['baseurl'].'/files/uploads/'.$page->image);
        }

        //set page status
        Page::setPageStatus($page->id);

        $photos       = Uploads::getPhotosByPageId($page->id);
        $related_news = Page::getRelatedNews($page->id, 2);

        $page_status = Pagestatus::getPageStatus($page->id);
        $user        = User::find($page->user_id);
    	$more_news   = PageMore::where('page_id','=', $page->id)->orderBy('created_at', 'DESC')->get();
        
    	return \View::make('front.page.show')
						->with('page',         $page)
                        ->with('related_news', $related_news)
                        ->with('photos',       $photos)
                        ->with('cate',         $page->page_id)
                        ->with('page_status',  $page_status)
                        ->with('more_news',    $more_news)
						->with('user',         $user);
    }

    public function Category($id)
    {
        $category   = Category::find($id);
        if(!$category)
            App::abort(404);

        SEOMeta::setTitle($category->name);
        SEOMeta::setDescription($category->name);

        $fpages = Page::getPagesByCategory($category->id);
        $pages  = Page::where('type', '=', 1)
                      ->where('page_id', '=', $category->id)
                      ->where('is_active', '=', 1)
                      ->orderBy('created_at', 'DESC')
                      ->remember(5)
                      ->paginate(10);
        return \View::make('front.page.category')
                        ->with('category', $category)
                        ->with('fpages',   $fpages)
                        ->with('cate',     $category->id)
                        ->with('pages',    $pages);
    }

    public function search($q)
    {
        $q = strip_tags(mysql_real_escape_string($q));

        $pages  = Page::where('type', '=', 1)
                      ->where('body', 'LIKE', '%'.$q.'%')
                      ->where('is_active', '=', 1)
                      ->orderBy('created_at', 'DESC')
                      ->paginate(10);

        return \View::make('front.page.search')
                        ->with('q',          $q)
                        ->with('pages',      $pages);
    }

    public function oldToNew($id)
    {
        echo $id; die;
        return Redirect::to('/n/'.$id);
    }  

    public function help()
    {
        $page  = Page::find(12);

        return \View::make('front.page.help')
                        ->with('page',      $page);
    }  
}