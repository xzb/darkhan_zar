<?php namespace App\Controllers\Front;
 
use App\Models\Product;
use App\Models\Productimage;
use App\Models\sfUser;
use App\Models\Page;
use App, Input, Image, Request;

class ImageController extends \FrontBaseController {

    public function imageList($pid) {
      
      $product = Product::find($pid);
      if(!$product)
          App::abort(404);
      // image
      $images = Productimage::where('product_id', '=', $product->id)->get();

      return \View::make('front.product.imageList')
                      ->with('images', $images);
    }

    public function upload2() {
      if (Request::isMethod('post'))
      {
        ini_set('memory_limit','256M');

        $output_dir = public_path().'/uploads/tmp/';
        if(isset($_FILES["myfile"]))
        {
          $ret = array();
          
        //  This is for custom errors;  
        /*  $custom_error= array();
          $custom_error['jquery-upload-file-error']="File already exists";
          echo json_encode($custom_error);
          die();
        */
          $error =$_FILES["myfile"]["error"];
          //You need to handle  both cases
          //If Any browser does not support serializing of multiple files using FormData() 
          if(!is_array($_FILES["myfile"]["name"])) //single file
          {
            $fileName = $_FILES["myfile"]["name"];
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
              $ret[]= $fileName;
          }
          else  //Multiple files, file[]
          {
            $fileCount = count($_FILES["myfile"]["name"]);
            for($i=0; $i < $fileCount; $i++)
            {
              $fileName = $_FILES["myfile"]["name"][$i];
            move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
              $ret[]= $fileName;
            }
          
          }
            echo json_encode($ret);
         }

      }
      else
      {
        App::abort(404);
      }

    }

    public function upload() {
      if (Request::isMethod('post'))
      {
        ini_set('memory_limit','256M');
        $data = array('error' => true);

        $tmp_name  = Input::file('userfile')->getRealPath();
        $valid     = Input::file('userfile')->isValid();
        $mime_type = Input::file('userfile')->getMimeType();

        // error occured while uploading file
        if (!$valid) {
          $data['msg'] = 'Зураг илгээx явцад алдаа гарлаа. Та бага хэмжээтэй зураг оруулна уу';
        } else {
          // success blah blah check file type
          if (!in_array($mime_type, array('image/jpeg', 'image/jpg', 'image/png', 'image/gif'))) {
            $data['msg'] = 'Та зөвxөн (png, jpg, gif) өргөтгөлтэй файл оруулна уу.';
          } else {
            $filename = md5(uniqid(rand(), true)) . '.jpg';
            
            // 
            $data['error'] = false;
            $data['filename'] = $filename;

            // move uploaded files to new destination
            move_uploaded_file($tmp_name, public_path().'/uploads/tmp/' . $filename);

            //create orig image
            $image = Image::make(public_path().'/uploads/tmp/' . $filename);
            $image->resize(900, 700, function ($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                    });        
            $image->save(public_path().'/uploads/tmp/' . $filename);


          }
        }

        echo json_encode($data);

        // exit
        exit();
      }
      else
      {
        App::abort(404);
      }
      
    }

  public function delete() {
    $pid      = Input::get('pid', -1);
    $filename = Input::get('filename');

    // image
    $image = Productimage::where('product_id', '=', $pid)
                        ->where('filename', '=', $filename)
                        ->first();

    $filenameF = public_path() . '/uploads/tmp/'   . $filename;
    $filenameO = public_path() . '/uploads/orig/'  . $filename;
    $filenameT = public_path() . '/uploads/thumb/' . $filename;
    $filenameS = public_path() . '/uploads/thumb/s_' . $filename;

    if($image) {
      // check permission
      if (sfUser::hasPid($pid)) {
        if (is_file($filenameO)) { @unlink($filenameO); }
        if (is_file($filenameT)) { @unlink($filenameT); }
        if (is_file($filenameS)) { @unlink($filenameS); }
        $image->delete();

        $firstimage = Productimage::where('product_id',$pid)->orderBy('id','ASC')->first();
        if($firstimage) {
            $product = Product::find($pid);
            $product->image_filename = $firstimage->filename;
            $product->save();            
        }

      }
    } else {
      if (is_file($filenameF)) {
        @unlink($filenameF);
      }
    }
    exit();
  }

  public function load($pid) {

    $product = Product::find($pid);
    if(!$product)
        App::abort(404);
    // image
    $files = Productimage::where('product_id', '=', $product->id)->orderBy('id', 'DESC')->get();

    $ret= array();
    foreach($files as $file)
    {
      $filePath='/uploads/thumb/'.$file->filename;
      $details = array();
      $details['name']=$file->filename;
      $details['path']=$filePath;
      $details['size']=filesize(public_path().'/uploads/thumb/' . $file->filename);
      $ret[] = $details;
    }
    echo json_encode($ret);
  }
    
}