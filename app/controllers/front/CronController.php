<?php namespace App\Controllers\Front;
 
use App\Models\Product;
use App\Models\Category;
use App\Models\UserKeyword;
use App\Models\Profile;
use App\Models\State;
use App\Models\ZKStockImages;
use DB, URL, Email;

class CronController extends \FrontBaseController {

  /**
   * EVERY 1 day
   *
   */
  public function productDaily() {
    $datetime = date('Y-m-d H:i:s', time() - 86400);

    // $conn =
    $query = "SELECT u.user_id AS user_id, h.email AS email, GROUP_CONCAT(DISTINCT u.category_id) AS cids, GROUP_CONCAT(DISTINCT p.id) AS pids " .
            "FROM user_category u " .
            "INNER JOIN user h ON h.id = u.user_id " .
            "INNER JOIN category c ON u.category_id = c.id " .
            "INNER JOIN category ch ON ch.lft BETWEEN c.lft AND c.rgt " .
            "INNER JOIN product p ON p.category_id = ch.id " .
            "WHERE (u.is_daily = 1) AND (p.created_at > '{$datetime}') AND (ch.lft + 1 = ch.rgt) " .
            "GROUP BY u.user_id";

    //
    $results = DB::select(DB::raw($query));

    foreach ($results as $result) {
      $user_id = $result->user_id;
      $email = $result->email;
      $cids = explode(',', $result->cids);
      $pids = explode(',', $result->pids);
      // categories
      $categories  = Category::whereIn('id', $cids)->get();
      // products
      $products    = Product::whereIn('id', $pids)->get();

      $html = \View::make('emails.category')
                    ->with('products', $products)
                    ->with('categories', $categories)
                    ->with('user_id', $user_id)
                    ->with('email', $email);

      // Send email
      $to = array(array('email'=> $email));
           $subject = "Таны сонгосон ангилалд нэмэгдсэн зарууд." ;
           $body = $html;
              
              $headers = "From: dadazar.mn <info@dadazar.mn>" . "\r\n" ;
              $headers .="Message-ID: <". md5(time())."@dazar.mn" . ">\r\n" ;
              $headers .="Reply-To: <". $to . ">\r\n" ;
              $headers .="Reply-To: <". $to . ">\r\n" ;
              $headers .="X-Mailer: PHP/" . phpversion();
              $headers .= "MIME-Version: 1.0\r\n";
              $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
              $headers .= "Return-Path: The Sender <info@dadazar.mn>\r\n";
              $headers .= "Organization: Dadazar.mn\r\n";
              $headers .= "X-Priority: 3\r\n";
              
              mail($to, $subject, $body,$headers); 
    }


    // OMG THIS QUERY IS BLAH BLAH
    $query = "SELECT user_id, email, GROUP_CONCAT(DISTINCT kids) AS kids, GROUP_CONCAT(DISTINCT pids) AS pids " .
            "FROM ( " .
            "SELECT p.user_id, i.email AS email, GROUP_CONCAT(DISTINCT p.id) AS kids, GROUP_CONCAT(DISTINCT k.product_id) AS pids, p.nb_word " .
            "FROM user_keyword p " .
            "INNER JOIN user_keyword_word u ON p.id = u.keyword_id " .
            "INNER JOIN user i ON i.id = p.user_id " .
            "INNER JOIN product_word k ON k.word_id = u.word_id " .
            "INNER JOIN product t ON t.id = k.product_id " .
            "WHERE (p.is_daily = 1) AND (t.created_at > '{$datetime}') " .
            "GROUP BY u.keyword_id, k.product_id " .
            "HAVING COUNT(k.product_id) = p.nb_word) AS tmp " .
            "GROUP BY tmp.user_id";

    $results = DB::select(DB::raw($query));

    foreach ($results as $result) {
      $user_id = $result->user_id;
      $email = $result->email;
      $kids = explode(',', $result->kids);
      $pids = explode(',', $result->pids);

      $keywords  = UserKeyword::whereIn('id', $kids)->get();
      $products  = Product::whereIn('id', $pids)->get();

      $html = \View::make('emails.keyword')
                    ->with('products', $products)
                    ->with('keywords', $keywords)
                    ->with('user_id', $user_id)
                    ->with('email', $email);

      // Send email
      $message = array(
          'subject'    => 'Таны хайсан түлxүүр үгэнд нэмэгдсэн зарууд.',
          'html'       => $html,
          'from_email' => 'dadazar.mn@dadazar.mn',
          'to'         => array(array('email'=> $email))
      );

      $response = Email::messages()->send($message);
    }
    return '.';
  }

  /**
   * EVERY 1 MINUTE
   */
  public function productInstant() {
    $datetime = date('Y-m-d H:i:s', time() - 60);

    // $conn =
    $query = "SELECT u.user_id AS user_id, h.email AS email, GROUP_CONCAT(DISTINCT u.category_id) AS cids, GROUP_CONCAT(DISTINCT p.id) AS pids " .
            "FROM user_category u " .
            "INNER JOIN user h ON h.id = u.user_id " .
            "INNER JOIN category c ON u.category_id = c.id " .
            "INNER JOIN category ch ON ch.lft BETWEEN c.lft AND c.rgt " .
            "INNER JOIN product p ON p.category_id = ch.id " .
            "WHERE (u.is_daily = 0) AND (p.created_at > '{$datetime}') AND (ch.lft + 1 = ch.rgt) " .
            "GROUP BY u.user_id";

    //
    $results = DB::select(DB::raw($query));

    foreach ($results as $result) {
      $user_id = $result->user_id;
      $email = $result->email;
      $cids = explode(',', $result->cids);
      $pids = explode(',', $result->pids);
      // categories
      $categories  = Category::whereIn('id', $cids)->get();
      // products
      $products    = Product::whereIn('id', $pids)->get();

      $html = \View::make('emails.category')
                    ->with('products', $products)
                    ->with('categories', $categories)
                    ->with('user_id', $user_id)
                    ->with('email', $email);

      // Send email
      $message = array(
          'subject'    => 'Таны сонгосон ангилалд нэмэгдсэн зарууд.',
          'html'       => $html,
          'from_email' => 'dazar.mn@dazar.mn',
          'to'         => array(array('email'=> $email))
      );
      $response = Email::messages()->send($message);
    }

    // OMG THIS QUERY IS BLAH BLAH
    $query = "SELECT user_id, email, GROUP_CONCAT(DISTINCT kids) AS kids, GROUP_CONCAT(DISTINCT pids) AS pids " .
            "FROM ( " .
            "SELECT p.user_id, i.email AS email, GROUP_CONCAT(DISTINCT p.id) AS kids, GROUP_CONCAT(DISTINCT k.product_id) AS pids, p.nb_word " .
            "FROM user_keyword p " .
            "INNER JOIN user_keyword_word u ON p.id = u.keyword_id " .
            "INNER JOIN user i ON i.id = p.user_id " .
            "INNER JOIN product_word k ON k.word_id = u.word_id " .
            "INNER JOIN product t ON t.id = k.product_id " .
            "WHERE (p.is_daily = 0) AND (t.created_at > '{$datetime}') " .
            "GROUP BY u.keyword_id, k.product_id " .
            "HAVING COUNT(k.product_id) = p.nb_word) AS tmp " .
            "GROUP BY tmp.user_id";

    $results = DB::select(DB::raw($query));

    foreach ($results as $result) {
      $user_id = $result->user_id;
      $email = $result->email;
      $kids = explode(',', $result->kids);
      $pids = explode(',', $result->pids);

      $keywords  = UserKeyword::whereIn('id', $kids)->get();
      $products  = Product::whereIn('id', $pids)->get();

      $html = \View::make('emails.keyword')
                    ->with('products', $products)
                    ->with('keywords', $keywords)
                    ->with('user_id', $user_id)
                    ->with('email', $email);

      // Send email
      $message = array(
          'subject'    => 'Таны хайсан түлxүүр үгэнд нэмэгдсэн зарууд.',
          'html'       => $html,
          'from_email' => 'dadazar.mn@dadazar.mn',
          'to'         => array(array('email'=> $email))
      );
      $response = Email::messages()->send($message);
    }
    return '.';
  }

  /**
   * every 1 day
   * Delete search history
   */
  public function searchExpired() {
    set_time_limit(0);
    $datetime = date('Y-m-d H:i:s', time() - 86400 * 30);
    // delete subchildren
    DB::statement("DELETE FROM search WHERE created_at < '{$datetime}'");

    return '';
  }

  /**
   *  every 1 day
   */
  public function productExpired() {
    set_time_limit(0);
    $nbDay = 21;
    $query = Product::where('created_at', '<', date('Y-m-d H:i:s', time() - 86400 * $nbDay))->where('user_id', '=', 0)->where('is_featured','=',0);
    $query2 = Product::where('created_at', '<', date('Y-m-d H:i:s', time() - 86400 * $nbDay))->where('user_id', '>', 0)->where('is_featured','=',0);
    $nbProduct = $query->count();
    $perPage = 50;
    $nbPage = ceil($nbProduct / $perPage);

    for ($page = 1; $page <= $nbPage; $page++) {
      $products = $query->skip(($page - 1) * $perPage)->take($perPage)->get();
      $product2s = $query2->skip(($page - 1) * $perPage)->take($perPage)->get();
      foreach ($products as $product) {
        // delete product
        $product->delete();
      }
      foreach ($product2s as $product) {
        $product->is_active = 0;
        $product->save();
      }

    }

    return '';
  }

  /**
   * every 1 day
   * remove tmp files
   */
  public function tmp() {
    set_time_limit(0);
    $tempDir = public_path() . '/uploads/tmp/';

    // remove old files
    if ($handle = opendir($tempDir)) {
      // 
      $time = 24 * 3600;
      while (false !== ($file = readdir($handle))) {

        $fileName = $tempDir . DIRECTORY_SEPARATOR . $file;

        if (is_file($fileName) && (time() - filemtime($fileName)) > $time) {
          unlink($fileName);
        }
      }

      closedir($handle);
    }
    // 
    return '';
  }

  public function dashboard() {
      $date = date('Y-m-d', strtotime('-1 days'));
      $count = Product::getListDaysCount($date);
      $state = State::getDayCat($date, 0);
      if(!$state){
      $state = new State;
      }
      $state->created_at=$date;
      $state->numb=$count;
      $state->category_id = 0;
      $state->save();

      $topCategorys = array(0 => 2158, 1 => 1, 2 => 19, 3 => 26, 4 => 2182, 5 =>2282, 6=>2346);
      $between  = array($date.' 00:00:00', $date.' 23:59:59');
      foreach ($topCategorys as $key => $value) {
          $category = Category::find($value);
          $nb_product = Product::getBrowseQuery($category)->whereBetween('created_at', $between)->count();
          $state = State::getDayCat($date, $value);
          if(!$state){
          $state = new State;
          }
          $state->created_at=$date;
          $state->numb=$nb_product;
          $state->category_id = $value;
          $state->save();
      }
      return 'Ok.';
  }

  /**
   *  every 1 day
   */
  public function productGetFeaturedExpire() {
    $products = Product::where('featured_due_date', '<', date('Y-m-d', time()))->where('is_featured','=',1)->get();
    
    foreach ($products as $product) {
      
      $product->is_featured = 0;
      $product->featured_start_date = null;
      $product->featured_due_date = null;
      $product->created_at = date('Y-m-d', time());
      
      $product->save();
    }

    return 'Ok.';
  }

  public function activeProduct(){
    $products = Product::where('is_active', '=', 0)->orderByRaw("RAND()")->take(3)->get();
    if($products){
      foreach ($products as $product) {
        $product->is_active = 1;
        $product->save();
      }
    }

    return 'Ok.';
  }

  public function profileRenewProducts(){
    $profiles = Profile::where('is_company','=',1)->get();
    if(count($profiles) > 0)
    {
      foreach($profiles as $profile)
      { 
        $renewProduct = '';
        $products = Product::where('user_id','=',$profile->user_id)->select('id')->get();
        {

          foreach($products as $product)
          {
            $renewProduct .= $product->id.',';
          }
        }
        $profile->renew_products = $renewProduct;
        $profile->old_products = '';
        $profile->save();
      }
    }
    return 'success';
  }

  public function randomRenew()
  {
    $profiles = Profile::where('is_company','=',1)->get();
    if(count($profiles) > 0)
    {
      $changeArr = array();
      foreach($profiles as $profile)
      {
        if($profile->renew_products != '')
        {
          $changeArr = $this->renewAction($profile);
          
        }
        else if($profile->renew_products == '' && $profile->old_products != '')
        {
          $profile->renew_products = $profile->old_products;
          $profile->old_products = '';
          $profile->save();
          $changeArr = $this->renewAction($profile);
        }
        if($profile->renew_products != '' || $profile->old_products != '')
        {
          $renewedStr = $profile->renew_products;
          $oldStr = $profile->old_products;
          
          if(count($changeArr)>0)
          {
            foreach($changeArr as $id => $val)
            {
              $oldStr .= $val.',';
              $renewedStr = str_replace($val.',', '', $renewedStr);
            }

            $profile->renew_products = $renewedStr;
            $profile->old_products = $oldStr;
            $profile->save();    
          }  
          
        }  
        
      }
    }
    return 'success';

  }

  private function renewAction($profile){
      $return =array();
      $arrProducts = explode(',',$profile->renew_products);
      
      array_pop($arrProducts);
      if(count($arrProducts) >=5){
        $renew = array();
        for($i=0;$i<5;$i++)
        {
          $index = array_rand($arrProducts);
          $renew[] = $arrProducts[$index];
          unset($arrProducts[$index]);
        }

        Product::renew($renew);
        $return = $renew;
      }
      else
      {
        Product::renew($arrProducts);
        $return = $arrProducts;
      }
      
      return $return;
  }

  //zarket cron
  public function removeStockImagesNonFiles(){
    $images = ZKStockImages::get();
    if($images){
      foreach ($images as $i) {
        if(!file_exists('files/uploads/zarket/products/stock/'.$i->image))
        {
            $i->delete();
        } 
      }
      
    }

    return 'Ok.';
  }
 
  //  ------   tsag tutam ajillaj autorenew tohirgootoi zaruudiig shinechilne
  public function autoRenewProcessing () {
    $products = Product::where('auto_renew_time','!=',0)->get();
    foreach($products as $p)
    {
        $date = strtotime($p->created_at) + ($p->auto_renew_time * 60 * 60);
        if($date <= time() )
        {
          $p->created_at = date("Y-m-d", time());
          $p->save();
        }  
    }

    return 'OK';
  }


}