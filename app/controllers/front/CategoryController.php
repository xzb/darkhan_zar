<?php namespace App\Controllers\Front;
 
use App\Models\Category;
use App\Models\Location;
use App\Models\CategoryAttribute;
use Response, App, Input;
use App\Models\Product;
use App\Models\Attribute;

class CategoryController extends \FrontBaseController {

    public function ajaxList() {
        $id = intval(Input::get('id'));
        $categories = Category::getChildren($id);

        // json data
        $data = array();
        // 
        foreach ($categories as $category) {
          // category id blah blah
          if ($category->id === 0) {
            continue;
          }
          // data
          $data[] = array(
              'id' => $category->id,
              'name' => $category->name,
              'leaf' => $category->isLeaf($category->rgt, $category->lft) ? "true" : "false",
              'for_title'=> $category->for_title
          );
        }

        // render as json

        return Response::json($data);
  }

  public function ajaxLocation() {
        $id = intval(Input::get('id'));
        $locations = Location::getChilds($id);

        // json data
        $data = array();
        // 
        foreach ($locations as $loc) {
          // category id blah blah
          if ($loc->id === 0) {
            continue;
          }
          // data
          $data[] = array(
              'id' => $loc->id,
              'name' => $loc->name
          );
        }

        // render as json

        return Response::json($data);
  }

  public function attribute() {
        $id = intval(Input::get('id'));
        $attributes = CategoryAttribute::getAttributesByCategory($id);

        return \View::make('front._partials.attribute')->with('attributes', $attributes)->with('product', null);
  }

  public function compare(){
      $limit = 4;
      $ids = explode("-", Input::get('ids', array()), $limit);

      $productAttributes = array();
      $products = Product::whereIn('id', $ids)->take($limit)->get();

      //
      if(!count($products))
            App::abort(404);
      $category = Category::find($products[0]->category_id);
      $parent_categories = $category->getAncestors();
      
      $attributes = CategoryAttribute::getAttributesByCategory($category->id);
      return \View::make('front.product.compare')
             ->with('products', $products)
             ->with('attributes', $attributes)
             ->with('parents', $parent_categories)
             ->with('category', $category);
    
      
    }

   
}