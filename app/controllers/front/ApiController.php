<?php namespace App\Controllers\Front;
 
use App\Models\Page;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductWord;
use App\Models\SearchWord;
use App\Models\Productimage;
use App\Helpers\myHelpers;
use App, Redirect, DB, Input, Request, Response, Image, Cache;

class ApiController extends \FrontBaseController {

    public function favoriteCategories()
    {
        $category_list = Category::take(20)
                                    ->where('is_featured', '=', 1)
                                    ->where('parent_id','IS', 'NULL')
                                    ->orderBy('rank', 'ASC')
                                    ->get();
        $data = array();
        foreach ($category_list as $category) {
            $nb_product = Product::getBrowseQuery($category)->count();
            if ($nb_product == 0) {
              continue;
            }

            $data[] = array(
              'id' => $category->id,
              'name' => $category->name,
              'parent_id' => $category->parent_id,
              'nb_product' => $nb_product,
              'nb_child' => $category->descendants()->limitDepth(1)->count(),
              'feature_count' => Product::getBrowseQuery($category)->where('product.is_featured', '=', true)->count()
          );
        }
        return Response::json($data);
    }

    public function categories()
    {
        $parent_id = Input::get('parent_id');

        if (Cache::has('api_categories_'.$parent_id)) {
          return Cache::get('api_categories_'.$parent_id);
        }

        $category_list = Category::getChildren(Input::get('parent_id'));

        $data = array();
        foreach ($category_list as $category) {
            $nb_product = Product::getBrowseQuery($category)->count();
            
            // if ($nb_product == 0) {
            //   continue;
            // }
            
            $data[] = array(
              'id' => $category->id,
              'name' => $category->name,
              'parent_id' => $category->parent_id,
              'nb_product' => $nb_product,
              'nb_child' => $category->descendants()->limitDepth(1)->count(),

              'feature_count' => Product::getBrowseQuery($category)->where('product.is_featured', '=', true)->count()
          );
        }

        $data = Response::json($data);
        Cache::put('api_categories_'.$parent_id, $data, 180);

        return $data;
    }

    public function search(){
        $keyword = Input::get('keyword');
        $terms = textAnalyze($keyword);
        $words = array_keys($terms); // words
        sort($words); // sorted blah blah
        $text = join(' ', $words); // blah blah

        $page  = Input::get('page', 1);

        $res = SearchWord::getSearchQueryV2($keyword, $page, 25);

        // ids
        $ids = array(-1);

        foreach ($res as $pWord) {
          $ids[] = $pWord->product_id;
        }

        $products  = Product::whereIn('id', $ids)->orderBy('id', 'DESC')->get();

        $data = array();
        foreach ($products as $product) {
          $data[] = array(
              'id' => $product->id,
              'name' => $product->name,
              'price' => $product->price,
              'image' => ($product->image_filename ? '/uploads/orig/' . $product->image_filename : null),
              'is_featured' => $product->is_featured,
              'phone' => $product->contact,
              'desc' => mb_substr($product->description, 0, 100, 'UTF-8'),
              'created_at' => $product->created_at->format('Y-m-d h:i:s')
          );
        }

        return Response::json($data);
    }

    public function productList(){
        
        $category = Category::find(Input::get('category_id'));
        $query    = Product::getBrowseQuery($category);
        $products = $query->paginate(20);

        $data = array();
        foreach ($products as $product) {
          $data[] = array(
              'id' => $product->id,
              'name' => $product->name,
              'price' => $product->price,
              'image' => ($product->image_filename ? '/uploads/orig/' . $product->image_filename : null),
              'is_featured' => ($product->is_featured === null)?false:$product->is_featured,
              'phone' => $product->contact,
              'desc' => mb_substr($product->description, 0, 100, 'UTF-8'),
              'created_at' => $product->created_at->format('Y-m-d h:i:s')
          );
        }

        return Response::json($data);
    }

    public function productDetail(){
        $product = Product::find(Input::get('id'));
        
        $data = array(
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'image' => ($product->image_filename ? '/uploads/orig/' . $product->image_filename : null),
            'description' => $product->description,
            'created_at' => $product->created_at->format('Y-m-d'),
            'phone' => $product->contact,
            'main_attributes' => array(),
            'optional_attributes' => array(),
            'images' => array(),
        );
        $images = Productimage::where('product_id', '=', $product->id)->get();
        foreach ($images as $image) {
          $data['images'][] = '/uploads/orig/' . $image->filename;
        }

    return Response::json($data);
    }

    public function other(){
        $p = Product::find(Input::get('product_id'));
        $category = Category::find($p->category_id);
        $products = Product::getBrowseQuery($category)->take(6)->get();
        $data = array();
        foreach ($products as $product) {
          $data[] = array(
              'id' => $product->id,
              'name' => $product->name,
              'price' => $product->price,
              'image' => ($product->image_filename ? '/uploads/thumb/' . $product->image_filename : null),
              'is_featured' => $product->is_featured,
              'phone' => $product->contact,
              'desc' => mb_substr($product->description, 0, 100, 'UTF-8'),
              'created_at' => $product->created_at->format('Y-m-d h:i:s')
          );
        }
        return Response::json($data);
    }
    public function featured(){
        $category = Category::find(Input::get('category_id'));
        $products = Product::getBrowseQuery($category)->where('product.is_featured', '=', true)->take(6)->get();
        $data = array();
        foreach ($products as $product) {
          $data[] = array(
              'id' => $product->id,
              'name' => $product->name,
              'price' => $product->price,
              'image' => ($product->image_filename ? '/uploads/thumb/' . $product->image_filename : null),
              'is_featured' => $product->is_featured,
              'phone' => $product->contact,
              'desc' => mb_substr($product->description, 0, 100, 'UTF-8'),
              'created_at' => $product->created_at->format('Y-m-d h:i:s')
          );
        }
        return Response::json($data);
    }

    public function submitAd(){
        if (Request::isMethod('post'))
        {
            $name = Input::get('name');
            $description = Input::get('description');
            $category_id = Input::get('category_id');
            $price = Input::get('price');
            $phone = Input::get('phone');
            $password = Input::get('password', rand(1000, 9999));

            // 
            if (!$name || !$description || !$category_id || !$phone | !$password) {
              return Response::json(array(
                          'success' => false,
                          'message' => 'Гүйцэд бөглөнө үү.'
              ));
            }

            $product = new Product;
            $product->name = $name;
            $product->description = $description;
            $product->category_id = $category_id;
            $product->price = $price;
            $product->contact = $phone;
            $product->password = $password;
            $product->is_active = 1;
            $product->save();

            $image_names = explode(',', Input::get('images'));
            foreach ($image_names as $image) {
                
              //$product->Image[]->filename = $image_name;
              if(!$image) {
                continue;
              }

              $product->image_filename = $image;

              $tmp_file  = public_path().'/uploads/tmp/' . $image;
              $orig_file = public_path().'/uploads/orig/' . $image;
              $orig_file_thumb = public_path().'/uploads/thumb/' . $image;

              if (!is_file($tmp_file)) {
                continue;
              }

              $product_image = new Productimage();
              $product_image->filename = $image;
              $product_image->hashcode = md5_file($tmp_file);
              $product_image->product_id = $product->id;
              $product_image->save();


              //create orig image
              $image = Image::make($tmp_file);
              $image->resize(800, 600, function ($constraint) {
                           $constraint->aspectRatio();
                           $constraint->upsize();
                      });        
              $image->insert(public_path().'/uploads/logo.png', 'bottom-right');
              $image->save($orig_file);

              //thumbnail
              $thumb = Image::make($tmp_file);
              $thumb->resize(200, 100, function ($constraint) {
                           $constraint->aspectRatio();
                           $constraint->upsize();
                      });
              $thumb->save($orig_file_thumb);

              @unlink($tmp_file);

            }

            // first image
            if (count($image_names)) {
              $product->image_filename = $image_names[0];
            }
            $product->save();

            return Response::json(array(
                'success' => true,
                'message' => 'Амжилттай хадгаллаа.',
                'password' => $product->password,
                'id' => $product->id
            ));
        }else{
            return Response::json(array(
                      'success' => false,
                      'message' => 'Өгөгдөл пост төрөлтэй байх ёстой.'
            ));
        }
    }

    public function uploadImage(){
        if (Request::isMethod('post'))
        {

            // tmp name
            //$tmp_name = $_FILES['userfile']['tmp_name'];
            $tmp_name  = Input::file('userfile')->getRealPath();
            $valid     = Input::file('userfile')->isValid();
            $mime_type = Input::file('userfile')->getMimeType();

            //mime type 
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            //$mime_type = strtolower(finfo_file($finfo, $tmp_name));
            finfo_close($finfo);

            if (!in_array($mime_type, array('image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'))) {
              return Response::json(array(
                          'success' => false,
                          'message' => 'Зөвхөн jpeg, jpg, png, gif файл хуулах боломжтой.'
              ));
            }
            
            $filename = md5(uniqid(rand(), true)) . '.jpg';

            // move uploaded files to new destination
            move_uploaded_file($tmp_name, public_path().'/uploads/tmp/' . $filename);

            // upload file
            return Response::json(array(
                        'success' => true,
                        'id' => $filename,
            ));
        }else{
            return Response::json(array(
                      'success' => false,
                      'message' => 'Өгөгдөл пост төрөлтэй байх ёстой.'
            ));
        }
    }

    public function delete(){
        $id = Input::get('id');
        $password = Input::get('password');
        // product
        $product = Product::find($id);

        // product not found
        if (!$product) {
          return Response::json(array(
                      'success' => false,
                      'message' => 'NOT_FOUND',
          ));
        }

        // incorrect
        if ($product && $product->password != $password) {
          return Response::json(array(
                      'success' => false,
                      'message' => 'INVALID_PASSWORD',
          ));
        }

        try {
          // delete product
          $product->delete();
        } catch (Exception $e) {
          // 
          return Response::json(array(
                      'success' => false,
                      'message' => 'EXCEPTION',
          ));
        }

        return Response::json(array(
                    'success' => true,
                    'message' => 'SUCCESS',
        ));
    }
}