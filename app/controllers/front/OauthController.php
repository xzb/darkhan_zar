<?php namespace App\Controllers\Front;

use App\Models\User;
use App\Models\Group;
use App\Models\UsersGroup;
use App\Models\Product;
use App\Models\Profile;
use App\Models\sfUser;
use App\Services\Validators\RegisterValidator;
use OAuth, Input, Redirect, Sentry, Notification, FacebookConnect, Session, Request, Event, Response;

class OauthController extends \FrontBaseController {

    public function login()
    {
        $pos = strpos(Request::url(), 'auto.');
        $layout = 'front._layouts.product';
        if($pos !== false){
            $layout = 'front._layouts.auto_product';
        }
        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $layout = 'front._layouts.property_product';
        }
        
    	return \View::make('front.oauth.login')
                    ->with('layout',$layout)
                    ->with('pos',$pos);
    }

    public function logout()
    {
        Session::forget('pids');
        Session::forget('iids');
        $user = Sentry::getUser();
        $user->is_online = 0;
        $user->save(); 
        Sentry::logout();
        Notification::success('Холболт амжилттай саллаа');
        return Redirect::to('/');
    }

    public function postLogin()
        {
            $credentials = array(
                'email' => cp1251_utf8(sanitize(Input::get('email'))),
                'password' => sanitize(Input::get('password'))
            );

            try
            {
                $user = Sentry::authenticate($credentials, false);
                if ($user)
                {
                    sfUser::addPids();
                    Event::fire('event.user.login', $user);
                    $profile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
                    $user->is_online = 1;
                    $user->save();
                    if(!$profile){
                        Notification::success('Амжилттай нэвтэрлээ. Та нэмэлт мэдээллээ оруулна уу.');
                        return Redirect::to('profile/edit');
                    }else{
                        Notification::success('Амжилттай нэвтэрлээ');
                        if($profile->is_company == 1)
                        {
                            return Redirect::to('/profile/'.$user->id);
                        }
                        else
                        {
                            return Redirect::to('/home/'.$user->id);
                        }
                    }
                }
            }
            catch(\Exception $e)
            {
                return Redirect::to('/login')->withErrors(array('login' => 'Алдаа гарлаа. Дахин оролдоно уу!'));
            }
        }

    public function register()
    {
        $validation = new RegisterValidator;

        if ($validation->passes())
        {
            $activecode = md5(date('Y-m-d H:i:s'));
            
            $user = Sentry::createUser(array(
                'email'         => cp1251_utf8(sanitize(Input::get('email'))),
                'password'      => sanitize(Input::get('password')),
                'first_name'    => cp1251_utf8(sanitize(Input::get('firstname'))),
                'last_name'     => cp1251_utf8(sanitize(Input::get('lastname'))),
                'social_id'     => 0,
                'type'          => 0,
                'activated'     => true,
                'image'         => ''
            ));
            
            //setting for group and permission
            $group = Sentry::findGroupById(11);
            $user->addGroup($group);
            $user->activated = true;
            $user->gender = Input::get('gender');
            $user->year = Input::get('year');
            $user->activation_code = $activecode;

            $user->save();

            //sent mail
             // $subject = "Хэрэглэгч идэвхжүүлэх хүсэлт | dazar.mn" ;
             // $body = '<div> <a href="http://www.dazar.mn/userActive/'.$activecode.'" target="_blank">http://www.dazar.mn/userActive/'.$activecode.'</a> </div>';
                
             //    $headers = "From: dazar.mn <info@dazar.mn>" . "\r\n" ;
             //    $headers .="Message-ID: <". md5(time())."@dazar.mn" . ">\r\n" ;
             //    $headers .="X-Mailer: PHP/" . phpversion();
             //    $headers .= "MIME-Version: 1.0\r\n";
             //    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
             //    $headers .= "Return-Path: The Sender <info@dazar.mn>\r\n";
             //    $headers .= "Organization: dazar.mn\r\n";
             //    $headers .= "X-Priority: 3\r\n";
                
             //  if(mail($user->email, $subject, $body,$headers)) 
             //  {
             //    Notification::success('Амжилттай бүртгүүллээ. Таны имайл хаяг руу идэхвжүүлэх код явууллаа. Имайлээрээ орж идэвхжүүлээд хэрэглэгчийн нэр нууц үгээрээ нэвтрэн орно уу.');
             //  }
             //  else 
             //  {
             //    Notification::error('Бүртгүүлэх явцад алдаа гарлаа.');     
             //  }
            //endsent mail
            Notification::success('Амжилттай бүртгүүллээ.');
            return Redirect::to('/login');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validation->errors);
        }
    }

    public function facebook()
    {
        $code = Input::get( 'code' );
        $fb   = OAuth::consumer( 'Facebook' );

        if ( !empty( $code ) ) {
            $token = $fb->requestAccessToken( $code );
            $result = json_decode( $fb->request( '/me' ), true );
            $user = $this->registerAndLogin($result, $token, 1);
            if($user) {
                $user->is_online = 1;
                $user->save();
                $profile = Profile::where('user_id', '=', $user->id)->first();
                if(!$profile){
                    Notification::success('Амжилттай нэвтэрлээ. Та нэмэлт мэдээллээ оруулна уу.');
                    return Redirect::to('profile/edit');
                }else{
                 Notification::success('Амжилттай нэвтэрлээ');
                        if($profile->is_company == 1)
                        {
                            return Redirect::to('/profile/'.$user->id);
                        }
                        else
                        {
                            return Redirect::to('/home/'.$user->id);
                        }    
                    
                }

            }
        }
        else {
            $url = $fb->getAuthorizationUri();
             return Redirect::to( (string)$url );
        }
    }

    public function twitter()
    {
        $token  = Input::get( 'oauth_token' );
        $verify = Input::get( 'oauth_verifier' );
        Oauth::setHttpClient('CurlClient');
        $tw     = OAuth::consumer( 'Twitter' );
        

        if ( !empty( $token ) && !empty( $verify ) ) {

            $token = $tw->requestAccessToken( $token, $verify );
            $result = json_decode( $tw->request( 'account/verify_credentials.json' ), true );
            $user = $this->registerAndLogin($result, $token, 2);
            if($user) {
                $user->is_online = 1;
                $user->save();

                $profile = Profile::where('user_id', '=', $user->id)->first();
                if(!$profile){
                    Notification::success('Амжилттай нэвтэрлээ. Та нэмэлт мэдээллээ оруулна уу.');
                    return Redirect::to('profile/edit');
                }else{
                 Notification::success('Амжилттай нэвтэрлээ');
                        if($profile->is_company == 1)
                        {
                            return Redirect::to('/profile/'.$user->id);
                        }
                        else
                        {
                            return Redirect::to('/home/'.$user->id);
                        }
                }

            }
        }
        else {
            $reqToken = $tw->requestRequestToken();
            $url = $tw->getAuthorizationUri(array('oauth_token' => $reqToken->getRequestToken()));
            return Redirect::to( (string)$url );
        }

    }

    private function registerAndLogin($result = array(), $token = array(), $type = null)
    {
        $user = User::where('type', '=', $type)->where('social_id', '=', $result['id'])->first();
        $tmp_password = randomPassword();

        if(!$user) {
            
            //twitter
            if($type == 2) {
                $user = Sentry::createUser(array(
                    'email'         => $result['id'].'@twitter.com',
                    'password'      => $tmp_password,
                    'first_name'    => $result['name'],
                    'social_id'     => $result['id'],
                    'type'          => 2,
                    'activated'     => true,
                    'image'         => $result['profile_image_url'],
                    'token'         => $token->getAccessToken(),
                    'token_secret'  => $token->getAccessTokenSecret()
                ));
                $user->password  = $tmp_password;
            } 
            //facebook
            else {
                $user = User::where('email', '=', $result['email'])->first();
                if($user){
                    $user->first_name = $result['first_name'];
                    $user->last_name = $result['last_name'];
                    $user->social_id = $result['id'];
                    $user->type = 1;
                    $user->activated = true;
                    $user->image = 'https://graph.facebook.com/'.$result['id'].'/picture';
                    $user->token = $token->getAccessToken();
                }else{
                    $user = Sentry::createUser(array(
                        'email'         => $result['email'],
                        'password'      => $tmp_password,
                        'first_name'    => $result['first_name'],
                        'last_name'     => $result['last_name'],
                        'social_id'     => $result['id'],
                        'type'          => 1,
                        'activated'     => true,
                        'image'         => 'https://graph.facebook.com/'.$result['id'].'/picture',
                        'token'         => $token->getAccessToken()
                    ));
                    $user->password  = $tmp_password;
                }
            }

            //setting for group and permission
            $group = Sentry::findGroupById(11);
            $user->addGroup($group);
        }

        $user->token = $token->getAccessToken();
        if($type == 2) $user->token_secret = $token->getAccessTokenSecret();
        $user->activated = 1;
        $user->password  = $tmp_password;
        $user->save();

        $user = Sentry::findUserById($user->id);

        $credentials = array(
            'email'    => $user->email,
            'password' => $tmp_password
        );

        try
        {
            $user = Sentry::authenticate($credentials, false);
            if ($user)
            {
                sfUser::addPids();
                return $user;
            }
        }
        catch(\Exception $e)
        {
            echo 'Not logged'.print_r($e->getMessage());
            return null;
            exit();
        }

    }

    public function forgotPass(){
      return \View::make('front.profile.forgotpass')
                      ->with('user', 1);
    }

    public function sentPass(){
      $email = Input::get( 'email' );
      $user = User::searchByEmail($email);

      //$user = Sentry::findUserByResetPasswordCode($code);
      if($user){
        $resetcode = md5(date('Y-m-d H:i:s')).$user->id;
        $user->reset_password_code = $resetcode;
        $user->save();

        //sent mail
         $subject = "Нууц үг шинээр авах хүсэлт | dazar.mn" ;

         $body = '
         <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        </head>
        <body>
         <div style="background-color:#f8f8f8">
        <div style="max-width:700px;margin:0 auto;padding-right:0;padding-top:20px;padding-left:0;padding-bottom:20px">
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td align="left" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="700">
                            <tbody>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="http://www.dazar.mn/images/zarmnuriavgtei.png"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="vertical-align:middle;padding-top:8px">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div style="min-height:580px">
            <div style="max-width:700px;margin:0 auto;background:#ffffff;border:1px solid #eeeeee">
                <div style="padding:25px">
                    <span style="font-size:20px;color:#555555;padding-bottom:30px;font-family:Arial,sans-serif">
                        <b>Сайн байна уу</b> <br>
                    </span>
                </div>
                <div style="padding:0 25px">
                    <span style="font-size:13px;color:#555555;font-family:Arial,sans-serif">
                        <p>Таны “'.$user->last_name.' '.$user->first_name.'” хаягаас нууц үг сэргээх хүсэлт ирлээ.</p>

                         <p>Хэрэв та энэ хүсэлтийг гаргасан бол дараах холбоос дээр дарж нууц үгээ өөрчилнө үү.</p>
                         <br/>
                         <a style="background: red; color: #ffffff; padding: 5px;text-decoration: none;font-weight: bold;" href="http://www.dazar.mn/resetPass/'.$resetcode.'" target="_blank">Нууц үг сэргээх</a> 
                         <br/>
                         <br/>
                         <p>Хэрэв та нууц үг шинэчлэх хүсэлт гаргаагүй бол ямар нэгэн үйлдэл хийх шаардлагагүй.</p>

                        <p>Холбогдох дугаар: 70116300, 99173159</p><br>
                    </span>
                </div>

                <div style="padding:100px 10px;text-align:center">
                    <p style="font-size:12px;font-family:Geneva,Verdana,Arial">
                        Таны цахим худалдаа эндээс эхэлнэ!
                    </p>
                    <p style="font-size:12px;font-family:Geneva,Verdana,Arial">
                        © 2002-2015 ЗарЭмЭн ХХК
                    </p>

                </div>
            </div>
        </div>
        </div>
        </body>
        </html>';
            
            $headers = "From: dazar.mn <info@dazar.mn>" . "\r\n" ;
            $headers .="Message-ID: <". md5(time())."@dazar.mn" . ">\r\n" ;
            $headers .="X-Mailer: PHP/" . phpversion();
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8\r\n";   
            $headers .= "Return-Path: The Sender <info@dazar.mn>\r\n";
            $headers .= "Organization: dazar.mn\r\n";
            $headers .= "X-Priority: 3\r\n";
            
          if(mail($email, $subject, $body,$headers)) 
          {
            return Response::json('success');            
          }
          else 
          {
            return Response::json('error');     
          }
        //endsent mail

        return Response::json('success');
      }else{
        return Response::json('error'); 
      }
    }

    public function resetPass($code){
      // $pos = strpos(Request::url(), 'auto.');
      // $layout = ($pos === false)?'front._layouts.product':'front._layouts.auto_product';
      $pos = strpos(Request::url(), 'auto.');
        $layout = 'front._layouts.product';
        if($pos !== false){
            $layout = 'front._layouts.auto_product';
        }
        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $layout = 'front._layouts.property_product';
        }
      $error = null;
      $user = Sentry::findUserByResetPasswordCode($code);
      if($user){
        if (Request::isMethod('post'))
        {
          $password = sanitize(Input::get('password'));
          $password_com = sanitize(Input::get('password_com'));
          if($password && $password == $password_com){
             if ($user->checkResetPasswordCode($code))
              {
                  // Attempt to reset the user password
                  if ($user->attemptResetPassword($code, $password))
                  { 
                      Notification::success('Нууц үг амжилттай солигдлоо');
                      return Redirect::to('/login');
                  }
                  else
                  {
                   Notification::error('Нууц үг амжилтгүй солигдлоо');
                   return Redirect::to('/login');   
                  }
              }
          }else{
            $error = 'Нууц үг буруу байна.';
          }
        }
      }else{
        App::abort(404);
      }
      return \View::make('front.oauth.resetpass')
                      ->with('error',$error)
                      ->with('layout',$layout)
                      ->with('code', $code);
    }

    public function userActive($code){
      // $pos = strpos(Request::url(), 'auto.');
      // $layout = ($pos === false)?'front._layouts.product':'front._layouts.auto_product';
      $pos = strpos(Request::url(), 'auto.');
        $layout = 'front._layouts.product';
        if($pos !== false){
            $layout = 'front._layouts.auto_product';
        }
        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $layout = 'front._layouts.property_product';
        }
      $error = null;
      $user = Sentry::findUserByActivationCode($code);
      if($user){
        $user->activated = true;
        $user->activation_code = null;
        $user->save();
        Notification::success('Амжилттай идэвхжүүллээ.');
        return Redirect::to('/login'); 
      }else{
        App::abort(404);
      }
      return \View::make('front.oauth.resetpass')
                      ->with('error',$error)
                      ->with('layout',$layout)
                      ->with('code', $code);
    }

}