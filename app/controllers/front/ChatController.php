<?php namespace App\Controllers\Front;

use App\Models\User;
use App\Models\Profile;
use App\Models\Chats;
use App\Models\Notify;
use App\Models\UserState;
use App\Models\sfUser;
use App\Models\CoverImageType;
use App\Services\Validators\ProfileValidator;
use OAuth, Input, Request, Redirect, Sentry, Notification, FacebookConnect, Session, Image, DB, Validator, Response,SEOMeta, App;

class ChatController extends \FrontBaseController {
    
    public function chat($chat_user_id){
        if($chat_user_id != 0)
        {
            $user = User::find($chat_user_id);
            if(!$user)
                App::abort(404);

            if(Sentry::getUser()->id == $chat_user_id)
                App::abort(404);

        }
        
        $me = Sentry::getUser();
        $my_profile = Profile::where('user_id','=',$me->id)->first();
        $user1 = Chats::select('to_user_id')->where('user_id','=',$me->id)->groupBy('to_user_id')->get();
        $user1Arr = array();
        foreach($user1 as $u1)
        {
            $user1Arr[] = $u1->to_user_id;
        }  
        $user2 = Chats::select('user_id')->where('to_user_id','=',$me->id)->groupBy('user_id')->get();
        $user2Arr = array();
        foreach($user2 as $u2)
        {
            $user2Arr[] = $u2->user_id;
        }

        $user_ids = array_unique(array_merge($user1Arr, $user2Arr));
        if(count($user_ids)>0)
        {
            if($chat_user_id != 0 )
            {
                if(!in_array($chat_user_id, $user_ids))
                {
                    array_push($user_ids, $chat_user_id);
                }        
            }    
            $profiles = User::join('profile', function($join) {
                $join->on('users.id', '=', 'profile.user_id');
            })->select('users.id','users.last_login','profile.image', 'users.first_name')->whereIn('users.id',$user_ids)->get();
        }
        else
        {
            
            if($chat_user_id == 0)
            {
                $profiles = null;    
            }
            else
            {
                $profiles = User::join('profile', function($join) {
                    $join->on('users.id', '=', 'profile.user_id');
                })->select('users.id','users.last_login','profile.image', 'users.first_name')->where('users.id','=',$chat_user_id)->get();       
            }

        }

        $onlineUsers = User::getOnlineUsers();

        $notify = Notify::getNotifyForUser($me->id, 1, 0);
        if($notify)
        {
            foreach($notify as $not)
            {
                $not->is_read = 1;
                $not->save();
            }
        }    
        
        return \View::make('front.chat.home')
                  ->with('my_profile',$my_profile)
                  ->with('profiles',$profiles)
                  ->with('onlineUsers',$onlineUsers)
                  ->with('chat_user_id',$chat_user_id)
                  ->with('me',$me);
        
    }

    public function loadingMessages(){
        $to_user_id = Input::get("user_id");
        $me = Sentry::getUser();
        
        
        $profile_to = User::join('profile', function($join) {
                $join->on('users.id', '=', 'profile.user_id');
            })->select('users.id','users.last_login','profile.image', 'users.first_name')->where('users.id',$to_user_id)->first();
        
        $profile_me = User::join('profile', function($join) {
                $join->on('users.id', '=', 'profile.user_id');
            })->select('users.id','users.last_login','profile.image', 'users.first_name')->where('users.id',$me->id)->first();
        
        if(!$profile_to || !$profile_me)
        {
            App::abort(404);
        }

        $conversations = Chats::where(function($query)use ($to_user_id,$me) {
                    return $query->where('user_id','=',$to_user_id)->where('to_user_id','=',$me->id);
                })->orWhere(function($query)use ($to_user_id,$me) {
                    return $query->where('user_id','=',$me->id)->where('to_user_id','=',$to_user_id);
                })->orderBy('id')->get();
        
        

        $title = '<h4 class="chat-title">'.$profile_to->first_name.'</h4>';
        $body = '
        <input type="hidden"  id="conversationCount" value="'.count($conversations).'"/>
        <div class="chat-scroll">
            <div class="content">
                <ul class="chating" id="chat-log'.$profile_me->id.'_'.$profile_to->id.'">';
                foreach($conversations as $c)
                {
                    $image =($c->user_id == $profile_me->id)?$profile_me->image:$profile_to->image;
                    $name =($c->user_id == $profile_me->id)?$profile_me->first_name:$profile_to->first_name;

                    if($image == '')
                        $image = '/images/profile.png';
                    else
                        $image = '/uploads/profile/'.$image;    
                    $body .= '
                    <li>
                        <a href="" class="img">
                            <img src="'.$image.'" alt="" style="width:42px; height:42px" />
                        </a>
                        <dl class="timeline-series">
                            <dt>
                                <a href="#" class="name"><strong>'.$name.'</strong><span class="date">'.$c->created_at.'</span></a>
                            </dt>
                            <dd class="timeline-event-content" id="event3EX">
                                <p>'.$c->text.'</p>
                            </dd>
                        </dl>
                    </li>';    
                }
                
                $body.= '</ul>
            </div>
        </div>
            <div class="chat-frm">
                <div class="col-sm-10 col">
                    <input type="text" id="chat_input" placeholder="Хариулах..."  style="width:463px; height:43px">
                </div>
                <div class="col-sm-2 col text-right">
                    <button id="chat_button" class="btn btn-danger">Илгээх</button>
                </div>
            </div>';

        return $title.$body;


    }

    public function saveConversation()
    {
        $message = cp1251_utf8(sanitize(Input::get('message')));
        $user_id = intval(Input::get('user_id'));
        $to_user_id = intval(Input::get('to_user_id'));
        

        $chat = new Chats;
        $chat->text = $message;
        $chat->user_id = $user_id;
        $chat->to_user_id = $to_user_id;
        $chat->is_read = 0;
        $chat->created_at = date("Y-m-d H:i:s", time());
        
        $chat->save();

        $notify = Notify::where('user_id','=',$chat->to_user_id)->where('object_type','=',1)->where('is_read','=',0)->first();
        if (!$notify) 
        {
            $not = new Notify;
            $not->user_id = $chat->to_user_id;
            $not->from_id = $chat->user_id;
            $not->object_id = $chat->id;
            $not->object_type = 1;
            $not->is_read = 0;
            $not->save();
        }

        return 'success';
    }


}