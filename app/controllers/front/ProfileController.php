<?php namespace App\Controllers\Front;

use App\Models\User;
use App\Models\Group;
use App\Models\UsersGroup;
use App\Models\Profile;
use App\Models\Product;
use App\Models\CompanyBanner;
use App\Models\Banner;
use App\Models\CompanyUser;
use App\Models\CompanyFeedback;
use App\Models\CompanyFollow;
use App\Models\CompanyDetail;
use App\Models\UserState;
use App\Models\Notify;
use App\Models\sfUser;
use App\Models\Events;
use App\Models\CoverImageType;
use App\Models\Category;
use App\Models\CompanyCategory;
use App\Services\Validators\ProfileValidator;
use OAuth, Input, Request, Redirect, Sentry, Notification, FacebookConnect, Session, Image, DB, Validator, Response,SEOMeta, App, Mail;

class ProfileController extends \FrontBaseController {
    
    public function businessProfile($id)
    {
        $sort_type = intval(sanitize(Input::get('sortType')));
        $order_type = 'DESC';
        $user = User::find($id);
        if(!$user)
          App::abort(404);
        $order = 'created_at';

        switch ($sort_type) {
          case 1:
            $order = 'price';$order_type = 'DESC';
            break;
          case 2:
            $order = 'name';$order_type = 'DESC';
            break;
          case 3:
            $order = 'created_at';$order_type = 'DESC';
            break;
          case 4:
            $order = 'price';$order_type = 'asc';
            break;
          case 5:
            $order = 'name';$order_type = 'asc';
            break;
          case 6:
            $order = 'created_at';$order_type = 'asc';
            break;
          case 7:
            $order = 'created_at';$order_type = 'DESC';
            break;
        }
        

        $products  = Product::where('user_id', $id)->orderBy($order, $order_type)->paginate(20);
        $counter = Product::where('user_id', $id)->count();
        $profile = Profile::where('user_id', '=', $id)->first();
        if(!$profile)
          App::abort(404);
        if($profile->is_company == 1){
          //return Redirect::to("/company/".$profile->company_name);
        }
        $state = UserState::where('user_id', '=', $id)->count();
        $banners = null;
        $viewType = Input::get('viewType', Session::get('viewType', 'grid'));
        Session::put('viewType', $viewType);
        $page = Input::get('page', 1);
        if($profile->is_company){
          $image_types = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
          $image_types_array = array();
          foreach($image_types as $key=>$i)
          {
            $image_types_array[$key] = $i->id;
          }
          $banners = CompanyBanner::where('company_id', '=', $id)->whereIn('image_type_id',$image_types_array)->get();
        }
        return \View::make('front.profile.businessProfile')
                        ->with('user', $user)
                        ->with('state', $state)
                        ->with('counter', $counter)
                        ->with('profile', $profile)
                        ->with('banners', $banners)
                        ->with('page', $page)
                        ->with('viewType', $viewType)
                        ->with('sort_type', $sort_type)
                        ->with('products', $products);
    }

    public function home($id)
    {
      $other = null;
      if(Sentry::getUser()->id != $id)
      {
        $other = User::join('profile', function($join) {
            $join->on('profile.user_id', '=', 'users.id');
        })->where('users.id','=',$id)->first();
        
        if(!$other)
          App::abort(404);
      }
      $profile = Profile::where('user_id', '=', $id)->first();
        if(!$profile)
          App::abort(404);

      // $banners  = Banner::getBannersByLocation(2);
      $products  = Product::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(30);
      $countProducts = Product::where('user_id', $id)->get()->count();
      return \View::make('front.profile.home')
                  ->with('id',$id)
                  ->with('other',$other)
                  ->with('profile',$profile)
                  ->with('countProducts',$countProducts)
                  ->with('products',$products);
    }

    public function followedCompanies($id)
    {
      $user = User::find($id);
      $followedCompanyCount = CompanyFollow::where('user_id','=',$user->id)->get()->count();
      return \View::make('front.profile.followedCompanies')
                  ->with('id',$id)
                  ->with('followedCompanyCount',$followedCompanyCount)
                  ->with('user',$user); 
    }

    public function followedCompaniesAjax()
    {
      $id = Input::get('id');
      $page = Input::get('page');
      $followedCompanyCount = CompanyFollow::where('user_id','=',$id)->get()->count();
      $followedCompany = CompanyFollow::join('profile', function($join) {
            $join->on('profile.user_id', '=', 'company_follow.company_id');
        })->where('company_follow.user_id','=',$id)->paginate(1);

      return \View::make('front.profile._followedCompanies')
                  ->with('id',$id)
                  ->with('followedCompanyCount',$followedCompanyCount)
                  ->with('followedCompany',$followedCompany)
                  ->with('page',$page)
                  ->with('paginate',1)
                  ;
    }

    public function myProduct($id)
    {
      return Redirect::to('/home/'.$id);
      // $user = User::find($id);
      // $countProducts = Product::where('user_id', $id)->get()->count();
      // $my_profile = Profile::where('user_id','=',$user->id)->first();
      // return \View::make('front.profile.myProduct')
      //             ->with('id',$id)
      //             ->with('my_profile',$my_profile)
      //             ->with('user',$user)
      //             ->with('countProducts',$countProducts)
      //             ;
    }
    
    public function othersProduct($id)
    {
      
      $user = User::join('profile', function($join) {
        $join->on('profile.user_id', '=', 'users.id');
      })->where('users.id','=',$id)->select('users.id','users.first_name','users.last_name','profile.email','profile.position','profile.image','profile.phone')->first();

      $countProducts = Product::where('user_id', $id)->get()->count();
      return \View::make('front.profile.othersProduct')
                  ->with('id',$id)
                  // ->with('my_profile',$my_profile)
                  ->with('user',$user)
                  ->with('countProducts',$countProducts)
                  ;
    }

    public function myProductAjax()
    {
      $id = Input::get('id');
      $page = Input::get('page');
      $countProducts = Product::where('user_id', $id)->get()->count();
      $products  = Product::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(10);
      $is_mine = ($id == Sentry::getUser()->id)?true:false;
      if(count($products)>0)
      {
        return \View::make('front.profile._myProduct')
                  ->with('products',$products)
                  ->with('countProducts',$countProducts)
                  ->with('user_id',$id)
                  ->with('page',$page)
                  ->with('is_mine',$is_mine)
                  ->with('paginate',10)
                  ;  
      }  
      else
      {
        return '<center>Та <a href="/add" style="color:red">ЭНД</a> дарж зараа оруулна уу</center>';
      }
      

    }

    public function events($id)
    {
      $eventsCount = Events::orderBy('created_at', 'DESC')->get()->count();
      
      return \View::make('front.profile.events')
                  ->with('id',$id)
                  ->with('eventsCount',$eventsCount)
                  ;
    }

    public function eventsAjax()
    {
      $id = Input::get('id');
      $page = Input::get('page');
      $eventsCount = Events::orderBy('created_at', 'DESC')->get()->count();
      $events = Events::orderBy('created_at', 'DESC')->paginate(5);
      
      return \View::make('front.profile._events')
                  ->with('events',$events)
                  ->with('eventsCount',$eventsCount)
                  ->with('id',$id)
                  ->with('page',$page)
                  ->with('paginate',5)
                  ;
    }

    public function chat($user_id)
    {
      $me = Sentry::getUser();
      $my_profile = Profile::where('user_id','=',$me->id)->first();
      return \View::make('front.profile.chat')
                  ->with('my_profile',$my_profile)
                  ->with('to',$user_id)
                  ->with('me',$me);
    }

    public function coupon()
    {   
        $cid = sanitize(Input::get('cid',0));
        $pid = sanitize(Input::get('pid',0));
        $query   = Product::where('is_coupon','=',1);
        if($cid != 0)
        {
            $category = Category::find($cid);
            $between  = array($category->lft, $category->rgt);

            $query->whereBetween('category_id', $between);
        }

        if($pid != 0)
        {
            $query->where('coupon_percent','=',$pid);
        } 

        $coupons = $query->paginate(30);
        $categories = Category::where('parent_id','=',1)->where('level','=',1)->where('is_visible','=',1)->orderBy('rank','asc')->get();
        
        SEOMeta::setTitle('Coupon shop');
        SEOMeta::setDescription('dazar.mn');
        $is_coupon = true;
        
        return \View::make('front.profile.couponHome')
                        ->with('coupons', $coupons)
                        ->with('categories', $categories)
                        ->with('is_coupon',$is_coupon)
                        ->with('cid',$cid)
                        ->with('pid',$pid)
                        ;
    }

    public function getLeafs($root_id)
    {
        $data = array();
        $leaves = Category::find($root_id)->descendants()->limitDepth(1)->get();
        foreach($leaves as $l){
            $data[] = array("id"=>$l->id, "name"=>$l->name,"is_leaf"=>(!$l->isLeaf())?"1":"0","parent"=>$l->parent_id);
        }
        return Response::json($data);
    }

    public function setCategory()
    {
        $roots = Category::roots()->get();
        $cArray = array(array('id'=>0,'name'=>'--Ангилал--'));
        
        foreach($roots as $id=>$r){

            $cArray[$id+1]['id']= $r->id;
            $cArray[$id+1]['name']= $r->name;
            $cArray[$id+1]['leaf']= (!$r->isLeaf())?true:false;
        }

        
        //  if (Request::isMethod('post')){
        //       $category_id = sanitize(Input::get('category_id'));
        //       $companyCategory = CompanyCategory::where('category_id','=',$category_id)->where('company_id','=',$id)->first();
        //       $data['same'] = '';
        //       if($companyCategory)
        //       {
        //         $data['same'] = 'like';
        //       }
        //       else 
        //       { 
        //         $category = Category::find($category_id);
        //         $companyCat = new CompanyCategory;
        //         $companyCat->category_id = $category_id;
        //         $companyCat->category_name = $category->name;
        //         $companyCat->company_id = Sentry::getUser()->id;
        //         $companyCat->save();
                
        //         $data['id'] = $companyCat->category_id;
        //         $data['name'] = $companyCat->category_name;
        //       }
        //       return Redirect::to('/profile/'.$id);

        // }else{
         
        // }
         return \View::make('front.profile.createCategory')
                  ->with('cArray', $cArray);  
    }

    public function setCat()
    {
      $category_id = sanitize(Input::get('id'));
      $data = array();
      $companyCategory = CompanyCategory::where('category_id','=',$category_id)->where('company_id','=',Sentry::getUser()->id)->first();
      $data['same'] = '';
      if($companyCategory)
      {
        $data['same'] = 'like';
      } 
      else 
      { 
        $category = Category::find($category_id);
        $companyCat = new CompanyCategory;
        $companyCat->category_id = $category_id;
        $companyCat->category_name = $category->name;
        $companyCat->company_id = Sentry::getUser()->id;
        $companyCat->save();
        
        $data['id'] = $companyCat->category_id;
        $data['name'] = $companyCat->category_name;
        $data['profile_id'] = Sentry::getUser()->id;
      }
      
      return Response::json($data);
    }

    public function removeCategory()
    {
      $category_id = sanitize(Input::get('id'));
      $data = array();
      $companyCategory = CompanyCategory::find($category_id);
      if($companyCategory)
      {
         $data['id'] = $companyCategory->id;
         $companyCategory->delete();
      }
      
      return Response::json($data);
    }

    public function changeVideo()
    {
      $profile_id = sanitize(Input::get('pid'));
      $video_url = sanitize(Input::get('name'));
      $data = array();
      $url_array = parse_url($video_url);
      if (filter_var($video_url, FILTER_VALIDATE_URL) === FALSE) {
        $data['error'] = 'its not a url';
      }
      else 
      if($url_array['host'] != 'www.youtube.com')
      {
        $data['error'] = 'not a youtube url';
      }
      else  
      {
        $profile = Profile::find($profile_id);
        if($profile->video == $video_url)
        {
          $data['same']  = 'like';
        }
        else
        {
          $data['same']  = '';
          $profile->video = $video_url;
          $profile->save();

        }
        
        
        $data['id'] = $profile->user_id;
        $data['name'] = $profile->video;
        $data['error'] = '';
      }

      return Response::json($data);
    }

    public function setCoverStyle($id)
    {
      if(Sentry::check())
      {
        $user_id = Sentry::getUser()->id;
        $profile = Profile::where('user_id','=',$user_id)->first();
        $profile->cover_style = intval($id);
        $profile->save();  
      }
      else
      {
          return Redirect::to('/login');        
      }
      

      return Redirect::to('/profile/'.$user_id);
    }

    public function colorPick()
    {
      $color_code = sanitize(Input::get('color_code'));
      $data = array();      
      $user_id = Sentry::getUser()->id;
      $profile = Profile::where('user_id','=',$user_id)->first();
      $profile->color_code = $color_code;
      $profile->save();
      $data['pid'] = $user_id;

      return Response::json($data);
    }

    public function editTitle()
    {
      $id = sanitize(Input::get('id'));
      $val = sanitize(Input::get('value'));

      $detail = CompanyDetail::find($id);
      $detail->title  = $val;
      $detail->save();
      return $val;

    }

    public function editBody()
    {
      $id = sanitize(Input::get('id'));
      $value = str_replace('script', '', stripslashes(Input::get('value')));
      $value = str_replace('javascript', '', stripslashes($value));

      $detail = CompanyDetail::find($id);
      $detail->value = $value;
      $detail->save();
      return $value;
    }

    public function createsub()
    {
      $profile_id = sanitize(Input::get('pid'));
      $menu_name = sanitize(Input::get('name'));
      $data = array();

      $detail = new CompanyDetail;
      $detail->profile_id = $profile_id;
      $detail->title      = $menu_name;
      $detail->user_id    = Sentry::getUser()->id;
      $detail->save();
      
      $data['id'] = $detail->id;
      $data['name'] = $detail->title;

      return Response::json($data);
    }

    public function removesub()
    {
      $submenu_id = sanitize(Input::get('id'));
      $data = array();      
      $detail = CompanyDetail::find($submenu_id);
      $data['pid'] = $detail->user_id;
      $detail->delete();

      return Response::json($data);
    }

    public function companyRemove()
    {
        $id = Sentry::getUser()->id;
        $profile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
        if($profile->is_company == 1)
        {
          $profile->is_company = 0;
          $profile->save();
        }
        return Redirect::to('/home/'.Sentry::getUser()->id);
    }

    public function company()
    {
        // return Redirect::to('/profile/comingsoon');
        $id = Sentry::getUser()->id;
        $user = User::find(Sentry::getUser()->id);
        $profile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
        if(!$profile){
            $profile = new Profile;
            $profile->user_id = Sentry::getUser()->id;
            $profile->save();
        }
        
        if (Request::isMethod('post'))
        {
            $company_name = cp1251_utf8(sanitize(Input::get('company_name')));
            $about_us = cp1251_utf8(sanitize(Input::get('department')));
            $company_detail = cp1251_utf8(sanitize(Input::get('about_us')));
            if($company_name){
              $profile->is_company = 1;
              $profile->company_name = $company_name;
              $profile->about_us = $about_us;
              if($company_detail)
              {
                $detail = new CompanyDetail;
                $detail->profile_id = $profile->id;
                $detail->user_id = Sentry::getUser()->id;
                $detail->title = 'Бидний тухай';
                $detail->value = $company_detail;
                $detail->save();
              }
            }
            $profile->save();
            
            return Redirect::to('/profile/'.Sentry::getUser()->id);
        }

        $products = DB::table('product')
          ->join('user_state', 'product.id', '=', 'user_state.product_id')
          ->where('user_state.user_id', '=', $id)
          ->paginate(50);
        $counter = Product::where('user_id', $id)->count();
        $state = UserState::where('user_id', '=', $id)->count();
        $banners = null;
        if($profile->is_company){
          $banners = CompanyBanner::where('company_id', '=', $id)->get();
        }
        return \View::make('front.profile.company')
                        ->with('user', $user)
                        ->with('state', $state)
                        ->with('counter', $counter)
                        ->with('profile', $profile)
                        ->with('banners', $banners)
                        ->with('products', $products);

    }

    public function edit()
    {
        $id = Sentry::getUser()->id;
        $user = User::find(Sentry::getUser()->id);
        $countProducts = Product::where('user_id', $id)->get()->count();
        $profile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
        if(!$profile){
            $profile = new Profile;
            $profile->user_id = Sentry::getUser()->id;
            $profile->save();
        }
        if (Request::isMethod('post'))
        {

            // $validation = new ProfileValidator;
            // if ($validation->passes())
            // {
                $user->first_name = cp1251_utf8(sanitize(Input::get('first_name')));
                $user->last_name = cp1251_utf8(sanitize(Input::get('last_name')));
                $user->gender = Input::get('gender');
                $user->year = Input::get('year');
                $user->save();
                $profile->email = cp1251_utf8(sanitize(Input::get('email')));
                $profile->position = cp1251_utf8(sanitize(Input::get('position')));
                $profile->phone = cp1251_utf8(sanitize(Input::get('phone')));
                $profile->address = cp1251_utf8(sanitize(Input::get('address')));
                
                $tmp_banner = Input::file('banner');
                if($tmp_banner){
                    $bannername = md5(uniqid(rand(), true)).'.jpg';
                    $old_name = public_path().'/uploads/profile/'.$profile->banner;
                    $orfile = public_path().'/uploads/profile/' . $bannername;
                        //create orig image
                          $banner = Image::make($tmp_banner);
                          $banner->resize(750, 900, function ($constraint) {
                                       $constraint->aspectRatio();
                                       $constraint->upsize();
                                  });        
                          $banner->save($orfile);
                          @unlink($old_name);
                        $profile->banner = $bannername;
                }
                
                $profile->save();
                Notification::success('Мэдээлэл амжилттай хадгалагдлаа');
                return Redirect::to('/home/'.Sentry::getUser()->id);
            // }
            // else
            // {
            //     return Redirect::back()->withInput()->withErrors($validation->errors);
            // }
        }

    	return \View::make('front.profile.edit')->with('profile', $profile)->with('user', $user)->with('id', $id)->with('countProducts', $countProducts);
    }

    public function saw($id)
    {
      $user = User::find($id);
      //$products  = Product::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(50);
      $products = DB::table('product')
        ->join('user_state', 'product.id', '=', 'user_state.product_id')
        ->where('user_state.user_id', '=', $id)
        ->paginate(50);
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }
      return \View::make('front.profile.saw')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners)
                      ->with('products', $products);
    }

    public function banner(){
      $user = User::find(Sentry::getUser()->id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $banners = CompanyBanner::where('company_id', '=', $user->id)->get();
      $state = UserState::where('user_id', '=', $user->id)->count();
      $counter = Product::where('user_id', $user->id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $user->id)->get();
      }
      return \View::make('front.profile.banner')
                      ->with('banners', $banners)
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile);
    }

    public function bannernew(){
      $user = User::find(Sentry::getUser()->id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $state = UserState::where('user_id', '=', $user->id)->count();
      $counter = Product::where('user_id', $user->id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $user->id)->get();
      }
      if(Request::isMethod('post')){
        $rules = array(
          'image' => 'required|image|mimes:jpg,jpeg,png,bmp,gif'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
        return Redirect::back()->withInput()->withErrors($validator->errors());
        } else
        {
          $cBanner = new CompanyBanner;
          $cBanner->company_id = $user->id;
          $cBanner->link = Input::get('link');

            $tmp_file = Input::file('image');
            if($tmp_file){
                $filename = md5(uniqid(rand(), true)).'.jpg';
                $orig_file = public_path().'/uploads/company/' . $filename;
                    //create orig image
                      $image = Image::make($tmp_file);
                      $image->resize(720, 280, function ($constraint) {
                                   $constraint->aspectRatio();
                                   $constraint->upsize();
                              });        
                      $image->save($orig_file);
                    $cBanner->image = $filename;
            }
            $cBanner->save();
            return Redirect::to('/profile/banner');
        }

      }
      return \View::make('front.profile.bannernew')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('banners', $banners)
                      ->with('profile', $profile)
                      ->with('error', null);
    }

    public function bannerdelete($id){
      $user = User::find(Sentry::getUser()->id);
      $banner = CompanyBanner::find($id);
      if($user->id == $banner->company_id){
        $old_name = public_path().'/uploads/company/'.$banner->image;
        @unlink($old_name);
        $banner->delete();
      }
      return Redirect::to('/profile/banner');
    }

    public function info($id){
      $user = User::find($id);
      
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }
      return \View::make('front.profile.info')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function crop(){
      $id = Sentry::getUser()->id;
      $user = User::find($id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }

      if(Request::isMethod('post')){
        $filename = md5(uniqid(rand(), true)).'.jpg';
        $targ_w = $targ_h = 150;
        $jpeg_quality = 100;
        $imagename = Input::get('filename');
        
        $src = public_path().'/fileupload/uploads/'.$imagename;
        $newFile = public_path().'/uploads/profile/'.$filename;

       $type = exif_imagetype($src); // [] if you don't have exif you could use getImageSize()
        $allowedTypes = array(
            1,  // [] gif
            2,  // [] jpg
            3,  // [] png
            6   // [] bmp
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $img_r = imageCreateFromGif($src);
            break;
            case 2 :
                $img_r = imageCreateFromJpeg($src);
            break;
            case 3 :
                $img_r = imageCreateFromPng($src);
            break;
            case 6 :
                $img_r = imageCreateFromBmp($src);
            break;
        }    
        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

        imagecopyresampled($dst_r,$img_r,0,0,Input::get('x'), Input::get('y'), $targ_w,$targ_h, Input::get('w'), Input::get('h'));
        
        imagejpeg($dst_r, $newFile, $jpeg_quality);
        @unlink($src);
        $profile->image = $filename;
        $profile->save();
        return Redirect::to('/home/'.$id);
      }

      return \View::make('front.profile.crop')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function banners(){
      $id = Sentry::getUser()->id;
      $user = User::find($id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }

      if(Request::isMethod('post')){
        $filename = md5(uniqid(rand(), true)).'.jpg';
        $pos = intval(Input::get('pos'));
        $cover_image_type = CoverImageType::find($pos);
        $targ_w = intval($cover_image_type->width);
        $targ_h = intval($cover_image_type->height);

        $jpeg_quality = 100;
        $imagename = Input::get('filename'.$pos);
        $src = public_path().'/fileupload/uploads/'.$imagename;
        $newFile = public_path().'/uploads/company/'.$filename;
        
        $type = exif_imagetype($src); // [] if you don't have exif you could use getImageSize()
        $allowedTypes = array(
            1,  // [] gif
            2,  // [] jpg
            3,  // [] png
            6   // [] bmp
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $img_r = imageCreateFromGif($src);
            break;
            case 2 :
                $img_r = imageCreateFromJpeg($src);
            break;
            case 3 :
                $img_r = imageCreateFromPng($src);
            break;
            case 6 :
                $img_r = imageCreateFromBmp($src);
            break;
        }
        
        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

        imagecopyresampled($dst_r,$img_r,0,0,Input::get('x'.$pos), Input::get('y'.$pos), $targ_w,$targ_h, Input::get('w'.$pos), Input::get('h'.$pos));
        
        imagejpeg($dst_r, $newFile, $jpeg_quality);
        @unlink($src);
        $cBanner = CompanyBanner::where('company_id','=',$id)->where('image_type_id','=',$pos)->first();
        if($cBanner)  
        {
          $oldFile = public_path().'/uploads/company/'.$cBanner->image;
          @unlink($oldFile);
          $cBanner->company_id = $user->id;
          $cBanner->image = $filename;
          $cBanner->save();
        }
        else
        {
          $cBanner = new CompanyBanner;
          $cBanner->company_id = $user->id;
          $cBanner->image = $filename;
          $cBanner->image_type_id = $pos;
          $cBanner->save();
        }

        return Redirect::to('/profile/'.$id);
      }

      return \View::make('front.profile.crop')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function companyuser(){
      $id = Sentry::getUser()->id;
      $user = User::find($id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      $users = null;
      if($profile->is_company){
        $cover_array = array();
        $cover_styles = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
        foreach($cover_styles as $c)
        {
          $cover_array[] = $c->id;
        }

        $banners = CompanyBanner::where('company_id', '=', $id)->whereIn('image_type_id',$cover_array)->get();
        //
        $users = CompanyUser::where('company_id', '=', $id)->get();
        //
      }

      return \View::make('front.profile.users')
                      ->with('user', $user)
                      ->with('users', $users)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function suggest()
    {
      $max_length = 30;
      $q = addslashes (Input::get('keyword'));
      $keyword = mb_substr($q, 0, $max_length);
      $data = array();

      $users = User::where('email', 'LIKE', $keyword.'%')->take(10)->get();
      foreach ($users as $cuser) {
        $data[] = $cuser->email;
      }
      return Response::json($data);
    }

    public function adduser(){
      $id = Sentry::getUser()->id;
      $email = Input::get('email');
      $user = User::where('email', '=', $email)->first();
      if($user){
        $companyUser = CompanyUser::where('company_id', '=', $id)->where('user_id', '=', $user->id)->first();
        if(!$companyUser){
          $companyUser = new CompanyUser;
          $companyUser->company_id = $id;
          $companyUser->user_id = $user->id;
          $companyUser->save();
        }
      }
      return \View::make('front.profile._user')
                ->with('user', $user);
    }

    public function deleteuser($id){
      $company_id = Sentry::getUser()->id;
      $user = User::find($company_id);
      $companyUser = CompanyUser::where('company_id', '=', $company_id)->where('user_id', '=', $id)->first();
      if($companyUser){
        $companyUser->delete();
      }
      return Redirect::to('/profile/user');
    }

    public function feedback($id){

      //$id = Sentry::getUser()->id;
      $user = User::find($id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      if(!$profile->is_company){
        App::abort(404);
      }  
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      $feedbacks = array();
      if($profile->is_company){
        $cover_array = array();
        $cover_styles = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
        foreach($cover_styles as $c)
        {
          $cover_array[] = $c->id;
        }

        $banners = CompanyBanner::where('company_id', '=', $id)->whereIn('image_type_id',$cover_array)->get();
        //
        $feedbacks = CompanyFeedback::where('company_id', '=', $id)->get();
        //
      }

      return \View::make('front.profile.feedback')
                      ->with('user', $user)
                      ->with('feedbacks', $feedbacks)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function savefeed(){

      $company_id = cp1251_utf8(sanitize(Input::get('objId')));
      $profile = Profile::where('user_id', '=', $company_id)->first();
      if($profile){
        $content = cp1251_utf8(sanitize(Input::get('content')));
        $user_name = 'Зочин';
        if(Sentry::check()){
          if(Sentry::getUser()->id == $company_id){
            
            $user_name = $profile->company_name;
          }else{
            $loggedUser = User::find(Sentry::getUser()->id);
            $user_name = $loggedUser->first_name;
          }
        }
        $ipAddress = Request::getClientIp();
        $comFeed = new CompanyFeedback;
        $comFeed->company_id = $company_id;
        $comFeed->content = $content;
        $comFeed->created_at = date('Y-m-d H:i:s', time());
        $comFeed->is_new = 1;
        $comFeed->user_name = $user_name;
        $comFeed->ip_address = $ipAddress;
        $comFeed->save();

        if(Sentry::getUser()->id != $company_id)
        {
          $notify = Notify::where('user_id','=',$company_id)->where('object_type','=',5)->where('is_read','=',0)->first();
          if (!$notify) 
          {
              $not = new Notify;
              $not->user_id = $company_id;
              $not->from_id = (Sentry::check())?Sentry::getUser()->id:0;
              $not->object_id = $company_id;
              $not->object_type = 5;
              $not->is_read = 0;
              $not->save();
          }
        }  
      }
      return Redirect::to('/profile/feedback/'.$company_id);
    }

    public function deletefeed(){
      $feed = Input::get('id');
      $comFeed = CompanyFeedback::find($feed);
      if(Sentry::getUser()->id == $comFeed->company_id){
        $comFeed->delete();
      }
      $data = array(true);
      return Response::json($data);
    }

    public function expired(){
      $id = Sentry::getUser()->id;
      $user = User::find($id);
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      $exProducts = null;
      if($profile->is_company){
        $cover_array = array();
        $cover_styles = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
        foreach($cover_styles as $c)
        {
          $cover_array[] = $c->id;
        }

        $banners = CompanyBanner::where('company_id', '=', $id)->whereIn('image_type_id',$cover_array)->get();
        //
        $exProducts = Product::where('user_id', $id)->where('is_active', '=', 0)->get();
        //
      }

      return \View::make('front.profile.expired')
                      ->with('user', $user)
                      ->with('exProducts', $exProducts)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function follow(){
      $company_id = Input::get('company_id');
      $profile = Profile::where('user_id', '=', $company_id)->first();
      $html = '<i class="fa fa-minus-circle" style="padding-right: 4px;"></i>Дагасан';
      if($profile && Sentry::getUser()->id){
        $follow = CompanyFollow::where('company_id', '=', $company_id)->where('user_id', '=', Sentry::getUser()->id)->first();
        if($follow){
          $html = '<i class="fa fa-plus-circle" style="padding-right: 4px;"></i>Дагах';
          $follow->delete();
        }else{
          $fol = new CompanyFollow;
          $fol->company_id = $company_id;
          $fol->user_id = Sentry::getUser()->id;
          $fol->save();
        }
      }
      $comFol = CompanyFollow::where('user_id', '=', Sentry::getUser()->id)->first();
      if($comFol){
        Session::put('isFollow', true);
      }else{
        Session::put('isFollow', false);
      }
      return $html;
    }

    public function content($id){
      $company_detail = CompanyDetail::find($id);
      if(!$company_detail)
          App::abort(404);
      $user = User::find($company_detail->user_id);
      
      $counter = Product::where('user_id', $user->id)->count();
      $profile = Profile::where('user_id', '=', $user->id)->first();
      $state = UserState::where('user_id', '=', $user->id)->count();
      $banners = null;
      if($profile->is_company){
        $cover_array = array();
        $cover_styles = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
        foreach($cover_styles as $c)
        {
          $cover_array[] = $c->id;
        }

        $banners = CompanyBanner::where('company_id', '=', $user->id)->whereIn('image_type_id',$cover_array)->get();
      }
      return \View::make('front.profile.content')
                      ->with('user', $user)
                      ->with('detail', $company_detail)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function news($id){
      $user = User::find($id);
      
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }
      return \View::make('front.profile.news')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners);
    }

    public function showcompany($slug){
      $profile = Profile::where('company_name', '=', $slug)->first();
      
      if(!$profile)
        App::abort(404);
      $id = $profile->user_id;
      $user = User::find($id);
      if(!$user)
        App::abort(404);
      $products  = Product::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(20);
      $counter = Product::where('user_id', $id)->count();
      //$profile = Profile::where('user_id', '=', $id)->first();
      if(!$profile)
        App::abort(404);
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      $viewType = Input::get('viewType', Session::get('viewType', 'grid'));
      Session::put('viewType', $viewType);
      $page = Input::get('page', 1);
      if($profile->is_company){
        $banners = CompanyBanner::where('company_id', '=', $id)->get();
      }
      return \View::make('front.product.profile')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners)
                      ->with('page', $page)
                      ->with('viewType', $viewType)
                      ->with('products', $products);
    }

    public function changePass(){
      return \View::make('front.profile.changepass')
                      ->with('user', 1);
    }

    public function setPass(){
      $id = Sentry::getUser()->id;
      $user = Sentry::findUserById($id);
      if( $user->checkPassword( Input::get( 'password' ) ) && Input::get( 'new_password' ) == Input::get( 'comfirm_password' ))
      {
          $user->password = Input::get( 'new_password' );
          $user->save();

          return Response::json('success');
      }
      else
      {
          return Response::json('error');
      }
    }

    public function comingsoon(){
      return \View::make('front.profile.comingsoon');
    }
    
}