<?php namespace App\Controllers\Front;

use App\Models\User;
use App\Models\Profile;
use App\Models\Chats;
use App\Models\Notify;
use App\Models\sfUser;
use App\Models\CoverImageType;
use App\Services\Validators\ProfileValidator;
use OAuth, Input, Request, Redirect, Sentry, Notification, FacebookConnect, Session, Image, DB, Validator, Response,SEOMeta, App;

class NotifyController extends \FrontBaseController {
    
    public function readed($id){
        $notify = Notify::find($id);
        if($notify)
        {
            if($notify->object_type == 1)
            {
                $chat_notify = Notify::getNotifyForUser($notify->user_id, 1, 0);
                foreach($chat_notify as $cn)
                {
                    $cn->is_read = 1;
                    $cn->save();        
                }    
            }
            else
            {
                $notify->is_read = 1;
                $notify->save();    
            }
        }
        return 'success';
    }

    public function checkReadedNotify($user_id){
        $chat_notify = Notify::getNotifyForUser($user_id, 1, 0);
        
        if($chat_notify)
        {
            $chat = Chats::where('to_user_id','=',$user_id)->orderBy('created_at','desc')->first();
            foreach($chat_notify as $cn)
            {
                if($chat)
                {
                    if($chat->created_at >= $cn->created_at)
                    {
                        $cn->is_read = 1;
                        $cn->save();               
                    } 
                }    
            }
        }
        return 'success';
    }
    
}