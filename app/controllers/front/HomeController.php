<?php namespace App\Controllers\Front;
use App\Models\Category;
use App\Models\Product;
use App\Models\UserCategory;
use App\Models\UserKeyword;
use App\Models\UserMail;
use App\Models\Profile;
use App\Services\Validators\MailValidator;
use SEOMeta, OpenGraph, Input, Request, Response, Session, App, Notification, Redirect,Cache;

class HomeController extends \FrontBaseController {

    public function index()
    {
        OpenGraph::addImage($_ENV['baseurl'].'/images/default_og_image.jpg');
        return \View::make('front.home.index');
    }

    private function setSessionEmail($email){
        Session::set('email', $email);
    }

    private function subscribeWord($keyword) {
    $words = Session::get('swords', array());
        $keyword = mb_strtolower($keyword, 'UTF-8');
          $terms = array_keys(textAnalyze($keyword));
          // sort words
          sort($terms);
          // keyword
          $keyword = join(' ', $terms);
        $words[$keyword] = $keyword;
        Session::set('swords', $words);
    }

    private function unsubscribeWord($word) {
    $words = Session::get('swords', array());
    $terms = array_keys(textAnalyze($word));
    // sort words
    sort($terms);
    $word = mb_strtolower(join(' ', $terms), 'UTF-8');
    $words[$word] = time();
    Session::set('swords', $words);
    return $words;
    }

    private function subscribeCid($id) {
        $cids = Session::get('scids', array());
        $cids[$id] = $id;
        Session::set('scids', $cids);
    }    

    private function unsubscribeCid($cid) {
        $cids = Session::get('scids', array());
        $cids[$cid] = time();
        Session::set('scids', $cids);
    }

    public function subCategory($id){

        $category = Category::find($id);
        
    	if (Request::isMethod('post'))
        {
            $data = array('errors' => array());
            $email = Input::get('email');
            $regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
            if(!preg_match($regex, $email)) {
                $data['errors'][] = 'email';
                return Response::json($data);
            }
            else {
                UserCategory::subscribe($email, $id, Input::get('is_daily'));
                $this->setSessionEmail($email);
                $this->subscribeCid($id);
                return Response::json($data);
            }
        }else{
            return \View::make('front.home.subcategory')->with('category', $category)->with('class', 'input-email');
        }
    }

    public function subKeyword(){
        $keyword = Input::get('keyword');

    	if (Request::isMethod('post'))
        {
            $data = array('errors' => array());
            $email = Input::get('email');
            $regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
            if(!preg_match($regex, $email)) {
                $data['errors'][] = 'email';
                return Response::json($data);
            }else{
                UserKeyword::subscribe($email, $keyword, Input::get('is_daily'));
                $this->setSessionEmail($email);
                $this->subscribeWord($keyword);
                return Response::json($data);
            }
        }else{
            return \View::make('front.home.subkeyword')->with('keyword', $keyword)->with('class', 'input-email');
        }
    }

    public function category() {
    // user id
    $user = UserMail::find(Input::get('id'));
    $hash = Input::get('hash');

    // blah blah
    if(!$user)
    App::abort(404);

    // checking hash blah blah
    if(md5($user->email) !== $hash)
        App::abort(404);

    // 
    $list = UserCategory::listus($user->id);

    // request is post
    if (Request::isMethod('post')){
      // is_daily
      $daily = (array) Input::get('daily', array());

      // all cids
      $cids = array();
      foreach ($list as $record) {
        $cids[] = $record->category_id;
      }
      // 
      $cids_against = array_filter((array) Input::get('cids', array()));

      // =================
      // ==  subscribe ===
      // =================
      $cids_intersect = array_intersect($cids, $cids_against);

      foreach ($cids_intersect as $cid) {
        $is_daily = isset($daily[$cid]) ? ((bool) $daily[$cid]) : true;
        UserCategory::subscribe($user->email, $cid, $is_daily);
        // subscribe cid
        $this->subscribeCid($cid);
      }

      // ===================
      // ==  unsubscribe ===
      // ===================
      $cids_diff = array_diff($cids, $cids_against);
      foreach ($cids_diff as $cid) {
        UserCategory::unsubscribe($user->email, $cid);
        
        $this->unsubscribeCid($cid);
      }
      Notification::success('Амжилттай хадгаллаа');
      return Redirect::to('/');
    }

    return \View::make('front.home.category')->with('user', $user)->with('list', $list)->with('hash', $hash);    
  }

  public function keyword() {
    // user id
    $user = UserMail::find(Input::get('id'));
    $hash = Input::get('hash');
    if(!$user)
    App::abort(404);
    if(md5($user->email) !== $hash)
        App::abort(404);

    $this->list = UserKeyword::where('user_id', $user->id)->get();


    // request is post
    if (Request::isMethod('post')){
      // is_daily
      $daily = (array) Input::get('daily', array());

      // all ids
      $ids = array();
      foreach ($this->list as $record) {
        $ids[] = $record->id;
      }

      // 
      $ids_against = array_filter((array) Input::get('ids', array()));

      // ==================
      // === subscribe ====
      // ==================

      $ids_intersect = array_intersect($ids, $ids_against);

      foreach ($ids_intersect as $id) {
        $is_daily = isset($daily[$id]) ? ((bool) $daily[$id]) : true;
        $text = UserKeyword::updates($id, $is_daily);
        // subscribe word
        $this->subscribeWord($text);
      }

      // ===================
      // ==  unsubscribe ===
      // ===================
      $ids_diff = array_diff($ids, $ids_against);
      foreach ($ids_diff as $id) {
        // unsubscribe
        $text = UserKeyword::unsubscribe($id);
        // session
        $this->unsubscribeWord($text);
      }

      Notification::success('Амжилттай хадгаллаа');
      return Redirect::to('/');
    }
    return \View::make('front.home.keyword')->with('user', $user)->with('list', $this->list)->with('hash', $hash);    
  }

  public function rate()
  {
    $product_id = Input::get('product_id');
    $star    = Input::get('star');
    $star = (int) ($star > 0 && $star < 6) ? $star : 3;

    $product = Product::find($product_id);
    
    if (!$product)
    {
      App::abort(404);
    }
    $is_rated = Session::get('rate_'.$product_id, false);

    //news rate ogoogyu bol
    if (!$is_rated)
    {
      $product->nb_vote = (int) ($product->nb_vote + 1);
      $product->nb_totalvote = (int) ($product->nb_totalvote + $star);
      $product->save();

      //Cookie-d rate ugsun utga hadgalna
      Session::set('rate_'.$product_id, true, time() + 60 * 60 * 24);
    }
    
    return \View::make('front.home.rate')->with('product', $product)->with('is_rate', true);    
  }

  public function company() {

    $companies = Profile::where('is_company','=',1)->where('is_paid','=',1)->paginate(10);

    return \View::make('front.home.company')->with('companies', $companies);   
  }
}