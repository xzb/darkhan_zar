<?php namespace App\Controllers\Front;
 
use App\Models\Category;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\Banner;
use App\Models\AttributeValues;
use App\Models\sfUser;
use Response, App, Input, Session, Redirect, OpenGraph, SEOMeta, Request, View, Cookie, Cache;

class AutoHomeController extends \FrontBaseController {

    public function index() {

        // -------------old home page

        // $attribute2 = AttributeValues::getValuesByAtt(105);
        // $banners = Banner::getBannersByLocation(9);
        // return \View::make('front.auto.index')
        //                 ->with('coworkers', $banners)
        //                 ->with('attr2', $attribute2);
        $id = 19;
        $avtexts = Session::get('avtexts', array());
        $avalids = Session::get('avalids', array());
        $productsArray = array();
        if($id == 11 || $id == 966)
            App::abort(404);
        $category = Category::find($id);
        if(!$category)
            App::abort(404);
        $date = date('Y-m-d', time());
        

        OpenGraph::addImage($_ENV['baseurl'].'/images/default_og_image.jpg');
        if($category->seo_title){
          SEOMeta::setTitle($category->seo_title);  
        }else{
          SEOMeta::setTitle($category->name);
        }
        if($category->seo_description){
          SEOMeta::setDescription($category->seo_description);  
          SEOMeta::setKeywords($category->seo_description);  
        }
        $parents = $category->getAncestors();
        $parentsIds = array(); $parentsIds[] = $category->id;
        foreach ($parents as $key => $value) {
            $parentsIds[] = $value->id;
          }

        $sortType = Input::get('sortType', 'date_desc');
        $between  = array($category->lft, $category->rgt);
        $avalids = Session::get('avalids', array());
        $pos = strpos(Request::url(), 'auto.');
        if ($pos == true && (in_array(21, $parentsIds) || in_array(151, $parentsIds))){
          
          $productsArray = array();
          $i= 0;
          $return = array('0'=>'');

          if(!empty($avalids))
          {
            foreach($avalids as $a)
            {
                $productsArray[$i] = ProductAttribute::getProductIdsByAttrVal($a);
                $i ++;
            }
            
            $return = ProductController::getIntersect($productsArray);
          }
          $query = Product::join('category', function($join) {
                $join->on('product.category_id', '=', 'category.id');
              });
          $query2 = Product::join('category', function($join) {
                $join->on('product.category_id', '=', 'category.id');
              });
          if(!empty($return[0]))
          {
              $query 
              ->whereBetween('category.lft', $between)
              ->select('product.id', 'product.name', 'product.image_filename', 'product.contact', 'product.category_id', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
              ->where('product.is_active', '=', 1)
              ->where('product.is_featured', '=', 0)
              ->whereIn('product.id',$return);

              $query2 
              ->whereBetween('category.lft', $between)
              ->select('product.id', 'product.name', 'product.image_filename', 'product.contact', 'product.category_id', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
              ->where('product.is_active', '=', 1)
              ->where('product.is_featured', '=', 1)
              ->whereIn('product.id',$return);  
          }
          else
          {
            if(!empty($avalids))
            {
              $query ->whereRaw('1=2');  
              $query2 ->whereRaw('1=2');  
            }  
            else
            {
              $query
              ->whereBetween('category.lft', $between)
              ->select('product.id', 'product.name', 'product.image_filename','product.category_id',  'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
              ->where('product.is_active', '=', 1)
              ->where('product.is_featured', '=', 0);  

              $query2
              ->whereBetween('category.lft', $between)
              ->select('product.id', 'product.name', 'product.image_filename','product.category_id',  'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
              ->where('product.is_active', '=', 1)
              ->where('product.is_featured', '=', 1);  
            }  

          }

          
          if(array_key_exists('price',$avtexts))
          {
            switch ($avtexts['price']) {
              case '1':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) < ?', array(5));
                break;
              case '2':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) between ? and ?', array(5, 10));
                break;
              case '3':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) between ? and ?', array(10, 20));
                break;
              case '4':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) between ? and ?', array(20, 30));
                break; 
              case '5':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) between ? and ?', array(30, 50));
                break;
              case '6':
                  $query->whereRaw('CAST(product.price AS DECIMAL(10,3)) > ?', array(50));
                break;
            }
          }
                  
          

          
        }else{
          $query = Product::join('category', function($join) {
            $join->on('product.category_id', '=', 'category.id');
        })
        ->whereBetween('category.lft', $between)
        ->select('product.id', 'product.name', 'product.category_id', 'product.image_filename', 'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
        ->where('product.is_active', '=', 1)
        ->where('product.is_featured', '=', 0);

        $query2 = Product::join('category', function($join) {
            $join->on('product.category_id', '=', 'category.id');
        })
        ->whereBetween('category.lft', $between)
        ->select('product.id', 'product.name', 'product.category_id', 'product.image_filename', 'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str')
        ->where('product.is_active', '=', 1)
        ->where('product.is_featured', '=', 1);
        }

        if($sortType == 'year_asc' || $sortType == 'year_desc' || array_key_exists('make_year',$avtexts) || array_key_exists('cc',$avtexts))
        {
          $query->join('product_attribute', function($join) {
                  $join->on('product.id', '=', 'product_attribute.product_id');
              });
          
          if($sortType == 'year_asc' || $sortType == 'year_desc'){
            $query->where('product_attribute.attribute_id','=',108);  
          }
          else if(array_key_exists('make_year',$avtexts))
          {
            // $query->where('product_attribute.attribute_id','=',107);  
            // switch ($avtexts['make_year']) {
            //   case '1':
            //       $query->where('product_attribute.attribute_value','<=', 1990);
            //     break;
            //   case '2':
            //       $query->whereBetween('product_attribute.attribute_value', array(1990, 2000));
            //     break;
            //   case '3':
            //       $query->whereBetween('product_attribute.attribute_value', array(2000, 2010));
            //     break;
            //   case '4':
            //           $query->where('product_attribute.attribute_value','>=', 2010);
            //     break; 
            // }
          }
          else if(array_key_exists('cc',$avtexts))
          {
            $query->where('product_attribute.attribute_id','=',102);
            switch ($avtexts['cc']) {
                  case '1':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) < ?',array(1));
                    break;
                  case '2':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) between ? and  ?', array(1.0, 1.5));
                    break;
                  case '3':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) between ? and  ?',array(1.6, 2.0));
                    break;
                  case '4':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) between ? and  ?', array(2.1, 3.0));
                    break; 
                  case '5':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) between ? and  ?', array(3.1, 4.0));
                    break;
                  case '6':
                      $query->whereRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,3)) > ?',array(4));
                    break;
            }
          }  

          

          if($sortType == 'year_asc')
          {
            $query->orderByRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,4)) asc');    
          } 
          elseif($sortType == 'year_desc')
          {
            $query->orderByRaw('CAST(product_attribute.attribute_value AS DECIMAL(10,4)) desc');    
          }

        }

        // $query  ->orderBy('product.is_featured', 'desc')
        //             ->orderBy('product.featured_start_date', 'asc');  
        $query->orderBy('product.created_at', 'desc');
        if($sortType == 'date_asc'){
          $query = $query->orderBy('product.created_at', 'asc');
        }elseif($sortType == 'date_desc'){
          $query = $query->orderBy('product.created_at', 'desc');
        }elseif($sortType == 'price_asc'){
          $query = $query->orderBy('product.price', 'asc');
        }elseif($sortType == 'price_desc'){
          $query = $query->orderBy('product.price', 'desc');
        }
        $products = $query->paginate(50);

        $query2->orderByRaw("RAND()");
        $featuredProducts = $query2->get();

        View::share('cidd', $category->id);
        $plists = array();
        foreach($products as $p){
          $plists[] = $p->id; 
        }

        Cookie::queue('cate_id', $id, 10);
        Cache::put('id'.$category->id, $plists, 10);
        
        $pos = strpos(Request::url(), 'auto.');
        if ($pos === false){
          return \View::make('front.product.category')
            ->with('category', $category)
            ->with('products', $products);
        }else{
          if (Request::isMethod('post')) {
            $stockAttributes = Attribute::where('is_stock', '=', 1)->where('id', '<', 110)->get();
            return \View::make('front.auto._list2')
                ->with('stockAttributes', $stockAttributes)
                ->with('featuredProducts', $featuredProducts)
                ->with('products', $products);
          }
          else
          {
            $parentCates = array('21'=>'Суудлын машин','151'=>'Жийп','22'=>'Ачааны машин','218'=>'Автобус, микро','23'=>'Мотоцикл, мопед','24'=>'Машин механизм',
                                  '25'=>'Сэлбэг','2151'=>'Солино','2156'=>'Түрээслэнэ','2155'=>'Түрээслүүлнэ','2152'=>'Авна','2333'=>'Дугуй');

            $filterArray = array('selectbox', 'checkbox', 'radio');
            $attributes = Attribute::where('is_filterable', '=', 1)->whereIn('type', $filterArray)->where('id', '<', 110)->get();
            $attributesText = Attribute::where('is_filterable', '=', 1)->where('type','=', 'textbox')->where('id', '<', 110)->get();
            $stockAttributes = Attribute::where('is_stock', '=', 1)->where('id', '<', 110)->get();
            //$attribute = Attribute::find($aid);
            return \View::make('front.auto.category')
              ->with('category', $category)
              ->with('sortType', $sortType)
              ->with('parents', $parents)
              ->with('parentsIds', $parentsIds)
              ->with('avalids', $avalids)
              ->with('avtexts', $avtexts)
              ->with('attributes', $attributes)
              ->with('attributesText', $attributesText)
              ->with('stockAttributes', $stockAttributes)
              ->with('parentCates', $parentCates)
              ->with('featuredProducts', $featuredProducts)
              ->with('products', $products);
          }
        }

    }

    public function autoHomeSearch() {
        $category = intval(Input::get('category_id'));
        $attr1 = Input::get('attr_1');
        $attr2 = Input::get('attr_2');
        $avalids = array();
        Session::forget('avalids');
        if($attr1 != 0)
        {
            $avalids[101] = $attr1;
        }  
        if($attr2 != 0)
        {
          $avalids[105] = $attr2;
        } 
        Session::put('avalids', $avalids);

        if($category == 0)
          $category = 19;

        return Redirect::to('c/'.$category);
    }

    public function toggle() {
      $pid = (int) Input::get('pid');

      // check product is exist
      $product = Product::find($pid);

      if ($product) {
        if (sfUser::hasAuto($pid)) {
          // delete interest
          sfUser::deleteAuto($pid);
        } else {
          // add interest
          sfUser::addAuto($pid);
        }
      }

      return count(sfUser::getAuto());
    }

    public function compare(){
      $limit = 4;
      $ids = explode("-", Input::get('ids', array()), $limit);

      $productAttributes = array();
      $products   = Product::whereIn('id', $ids)->take($limit)->get();
      //
      if(!count($products))
            App::abort(404);
      $category = Category::find($products[0]->category_id);
      $parent_categories = $category->getAncestors();
      
      $pos = strpos(Request::url(), 'auto.');
        
        if($pos !== false){
          $attributes = Attribute::where('is_main', '=', 1)->where('id', '<', 110)->get();
            return \View::make('front.auto.compare')
                         ->with('products', $products)
                         ->with('attributes', $attributes)
                         ->with('parents', $parent_categories)
                         ->with('category', $category);
        }
        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $attributes = Attribute::where('is_main', '=', 1)->where('id', '>', 110)->get();
            return \View::make('front.property.compare')
                         ->with('products', $products)
                         ->with('attributes', $attributes)
                         ->with('parents', $parent_categories)
                         ->with('category', $category);
        }
      
    }

    public function catlist(){
      $cid = Input::get('cid');
      $aid = Input::get('aid');
          if($aid){
            $attValue = AttributeValues::find($aid);
            $att_id = Session::get('att'.$attValue->attribute_id, null);
            if($att_id == $aid){
              Session::put('att'.$attValue->attribute_id, null);
              $avalids = Session::get('avalids', array());
              if (isset($avalids[$attValue->attribute_id])) {
                unset($avalids[$attValue->attribute_id]);
              }
              Session::put('avalids', $avalids);
            }else{
              Session::put('att'.$attValue->attribute_id, $aid);
              $avalids = Session::get('avalids', array());
              $avalids[$attValue->attribute_id] = $aid;
              Session::put('avalids', $avalids);
            }
          }
          $avalids = Session::get('avalids', array());
          
      return Response::json($avalids);
    }

    public function autolistText(){
      $cid = Input::get('cid');
      $aid = Input::get('aid');
      $name = Input::get('name');
      $avalids = Session::get('avtexts',array());
            if(array_key_exists($name, $avalids))
            {
                if (isset($avalids[$name])) {
                  unset($avalids[$name]);
                }
              Session::put('avtexts', $avalids);
            }else{
              
              $avalids[$name] = $aid;
              Session::put('avtexts', $avalids);
            }
          
          $avalids = Session::get('avtexts', array());
      
      return Response::json($avalids);
    }

    public function carlist(){
      $id = Input::get('cid');
      $avalids = Session::get('avalids', array());

      $category = Category::find($id);
        if(!$category)
            App::abort(404);

        $sortType = Input::get('sortType', 'date_desc');
        $between  = array($category->lft, $category->rgt);
      $query = Product::join('category', function($join) {
        $join->on('product.category_id', '=', 'category.id');
      })
      ->whereBetween('category.lft', $between)
      ->select('product.id', 'product.name', 'product.image_filename', 'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description')
      ->where('product.is_active', '=', 1)
      ->orderBy('product.is_featured', 'desc')
      ->orderBy('product.featured_start_date', 'asc');

      if($sortType == 'date_asc'){
          $query = $query->orderBy('product.created_at', 'asc');
        }elseif($sortType == 'date_desc'){
          $query = $query->orderBy('product.created_at', 'desc');
        }elseif($sortType == 'price_asc'){
          $query = $query->orderBy('product.price', 'asc');
        }elseif($sortType == 'price_desc'){
          $query = $query->orderBy('product.price', 'desc');
        }
        $products = $query->paginate(2);
        $stockAttributes = Attribute::where('is_stock', '=', 1)->get();
        return \View::make('front.auto._list2')
            ->with('stockAttributes', $stockAttributes)
            ->with('products', $products);
    }

    public function resetall(){
      Session::forget('avalids');
      Session::forget('avtexts');
      Session::forget('price_min');
      Session::forget('price_max');
      $avalids = Session::get('avalids', array());
      return Response::json($avalids);
    }
   
}