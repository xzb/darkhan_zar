<?php namespace App\Controllers\Front;

use App\Models\Page;
use App\Models\Category;
use App\Models\Product;
use App\Models\Productimage;
use App\Services\Validators\ProductValidator;
use App\Services\Validators\AutoProductValidator;
use App\Services\Validators\PropertyProductValidator;
use App\Models\ProductWord;
use App\Models\SearchWord;
use App\Models\Search;
use App\Models\Banner;
use App\Models\CategoryBanner;
use App\Models\User;
use App\Models\PaymentOrder;
use App\Models\OrderItems;
use App\Models\UserState;
use App\Models\Profile;
use App\Models\CompanyBanner;
use App\Models\CompanyUser;
use App\Models\CompanyFollow;
use App\Models\Prices;
use App\Models\sfUser;
use App\Models\Attribute;
use App\Models\AttributeValues;
use App\Models\CategoryAttribute;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductComment;
use App\Models\ProductSale;
use App\Models\CoverImageType;
use App\Models\CategoryStats;
use App\Models\Notify;
use App\Models\UserInterest;
use App\Models\UsersGroup;
use App\Models\ProfilePrice;
use App\Models\Location;

use Response, App, SEOMeta, OpenGraph, DB,
    Paginator, Input, Session, Redirect, Image, 
    Request, View, Notification, Cookie, Sentry, Cache, Config, URL;


class ProductController extends \FrontBaseController {

    public function savefeed(){

      $product_id = cp1251_utf8(sanitize(Input::get('objId')));
      $product_user_id = cp1251_utf8(sanitize(Input::get('product_user_id')));
      $parent_id = cp1251_utf8(sanitize(Input::get('parent_id')));
      
        $content = cp1251_utf8(sanitize(Input::get('content')));
        $user_name = 'Зочин';
        if(Sentry::check()){
          $loggedUser = User::find(Sentry::getUser()->id);
          $user_name = $loggedUser->first_name;
        }
        $ipAddress = Request::getClientIp();
        $comFeed = new ProductComment;
        $comFeed->product_id = $product_id;
        $comFeed->content = $content;
        $comFeed->created_at = date('Y-m-d H:i:s', time());
        $comFeed->username = $user_name;
        $comFeed->user_id = $loggedUser->id;
        $comFeed->ip_address = $ipAddress;
        $comFeed->parent_id = $parent_id;
        $comFeed->save();

        if($product_user_id != 0 && $product_user_id != Sentry::getUser()->id)
        {
          $notify = Notify::where('user_id','=',$product_user_id)->where('object_type','=',4)->where('is_read','=',0)->first();
          if (!$notify) 
          {
              $not = new Notify;
              $not->user_id = $product_user_id;
              $not->from_id = (Sentry::check())?Sentry::getUser()->id:0;
              $not->object_id = $product_id;
              $not->object_type = 4;
              $not->is_read = 0;
              $not->save();
          }
        }
        
      
      return Redirect::to('/p/'.$product_id);
    }

    public function deletefeed(){
      $feed = Input::get('id');
      $comFeed = ProductComment::find($feed);
      $user_group = UsersGroup::where('user_id','=',Sentry::getUser()->id)->first();
      $is_admin  = ($user_group->group_id != 11) ? true : false;
        
      if(Sentry::getUser()->id == $comFeed->user_id || $is_admin){
        $childs = ProductComment::where('parent_id','=',$feed)->get();
        foreach($childs as $c)
        {
          $c->delete();   
        }
        $comFeed->delete();
      }
      $data = array(true);
      return Response::json($data);
    }

    public function show($id)
    {
        $product = Product::find($id);
        if(!$product)
            App::abort(404);
        $product->view_count = $product->view_count + 1;
        $product->save(array('from' => 'front_show'));
        $images = Productimage::where('product_id', '=', $product->id)->orderBy('id', 'ASC')->get();

        //SEO
        SEOMeta::setTitle($product->name);
        SEOMeta::setDescription($product->description);
        SEOMeta::addMeta('article:published_time', $product->created_at->toW3CString(), 'property');
        OpenGraph::setType('article');
        OpenGraph::setSiteName('dazar.mn');
        foreach($images as $image) {
          OpenGraph::addImage($_ENV['baseurl'].'/uploads/orig/'.$image->filename);
        }
        $user = null;
        $profile = null;
        if($product->user_id>0){
          $user = User::find($product->user_id);
          $profile = Profile::where('user_id', '=', $product->user_id)->first();
            if(!$profile){
              $profile = new Profile;
              $profile->user_id = $product->user_id;
              $profile->save();
          }
        }

        $feedbacks = ProductComment::where('product_id', '=', $id)->where('parent_id','=',0)->get();

        if(Sentry::check()){
          $userState = UserState::where('product_id', '=', $id)->where('user_id', '=', Sentry::getUser()->id)->first();
          if(!$userState){
            $userState = new UserState;
            $userState->user_id = Sentry::getUser()->id;
            $userState->product_id = $id;
            $userState->save();
          }
        }

        View::share('cidd', $product->category_id);
          
        $category = Category::find($product->category_id);
        $parents = $category->getAncestors();

        $other = array();
        if($profile && $profile->is_company == 1){
          $aothers = ProductSale::where('product_id', '=', $id)->get();
          $oIds = array();
          foreach ($aothers as $key => $sale) {
            $oIds[]=$sale->sale_id;
          }
          if($oIds){
          $other = Product::where('id', '<>', $id)->where('is_active', '=', 1)->whereIn('id', $oIds)->orderBy('created_at', 'DESC')->take(10)->get();
          }
        }
      
        $other = Product::where('category_id', '=', $product->category_id)->where('id', '<>', $id)->where('is_active', '=', 1)->orderBy('created_at', 'DESC')->take(12)->get();

        $attributes = CategoryAttribute::getAttributesByCategory($product->category_id);
      
        $returnTemplate = \View::make('front.product.show')
                        ->with('images',  $images)
                        ->with('user',  $user)
                        ->with('other',  $other)
                        ->with('profile',  $profile)
                        ->with('parents',  $parents)
                        ->with('category',  $category)
                        ->with('product', $product)
                        ->with('attributes',$attributes)
                        ->with('is_rate', Session::get('rate_'.$id, false))
                        ->with('feedbacks', $feedbacks);
          if(Sentry::check())
          {
            $user_group = UsersGroup::where('user_id','=',Sentry::getUser()->id)->first();
            if($user_group->group_id != 11)
              $returnTemplate->with('is_admin',true);
            else
              $returnTemplate->with('is_admin',false);
          }
          return $returnTemplate; 

    }

    protected function processCid($cid) {
        $this->category = null;
        if (null !== $cid) {
            $cid = (int) $cid;
            $this->category = Category::find($cid);
            if(!$this->category)
                App::abort(404);
            $breadcrumb = Category::getParents($cid);   
        }
        // 
        $this->cArray = array();
        $this->breadcrumb = array();

        // parentid 
        if ($this->category) {
          foreach ($breadcrumb as $category) {
            // adding to array
            $this->cArray[] = Category::getChildren($category->parent_id);
            $this->breadcrumb[] = $category;
          }

          if ($this->category) {
            $children = Category::getChildren($category->id);
            // 
            if ($children->count()) {
              $this->cArray[] = $children;
            }
          }
        } else {
          $this->cArray[] = Category::getChildren($cid);
        }
      }


    public function edit($pid)
    {
      $pid      = (int) $pid;
      $password = Input::get('password');
      $product  = Product::where('id', '=', $pid)->where('password', '=', $password)->first();
      $arrayPA = array(array());
      $arrayPAV = array();

      if (Request::isMethod('post'))
      {
        if(!$product) {
            App::abort(404);
        }
      } else if (Request::isMethod('get')) {

        if(!sfUser::hasPid($pid)) {
          App::abort(404);
        }

        $product = Product::find($pid);
        if(!$product) {
          App::abort(404);
        }
      }

      $parentLocations = array();
        $location = Location::where('parent_id','=',0)->get();
        foreach($location as $l)
        {
          $parentLocations[$l->id] = $l->name; 
        }  
        
      $productAttribute = ProductAttribute::getAttrValues($product->id);

            foreach($productAttribute as $pa)
            {   
                $arrayPAV[$pa->id] = 0;
                $arrayPA[$pa->attribute_id] = array('id'=>$pa->id, 'value'=> $pa->attribute_value);
                $productAttributeValues = ProductAttributeValue::getProductAttrValues($pa->id);
                if($productAttributeValues)
                {
                    foreach($productAttributeValues as $pav) 
                    {
                        $arrayPAV[$pa->id] = $pav->attribute_value_id;
                    }
                    
                }
            } 
      $this->processCid($product->category_id);

      $images    = Productimage::where('product_id', '=', $product->id)->get();
      $img_count = count($images);

      $imgs =  array();

      for ($i = 0; $i < 8; $i++) {

        if(isset($images[$i])) {
          $imgs[] = $images[$i];
        } else {
          $imgs[] = new Productimage();
        }
      }
      
      sfUser::addPid($pid);
      $whos = array();
        if(Sentry::check()){
          $user_id = Sentry::getUser()->id;
          $whos[$user_id] = 'Өөрийн Профайл';
          $companyUsers = CompanyUser::where('user_id', '=', $user_id)->get();
          if($companyUsers){
            foreach ($companyUsers as $companyUser) {
              $profile = Profile::where('user_id', '=', $companyUser->company_id)->where('is_company', '=', 1)->first();
              if($profile){
                $whos[$profile->user_id] = $profile->company_name;
              }
            }
          }
        }
      $referrer = URL::previous();
      

      $layout = 'front._layouts.product';
      $attributes = CategoryAttribute::getAttributesByCategory($product->category_id);
      return \View::make('front.product.add')-> with('product', $product)
                      ->with('category',$this->category)
                      ->with('breadcrumb',$this->breadcrumb)
                      ->with('cArray',$this->cArray)
                      ->with('images', $imgs)
                      ->with('whos',$whos)
                      ->with('attributes',$attributes)
                      ->with('arrayPAV',$arrayPAV)
                      ->with('arrayPA',$arrayPA)
                      ->with('parentLocations',$parentLocations)
                      ->with('is_edit',true)
                      ->with('layout',$layout)
                      ->with('referrer',$referrer)
                      ;
    }

    public function loadLocation()
    {
      $id = intval(Input::get('id'));
        $locations = Location::getChilds($id);

        // json data
        $data = array();
        // 
        foreach ($locations as $location) {
          // category id blah blah
          if ($location->id === 0) {
            continue;
          }
          // data
          $data[] = array(
              'id' => $location->id,
              'name' => $location->name,
          );
        }

        return Response::json($data);
    }

    public function add()
    {
        ini_set('memory_limit','256M');
        $this->processCid(Input::get('cid'));
        $arrayPA = array(array());
        $arrayPAV = array();   
        //SEO
        if($this->category) {
          SEOMeta::setTitle($this->category->name.' ангилалд зар нэмэх');
        } else {
          SEOMeta::setTitle('Зар нэмэх, Зар оруулах');
        }
        $parentLocations = array();
        $location = Location::where('parent_id','=',0)->orderBy('sort','asc ')->get();
        foreach($location as $l)
        {
          $parentLocations[$l->id] = $l->name; 
        }  
        
        $product = null;
        $images = $product ? Productimage::where('product_id', '=', $product->id)->get() : null;
        if(!$images) {
          for ($i = 0; $i < 5; $i++) {
            $images[] = new Productimage();
          }
        }

        View::share('cidd', Input::get('cid'));
        $whos = array();
        if(Sentry::check()){
          $user_id = Sentry::getUser()->id;
          $loggedUser = User::find($user_id);
          $whos[$user_id] = 'Өөрийн Профайл';
          $companyUsers = CompanyUser::where('user_id', '=', $user_id)->get();
          if($companyUsers){
            foreach ($companyUsers as $companyUser) {
              $profile = Profile::where('user_id', '=', $companyUser->company_id)->where('is_company', '=', 1)->first();
              if($profile){
                $whos[$profile->user_id] = $profile->company_name;
              }
            }
          }
          // if(!Input::get('cid') && $loggedUser->last_cat_id){
          //   $this->processCid($loggedUser->last_cat_id);
          //   View::share('cidd', $loggedUser->last_cat_id);
          // }
        }
        $pos = strpos(Request::url(), 'auto.');
        $layout = 'front._layouts.product';
        if($pos !== false){
            $layout = 'front._layouts.auto_product';
        }
        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $layout = 'front._layouts.property_product';
        }
        $referrer = URL::previous();

        return \View::make('front.product.add')-> with('product', $product)
                        ->with('category',$this->category)
                        ->with('breadcrumb', $this->breadcrumb)
                        ->with('cArray',$this->cArray)
                        ->with('images',$images)
                        ->with('whos',$whos)
                        ->with('arrayPAV',$arrayPAV)
                        ->with('arrayPA',$arrayPA)
                        ->with('parentLocations',$parentLocations)
                        ->with('attributes', array())
                        ->with('is_edit',false)
                        ->with('layout',$layout)
                        ->with('referrer',$referrer)
                        ;
    }

    public function store(){
            ini_set('memory_limit','256M');
            $id = Input::get('product_id');
            
            $product = Product::find($id);
            $validation = new ProductValidator;

            if ($validation->passes())
            {
              if(!$product) {
                $product = new Product;
              }
              $ipAddress = Request::getClientIp();
              $product->name           = cp1251_utf8(sanitize(Input::get('name')));
              $product->password       = cp1251_utf8(Input::get('password'));
              $product->price          = preg_replace('/[^0-9\.]/', '', sanitize(Input::get('price')));
              $product->description    = sanitize(Input::get('description'), '<br><br/>');
              $product->category_id    = sanitize(Input::get('category_id'));
              $product->location_id    = sanitize(Input::get('location_id'));
              $product->contact        = sanitize(Input::get('contact'));
              $product->changes        = sanitize(Input::get('changes'));
              $product->ip_address     = $ipAddress;
              $product->is_active     = 1;
              $product->owner_type     = sanitize(Input::get('owner_type'));
              $product->latlng         = sanitize(Input::get('latlng'));
              $category = Category::find($product->category_id);
              $parent = Category::find($category->parent_id);
              $namemore = '';
              if($parent)
                $namemore = $parent->name;
              $product->name_more = $namemore.' '.$category->name;
              if(!$product) {
                $product->save(array('from' => 'front_save'));
              }
              else
              {
                $product->save();
              }


              // attribute save
              $is_stock_str = '';
              $attributes = CategoryAttribute::getAttributesByCategory($product->category_id);
                $attrValueIds = array();
                $moretitle ='';
                foreach($attributes as $a)
                {

                  $pa = ProductAttribute::where('product_id','=',$product->id)->where('attribute_id','=',$a->id)->get();
                  if($pa)
                  {
                    foreach($pa as $p)
                    {
                       if($p)
                       {
                         $p->delete();
                       }  

                    }
                  }
                  

                  if($a->type == 'textbox' || $a->type == 'textarea')
                  {
                    if(Input::get($a->type.$a->id) != '')
                    {
                      $pa = new ProductAttribute;
                      $pa->product_id = $product->id;
                      $pa->attribute_id = $a->id;
                      $pa->attribute_value = sanitize(Input::get($a->type.$a->id));
                      $pa->save();

                      if($a->is_stock == 1){
                        $is_stock_str .= $pa->attribute_value.'|';
                      }

                      if($a->for_title == 1)
                      {
                        $moretitle .= $pa->attribute_value.'_';
                      }  
                    }
                     
                  }
                  elseif($a->type == 'checkbox')
                  {
                    $checked = Input::get($a->type.$a->id);
                    if(is_array($checked))
                    {
                      $pa = new ProductAttribute;
                      $pa->product_id = $product->id;
                      $pa->attribute_id = $a->id;
                      $pa->save();
                      
                      foreach($checked as $c)
                      {
                        $pav = new ProductAttributeValue;
                        $pav->product_attribute_id = $pa->id;
                        $pav->attribute_value_id = $c;
                        $pav->save();

                        $attrValueIds[] = $c;
                      }
                      
                      if($a->for_title == 1)
                      {
                        $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                        if($attributeValue) {
                          if($attributeValue != 'Сонгоно уу')
                          $moretitle .= $attributeValue.'_';
                        }
                      }

                      if($a->is_stock == 1){
                        $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                        if($attributeValue) {
                          if($attributeValue != 'Сонгоно уу')
                          $is_stock_str .= $attributeValue.' |';
                        }
                      }
                    }
                  }
                  elseif($a->type == 'selectbox')
                  {
                      if(Input::get('selectbox'.$a->id) != 0)
                      {
                        $pa = new ProductAttribute;
                        $pa->product_id = $product->id;
                        $pa->attribute_id = $a->id;
                        $pa->save();

                        $pav = new ProductAttributeValue;
                        $pav->product_attribute_id = $pa->id;
                        $pav->attribute_value_id = Input::get('selectbox'.$a->id);
                        $pav->save();

                        $attrValueIds[] = Input::get('selectbox'.$a->id);  
                        
                        if($a->is_stock == 1){

                          $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                          if($attributeValue) {
                            if($attributeValue != 'Сонгоно уу')
                            $is_stock_str .= $attributeValue.'|';
                          }
                        }
                        if($a->for_title == 1)
                        {
                          $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                          if($attributeValue) {
                            if($attributeValue != 'Сонгоно уу')
                            $moretitle .= $attributeValue.'_';
                          }
                        }
                      }
                  }
                  elseif( $a->type == 'radio')
                  {   
                      if(Input::get('radio'.$a->id) != 0)
                      {
                        $pa = new ProductAttribute;
                        $pa->product_id = $product->id;
                        $pa->attribute_id = $a->id;
                        $pa->save();

                        $pav = new ProductAttributeValue;
                        $pav->product_attribute_id = $pa->id;
                        $pav->attribute_value_id = Input::get('radio'.$a->id);
                        $pav->save();

                        $attrValueIds[] = Input::get('radio'.$a->id);  
                        if($a->is_stock == 1){
                            $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                            if($attributeValue) {
                              if($attributeValue != 'Сонгоно уу')
                              $is_stock_str .= $attributeValue.'|';
                            }
                        }

                        if($a->for_title == 1)
                        {
                          $attributeValue = ProductAttribute::getProductAttrValues($product->id, $a->id, $a->type);
                          if($attributeValue) {
                            if($attributeValue != 'Сонгоно уу')
                            $moretitle .= $attributeValue.'_';
                          }
                        }
                      }  
                      
                  }  

                } 
                if($moretitle != '')
                {
                  $product->name = $product->name.$moretitle;
                }
                
                $product->name_more = $product->name_more.' '.$moretitle;
                $product->attribute_value_ids = join(',', $attrValueIds);

                //echo rtrim($is_stock_str, '|'); die;
                
                $product->is_stock_str=rtrim($is_stock_str, '|');
                $product->save();
              //end attribute save

              $images = Input::get('images');
              if(count($images)) {
              foreach($images as $i => $image) {
                if(!$image) {
                  continue;
                }

                $product->image_filename = $image;

                $tmp_file  = public_path().'/uploads/tmp/' . $image;
                $orig_file = public_path().'/uploads/orig/' . $image;
                $orig_file_thumb = public_path().'/uploads/thumb/' . $image;
                $orig_file_thumb_new = public_path().'/uploads/thumb/s_' . $image;

                if (!is_file($tmp_file)) {
                  continue;
                }

                $product_image = new Productimage();
                $product_image->filename = $image;
                $product_image->hashcode = md5_file($tmp_file);
                $product_image->product_id = $product->id;
                $product_image->save();


                //create orig image
                $image = Image::make($tmp_file);
                $image->resize(800, 600, function ($constraint) {
                             $constraint->aspectRatio();
                             $constraint->upsize();
                        });
                // $image->insert(public_path().'/uploads/logo.png', 'bottom-right');
                $image->save($orig_file);

                //thumbnail
                $thumb = Image::make($tmp_file);
                $thumb->fit(313, 214);
                $thumb->save($orig_file_thumb);

                @unlink($tmp_file);

                //$tmp_file  = public_path().'/uploads/tmp/' . $image;
                //$orig_file = public_path().'/uploads/orig/' . $image;
                //move_uploaded_file($tmp_file, $orig_file);
                
              }
              }

              //setting for user
              if (Sentry::check()) {
                  $whois = Input::get('whois');
                  if($whois){
                    if($whois == Sentry::getUser()->id){
                      $product->user_id = Sentry::getUser()->id;  
                    }else{
                      $hprofile = Profile::where('user_id', '=', $whois)->first();
                      $hComUser = CompanyUser::where('company_id', '=', $whois)->where('user_id', '=', Sentry::getUser()->id)->first();
                      if($hprofile && $hComUser){
                        $product->user_id = $whois;

                        //Дагасан хүмүүст сонордуулга явуулах
                        $companyFollows = CompanyFollow::where('company_id', '=', $whois)->get();
                        foreach ($companyFollows as $value) {
                              $not = new Notify;
                              $not->user_id =  $value->user_id;
                              $not->from_id = $whois;
                              $not->object_id = $product->id;
                              $not->object_type = 2;
                              $not->is_read = 0;
                              $not->save();
                        }
                        $hprofile->renew_products = $hprofile->renew_products.','.$product->id;

                      }else{
                        $product->user_id = Sentry::getUser()->id;    
                      }
                    }
                  }else{
                    // hervee company admin uuruu nemj bgaa tohioldold
                    $cprofile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
                    if($cprofile->is_company == 1)
                    {
                        $companyFollows = CompanyFollow::where('company_id', '=', $cprofile->user_id)->get();
                        foreach ($companyFollows as $value) {
                              $not = new Notify;
                              $not->user_id =  $value->user_id;
                              $not->from_id = $cprofile->user_id;
                              $not->object_id = $product->id;
                              $not->object_type = 2;
                              $not->is_read = 0;
                              $not->save();
                        }
                        $cprofile->renew_products = $cprofile->renew_products.','.$product->id;
                    }


                    $product->user_id = Sentry::getUser()->id;
                  }
                  $loggedUser = User::find(Sentry::getUser()->id);
                  $loggedUser->last_cat_id = $product->category_id;
                  $loggedUser->save();


              }
              $product->save();

              // adding product id to session
              sfUser::addPid($product->id);

              $pricesArray= array();
              $prices = Prices::orderBy('id','asc')->get();
              foreach($prices as $p)
              {
                $pricesArray[$p->category_id] = $p->price;
                $pricesCategoryArray[] = $p->category_id;
              }  


              $category = Category::getParents($product->category_id);
              $parent_ids = array();
              $price = '';
              foreach($category as $c)
              {
                if(in_array($c->id, $pricesCategoryArray))
                {
                  $price = $pricesArray[$c->id];
                  break;
                }
              }

              $print_price = ($price == '')?2500:$price;

              if($product->id) {
                if($id)
                  Notification::success('Зар амжилттай заслаа.');
                else
                  Notification::success('Амжилттай нэмэгдлээ. Та энэ зараа '.$print_price.' төгрөгөөр <a href="/addBasket/'.$product->id.'">Энд</a> дарж онцлох зар болгох боломжтой.');
                $profile = Profile::where('user_id', '=', $product->user_id)->first();
                if($profile && $profile->is_company == 1){
                  // $product->is_active = 0;
                  // $product->save();
                  return Redirect::to('/profile/'.$profile->user_id);
                }
                
                if(Sentry::check()) {
                  return Redirect::to('/myProduct/'.$profile->user_id);
                } else {
                  return Redirect::to('c/'.$product->category_id);
                }
              } else {
                return Redirect::to('/page/terms');
              }
          }else{
            return Redirect::back()->withInput()->withErrors($validation->errors);
          }
    }

    public function isValid(){
        
        $data['errors'] = array();
        // validate duplicate
        if(Input::get('is_edit') != 1){
            if(Input::get('name') && Input::get('contact')) {
                $count = DB::table('product')
                    ->where('created_at', '>', date('Y-m-d h:i:s', time() - 28800))
                    ->where(function($query)
                    {
                        $query->where('name', '=', sanitize(Input::get('name')))
                              ->where('contact', '=', sanitize(Input::get('contact')));
                    })
                    ->count();

                if($count) {
                    Input::merge(array('duplicate' => ''));
                }
            }
        }
            
        $validation = new ProductValidator;
        if (!$validation->passes())
        {
            $messages = $validation->messages();
            
            foreach ($messages->all() as $message)
            {
                if ($message) {
                    $data['errors'][] = $message;
                }
            }

        }
        $cat_id = intval(Input::get('category_id'));
          if($cat_id != 0)
          {
            $category = Category::find($cat_id);
            if($category) {
              if(!$category->isLeaf()) {
                $data['errors'][] = 'category_id_leaf';
              }
            }
          }  
            
            
        return Response::json($data);

    }

    public function priceFilter()
    {
      $min = Input::get('min');
      $max = Input::get('max');

      Session::put('price_min', $min);
      Session::put('price_max', $max);

      $data = array('max'=>$max, 'min'=>$min);
      return Response::json($data);
    }
    
    public function category($id)
    {
        $price_min = Session::get('price_min');
        $price_max = Session::get('price_max');
        $avtexts = Session::get('avtexts', array());
        $avalids = Session::get('avalids', array());
        
        $productsArray = array();
        if($id == 11 || $id == 966)
            App::abort(404);
        $category = Category::find($id);
        if(!$category)
            App::abort(404);
        OpenGraph::addImage($_ENV['baseurl'].'/images/default_og_image.jpg');
        if($category->seo_title){
          SEOMeta::setTitle($category->seo_title);  
        }else{
          SEOMeta::setTitle($category->name);
        }
        if($category->seo_description){
          SEOMeta::setDescription($category->seo_description);  
          SEOMeta::setKeywords($category->seo_description);  
        }
        $categoryBanner = CategoryBanner::where('category_id', '=', $id)->orderByRaw("RAND()")->first(); 
        $parents = $category->getAncestors();

        $parentsIds = array(); $parentsIds[] = $category->id;
        $parentName = '';
        foreach ($parents as $key => $value) {
            $parentsIds[] = $value->id;
            $parentName.= $value->name.' -> ';
          }
        $parentName.= $category->name;
        $date = date('Y-m-d', time());
        $category_stats = CategoryStats::where('category_id','=',$category->id)->where('created_at', '=', $date)->first();
        if(!$category_stats)
        {
          $category_stats = new CategoryStats;  
          $category_stats->category_id = $category->id;
          $category_stats->category_name = $parentName;
          $category_stats->created_at = $date;
          $category_stats->count = 1;
        }
        else{
          $category_stats->count = $category_stats->count + 1;
        }
        $category_stats->save();

        $sortType = Input::get('sortType', 'date_desc');
        $between  = array($category->lft, $category->rgt);
        
          $query = Product::join('category', function($join) {
              $join->on('product.category_id', '=', 'category.id');
          })
          ->whereBetween('category.lft', $between)
          ->select('product.id', 'product.name', 'product.name_more', 'product.category_id', 'product.image_filename', 'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str','product.owner_type','product.location_id')
          ->where('product.is_active', '=', 1)
          ->where('product.is_featured', '=', 0);

          // featured products select
          $query2 = Product::join('category', function($join) {
                $join->on('product.category_id', '=', 'category.id');
            })
            ->whereBetween('category.lft', $between)
            ->select('product.id', 'product.name', 'product.name_more', 'product.category_id', 'product.image_filename', 'product.contact', 'product.price', 'product.created_at','product.is_featured','product.user_id','product.description','product.is_stock_str','product.owner_type','product.location_id')
            ->where('product.is_active', '=', 1)
            ->where('product.is_featured', '=', 1)
            ->orderByRaw("RAND()");

            $featuredProducts = $query2->get();
          // end featured product selects
        
          // price filter
          if($price_min > 0 && $price_max > 0)
          {
             $query->where('product.price','>',$price_min)->where('product.price','<',$price_max);
             $query2->where('product.price','>',$price_min)->where('product.price','<',$price_max);
          } 
          else if($price_min >0 && $price_max == 0) 
          {
             $query->where('product.price','>',$price_min);
             $query2->where('product.price','>',$price_min);
          }
          else if($price_min ==0 && $price_max > 0)
          {
             $query->where('product.price','<',$price_max);
             $query2->where('product.price','<',$price_max);
          }

        // $query  ->orderBy('product.is_featured', 'desc')
        //             ->orderBy('product.featured_start_date', 'asc');  
        
        
        if($sortType == 'date_asc'){
          $query->orderBy('product.created_at', 'asc');
        }elseif($sortType == 'date_desc'){
          $query->orderBy('product.created_at', 'desc');
        }elseif($sortType == 'price_asc'){
          $query->orderBy('product.price', 'asc');
        }elseif($sortType == 'price_desc'){
          $query->orderBy('product.price', 'desc');
        }else{
          $query->orderBy('product.created_at', 'desc');
        }
        
        if (Request::isMethod('post')) {
          $avalids = Session::get('avalids', array());
          $price_min = Session::get('price_min',0);
          $price_max = Session::get('price_max',0);
          
          $productsArray = array();
            $i= 0;
            $return = array('0'=>'');

            if(!empty($avalids))
            {
              foreach($avalids as $a)
              {
                  $productsArray[$i] = ProductAttribute::getProductIdsByAttrVal($a);
                  $i ++;
              }
              
              $return = ProductController::getIntersect($productsArray);
            }
            
            if(!empty($return[0])){
              $query->whereIn('product.id',$return); 
              $query2->whereIn('product.id',$return); 
            }
            else
            {
              if(!empty($avalids))
              {
                $query ->whereRaw('1=2');
                $query2 ->whereRaw('1=2');  
              }
            }


            if($price_min > 0 && $price_max > 0)
            {
               $query->where('product.price','>',$price_min)->where('product.price','<',$price_max);
               $query2->where('product.price','>',$price_min)->where('product.price','<',$price_max);
            } 
            else if($price_min >0 && $price_max == 0) 
            {
               $query->where('product.price','>',$price_min);
               $query2->where('product.price','>',$price_min);
            }
            else if($price_min ==0 && $price_max > 0)
            {
               $query->where('product.price','<',$price_max);
               $query2->where('product.price','<',$price_max);
            }
            $products = $query->paginate(50);
            $stockAttributes = Attribute::where('is_stock', '=', 1)->get();
            $query2->orderByRaw("RAND()");
            $featuredProducts = $query2->get();
            return \View::make('front.product._list')
                ->with('stockAttributes', $stockAttributes)
                ->with('featuredProducts', $featuredProducts)
                ->with('products', $products)
                ->with('parent', null)
                ->with('keyword', null);
          }else{
            Session::forget('avalids');
            Session::forget('avtexts');
            Session::forget('price_max');
            Session::forget('price_min');
          }
        $products = $query->paginate(50);
        View::share('cidd', $category->id);
        $plists = array();
        foreach($products as $p){
          $plists[] = $p->id; 
        }

        Cookie::queue('cate_id', $id, 10);
        Cache::put('id'.$category->id, $plists, 10);
        
        $banner_id = 0;
        if($categoryBanner) $banner_id = $categoryBanner->banner_id;
        $parents = $category->getAncestors();
                if(count($parents)>0){

        }else{
          $parents[0] = $category;
        }
        $filterArray = array('selectbox', 'checkbox');
        $attributes = CategoryAttribute::getAttributesByCategoryFilter($category->id);
        return \View::make('front.product.category')
            ->with('category', $category)
            ->with('parents', $parents)
            ->with('sortType', $sortType)
            ->with('attributes', $attributes)
            ->with('avalids', $avalids)
            ->with('avtexts', $avtexts)
            ->with('featuredProducts', $featuredProducts)
            ->with('categoryBanner', $categoryBanner)
            ->with('banner_id', $banner_id)
  	        ->with('products', $products);
        
    }

    public function page($slug)
    {
        $page = Page::where('slug', '=', $slug)->first();
        if(!$page)
            App::abort(404);

        //SEO
        SEOMeta::setTitle($page->title);
        SEOMeta::setDescription($page->body);
        OpenGraph::addImage($_ENV['baseurl'].'/images/default_og_image.jpg');
        $pos = strpos(Request::url(), 'auto.');
        $layout = 'front._layouts.product';
        if($pos !== false){
            $layout = 'front._layouts.auto_product';
        }
        if($slug == 'feedback' && $pos == true){
           return \View::make('front.auto.page')
                        ->with('page', $page)
                        ->with('layout', $layout)
                        ;
        }

        $pos = strpos(Request::url(), 'property.');
        if($pos !== false){
            $layout = 'front._layouts.property_product';
        }
        

        if($slug == 'feedback' && $pos == true){
           return \View::make('front.property.page')
                        ->with('page', $page)
                        ->with('layout', $layout)
                        ;
        }

        return \View::make('front.product.page')
                        ->with('page', $page)
                        ->with('layout', $layout)
                        ;
    }

    public function search()
    {
        $keyword = sanitize(Input::get('keyword'));
        $category_id = sanitize(Input::get('category_id',0));
        $childsArray = array ();
        if($category_id != 0)
        {
            $category = Category::find($category_id);
            if(!$category){
              App::abort(404);
            }  

            foreach($category->descendants()->get() as $c)
            {
              array_push($childsArray,$c->id);
            }
        }
        
        
        // $pos = strpos(Request::url(), 'auto.');
        
        // if($pos !== false && $keyword == ''){
        //   return Redirect::to('/c/19');
        // }
        // $pos = strpos(Request::url(), 'property.');        
        // if($pos !== false && $keyword == ''){
        //   return Redirect::to('/');
        // }

        if($keyword == '')
        {
          return Redirect::to('/');
        }  

        SEOMeta::setTitle('Зар хайх: '. $keyword);
        OpenGraph::addImage($_ENV['baseurl'].'/images/default_og_image.jpg');

        Search::insert($keyword);

        $page = Input::get('page', 1);
        if(count($childsArray) > 0)
        {
            $ids  = DB::table('product')->where('search_text', 'LIKE', '%'.$keyword.'%')
                   ->whereIn('category_id',$childsArray)
                    ->orderBy('is_featured','desc')
                    ->orderBy('id','desc')
                    ->take(500)
                    ->lists('id');
  
        }
        else
        {
              $ids  = DB::table('product')->where('search_text', 'LIKE', '%'.$keyword.'%')
                    ->orderBy('is_featured','desc')
                    ->orderBy('id','desc')
                    ->take(500)
                    ->lists('id');
          
        }  
        
        $avalids = array();
        $avtexts = array();          

        $avtexts = Session::get('avtexts', array());
        $avalids = Session::get('avalids', array());




        if(count($ids) == 0)
          $ids = array(-1);
        $query  = Product::whereIn('id', $ids)
          ->where('is_active', '=', 1)
          ->where('is_featured', '=', 0)
          ->orderBy('id', 'DESC');

        $query2  = Product::whereIn('id', $ids)
          ->where('is_active', '=', 1)
          ->where('is_featured', '=', 1)
          ->orderBy('id', 'DESC');

          $filterArray = array('selectbox', 'checkbox', 'radio');
          $attributes = Attribute::where('is_filterable', '=', 1)->whereIn('type', $filterArray)->get();
          $attributesText = Attribute::where('is_filterable', '=', 1)->where('type','=', 'textbox')->get();

            if(!empty($avalids))
            {
              $i = 0;
              foreach($avalids as $a)
              {
                  $productsArray[$i] = ProductAttribute::getProductIdsByAttrVal($a);
                  $i ++;
              }
              
              $return = ProductController::getIntersect($productsArray);

              if(!empty($return[0]) || !empty($return))
              {
                  if(is_array($return[0]))
                    $ids = array_intersect($ids, $return[0]);
                  else
                    $ids = array_intersect($ids, $return);  
              }
              else
              {
                if(!empty($avalids))
                {
                  $ids = array();
                }  
              }
            }
            


        if(array_key_exists('price',$avtexts))
        {
          switch ($avtexts['price']) {
            case 1:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) < ?', array(5));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) < ?', array(5));
              break;
            case 2:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(5, 10));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(5, 10));
              break;
            case 3:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(10, 20));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(10, 20));
              break;
            case 4:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(20, 30));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(20, 30));
              break;
            case 5:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(30, 50));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) between ? and ?', array(30, 50));
              break;
            case 6:
                $query->whereRaw('CAST(price AS DECIMAL(10,3)) > ?', array(50));
                $query2->whereRaw('CAST(price AS DECIMAL(10,3)) > ?', array(50));
              break;
          }
        }

        $products = $query->paginate(50);
        $res      = $products;
        $query2->orderByRaw("RAND()");
        $featuredProducts = $query2->get();

        Cookie::queue('search', $keyword, 10);
        Cache::put($keyword, $ids, 10);

        $cookie = Cookie::forget('cate_id');

        

        return Response::make(\View::make('front.product.search')
                        ->with('pager',    $res)
                        ->with('keyword',    $keyword)
                        ->with('featuredProducts', $featuredProducts)
                        ->with('products', $products))->withCookie($cookie);
        
        
    }

    public function suggest()
    {
        $max_length = 100;

        // TODO
        $q       = addslashes (Input::get('keyword'));
        $keyword = mb_substr($q, 0, $max_length);

        // terms
        $terms = textAnalyze($keyword);

        if (count($terms) === 0) {
          return Response::json($terms);
        }
        // words list
        $words = array_keys($terms);

        // last word
        $last_word = array_pop($words);

        // words against ids
        $word_ids = array();

        
        if (count($words)) {
          $word_ids[] = DB::table('word')->whereIn('name', $words)->pluck('id');
        }

        $prefix = join(' ', $words);
        

        //array_push($word_ids,-1);
        

        $record = DB::table('word')->where('name', $last_word)->first();

        // suggestion limit
        $limit = 5;

        // date time last 15 day
        $datetime = date('Y-m-d H:i:s', time() - 86400 * 30);

        $data = array();
        // record is exist search next blah blah
        if ($record) {
          $data[] = trim($prefix . ' ' . $last_word);
          
          // search word
          // against blah blah
          $word_ids[] = $record->id;

          $query = 
          "
            SELECT DISTINCT w2.id, w2.name
            FROM word w2
            INNER JOIN search_word s2 ON w2.id = s2.word_id
            AND ( s2.created_at > '".$datetime."')
            WHERE w2.id NOT
            IN (".implode(',', $word_ids).")
            AND s2.search_id
            IN (
                SELECT s3.search_id AS s3__search_id
                FROM search_word s3
                WHERE (
                        s3.word_id = ".$record->id."
                        AND s3.created_at > '".$datetime."'
                      )
            )
            GROUP BY w2.id
            ORDER BY COUNT( w2.id ) DESC
            LIMIT 5 
          ";

          $results = DB::select(DB::raw($query));

          foreach ($results as $r) {
            $data[] = trim($prefix . ' ' . $last_word . ' ' . $r->name);
          }
        } else {

          $query = 
          "
            SELECT DISTINCT w2.id, w2.name
            FROM word w2
            LEFT JOIN search_word s2 ON w2.id = s2.word_id
            AND ( s2.created_at > '".$datetime."')
            WHERE w2.name LIKE '".mysql_real_escape_string($last_word)."%'
            AND w2.id NOT IN (".implode(',', $word_ids).")
            GROUP BY w2.id
            ORDER BY COUNT( w2.id ) DESC
            LIMIT 5 
          ";
          $results = DB::select(DB::raw($query));

          foreach ($results as $r) {
            $data[] = trim($prefix . ' ' . $r->name);
          }
        }
        return Response::json($data);
    }

    public function toggle() {
      $pid = (int) Input::get('pid');

      // check product is exist
      $product = Product::find($pid);

      if ($product) {
        if(Sentry::check()){
          $interest = UserInterest::where('user_id','=',Sentry::getUser()->id)->where('product_id','=',$product->id)->first();
          if($interest)
          {
            $interest->delete();  
          }
          else 
          {
            $interest = new UserInterest;
            $interest->product_id = $product->id;
            $interest->user_id = Sentry::getUser()->id;
            $pos = strpos(Request::url(), 'auto.');
            if($pos === false){
              $posProperty = strpos(Request::url(), 'property.');
              if($posProperty === false){
                $interest->type = 1;
              }
              else
              {
                $interest->type = 3; 
              }  
            }else{
              $interest->type = 2;
            }

            $interest->save();
          }

        }
        
        if (sfUser::hasInterest($pid)) {
          // delete interest
          sfUser::deleteInterest($pid);
        } else {
          // add interest
          sfUser::addInterest($pid);
        }
        
        
      }


      if(Sentry::check())
      {
        return count(sfUser::getInterest());  
      }
      else
      {
        return count(Session::get('iids', array()));
      }  

      
    }

    public function setbasket() {
      $pid = (int) Input::get('pid');

      // check product is exist
      $product = Product::find($pid);

      if ($product) {
        if (sfUser::hasBasket($pid)) {
          // delete interest
          sfUser::deleteBasket($pid);
        } else {
          // add interest
          sfUser::addBasket($pid);
        }
      }

      return count(sfUser::getBasket());
    }

    public function my()
    {
        $products  = Product::where('user_id', Sentry::getUser()->id)->orderBy('created_at', 'DESC')->take(100)->get();
        return \View::make('front.product.my')
                    ->with('products', $products);
    }

    public function renewajax($id)
    {
        $product = Product::find($id);

        if(!$product) {
          App::abort(404);
        }

        if(Sentry::getUser()->id != $product->user_id) {
          App::abort(404);
        }

        // 8 hours before
        $date = date('Y-m-d h:i:s', time() - 28800);
        if($product->created_at > $date) {
          return  'Та зараа 8 цаг тутамд шинэчиллэх боломжтой!';
        } else {
          $product->created_at = date('Y-m-d H:i:s');
          $product->is_active = 1;
          $product->save();
          return 'Амжилттай шинэчиллээ.';
        }

    }

    public function renew($id)
    {
        $product = Product::find($id);

        if(!$product) {
          App::abort(404);
        }

        if(Sentry::getUser()->id != $product->user_id) {
          App::abort(404);
        }

        $pricesArray= array();
        $prices = Prices::orderBy('id','asc')->get();
        foreach($prices as $p)
        {
          $pricesArray[$p->category_id] = $p->price;
          $pricesCategoryArray[] = $p->category_id;
        }  


        $category = Category::getParents($product->category_id);
        $parent_ids = array();
        $price = '';
        foreach($category as $c)
        {
          if(in_array($c->id, $pricesCategoryArray))
          {
            $price = $pricesArray[$c->id];
            break;
          }
        }

        // if($product->user_id != Sentry::getUser()->id) {
        //   App::abort(404);
        // }

        // 8 hours before
        $date = date('Y-m-d h:i:s', time() - 28800);
        if($product->created_at > $date) {
          Notification::error('Та зараа 8 цаг тутамд шинэчиллэх боломжтой!');
        } else {
          $product->created_at = date('Y-m-d H:i:s');
          $product->is_active = 1;
          $product->save();
          $print_price = ($price == '')?2500:$price;
          Notification::success('Амжилттай шинэчиллээ. Та энэ зараа '.$print_price.' төгрөгөөр <a href="/addBasket/'.$product->id.'">Энд</a> дарж онцлох зар болгох боломжтой.');
        }

        return Redirect::to('/p/'.$id);
        // return Redirect::to('/myProduct/'.$product->user_id);
    }

    public function images($pid) {
      
    }

    public function interestList() {
      
      $pids    = sfUser::getInterest();
      $pids[0] = 0;
      $products = array();
      if(Sentry::check())
      {
        $interest = UserInterest::where('user_id','=',Sentry::getUser()->id)->where('type','=',1)->get();
        
        foreach($interest as $i)
        {
          $products[] = $i->product_id;
        }  
 
      }

      SEOMeta::setTitle('Дугуйлсан зарууд');
      if(count($products)>0)
        $pids = array_merge($pids,$products);
      // query
      $products  = Product::whereIn('id', $pids)->orderBy('id', 'DESC')->paginate(25);
      Session::put('iids', $pids);
      return \View::make('front.product.interest')
                      ->with('products', $products);

    }

    public function checkCode() {
      // TODO prevent brute force attack
      $pid = (int) Input::get('pid');
      $password = mb_substr(Input::get('password'), 0, 32);
      if($password == "1c2514b67bc22476c198dc2c1937b822"){
        $data = array('isValid' => false);
        return Response::json($data);
      }
      if(md5($password) == "72a2907ce3bd6cfce6de777505b9f750"){
        $product = Product::find($pid);
      }else{
        $product = Product::where('id', '=', $pid)->where('password', '=', $password)->first();
      }

      $data = array('isValid' => true);
      if (!$product) {
        $data = array('isValid' => false);
      } else {
        // add product id
        sfUser::addPid($pid);
      }
      return Response::json($data);
    }

    public function delete($id) {
      //$request->checkCSRFProtection();

      $product = Product::find($id);
      if(!$product)
          App::abort(404);

      if(!sfUser::hasPid($product->id))
          App::abort(404);

      // cid
      $cid = $product->category_id;
      $product->delete();
      Notification::success('Зар амжилттай устгалаа.');
      if(!Sentry::check())
      {
          return Redirect::to("/c/".$cid);  
      }  
      else
      {
        $profile = Profile::where('user_id', '=', Sentry::getUser()->id)->first();
        if($profile)
        {
          if($profile->is_company == 1)
          {
              return Redirect::to('/profile/'.$profile->user_id);
          }
          else
          {
              return Redirect::to('/myProduct/'.$profile->user_id);
          }    
        }
        else
        {
            return Redirect::to('/');
        } 
        
      }  
      
    }

    public function profile($id){
      // return Redirect::to('/profile/comingsoon');
      $sort_type = intval(sanitize(Input::get('sortType')));
      $category_id = intval(sanitize(Input::get('category')));
      $order_type = 'DESC';
      $user = User::find($id);
      if(!$user)
        App::abort(404);
      $order = 'created_at';

      switch ($sort_type) {
        case 1:
          $order = 'price';$order_type = 'DESC';
          break;
        case 2:
          $order = 'name';$order_type = 'DESC';
          break;
        case 3:
          $order = 'created_at';$order_type = 'DESC';
          break;
        case 4:
          $order = 'price';$order_type = 'asc';
          break;
        case 5:
          $order = 'name';$order_type = 'asc';
          break;
        case 6:
          $order = 'created_at';$order_type = 'asc';
          break;
        case 7:
          $order = 'created_at';$order_type = 'DESC';
          break;
      }
      $query = Product::where('user_id', $id);
      if($category_id != 0)
      {
        $query  = $query->where('category_id','=',$category_id);
      } 
      $products  = $query->orderBy($order, $order_type)->paginate(20);  
      
      

      // next prev
        $plists = array();
        foreach($products as $p){
          $plists[] = $p->id; 
        }
        Cookie::queue('show_profile_id', $id, 20);
        Cache::put('profile_id'.$id, $plists, 20);
      // end next
      $counter = Product::where('user_id', $id)->count();
      $profile = Profile::where('user_id', '=', $id)->first();
      if(!$profile)
        App::abort(404);
      if($profile->is_company == 1){
        //return Redirect::to("/company/".$profile->company_name);
      }
      $state = UserState::where('user_id', '=', $id)->count();
      $banners = null;
      $viewType = Input::get('viewType', Session::get('viewType', 'grid'));
      Session::put('viewType', $viewType);
      $page = Input::get('page', 1);
      if($profile->is_company){
        $image_types = CoverImageType::where('cover_id','=',$profile->cover_style)->get();
        $image_types_array = array();
        foreach($image_types as $key=>$i)
        {
          $image_types_array[$key] = $i->id;
        }
        if(!empty($image_types_array))
        {
          $banners = CompanyBanner::where('company_id', '=', $id)->whereIn('image_type_id',$image_types_array)->get();
        }
        
      }
      return \View::make('front.product.profile')
                      ->with('user', $user)
                      ->with('state', $state)
                      ->with('counter', $counter)
                      ->with('profile', $profile)
                      ->with('banners', $banners)
                      ->with('page', $page)
                      ->with('viewType', $viewType)
                      ->with('sort_type', $sort_type)
                      ->with('products', $products);
    }

    public function buyProfile()
    {
      if(!Sentry::check())
        return Redirect::to('/login');   
      
      $allprices = ProfilePrice::get();
      $prices = array();
      foreach ($allprices as $price) {
        $prices[$price->id] = $price->price;
      }
      if (Request::isMethod('post'))
      {
          $payment_type = Input::get('payment_type');
          $profile_type = Input::get('profile_type');

          $order = new PaymentOrder;

          $order->type_order = 1;
          $order->status = 1;
          $order->user_id = Sentry::getUser()->id;
          $order->created_at = date("Y-m-d H:i:s", time());
          $order->is_order = 0;
          $order->total = $prices[$profile_type];
          $order->payment_type = $payment_type;
          $order->save();

          $orderItem = new OrderItems;
          $orderItem->payment_order_id = $order->id;
          $orderItem->profile_type;
          //$orderItem->amount->
          $orderItem->price = $prices[$profile_type];
          $orderItem->is_coupon = 3;      // profile turuliig hadgalav
          $orderItem->save();

          if($payment_type == 'golomt')
          {
              //$url = 'https://www.egolomt.mn/billingnew/test.htm';
              $key_number = '250246246246241249250227204254244235199166167162176164169170172';
              return \View::make('front.profile.golomt')->with('key_number', $key_number)->with('order_code', $order->order_code)->with('amount', $prices[$profile_type]);
          }
          elseif($payment_type == 'saving')
          {
            return \View::make('front.profile.saving')->with('order_code', $order->order_code)->with('amount', $prices[$profile_type]);
          }
          elseif($payment_type == 'transfer')
          {
            return \View::make('front.profile.transfer')->with('amount', $prices[$profile_type])->with('order_id', $order->id);
          }
      }
      else 
      {
        return \View::make('front.product.buyProfile')->with('allprices', $allprices);  
      }  

    }

    public function basket(){

      $allprices = Prices::get();
      $prices = array();
      foreach ($allprices as $price) {
        $prices[$price->category_id] = $price->price;
      }

      
      $coup_ids = sfUser::getCouponBasket();
      
      $ids = sfUser::getBasket();
      if(!$ids && count($coup_ids)==0){
        Notification::error('Онцлох болгох зараа сонгоно уу.');
        return Redirect::to('/my');
      }


      $coupons = null;
      $products = null;
      if(count($ids)>0)
      {
        $products  = Product::whereIn('id', $ids)->orderBy('created_at', 'DESC')->get();
      }  
      if(count($coup_ids)>0)
      {
        if(!Sentry::check())
          return Redirect::to('/login');   
        
        $coupons   = Product::whereIn('id', $coup_ids)->get();  
      }
      
      if (Request::isMethod('post'))
      {
        $userId = 0;
        if(Sentry::check()){
          $userId = Sentry::getUser()->id;
        }
        $total = 0;
        $payment_type = Input::get('payment_type');

        $order = new PaymentOrder;

        $order->status = 1;
        $order->user_id = $userId;
        $order->created_at = date("Y-m-d H:i:s", time());
        $order->is_order = 0;
        $order->payment_type = $payment_type;
        $order->save();
        $nb_item = 0;
        if($coupons)
        {
          foreach($coupons as $c)
          {

              $orderItem = new OrderItems;
              $orderItem->payment_order_id = $order->id;
              $orderItem->product_id = $c->id;
              $orderItem->is_coupon = 1;
              //$orderItem->amount->
              $orderItem->price = $c->coupon_price;
              $total += $c->coupon_price;

              $orderItem->save();  
            
            
          }
        }
        if($ids)
        {
            foreach ($ids as $key => $id) 
            {
              $basketProduct = Product::find($id);
              if($basketProduct){
                $price = Input::get('pVal'.$id);
                $total+=$price;
                $nb_item++;
                $orderItem = new OrderItems;
                $orderItem->payment_order_id = $order->id;
                $orderItem->product_id = $id;
                //$orderItem->amount->
                $orderItem->price = $price;
                $orderItem->save();
              }
              else
              {
                sfUser::deleteBasket($id);
              }
            }
        }
        

        $order->nb_item = $nb_item;
        $order->total = $total;
        $order->order_code = substr(md5($order->id . time()), 0, 12);
        $order->save();
        if($payment_type == 'golomt')
        {
            //$url = 'https://www.egolomt.mn/billingnew/test.htm';
            $key_number = '250246246246241249250227204254244235199166167162176164169170172';
            return \View::make('front.profile.golomt')->with('key_number', $key_number)->with('order_code', $order->order_code)->with('amount', $total);
        }
        elseif($payment_type == 'saving')
        {
          return \View::make('front.profile.saving')->with('order_code', $order->order_code)->with('amount', $total);
        }
        elseif($payment_type == 'transfer')
        {
          return \View::make('front.profile.transfer')->with('amount', $total)->with('order_id', $order->id);
        }
      }
      else
      {      
        return \View::make('front.product.basket')->with('products', $products)->with('coupons', $coupons)->with('prices', $prices);
      }
    }

    public function addBasket($id){
      $product = Product::find($id);
      if ($product) {
        
        if(intval(Input::get('is_coupon'))==1)
        {
          sfUser::addCouponToBasket($id);
        }  
        else
        {
          sfUser::addBasket($id);
        }  
      }
      return Redirect::to('/basket');
    }

    public function removeBasket($id)
    {
      $product = Product::find($id);
      if ($product) {
        
        if(intval(Input::get('is_coupon'))==1)
        {
          sfUser::delCouponBasket($id);
        }  
        else
        {
          sfUser::deleteBasket($id);
        }
      }
      return Redirect::to('/basket'); 
    }

    public function followList() {
      $comFols = CompanyFollow::where('user_id', '=', Sentry::getUser()->id)->get();
      $pids = array();
      foreach ($comFols as $key => $value) {
        $pids[] = $value->company_id;
      }
      SEOMeta::setTitle('Дагасан байгууллагын зарууд');

      // query
      $products  = Product::whereIn('user_id', $pids)
        ->where('product.is_active', '=', 1)
        ->orderBy('product.is_featured', 'desc')
        ->orderBy('product.featured_start_date', 'asc')
        ->orderBy('product.created_at', 'desc')->paginate(25);

      return \View::make('front.product.follow')
                      ->with('products', $products);
    }

    public function getIntersect($arr)
    {
      
      $returnArr = array();
      $cntArr = count($arr);
      if($cntArr != 0)
      { 
        if($cntArr == 1)
        {
            $returnArr = $arr[0];
            return $returnArr;
        }
        else
        {
            if(($cntArr%2)==0)
            {
              $k=0;
              for($j = 0; $j < $cntArr;  $j+= 2)
              {
                  $returnArr[$k] =  array_intersect($arr[$j], $arr[$j+1]);
                  $k++;
              }
              
            }
            else
            {
              $k=0;
              for($j = 0; $j < $cntArr-1; $j+= 2)
              {
                  $returnArr[$k] =  array_intersect($arr[$j], $arr[$j+1]);
                  $k++;
              }
              if(array_key_exists($k, $returnArr))
              {
                $returnArr[$k] = array_intersect($arr[count($arr)-1], $returnArr[$k]);  
              }  
              else
              {
                if(array_key_exists(0, $returnArr))
                $returnArr[0] = array_intersect($arr[count($arr)-1], $returnArr[0]);
              }
            }

            if(count($returnArr) > 1)
            {
              return ProductController::getIntersect($returnArr);
            }
            else
            {
              return $returnArr;
            }  
        }  

      }  
      
    }

    public function sale($pid) {

      $user_id = Sentry::getUser()->id;
      $product = Product::find($pid);
      if(!$product)
          App::abort(404);

      // query
      $products  = Product::where('product.is_active', '=', 1)
        ->where('product.id', '<>', $pid)
        ->where('product.user_id', '=', $user_id)
        ->orderBy('product.is_featured', 'desc')
        ->orderBy('product.featured_start_date', 'asc')
        ->orderBy('product.created_at', 'desc')->take(25)->get();

      return \View::make('front.product.sale')
                      ->with('product_id', $pid)
                      ->with('products', $products);
    }

    public function salesave(){

      $percent = Input::get('percent');
      $sale_id = Input::get('sale_id');
      
      $productSale = ProductSale::where('product_id', '=', Input::get('product_id'))->where('sale_id', '=', $sale_id)->first();  
      if(!$productSale){
        $productSale = new ProductSale();
      }
      $productSale->product_id = Input::get('product_id');
      $productSale->sale_id = $sale_id;
      $productSale->sale_percent = $percent;
      $productSale->save();

      Notification::success('Амжилттай хадгаллаа.');
      return Redirect::to('/p/'.Input::get('product_id'));
    }
    public function saledelete($sid){
      $productSale = ProductSale::find($sid);
      $product_id = $productSale->product_id;
      if($productSale){
        $productSale->delete();
      }
      return Redirect::to('/p/'.$product_id);
    }
    public function salesearch(){
      $user_id = Sentry::getUser()->id;
      $keyword = Input::get('keyword');
      $product = Product::find(Input::get('product_id'));
      if(!$product)
          App::abort(404);
      if($keyword){
      // terms
        $terms = textAnalyze($keyword);

        // words
        $words = array_keys($terms);

        // sorted blah blah
        sort($words);

        // blah blah
        $text = join(' ', $words);

        // adding to search keyword table
        if (!sfUser::hasKeyword($text, false) && count($words) > 0) {
          // add keyword
          sfUser::addKeyword($text);

          // adding to search table blah blah
          Search::insert($text);
        }

        $page  = Input::get('page', 1);

        $res = SearchWord::getSearchQueryV2($keyword, $page, 50);

        // ids
        $ids = array();

        foreach ($res as $pWord) {
          $ids[] = $pWord->product_id;
        }

      // query
      $products  = Product::where('product.is_active', '=', 1)
        ->where('product.id', '<>', Input::get('product_id'))
        ->where('product.user_id', '=', $user_id)
        ->whereIn('id', $ids)
        ->orderBy('product.is_featured', 'desc')
        ->orderBy('product.featured_start_date', 'asc')
        ->orderBy('product.created_at', 'desc')->take(25)->get();
      }else{
        $products  = Product::where('product.is_active', '=', 1)
        ->where('product.id', '<>', Input::get('product_id'))
        ->where('product.user_id', '=', $user_id)
        ->orderBy('product.is_featured', 'desc')
        ->orderBy('product.featured_start_date', 'asc')
        ->orderBy('product.created_at', 'desc')->take(25)->get();
      }
      return \View::make('front.product._list3')
                      ->with('products', $products);
    }

    // tuhain buteegdehuunii listend haragdah attribute-g butsaana
    // type=1 auto, type=2 property
    private function getStockStr($product_id, $type) {

        if($type == 1) {
          $attributes   = Attribute::where('is_stock', '=', 1)->where('id', '<', 110)->get();
        } else if($type == 2) {
          $attributes   = Attribute::where('is_stock', '=', 1)->where('id', '>', 110)->get();
        }

        $is_stock_str = '';
        foreach($attributes as $attribute) {
            $attributeValue = ProductAttribute::getProductAttrValues($product_id, $attribute->id, $attribute->type);
            if($attributeValue) {
              $is_stock_str .= $attributeValue.'|';
            }
        }
        return rtrim($is_stock_str, '|') ;
    }

    public function insertSearchText()
    {
        $pages = Product::where("search_text", "")
                      ->orderBy('created_at', 'DESC')
                      ->take(1000)
                      ->get();

        foreach($pages as $page) {
            $page->search_text = $page->name.' '.$page->description.' '.translateText($page->name.' '.$page->description);
            $page->save();
            unset($page);
            gc_collect_cycles();
        }

        unset($pages);
        gc_collect_cycles();

        return 'OK';

    }

    public function tellAdmin($id)
    {
        $product_id = $id;
        if (Request::isMethod('post'))
        {
            if(isset($_POST['url']) && $_POST['url'] == ''){
              $product_id = intval($id);
              $product = Product::find($product_id);
              if($product)
              {

                 $phone_number = htmlspecialchars(Input::get('phone_number'));
                  $msgs = htmlspecialchars(Input::get('msgs'));
                  $subject = "Админд мэдэгдэх нь." ;
                  $body = '<html><body><b>'.$phone_number.'</b> утасны дугаартай хэрэглэгчээс ирсэн санал'.'<br/>';
                  $body .= '<b>'.$product->id. '</b> дугаартай бүтээгдэхүүнд'.'<br/><br/>';
                  $body .= $msgs.'</body></html>';
                  $to = 'zarturbo@gmail.com';
                  $headers = "From: dazar.mn <info@dazar.mn>" . "\r\n" ;
                  $headers .="Message-ID: <". md5(time())."@dazar.mn" . ">\r\n" ;
                  $headers .="Reply-To: <". $to . ">\r\n" ;
                  $headers .="Reply-To: <". $to . ">\r\n" ;
                  $headers .="X-Mailer: PHP/" . phpversion();
                  $headers .= "MIME-Version: 1.0\r\n";
                  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
                  $headers .= "Return-Path: The Sender <info@dazar.mn>\r\n";
                  $headers .= "Organization: dazar.mn\r\n";
                  $headers .= "X-Priority: 3\r\n";
                  
                  mail($to, $subject, $body,$headers);  
                  Notification::success('Таны хүсэлт амжилттай илгээгдлээ.');
                  return Redirect::to('/p/'.$product->id);
              }
              else
              {
                App::abort(404);
              }
            }
            
        }
        $layout = 'front._layouts.product';
        return \View::make('front.product.tellAdmin')
                      ->with('layout', $layout)
                      ->with('product_id', $product_id);
    }

}