<?php namespace App\Controllers\Front;

use App\Models\User;
use App\Models\PaymentOrder;
use App\Models\OrderItems;
use App\Models\Product;
use App\Models\Category;
use App\Models\Prices;
use App\Models\sfUser;

use OAuth, Input, Request, Redirect, Sentry, Notification, FacebookConnect, Session, Image, DB, Config, App,SoapWrapper;

class ShoppingController extends \FrontBaseController {

  public function golomtApproved()
  {
    $error_code = $_GET['error_code'];
    $error_desc = $_GET['error_desc'];
    $card_number= $_GET['card_number'];
    try
    {
      $success = $_GET ['success']; //formiin get argaar utgaa avna
      $trans_number = $_GET ['trans_number']; //formiin get argaar utgaa avna

      $order_code = Input::get('trans_number');
      $order = PaymentOrder::where('order_code', '=', $order_code)->first();
      if(!$order)
        App::abort(404);

       SoapWrapper::add(function ($service) {
            $service
                ->name('golomtsoap')
                ->wsdl('http://m.egolomt.mn:7070/persistence.asmx?WSDL')
                ->trace(true);                                                  // Optional: (parameter: true/false)
                // ->cache(WSDL_CACHE_NONE);                                        // Optional: Set the WSDL cache
                // ->options(['login' => '66217', 'password' => 'ZaRm7']);   // Optional: Set some extra options
        });

        $data = [
            'v0'=>'66217', 'v1'=>'ZaRm7', 'v2'=>$trans_number, 'v3'=>date('Ymd', strtotime($order->created_at)),'v4'=>$order->total
        ];

        // Using the added service
        SoapWrapper::service('golomtsoap', function ($service) use ($data) {
            // var_dump($service->getFunctions());
            $return = $service->call('Get_new', [$data])->Get_newResult;
            // print_r($return); die;
        });

        


      

      //$sh_card = Doctrine::getTable('ShoppingCart')->find(array($order->getShoppingCartId()));
      $sh_card = OrderItems::where('payment_order_id', '=', $order->id)->get();
      if(!$sh_card)
        App::abort(404);
      
      //echo $responseCode; die;
      # ari: -Голомтоор ирэх дүнг нь шалгах боломжгүй байна
      //TODO check OrderID == Bank Order ID And Amount == BankAmount
      // if (strlen ( $responseCode ) == 6) {
        if ($success == 0 && $error_code == '000' && strlen($card_number)== 19)
        {
          if ($order instanceof PaymentOrder)
            if ($order->status == 1)
            {
             $this->setOrderStatus($order); 
            }
          Notification::success('Таны төлбөр амжилттай хийгдлээ!');
          return Redirect::to('/');
        }
        else
        {
          $order->status = "Failed";
          $order->save();
          Notification::error('Таны төлбөр амжилтгүй боллоо! Картын мэдээллээ болон нууц кодоо зөв оруулсан эсэхээ нягтлана уу? Ямар нэгэн асуудалтай тулгарсан бол бидэнтэй холбоо барих эсвэл <b>Тусламж</b> хэсгээс орж мэдээлэл авч болно. ');
          return Redirect::to('/');
        }
      // }
      // else 
      // {
      //   Notification::error('Таны төлбөр амжилтгүй боллоо! Картын мэдээллээ болон нууц кодоо зөв оруулсан эсэхээ нягтлана уу? Ямар нэгэн асуудалтай тулгарсан бол бидэнтэй холбоо барих эсвэл <b>Тусламж</b> хэсгээс орж мэдээлэл авч болно. ');
      //     return Redirect::to('/');
      //   // if($responseCode == 2) { Notification::error("Guilgee amjiltgui bolson baina.");  }
      //   // if($responseCode == 0) { Notification::error("Iim dugaar bolon guilgeenii duntei guilgee baazad burtgegdeegui baina."); }
      //   // if($responseCode == 3) { Notification::error("Hereglegchiin ner esvel nuuts ug buruu baina."); }

      // }

    }
    catch (Exception $e)
    {
      # some error occured
      //$this->getLogger()->log($e->getMessage());
      Notification::error($_GET['error_desc']);
      return Redirect::to('/');
    }
  }
  private function setOrderStatus($order){
    $allprices = Prices::get();
    $prices = array();
    foreach ($allprices as $price) {
      $prices[$price->category_id] = $price->price;
    }
    $order->status = 2;
    $order->save();

    //------company_start_and_due_date
    if($order->is_coupon == 3)
    {
      $profile = Profile::where('user_id','=',Sentry::getUser()->id)->first();
      $profile->is_company = 1;
      $profile->is_paid = 1;
      $profile->paid_until = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
      $profile->save();

    } 

    $orderItems = OrderItems::getListOfPaymentId($order->id);
    $start_date = date("Y-m-d", time());
    $week = 1;
    foreach ($orderItems as $key => $orderItem) {
      $product = Product::find($orderItem->product_id);
      if($product){
        $grand_id = Category::getGrandParentId($product->category_id);
        $thisPrice = $prices[$grand_id];
        if($thisPrice>0){
        $week = $orderItem->price/$thisPrice;
        }
        $times = $week*604800;
        $due_date = date("Y-m-d", time() + $times);
        $product->is_featured = 1;
        $product->is_active = 1;
        $product->featured_start_date = $start_date;
        $product->featured_due_date = $due_date;
        $product->save();
      }
    }
    
    try
    {
      //runninig cron
      // $this->getController()->getPresentationFor('cron', 'updateProductDoping');
    }
    catch (Exception $e)
    {

    }
  }
  public function postsaving(){
        $amount = Input::get('amount');
        $orderId = Input::get('orderId');

        if (Request::isMethod('post')) {
            $xml_str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
      <TKKPG>
        <Request>
         <Operation>CreateOrder</Operation> 
         <Language>EN</Language>
         <Order>
           <Merchant>11070036</Merchant>
           <Amount>".$amount."</Amount>
           <Currency>496</Currency>
           <Description>".$orderId."</Description>
           <ApproveURL>http://dazar.mn/shopping/saveApproved</ApproveURL>
           <CancelURL>http://dazar.mn/shopping/saveDeclined</CancelURL>
            <DeclineURL>http://dazar.mn/shopping/saveDeclined</DeclineURL>
            <AddParams>
              <p1>p1 Value<h2>hhh</h2></p1>
              <p2>p2 Value</p2>
                  </AddParams>
          </Order>
        </Request>
       </TKKPG>";
            // post xml data
            $post_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><TKKPG></TKKPG>');
            // $url = 'https://202.131.225.149:2233/Exec';
            $url = 'https://ecom.savingsbank.mn/Exec';
            // CURL
            $ch = curl_init();

            //curl_setopt($ch, CURLOPT_PORT, 2233);
            curl_setopt($ch, CURLOPT_URL, $url);
            // For xml, change the content-type.
                  // Return a variable instead of posting it directly
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // Active the POST method
            curl_setopt($ch, CURLOPT_POST, 1) ;
            // Request

            curl_setopt($ch, CURLOPT_USERPWD, "twpg:twpg");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);        
            curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_str);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            //}
            // Send to remote and return data to caller.

            $ret = curl_exec($ch);
            //print_r($ret); die;
            curl_close($ch);
        if (strlen($ret)>0)
        {
          //echo "ret ";
          $needle = '<TKKPG>';
          $result = substr($ret,  strpos($ret, $needle), strlen($ret)); // $result = php
                // $path = sfConfig::get('sf_upload_dir');
          $path = public_path().'/uploads';
                
          $myFile = $path.'/'.md5(date('Y-m-d H:i:s'))."testFile.xml";
          $fh = fopen($myFile, 'w') or die("can't open file"); 
          fwrite($fh, $result);    
          fclose($fh);
        }
        else
          exit('Receive Error11');
          //exit('aaaa');
        //write_log("fwrite "); 
        if (file_exists($myFile)) {
          $xml = simplexml_load_file($myFile);  
          unlink($myFile);
        } else {
          exit('Failed to open test.xml.');
        }
            
            if ($xml->Response->Status == "00") {
                $redirect_url = $xml->Response->Order->URL."?ORDERID=".$xml->Response->Order->OrderID."&SESSIONID=".$xml->Response->Order->SessionID;
                sleep(3);
                //return $this->redirect($redirect_url);
                return Redirect::to($redirect_url);
            }
            
        }
        return '.';
  }

  public function saveApproved(){
    if (Request::isMethod('post')) {
      $xmlmsg = Input::get("xmlmsg");
      $xml = new \SimpleXMLElement ( $xmlmsg );
      
      $order = PaymentOrder::where('order_code', '=', $xml->OrderDescription)->first();
      if(!$order)
        App::abort(404);

      #### Нууцлал, лавлагаа
      $post_xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><TKKPG></TKKPG>');
  // request
      $request = $post_xml->addChild('Request');
      $request->addChild('Operation', 'GetOrderStatus');
      $request->addChild('Language', 'EN');
      // order
      $order = $request->addChild('Order');
      $order->addChild('Merchant', 11070036);
      $order->addChild('OrderID', $xml->OrderID);
      $order->addChild('SessionID', $xml->SessionID);

      $xml_str = $post_xml->asXML();

      // request post url
      $url = 'https://ecom.savingsbank.mn/Exec';
      //
      // CURL
      $ch = curl_init();

      //curl_setopt($ch, CURLOPT_PORT, 2233);
      curl_setopt($ch, CURLOPT_URL, $url);
      // For xml, change the content-type.
      curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_str);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // ask for results to be returned

      if (preg_match('/^https/i', $url)) {            // is https
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      }

      // Send to remote and return data to caller.
      $result = curl_exec($ch);
      curl_close($ch);

      $xmlr = simplexml_load_string($result);

      if ($xmlr->Response->Status == "00") {
          $this->setOrderStatus($order); 
          Notification::success('Таны төлбөр амжилттай хийгдлээ!');
          return Redirect::to('/');
      }else{
          $order->status = "Failed";
          Notification::error('Таны төлбөр амжилтгүй боллоо! Картын мэдээллээ болон нууц кодоо зөв оруулсан эсэхээ нягтлана уу? Ямар нэгэн асуудалтай тулгарсан бол бидэнтэй холбоо барих эсвэл <b>Тусламж</b> хэсгээс орж мэдээлэл авч болно. ');
          return Redirect::to('/');
      }
      $order->save();
      #### Нууцлал, лавлагаа END

    }
  }

  public function saveDeclined(){
    //if (Request::isMethod('post')) {
      //echo $_SERVER['SERVER_PORT'];
      $xmlmsg = Input::get("xmlmsg");
      echo $xmlmsg; 
      die;
      $xml = new \SimpleXMLElement ( $xmlmsg );
      
      $order = PaymentOrder::where('order_code', '=', $xml->OrderDescription)->first();
      if(!$order)
        App::abort(404);
      $order->status = $xml->OrderStatus;
      $order->save ();
      Notification::error('Таны төлбөр амжилтгүй боллоо! Картын мэдээллээ болон нууц кодоо зөв оруулсан эсэхээ нягтлана уу? Ямар нэгэн асуудалтай тулгарсан бол бидэнтэй холбоо барих эсвэл <b>Тусламж</b> хэсгээс орж мэдээлэл авч болно. ');
      return Redirect::to('/');
    //}
  }
}