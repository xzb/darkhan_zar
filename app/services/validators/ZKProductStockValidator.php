<?php namespace App\Services\Validators;
 
class ZKProductStockValidator extends Validator {
 
    public static $rules = array(
        'quantity' => 'required',
    );

    public static $messages = array(
        'quantity.required' => 'Тоо ширхэгээ оруулна уу',
    );

   public static $fields = array(
        'quantity.required' => 'quantity',
    );
 
}