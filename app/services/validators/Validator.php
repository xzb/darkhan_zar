<?php namespace App\Services\Validators;

abstract class Validator {

	protected $data;

	public $errors;

	public static $rules;

	public static $messages;

	public function __construct($data = null)
	{
		$this->data = $data ?: \Input::all();
	}

	public function passes()
	{
		$rul = static::$rules;

		if(isset($this->data['email2']) && isset($this->data['email'])) {
			if($this->data['email2'] == $this->data['email']) {
				$rul['email'] = 'required|email';
			}
		}

		if(sizeOf(static::$messages)) {
			$validation = \Validator::make($this->data, $rul, static::$messages);
		} else {
			$validation = \Validator::make($this->data, $rul);
		}

		if ($validation->passes()) return true;

		$this->errors = $validation->messages();

		return false;
	}


    public function messages()
    {
        if(static::$fields)
	        $validation = \Validator::make($this->data, static::$rules, static::$fields);
        else
    	    $validation = \Validator::make($this->data, static::$rules);    
        
        return $validation->messages();
    }
 


}