<?php namespace App\Services\Validators;
 
class ProductEditValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
        'description'  => 'required',
        'price'  => 'required',
        'contact'  => 'required',
        'category_id'  => 'required',
        
    );

    public static $messages = array(
        'name.required' => 'Нэрээ оруулна уу',
        'description.required' => 'Зараа оруулна уу',
        'contact.required' => 'Утасны дугаараа оруулна уу',
        'price.required' => 'Үнээ оруулна уу',
        'category_id.required' => 'Ангилалаа сонгоно уу',
    );

   public static $fields = array(
        'name.required' => 'name',
        'description.required' => 'description',
        'contact.required' => 'contact',
        'price.required' => 'price',
        'category_id.required' => 'category_id',
    );
 
}