<?php namespace App\Services\Validators;
 
class LocationValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
    );

    public static $messages = array(
    	'name.required' => 'Байршлаа оруулна уу',	
    );
 
}