<?php namespace App\Services\Validators;

class RegisterTwoValidator extends Validator {

	public static $rules = array(
	'email' => 'required|email|unique:users',
	'firstname' => 'required',
	'lastname' => 'required',
	'password' =>'required',
	'confirm_password' => 'required|same:password',
	'vilchilgee' =>'required',
	);

	public static $messages = array(
	'email.required' => 'Имэйл хаяг оруулна уу',
	'email.unique' => 'Имэйл хаяг бүртгэгдсэн байна',
	'email.email' => 'Имэйл хаяг буруу байна',
	'firstname.required' => 'Нэр оруулна уу',
	'lastname.required' => 'Овог оруулна уу',
	'password.required' => 'Нууц үг оруулна уу',
	'confirm_password.required' => 'Нууц үг давтаж оруулна уу',
	'confirm_password.same' => 'Нууц үг давтаж оруулна уу',
	'vilchilgee.required' => 'Үйлчилгээний нөхцөл зөвшөөрнө үү',
	);

}