<?php namespace App\Services\Validators;
 
class AutoProductValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
        'description'  => 'required',
        'price'  => 'required',
        'contact'  => 'required',
        'password'  => 'required',
        'category_id'  => 'required',
        'duplicate'  => 'required',
        'vnagree'  => 'required',
        'type_category'  => 'required',
        'factory_category'  => 'required',
    );

    public static $messages = array(
        'name.required' => 'Нэрээ оруулна уу',
        'description.required' => 'Зараа оруулна уу',
        'contact.required' => 'Утасны дугаараа оруулна уу',
        'password.required' => 'Нууц үгээ оруулна уу',
        'price.required' => 'Үнээ оруулна уу',
        'category_id.required' => 'Ангилалаа сонгоно уу',
        'duplicate.required' => '1 зарыг өдөрт 1 л удаа оруулна',
        'vnagree.required' => 'Үйлчилгээний нөхцөл зөвшөөрнө үү',
        'type_category.required'  => 'Төрлөө сонгоно уу',
        'factory_category.required'  => 'Үйлдвэрлэгчээ сонгоно уу',
    );

   public static $fields = array(
        'name.required' => 'name',
        'description.required' => 'description',
        'contact.required' => 'contact',
        'password.required' => 'password',
        'price.required' => 'price',
        'category_id.required' => 'category_id',
        'duplicate.required' => 'duplicate',
        'vnagree.required' => 'vnagree',
        'type_category.required'  => 'type_category',
        'factory_category.required'  => 'factory_category',
    );
 
}