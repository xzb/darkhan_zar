<?php namespace App\Services\Validators;
 
class BannerLocationValidator extends Validator {
 
    public static $rules = array(
        'location' => 'required',
        'price' => 'required|numeric',
    );
 
}