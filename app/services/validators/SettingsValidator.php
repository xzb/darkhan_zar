<?php namespace App\Services\Validators;
 
class SettingsValidator extends Validator {
 
    public static $rules = array(
        'backend_title'  => 'required',
        'frontend_title'  => 'required',
        'frontend_description'  => 'required',
        'frontend_keywords'  => 'required',
    );

    public static $messages = array(
        'backend_title.required' => 'Backend site title заавал оруулна',
        'frontend_title.required' => 'Frontend site title заавал оруулна',
        'frontend_description.required' => 'Frontend site description заавал оруулна',
        'frontend_keywords.required' => 'Frontend site keywords заавал оруулна'
    );
 
}