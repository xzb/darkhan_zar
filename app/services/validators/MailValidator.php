<?php namespace App\Services\Validators;

class MailValidator extends Validator {

public static $rules = array(
'email' => 'required|email',
);

public static $messages = array(
'email.required' => 'Имэйл хаяг оруулна уу',
'email.email' => 'Имэйл хаяг буруу байна',
);

}