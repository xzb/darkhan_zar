<?php namespace App\Services\Validators;

class UsersValidator extends Validator {

public static $rules = array(
'email' => 'required|email|unique:users',
'first_name' => 'required',
'last_name' => 'required',
'password' =>'required',
'user_type' => 'required',
);

public static $messages = array(
'email.required' => 'Имэйл хаяг оруулна уу',
'email.unique' => 'Имэйл хаяг бүртгэгдсэн байна',
'email.email' => 'Имэйл хаяг буруу байна',
'first_name.required' => 'Нэр оруулна уу',
'last_name.required' => 'Овог оруулна уу',
'password.required' => 'Нууц үг оруулна уу',
'user_type.required' => 'Хэрэглэгчийн эрх сонгоно уу',

);

}