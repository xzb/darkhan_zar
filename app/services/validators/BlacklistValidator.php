<?php namespace App\Services\Validators;
 
class BlacklistValidator extends Validator {
 
    public static $rules = array(
        'type'  => 'required',
        'name'  => 'required',
    );

    public static $messages = array(
        'name.required' => 'Утасны дугаар эсвэл Имэйл хаяг оруулна уу',
        'type.required' => 'Төрлөө сонгоно уу',
    );
 
}