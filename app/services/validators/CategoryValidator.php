<?php namespace App\Services\Validators;
 
class CategoryValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
    );

    public static $messages = array(
    	'name.required' => 'Ангилалаа оруулна уу',	
    );
 
}