<?php namespace App\Services\Validators;

class ZKAttributeValidator extends Validator {

public static $rules = array(
'name' => 'required',
);

public static $messages = array(
'name.required' => 'Нэр оруулна уу',
);

}