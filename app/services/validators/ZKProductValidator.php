<?php namespace App\Services\Validators;
 
class ZKProductValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
        'price'  => 'required',
        'category_id'  => 'required|not_in:0',
        
    );

    public static $messages = array(
        'name.required' => 'Нэрээ оруулна уу',
        'price.required' => 'Үнээ оруулна уу',
        'category_id.required' => 'Ангилалаа сонгоно уу',
        'category_id.not_in' => 'Ангилалаа сонгоно уу',
        
    );

   public static $fields = array(
        'name.required' => 'name',
        'price.required' => 'price',
        'category_id.required' => 'category_id',
        'category_id.not_in' => 'category_id',
    );
 
}