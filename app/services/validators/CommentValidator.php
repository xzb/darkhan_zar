<?php namespace App\Services\Validators;

class CommentValidator extends Validator {

public static $rules = array(
'email' => 'required|email',
'content' => 'required',
);

public static $messages = array(
'email.required' => 'Имэйл хаяг оруулна уу',
'email.email' => 'Имэйл хаяг буруу байна',
'content.required' => 'Сэтгэгдэл оруулна уу',
);

}