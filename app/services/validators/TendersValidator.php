<?php namespace App\Services\Validators;
 
class TendersValidator extends Validator {
 
    public static $rules = array(
        'title' => 'required',
        'body'  => 'required',
    );
 
}