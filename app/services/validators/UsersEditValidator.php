<?php namespace App\Services\Validators;
 
class UsersEditValidator extends Validator {
 	
    public static $rules = array(
        'email' => 'required|email|unique:users',
        'first_name' => 'required',
        'last_name' => 'required',
        'user_type'  => 'required',
    );

 	public static $messages = array(
        'email.required' => 'Имэйл хаяг оруулна уу',
        'email.email' => 'Имэйл хаяг буруу байна',
        'first_name.required' => 'Нэр оруулна уу',
        'last_name.required' => 'Овог оруулна уу',
        'user_type.required' => 'Хэрэглэгчийн эрх сонгоно уу',
    );
}