<?php namespace App\Services\Validators;
 
class ProductValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
        'description'  => 'required',
        'price'  => 'required',
        'contact'  => 'required',
        'password'  => 'required',
        'category_id'  => 'required',
        'duplicate'  => 'required',
    );

    public static $messages = array(
        'name.required' => 'Нэрээ оруулна уу',
        'description.required' => 'Зараа оруулна уу',
        'contact.required' => 'Утасны дугаараа оруулна уу',
        'password.required' => 'Нууц үгээ оруулна уу',
        'price.required' => 'Үнээ оруулна уу',
        'category_id.required' => 'Ангилалаа сонгоно уу',
        'duplicate.required' => '1 зарыг өдөрт 1 л удаа оруулна',
    );

   public static $fields = array(
        'name.required' => 'name',
        'description.required' => 'description',
        'contact.required' => 'contact',
        'password.required' => 'password',
        'price.required' => 'price',
        'category_id.required' => 'category_id',
        'duplicate.required' => 'duplicate',
    );
 
}