<?php namespace App\Services\Validators;

class ProfileValidator extends Validator {

public static $rules = array(
'last_name' => 'required',
'first_name' => 'required',
'email' => 'required|email',
'position' => 'required',
'phone' => 'required',
'address' => 'required',
);

public static $messages = array(
'last_name.required' => 'Овогоо оруулна уу',
'first_name.required' => 'Нэрээ оруулна уу',
'email.required' => 'Имэйл хаяг оруулна уу',
'email.email' => 'Имэйл хаяг буруу байна',
'position.required' => 'Мэргэжил оруулна уу',
'phone.required' => 'Утасны дугаар оруулна уу',
'address.required' => 'Хаяг оруулна уу',
);

}