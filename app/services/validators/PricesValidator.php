<?php namespace App\Services\Validators;
 
class PricesValidator extends Validator {
 
    public static $rules = array(
        'price' => 'required|numeric',
    );
 
}