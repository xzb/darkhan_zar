<?php namespace App\Services\Validators;
 
class ContactValidator extends Validator {
 
    public static $rules = array(
        'answer' => 'required',
        'name'  => 'required',
        'email'  => 'required',
        'question'  => 'required',
	);
 
}