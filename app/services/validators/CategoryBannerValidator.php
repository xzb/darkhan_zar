<?php namespace App\Services\Validators;
 
class CategoryBannerValidator extends Validator {
 
    public static $rules = array(
        'banner_id' => 'required',
    );

    public static $messages = array(
    	'banner_id.required' => 'Баннер сонгоно уу',	
    );
 
}