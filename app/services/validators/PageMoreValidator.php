<?php namespace App\Services\Validators;
 
class PageMoreValidator extends Validator {
 
    public static $rules = array(
        'title' => 'required',
        'body'  => 'required',
    );
 
}