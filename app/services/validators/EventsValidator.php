<?php namespace App\Services\Validators;
 
class EventsValidator extends Validator {
 
    public static $rules = array(
        'title' => 'required',
        'body'  => 'required',
    );
 
}