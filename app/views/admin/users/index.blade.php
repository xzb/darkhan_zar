@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Хэрэглэгчид

              <a style="float:right;" href="{{ URL::route('admin.users.create') }}">
                <button type="button" class="btn btn-success">Хэрэглэгч нэмэх</button>
              </a>
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i>Нэр</th>
                  <th><i class="icon-bookmark"></i>Имэйл</th>
                  <th>Эрх</th>
                  <th>Хүйс</th>
                  <th>Төрсөн он</th>
                  <th>Төрөл</th>
                  <th>Зарын тоо</th>
                  <th>Статус</th>
                  <th></th>
              </tr>
              </thead>
              <tbody>
              @foreach ($users as $user)  
              <tr>
                  <td><a href="{{ URL::route('admin.users.edit', $user->id) }}">{{ $user->first_name }}</a></td>
                  <td>{{ $user->email }}</td>
                  <td>
                    @foreach($user->getGroups() as $group)
                    {{ $group->name }}
                    @endforeach
                  </td>
                  <td>
                    @if($user->gender == 1)
                      Эм
                    @else
                      Эр
                    @endif
                  </td>
                  <td>
                    {{$user->year}}
                  </td>
                  <td>
                    @if($user->type == 0)
                      Бүртгүүлсэн
                    @elseif($user->type == 1)
                      Facebook
                    @elseif($user->type == 2)
                      Twitter
                    @endif
                  </td>
                  <td>
                    {{ $user->myProductsCount() }}
                  </td>
                  <td>{{ ($user->activated == 0)?'идэвхгүй':'идэвхтэй' }}</td>
                  <td>
                    <a href="{{ URL::route('admin.users.edit', $user->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    @if($logged_group[0]['name'] == 'Admin')
                    {{ Form::open(array('route' => array('admin.users.destroy', $user->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.users.destroy', $user->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}
                    @endif
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $users->links() }}</div>
      </section>
  </div>
</div>
 
@stop