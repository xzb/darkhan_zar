@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
          <header class="panel-heading">
              Хайлт
          </header>
          {{ Form::open(array('class'=>'form-inline', 'method'=>'GET', 'route' => 'admin.users.search')) }}
          <div class="panel-body">
                  <div class="form-group">
                      <label for="title" class="sr-only">Түлхүүр үг</label>
                      <input type="text" placeholder="Түлхүүр үг" id="keyword" name ="keyword" class="form-control" value="{{$keyword}}">
                  </div>
                  <button class="btn btn-success" type="submit">Хайх</button>
          </div>
          {{ Form::close()}}
      </section>
      <section class="panel">
          <header class="panel-heading">
              Хэрэглэгчидийн хайсан түлхүүр үгс - нийт <b>{{$searchs->getTotal()}}</b>
          </header>
          <table class="table table-striped table-advance table-hover">
              <tbody>
              @foreach ($searchs as $search)  
              <tr>
                  <td>{{ $search->keyword }}</td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $searchs->appends(array('keyword' => $keyword))->links() }}</div>
      </section>
  </div>
</div>
 
@stop