@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.usersform', array('user'=>$user,'groups'=>$groups,'users_group'=> $users_group,'route'=>array('admin.users.store')))

@stop