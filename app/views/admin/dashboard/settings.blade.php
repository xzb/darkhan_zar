@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <form action="/admin/settings" method="POST" class="form-horizontal">

      @if ($errors->any())
      <div class="alert alert-error">
              {{ implode('<br>', $errors->all()) }}
      </div>
      @endif

      <section class="panel">
          <header class="panel-heading">
              Тохиргоо
          </header>
          
                  <div class="panel-body">

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Backend site title</label>
                          <div class="col-lg-6">
                          {{ Form::text('backend_title',($settings!=null)?$settings->backend_title:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Frontend site title</label>
                          <div class="col-lg-6">
                          {{ Form::text('frontend_title',($settings!=null)?$settings->frontend_title:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Frontend site description</label>
                          <div class="col-lg-6">
                          {{ Form::text('frontend_description',($settings!=null)?$settings->frontend_description :'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Frontend site keywords</label>
                          <div class="col-lg-6">
                          {{ Form::text('frontend_keywords',($settings!=null)?$settings->frontend_keywords :'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                          </div>
                      </div>

                  </div>

          </form>
      </section>
      </form>
  </div>
</div>

@stop