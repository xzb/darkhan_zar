@extends('admin._layouts.default')
 
@section('main')

<link href="/admin/template/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
<script src="/admin/template/assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
<script src="/admin/template/assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
<style type="text/css">
.bar .tooltips {
  background-color: #ff6c60 !important;
}
</style>

<div class="row">

  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Export to Excel
          </header>
          
          <form class="form-inline" method="POST" action="/admin/toExcel">
          <div class="panel-body">
                  <div class="form-group" style="width: 275px;">
                    <div class="col-lg-3">
                        <div class="input-group input-large" data-date="2014-07-14" data-date-format="yyyy-mm-dd">
                            <input type="text" placeholder="эхлэх" id="from" name ="from" class="form-control dpd1" style="width: 100px;" value="">
                            <span class="input-group-addon">-ээс</span>
                            <input type="text" placeholder="дуусах" id="to" name ="to" class="form-control dpd2" style="width: 100px;" value="">
                        </div>
                    </div>
                  </div>
                  <button class="btn btn-success" type="submit">Export to Excel</button>
          </div>
          </form>
      </section>

                  </div>
  <?php if($max>0):?>
  <div class="col-lg-6">
      <section class="panel">
          <header class="panel-heading">
              Сүүлийн нэмэгдсэн зарууд(өдрөөр)
          </header>
          
          <div class="custom-bar-chart chart_2" style="margin: 30px;">
              <ul class="y-axis">
                  <li><span><?php echo $max?></span></li>
                  <li><span><?php echo round($max/4*3)?></span></li>
                  <li><span><?php echo round($max/2)?></span></li>
                  <li><span><?php echo round($max/4*1)?></span></li>
                  <li><span>0</span></li>
              </ul>
              <?php foreach($counts as $d=>$count): $y = $d+1;?>
              <div class="bar">
                  <div class="title"><?php echo date('m/d', strtotime('-'.$y.' days'))?></div>
                  <div class="value tooltips" data-original-title="<?php echo $count;?>" data-toggle="tooltip" data-placement="top"><?php echo ($count * 100) / $max;?>%</div>
              </div>
              <?php endforeach;?>
          </div>
          <!--custom chart end-->
                  
      </section>
  </div>
  <?php endif;?>
  <div class="col-lg-6">
      <section class="panel">
          <header class="panel-heading">
              Сүүлийн нэмэгдсэн зарууд(сараар)
          </header>
          <?php if($mmax>0):?>
          <div class="custom-bar-chart" style="margin: 30px;">
              <ul class="y-axis">
                  <li><span><?php echo $mmax?></span></li>
                  <li><span><?php echo round($mmax/4*3)?></span></li>
                  <li><span><?php echo round($mmax/2)?></span></li>
                  <li><span><?php echo round($mmax/4*1)?></span></li>
                  <li><span>0</span></li>
              </ul>
              <?php foreach($months as $d=>$count): $dd = $d+1;?>
              <div class="bar" {{ $d == 0 ? 'style="margin-left: 20px;"' : '' }}>
                  <div class="title"><?php echo date("m", strtotime("-".$d." months"))?></div>
                  <div class="value tooltips" data-original-title="<?php echo $count;?>" data-toggle="tooltip" data-placement="top"><?php echo ($count * 100) / $mmax;?>%</div>
              </div>
              <?php endforeach;?>
          </div>
          <!--custom chart end-->
          <?php endif;?>
                  
      </section>
  </div>
</div>
 <br/>
<div class="row">
      <div class="col-lg-6">
          <section class="panel">
              <header class="panel-heading">
                  Үндсэн ангилалд сүүлд нэмэгдсэн зарууд(өдрөөр)
              </header>
              <div class="panel-body">
                  <div id="hero-area" class="graph"></div>
              </div>
          </section>
      </div>
      <div class="col-lg-6">
          <section class="panel">
              <header class="panel-heading">
                  Үндсэн ангилалд сүүлд нэмэгдсэн зарууд(сараар)
              </header>
              <div class="panel-body">
                  <div id="hero-donut" class="graph"></div>
              </div>
          </section>
      </div>
  </div>
</div>
<script>
    var Script = function () {

    //morris chart
    
    $(function () {
      Morris.Area({
        element: 'hero-area',
        data: <?php echo $jsonArray;?>,

          xkey: 'period',
          ykeys: ['job', 'prod', 'car', 'apart', 'serve'],
          labels: ['Ажилд авна', 'Бараа зарна', 'Авто машин', 'Үл хөдлөх', 'Үйлчилгээ'],
          hideHover: 'auto',
          lineWidth: 1,
          pointSize: 5,
          lineColors: ['#4a8bc2', '#ff6c60', '#a9d86e', '#9440ED', '#BD9B33'],
          fillOpacity: 0.5,
          smooth: true
      });

      Morris.Area({
        element: 'hero-donut',
        data: <?php echo $isonArray;?>,

          xkey: 'period',
          ykeys: ['job', 'prod', 'car', 'apart', 'serve'],
          labels: ['Ажилд авна', 'Бараа зарна', 'Авто машин', 'Үл хөдлөх', 'Үйлчилгээ'],
          hideHover: 'auto',
          lineWidth: 1,
          pointSize: 5,
          lineColors: ['#4a8bc2', '#ff6c60', '#a9d86e', '#9440ED', '#BD9B33'],
          fillOpacity: 0.5,
          smooth: true
      });

      $('.code-example').each(function (index, el) {
        eval($(el).text());
      });
    });

}();

$( "div.tooltips" ).hover(
  function() {
    $( this ).html(($( this ).attr('data-original-title')));
  }, function() {
    $( this ).html('');
  }
);


</script>
<style>
  .tooltips{
    color: black !important;
    font-size: 10px;
  }
</style>
@stop