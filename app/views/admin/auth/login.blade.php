<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Singleton Technology LLC">
    <title>Singleton Technology - CMS</title>
        @include('admin._partials.assets')
</head>

<body>
<div class="container">

      {{ Form::open(array('class'=>'form-signin')) }}

        @if ($errors->has('login'))
                <div class="alert alert-error">{{ $errors->first('login', ':message') }}</div>
        @endif

        <h2 class="form-signin-heading">Админ хэсэгт нэвтрэх</h2>
        <div class="login-wrap">

            {{ Form::text('email', null, array('placeholder'=>'Хэрэглэгчийн нэг', 'class' => 'form-control', 'autofocus'=>'autofocus')) }}
            {{ Form::password('password', array('placeholder'=>'Нууц үг', 'class' => 'form-control')) }}

            {{ Form::submit('Нэвтрэх', array('class' => 'btn btn-lg btn-login btn-block')) }}
        </div>
      {{ Form::close() }}    

</div>

<!-- js placed at the end of the document so the pages load faster -->
<script src="/admin/template/js/jquery.js"></script>
<script src="/admin/template/js/bootstrap.min.js"></script>
</body>

</html>