<?php use App\Models\Category; ?>
<?php use App\Models\Banner; ?>

@extends('admin._layouts.default')

@section('main')
<div class="row">
  <div class="col-lg-12" style="overflow: scroll">
    <section class="panel">
      <header class="panel-heading">
          Ангилал
      </header>
    <div class="panel-body">
        <div id="nestable_list_1" class="dd">
          <div align="right" style="padding-right: 20px; position: inherit">
          </div>
            <ol class="dd-list">
              @foreach ($banners as $i => $cat_banner)
              <?php $category = Category::find($cat_banner->category_id);?>
              <?php $banner1 = Banner::find($cat_banner->banner_id);?>
              <?php $banner2 = Banner::find($cat_banner->banner2_id);?>
              <?php $banner3 = Banner::find($cat_banner->banner3_id);?>
                <li data-id="1" class="dd-item dd-collapsed">
                  <div class="dd-handle" id="data{{ $cat_banner->id }}">{{ $category->name }} - 
                    @if($banner1) {{$banner1->name}} - @endif
                    @if($banner2) {{$banner2->name}} - @endif
                    @if($banner3) {{$banner3->name}} @endif
                      <div class="pull-right hidden-phone">
                        <a href="{{ URL::route('admin.categorybanner.edit', $cat_banner->id) }}">
                          <button class="btn btn-primary btn-xs icon-pencil"></button>
                        </a>  
                        {{ Form::open(array('route' => array('admin.categorybanner.destroy', $cat_banner->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.category.destroy', $cat_banner->id) }}" class="btn btn-danger btn-xs icon-trash"></button>
                        {{ Form::close() }}  
                        
                      </div>
                  </div>
                </li>
              @endforeach
            </ol>
            
        </div>
    </div>
    </section>    
  </div>
</div>

  
  



@stop