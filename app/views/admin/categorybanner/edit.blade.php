@extends('admin._layouts.default')
 
@section('main')
  @include('admin._partials.categorybannerform', array('category' => $category, 'root_id'=>$category->category_id, 'combobox' => $combobox, 'route' => array('admin.categorybanner.update',$category->id),'no_edit'=>$no_edit, 'banner2'=>$banner2)) 
@stop