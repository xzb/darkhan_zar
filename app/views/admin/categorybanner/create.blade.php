@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.categorybannerform', array('category'=>null,'root_id'=>$root_id,'combobox' => $combobox,'route'=>['admin.categorybanner.store',$root_id],'no_edit'=>$no_edit, 'banner2'=>$banner2, 'category_col'=>$category_col))

@stop