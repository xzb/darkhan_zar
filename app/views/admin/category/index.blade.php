@extends('admin._layouts.default')
{{ HTML::style('css/nestedlist.css') }}
{{ HTML::script('js/category_nested.js') }}

@section('main')
<div class="row">
  <div class="col-lg-12" style="overflow: scroll">
    <section class="panel">
      <header class="panel-heading">
          Ангилал
      </header>
    <div class="panel-body">
        <div id="nestable_list_1" class="dd">
          <div align="right" style="padding-right: 20px; position: inherit">
          <a href="{{ URL::route('admin.category.create') }}">
            Үндсэн ангилал нэмэх
          </a>
          </div>
            <ol class="dd-list">
              @foreach ($categories as $i => $category)
                <li data-id="1" class="dd-item dd-collapsed">
                  @if(!$category->isLeaf())
                  <button type="button" id="b{{ $category->id }}" data-action="expand" drawed="false" onclick="drawLeafs({{ $category->id }});">Expand</button>
                  @endif
                  <div class="dd-handle" id="data{{ $category->id }}" onclick="drawLeafs({{ $category->id }});">{{ $category->name }}
                      <div class="pull-right hidden-phone">
                        <a href="{{ URL::route('admin.category.add', $category->id) }}">
                          <button class="btn btn-success btn-xs icon-plus"></button>
                        </a>
                        <a href="{{ URL::route('admin.category.edit', $category->id) }}">
                          <button class="btn btn-primary btn-xs icon-pencil"></button>
                        </a>
                        <a href="{{ URL::route('admin.categorybanner.add', $category->id) }}" title="Хуудас эзэмшигч нэмэх">
                          <button class="btn btn-xs icon-plus"></button>
                        </a>  
                        {{ Form::open(array('route' => array('admin.category.destroy', $category->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.category.destroy', $category->id) }}" class="btn btn-danger btn-xs icon-trash"></button>
                        {{ Form::close() }}  
                        
                      </div>
                  </div>
                </li>
              @endforeach
            </ol>
            <div align="right" style="padding-right: 20px; position: inherit"  title="Ангилал устгасан эсвэл нэмсэн тохиолдолд дарна" alt="Ангилал устгасан эсвэл нэмсэн тохиолдолд дарна">
                <a href="{{ URL::route('admin.category.rebuild') }}">
                    Дахин эрэмбэлэх
                </a>
            </div>
        </div>
    </div>
    </section>    
  </div>
</div>

@stop