@extends('admin._layouts.default')
 
@section('main')
  @include('admin._partials.categoryform', array('category' => $category, 'combobox' => $combobox, 'route' => array('admin.category.update',$category->id),'no_edit'=>$no_edit, 'category_col'=>$category_col,'categoriesCombobox'=>$categoriesCombobox)) 
@stop