@extends('admin._layouts.default')
 
@section('main')
@if($root_id == 0)
@include('admin._partials.categoryform', array('category'=>null,'root_id'=>0,'combobox' => $combobox,'route'=>array('admin.category.storeAdd'),'no_edit'=>$no_edit,  'category_col'=>$category_col))
@else
@include('admin._partials.categoryform', array('category'=>null,'root_id'=>$root_id,'combobox' => $combobox,'route'=>['admin.category.store',$root_id],'no_edit'=>$no_edit, 'category_col'=>$category_col))
@endif
@stop