@section('main')

<!-- <form action="/file-upload" class="dropzone" id="my-awesome-dropzone"><input type="file" name="file" multiple /></form> -->
    <div class="row">
        <div class="col-lg-12">
          <?php if($page): ?>
            {{ Form::model($page, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'page_form')) }}
          <?php else:?>
            {{ Form::model($page, array('method' => 'put', 'files' => true, 'to' =>'admin/pages/store', 'class' => 'form-horizontal','id'=>'page_form')) }}
         <?php endif;?>

            @if ($errors->any())
            <div class="alert alert-error">
                    {{ implode('<br>', $errors->all()) }}
            </div>
            @endif
            <?php $pageType = array(0=>'Бүх сайтад', 1 => 'dazar.mn', 2=> 'auto.dazar.mn'); ?>
            <section class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-12">
                            {{ Form::select('type', $pageType, ($page!=null)?$page->type:0,array('class'=>'form-control m-bot15'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            {{ Form::select('page_id', $combobox, ($page!=null)?$page->page_id:0,array('class'=>'form-control m-bot15'))}}
                        </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-2">
                          <div class="fileupload fileupload-new" data-provides="fileupload">
                              <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                          
                                  <?php if($page != null && $page->image): ?>
                                  <img src="/files/uploads/<?php echo $page->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                  <?php else: ?>
                                  <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг" />
                                  <?php endif; ?>
                              </div>
                              <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                              <div>
                               <span class="btn btn-white btn-file">
                               <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                               <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                               {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
                               </span>
                                  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-5" style="margin-left: 5px;">
                          <div class="checkbox">
                            <label>{{ Form::checkbox('is_header', '1', ($page!=null)?$page->is_header:'', array('id' => 'is_header'))}}Толгой хэсэгт харуулах</label>
                          </div>

                          <div class="checkbox">
                            <label>{{ Form::checkbox('is_footer','1', ($page!=null)?$page->is_footer:'', array('id' => 'is_footer'))}}Хөл хэсэгт харуулах</label> 
                          </div>

                          <div class="checkbox">
                              <label>{{ Form::checkbox('is_right','1', ($page!=null)?$page->is_right:'', array('id' => 'is_right'))}}Баруун хэсэгт харуулах</label>
                          </div>
                          <div class="checkbox">
                              <label>{{ Form::checkbox('is_left', '1', ($page!=null)?$page->is_left:'', array('id' => 'is_left'))}}Зүүн хэсэгт харуулах</label> 
                          </div>
                          <div class="checkbox">
                              <label>{{ Form::checkbox('is_homepopup', '1', ($page!=null)?$page->is_homepopup:'', array('id' => 'is_homepopup'))}}Нүүр хуудас POPUP</label> 
                          </div>

                          <div class="checkbox">
                              <label>{{ Form::checkbox('is_active', '1', ($page!=null)?$page->is_active:1, array('id' => 'is_active'))}} <b>Идэвхитэй эсэх.</b> </label> 
                          </div>
                      </div>
                    </div>                    
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue ">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#home" data-toggle="tab">Монгол</a>
                        </li>
                        <li class="">
                            <a href="#about" data-toggle="tab">English</a>
                        </li>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                          <div class="form">
                              <div class="form-group">
                                   {{ Form::label('title', 'Гарчиг', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                   <div class="col-sm-10">
                                          {{ Form::text('title', ($page!=null)?$page->title:'', array('class' => 'form-control')) }}
                                   </div>
                              </div>

                              <div class="form-group">
                                   {{ Form::label('slug', 'Url', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                   <div class="col-sm-10">
                                          {{ Form::text('slug', ($page!=null)?$page->slug:'', array('class' => 'form-control')) }}
                                   </div>
                              </div>

                              <div class="form-group">
                                  {{ Form::label('body', 'Контент', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                                  <div class="col-sm-10">
                                        {{ Form::textarea('body', ($page!=null)?$page->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '6')) }}
                                  </div>
                              </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="about">
                          <div class="form">
                                  <div class="form-group">
                                       {{ Form::label('title_en', 'Title', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                       <div class="col-sm-10">
                                              {{ Form::text('title_en', ($page!=null)?$page->title_en:'', array('class' => 'form-control')) }}
                                       </div>
                                  </div>

                                  <div class="form-group">
                                      {{ Form::label('body_en', 'Content', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                                      <div class="col-sm-10">
                                            {{ Form::textarea('body_en', ($page!=null)?$page->body_en:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '6')) }}
                                      </div>
                                  </div>

                                  <div class="form-group">
                                       {{ Form::label('meta_title_en', 'Meta Title', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                       <div class="col-sm-10">
                                              {{ Form::text('meta_title_en', ($page!=null)?$page->meta_title_en:'', array('class' => 'form-control')) }}
                                       </div>
                                  </div>                
                                  <div class="form-group">
                                       {{ Form::label('meta_keywords_en', 'Meta Keywords', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                       <div class="col-sm-10">
                                              {{ Form::text('meta_keywords_en', ($page!=null)?$page->meta_keywords_en:'', array('class' => 'form-control')) }}
                                       </div>
                                  </div>                
                                  <div class="form-group">
                                       {{ Form::label('meta_description_en', 'Meta Description', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                       <div class="col-sm-10">
                                              {{ Form::text('meta_description_en', ($page!=null)?$page->meta_description_en:'', array('class' => 'form-control')) }}
                                       </div>
                                  </div>  
                          </div>
                        
                    </div>

                  </div>
                </div>
            </section>

            {{ Form::close() }}
            
<script src="/dropzone/dropzone.js" ></script>

<script type="text/javascript">
   function removeUploads(id){
        $.get('/admin/pages/removeUpload/'+id, function(data) {
        });
    }
    

    Dropzone.options.myDropzone = {
      init: function() {
          thisDropzone = this;

          $.get('/admin/pages/loadUploads/<?php echo ($page)?$page->id:0; ?>', function(data) {

            var data = jQuery.parseJSON(data);

            if(data.length > 0)
            {
                for (var i = 0; i < data.length; i++) {
                  var mockFile = { name: data[i]['name'], size: data[i]['size'], upload_id: data[i]['id'] };
                   
                  thisDropzone.options.addedfile.call(thisDropzone, mockFile);
   
                  thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/uploads/page/pictures/"+data[i]['name']);
              }
            } 
          });

          this.on("complete", function(file) {
            $.get('/admin/pages/loadPicAfterUpload', function(data) {

                   var data = jQuery.parseJSON(data);
          
                   var mockFile = { name: data[0]['name'], size: data[0]['size'], upload_id: data[0]['id'] };
                   
                   thisDropzone.options.addedfile.call(thisDropzone, mockFile);
   
                   thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/uploads/page/pictures/"+data[0]['name']);

                   $('.dz-processing').remove();
               
            });
          });
      }
  };
</script>

<div class="panel panel-default">
  <div class="panel-heading">
      <h4 class="panel-title">
          <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                Зураг оруулах
          </a>
      </h4>
  </div>
  <div class="panel-collapse collapse {{ ($page)?(($page->is_uploads == 1)?'in':''):''}}" id="collapseOne">
      <div class="panel-body">
        {{ Form::open(array('url'=>'admin/pages/upload','method' => 'post', 'files' => true,'class' => 'dropzone','id'=>'my-dropzone')) }}
        {{ Form::close()}}
      </div>
  </div>  
</div>


<section class="panel">
  <div class="panel-body" align="center">
      <a href="#"><button type="button" class="btn btn-info" onclick="document.getElementById('page_form').submit();">Хадгалах</button></a>
      <a href="{{ URL::route('admin.pages.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
  </div>
</section>

            

        </div>
    </div>

@stop