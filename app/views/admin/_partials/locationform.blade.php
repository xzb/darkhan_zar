@section('main')
      <div class="row">
          <div class="col-lg-12">
              <?php if($location): ?>
                {{ Form::model($location, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
              <?php else:?>
                {{ Form::model($location, array('method' => 'put', 'files' => true, 'to' =>'admin/location/store', 'class' => 'form-horizontal')) }}
             <?php endif;?>

              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Байршил тохиргоо
                  </header>
                  <div class="panel-body">
                    
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Эцэг ангилал</label>
                          <div class="col-lg-6">
                          {{ Form::select('parent_id',$selectbox, ($location!=null)?$location->parent_id:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                    
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                          {{ Form::text('name',($location!=null)?$location->name:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Дэс дараалал</label>
                          <div class="col-lg-6">
                          {{ Form::text('sort',($location!=null)?$location->sort:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.location.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop