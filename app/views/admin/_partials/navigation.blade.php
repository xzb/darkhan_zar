<div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    
            <ul class="sidebar-menu sub" id="nav-accordion">

                @if($logged_group[0]['name'] == 'Admin')
                <li class="sub-menu">
                    <a href="{{ URL::to('admin/dashboard') }}">
                        <i class="fa fa-pie-chart"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @endif

                <li class="sub-menu">
                    <a href="{{ URL::route('admin.product.index') }}">
                        <i class="icon-book"></i>
                        <span>Зарууд</span>
                    </a>
                </li>
        
                @if($logged_group[0]['name'] == 'Admin')
                <li class="sub-menu">
                    <a href="{{ URL::route('admin.pages.index') }}">
                        <i class="icon-laptop"></i>
                        <span>Хуудас</span>
                    </a>
                </li>
                @endif

                <li class="sub-menu">
                    <a href="{{ URL::route('admin.banner.index') }}" >
                        <i class="icon-picture"></i>
                        <span>Баннер</span>
                    </a>
                    {{-- <ul class="sub">
                        <li><a href="{{ URL::route('admin.categorybanner.index') }}">Хуудас эзэмшигч</a></li>
                        <li><a href="{{ URL::route('admin.bannerloc.index') }}">Баннер төрөл</a></li>
                        <li><a href="{{ URL::route('admin.banner.create') }}">Баннер нэмэх</a></li>
                    </ul> --}}
                </li>

                <li class="sub-menu">
                    <a href="{{ URL::route('admin.users.index') }}" >
                        <i class="icon-user-md"></i>
                        <span>Хэрэглэгчид</span>
                    </a>
                </li>

                @if($logged_group[0]['name'] == 'Admin')
                <li class="sub-menu">
                    <a href="{{ URL::route('admin.category.index') }}" >
                        <i class="icon-tasks"></i>
                        <span>Ангилал</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-tasks"></i>
                        <span>Шинж чанар</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ URL::route('admin.attribute.index') }}">Бүх attribute</a></li>
                        <li><a href="{{ URL::route('admin.attribute.create') }}">Шинж чанар нэмэх</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-tasks"></i>
                        <span>Байршил</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ URL::route('admin.location.index') }}">Жагсаалт</a></li>
                        <li><a href="{{ URL::route('admin.location.create') }}">Байршил нэмэх</a></li>
                    </ul>
                </li>
                @endif

                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-shield"></i>
                        <span>Хар жагсаалт</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ URL::route('admin.blacklist.index') }}">Бүх хар жагсаалт</a></li>
                        <li><a href="{{ URL::route('admin.blacklist.create') }}">Хар жагсаалт нэмэх</a></li>
                    </ul>
                </li>

                @if($logged_group[0]['name'] == 'Admin')
                <li class="sub-menu">
                    <a href="javascript:;" >
                        <i class="fa fa-line-chart"></i>
                        <span>Статистик</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ URL::route('admin.product.categorystats') }}">Категори хандалт</a></li>
                        <li><a href="{{ URL::route('admin.users.search') }}">Хайлтын дүн</a></li>
                    </ul>
                </li>
                @endif

                @if($logged_group[0]['name'] == 'Admin')
                <li class="sub-menu">
                    <a href="{{ URL::to('admin/settings') }}">
                        <i class="fa fa-cog"></i>
                        <span>Settings</span>
                    </a>
                </li>
                @endif

            </ul>
   
    <!-- sidebar menu end-->
</div>