<?php use App\Models\Category;?>
@section('main')

{{ HTML::script('js/category_nested.js') }}

  {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'product_form')) }}

<div class="row">

@if ($errors->any())
<div class="alert alert-error">
        {{ implode('<br>', $errors->all()) }}
</div>
@endif
  
  <div class="col-lg-12">
    <section class="panel">
        <div class="panel-body">
          <div class="form">
            <div class="form-inline" id="categoryCombos">
               
                  <input type="hidden" name="category_id" id="category_id" value="{{ $product->category_id }}"/>
                  <input type="hidden" name="categories" id="categories"/>
                  <div id="categories0" class="form-group" style="padding-left:16px">
                  <select class="form-control" name="category0" id="category0" style="width:150px" onchange="drawCategoryCombo($(this).val(),0,1)">
                      <?php
                         for($i= 0; $i<count($cArray); $i++)
                         {?>
                            <option value="<?php echo $cArray[$i]['id'];?>" <?php if(in_array($cArray[$i]['id'], $parents)){echo 'selected="selected"';}?>><?php echo $cArray[$i]['name'];?></option>   
                         <?php }
                      ?>
                  </select>
                  <?php foreach($parentcats as $parent):?>
                  <?php if($parent->parent_id < 1) continue;?>
                  <?php $levels = Category::getChildren($parent->parent_id);?>
                  <div id="categories<?php echo $parent->parent_id?>" class="form-group" style="padding-left:20px">
                    <select class="form-control" id="category<?php echo $parent->parent_id?>" style="width: 146px;" onchange="drawCategoryCombo($(this).val(),<?php echo $parent->parent_id?>,$('#category<?php echo $parent->parent_id?>').find('option:selected').attr('is_leaf'))">
                      <option class="form-control" value="0">Сонгоно уу</option>
                      <?php foreach($levels as $level):?>
                      <option value="<?php echo $level->id;?>" <?php if(in_array($level->id, $parents)){echo 'selected="selected"';}?> is_leaf="<?php echo (!$level->isLeaf())?1:0?>"><?php echo $level->name;?></option>
                      <?php endforeach;?>
                    </select>
                  <?php endforeach;?>
                  <?php foreach($parentcats as $parent):?>
                  <?php if($parent->parent_id < 1) continue;?>
                  </div>
                  <?php endforeach;?>
                  </div>
               
                
            </div>

            
                          
          </div>  
          
          
          
          
      </div>
  

      <div class="panel-body">

                <div class="form">
                        <div class="form-group">
                             <div class="col-sm-12">
                                <label>{{ Form::checkbox('is_featured','1', ($product!=null)?$product->is_featured:'', array('id' => 'is_featured'))}}Онцлох зар</label> 
                             </div>
                        </div>

                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('name', ($product!=null)?$product->name:'', array('class' => 'form-control', 'placeholder' => 'Гарчиг')) }}
                             </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                  {{ Form::textarea('description', ($product!=null)?$product->description:'', array('class' => 'form-control', 'rows' => '3', 'placeholder' => 'Тайлбар')) }}
                            </div>
                        </div>

                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('price', ($product!=null)?$product->price:'', array('class' => 'form-control', 'placeholder' => 'Үнэ')) }}
                             </div>
                        </div>
                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('contact', ($product!=null)?$product->contact:'', array('class' => 'form-control', 'placeholder' => 'Холбоо барих')) }}
                             </div>
                        </div>
                </div>


      </div>
  </section>
</div>


</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a href="#collapseOne1" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                  Байршил
            </a>
        </h4>
    </div>
    <div class="panel-collapse collapse in" id="collapseOne1">
        <div class="panel-body">

                           <?php $latlng = '47.91990271551152,106.91825866699219';
                                if($product){
                                     $latlng = ($product->latlng !== '')?$product->latlng:'47.91990271551152,106.91825866699219';
                                            } 
                           ?>
                              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKGHDGh5HEeEN9ediwKxFhAUbQoYolVb4"></script>
                              <script>

                              var ub = new google.maps.LatLng(<?php echo $latlng ?>);
                              var marker;
                              var map;

                              function initialize() {
                                var mapOptions = {
                                  zoom: 14,
                                  center: ub
                                };

                                map = new google.maps.Map(document.getElementById('map-canvas'),
                                        mapOptions);

                                marker = new google.maps.Marker({
                                  map:map,
                                  draggable:true,
                                  animation: google.maps.Animation.DROP,
                                  position: ub
                                });
                                google.maps.event.addListener(map, 'click', function (event) {
                                    marker.setPosition(event.latLng);
                                    $('#latlng').val(event.latLng.lat()+','+event.latLng.lng());
                                });

                              }

                              google.maps.event.addDomListener(window, 'load', initialize);

                                

                              </script>
                              <div class="row">
                                  <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
                                      <div class="form-group form-group-main form-group-unique">
                                        <div id="map-canvas" style="width:600px; height:400px"></div>
                                        <input id="latlng" name="latlng" value="<?php echo $latlng; ?>" type="hidden" />
                                      </div>
                                  </div>
                              </div>

        </div>
    </div>  
</div>


{{ Form::close() }}



<script src="/dropzone/dropzone.js" ></script>
<script type="text/javascript">
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

     function removeUploads(id){
          $.get('/admin/product/removeUpload/'+id, function(data) {
          });
      }
      
 
      Dropzone.options.myDropzone = {
        uploadMultiple: false,
        init: function() {
            thisDropzone = this;
            $.get('/admin/product/loadUploads/<?php echo ($product)?$product->id:0; ?>', function(data) {

              if(IsJsonString(data))
               var data = jQuery.parseJSON(data);

              if(data.length > 0)
              {
                  for (var i = 0; i < data.length; i++) {
                    var mockFile = { name: data[i]['name'], size: data[i]['size'], upload_id: data[i]['id'] };
                     
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
     
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/uploads/thumb/"+data[i]['name']);
                }
              }
            });

            this.on("complete", function(file) {
              $.get('/admin/product/loadPicAfterUpload/{{$product->id}}', function(data) {

                    if(IsJsonString(data))
                     var data = jQuery.parseJSON(data);

                     var mockFile = { name: data[0]['name'], size: data[0]['size'], upload_id: data[0]['id'] };
                     
                     thisDropzone.options.addedfile.call(thisDropzone, mockFile);
     
                     thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/uploads/thumb/"+data[0]['name']);

                     $('.dz-processing').remove();
                 
              });
            });
        }

    };
</script>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                  Зураг оруулах
            </a>
        </h4>
    </div>
    <div class="panel-collapse collapse in" id="collapseOne">
        <div class="panel-body">
          {{ Form::open(array('url'=>'admin/product/upload','method' => 'post', 'files' => true,'class' => 'dropzone','id'=>'my-dropzone')) }}
          @if($product)
          <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}"/>
          @endif
          {{ Form::close()}}
        </div>
    </div>  
</div>


<section class="panel">
    <div class="panel-body" align="center">
        <a href="#"><button type="button" class="btn btn-info" onclick="document.getElementById('product_form').submit();">Хадгалах</button></a>
        <a href="{{ URL::route('admin.product.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
    </div>
</section>

@stop