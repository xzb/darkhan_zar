@section('main')

<link href="/css/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
<link href="/css/app.css" rel="stylesheet" type="text/css">
<script src="/js/bootstrap-tagsinput.js"></script>
<script src="/js/bloodhound.min.js"></script>
<script src="/js/typeahead.bundle.min.js"></script>


<?php if($page): ?>
  {{ Form::model($page, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'page_form')) }}
<?php else:?>
  {{ Form::model($page, array('method' => 'put', 'files' => true, 'to' =>'admin/pages/store', 'class' => 'form-horizontal','id'=>'page_form')) }}
<?php endif;?>

<div class="row">

@if ($errors->any())
<div class="alert alert-error">
        {{ implode('<br>', $errors->all()) }}
</div>
@endif
  


<div class="col-lg-3 news_form">
  <section class="panel first_col">
      <div class="panel-body">
          <div class="form-group">
              <div class="col-lg-12">
                  {{ Form::select('page_id', $combobox, ($page!=null)?$page->page_id:0,array('class'=>'form-control m-bot15'))}}
              </div>
          </div>
          <div class="form-group">
            <div class="col_lg_2">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                        
                        <?php if($page != null && $page->image): ?>
                        <img src="/files/uploads/<?php echo $page->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                        <?php else: ?>
                        <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг"/>
                        <?php endif; ?>
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                    <div>
                     <span class="btn btn-white btn-file">
                     <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                     <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                     {{ Form::file('file', array('class' => 'default', 'name' => 'image','multiple'=>true)) }}
                     </span>
                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                    </div>
                </div>
            </div>

            <div class="col_lg_2">

                <div class="checkbox">
                  <label>{{ Form::checkbox('is_featured', '1', ($page!=null)?$page->is_featured:'', array('id' => 'is_featured'))}} Онцлох мэдээ</label>
                </div>

                <div class="checkbox">
                  <label>{{ Form::checkbox('is_emergency', '1', ($page!=null)?$page->is_emergency:'', array('id' => 'is_emergency'))}} Шуурхай мэдээ</label>
                </div>

                <div class="checkbox">
                  <label>{{ Form::checkbox('is_publish', '1', ($page!=null)?$page->is_publish:'', array('id' => 'is_publish'))}} Нийтлэл</label>
                </div>

                <div class="checkbox">
                  <label>{{ Form::checkbox('is_conversation', '1', ($page!=null)?$page->is_conversation:'', array('id' => 'is_conversation'))}} Ярилцлага</label>
                </div>
                
                <div class="checkbox">
                    <label>{{ Form::checkbox('is_active', '1', ($page!=null)?$page->is_active:true, array('id' => 'is_active'))}}Идэвхтэй эсэх</label> 
                </div>
                <div class="checkbox">
                    <label>{{ Form::checkbox('is_comment', '1', ($page!=null)?$page->is_comment:true, array('id' => 'is_comment'))}}Сэтгэгдэлтэй эсэх</label> 
                </div>
            </div>
          </div>
      </div>
  </section>
</div>


<div class="col-lg-9">
  <section class="panel">

      <div class="panel-body">

                <div class="form">

                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('title', ($page!=null)?$page->title:'', array('class' => 'form-control', 'placeholder' => 'Гарчиг')) }}
                             </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                  {{ Form::textarea('description', ($page!=null)?$page->description:'', array('class' => 'form-control', 'rows' => '3', 'placeholder' => 'Тайлбар')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                  {{ Form::textarea('body', ($page!=null)?$page->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '15')) }}
                            </div>
                        </div>

                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('youtube_link', ($page!=null)?$page->youtube_link:'', array('class' => 'form-control', 'placeholder' => 'Youtube линк')) }}
                             </div>
                        </div>

                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('redactor', ($page!=null)?$page->redactor:'', array('class' => 'form-control', 'placeholder' => 'Зохиогч')) }}
                             </div>
                        </div>
                        <div class="form-group">
                             <div class="col-sm-12">
                                   {{ Form::text('news_tags', ($page!=null)?$page_tag:'', array('id'=>'news_tags','data-role'=>'tagsinput','class' => 'form-control','placeholder' => 'Холбоотой мэдээ')) }} 
                                <script>
                                      var cities = new Bloodhound({
                                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                                        limit:200,
                                        remote: window.location.protocol +'//'+window.location.host+'/admin/pages/tagsQuery/%QUERY',
                                      });
                                      cities.initialize();

                                      var elt = $('#news_tags');
                                            elt.tagsinput({
                                              typeaheadjs: {
                                                name: 'cities',
                                                displayKey: 'name',
                                                valueKey: 'name',
                                                source: cities.ttAdapter()
                                              }
                                            });

                                  
                                  </script>
                             </div>

                        </div>

                </div>


      </div>
  </section>
</div>





<script src="/dropzone/dropzone.js" ></script>

<script type="text/javascript">

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

     function removeUploads(id){
          $.get('/admin/pages/removeUpload/'+id, function(data) {
          });
      }
      
 
      Dropzone.options.myDropzone = { 
        uploadMultiple: false,
        init: function() {
            thisDropzone = this;
            $.get('/admin/pages/loadUploads/<?php echo ($page)?$page->id:0; ?>', function(data) {

              if(IsJsonString(data))
               var data = jQuery.parseJSON(data);

              if(data.length > 0)
              {
                  for (var i = 0; i < data.length; i++) {
                    var mockFile = { name: data[i]['name'], size: data[i]['size'], upload_id: data[i]['id'] };
                     
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
     
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/uploads/page/pictures/"+data[i]['name']);
                }
              } 
            });

            this.on("complete", function(file) {
              $.get('/admin/pages/loadPicAfterUpload', function(data) {

                    if(IsJsonString(data))
                     var data = jQuery.parseJSON(data);

                     var mockFile = { name: data[0]['name'], size: data[0]['size'], upload_id: data[0]['id'] };
                     
                     thisDropzone.options.addedfile.call(thisDropzone, mockFile);
     
                     thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "/files/uploads/page/pictures/"+data[0]['name']);

                     $('.dz-processing').remove();
                 
              });
            });
        }

    };
</script>

</div>
{{ Form::close() }}

<div class="row">
  
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                  Зураг оруулах
            </a>
        </h4>
    </div>

    <div class="panel-collapse collapse {{ ($page)?(($page->is_uploads == 1)?'in':''):''}}" id="collapseOne">
        <div class="panel-body">
          {{ Form::open(array('url'=>'admin/pages/upload','method' => 'post', 'files' => true,'class' => 'dropzone','id'=>'my-dropzone')) }}
          {{ Form::close()}}
        </div>
    </div>  
</div>


<section class="panel">
    <div class="panel-body" align="center">
        <a href="#"><button type="button" class="btn btn-info" onclick="document.getElementById('page_form').submit();">Хадгалах</button></a>
        <a href="{{ URL::route('admin.pages.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
    </div>
</section>  

</div>
 
@stop