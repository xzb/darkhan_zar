@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($user): ?>
              {{ Form::model($user, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($user, array('method' => 'put', 'files' => true, 'to' =>'admin/users/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              <input type="hidden" name="email2" value="{{ ($user!=null) ? $user->email : '' }}">
              @if ($errors->any())
              <div class="alert alert-error">
                      {{ implode('<br>', $errors->all()) }}
              </div>
              @endif
                        
              <section class="panel">
                  <header class="panel-heading">
                        Админы тохиргоо
                  </header>
                  <div class="panel-body">
                        <div class="form-group">
                          <div class="col-sm-2">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                            
                                    <?php if($user != null && $user->image): ?>
                                    <img src="/files/uploads/users/<?php echo $user->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                    <?php else: ?>
                                    <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг" />
                                    <?php endif; ?>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                 <span class="btn btn-white btn-file">
                                 <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                                 <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                 {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
                                 </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                             {{ Form::label('last_name', 'Овог', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                             <div class="col-sm-2">
                                    {{ Form::text('last_name', ($user!=null)?$user->last_name:'', array('class' => 'form-control')) }}
                             </div>
                        </div>
                        <div class="form-group">
                             {{ Form::label('first_name', 'Нэр', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                             <div class="col-sm-2">
                                    {{ Form::text('first_name', ($user!=null)?$user->first_name:'', array('class' => 'form-control')) }}
                             </div>
                        </div>  
                        <div class="form-group">
                             {{ Form::label('email', 'Хэрэглэгчийн нэр', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                             <div class="col-sm-2">
                                    {{ Form::text('email', ($user!=null)?$user->email:'', array('class' => 'form-control')) }}
                             </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Нууц үг', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                            <div class="col-sm-2">
                                  {{ Form::password('password', array('id'=>'password','class'=>'form-control')) }}
                            </div>
                            <label class="checkbox-inline">{{ Form::checkbox('activated', '1', ($user!=null)?$user->activated:'', array('id' => 'activated'))}}Идэвхтэй эсэх </label>
                           
                        </div>
                        <div class="form-group">
                            {{ Form::label('aasdf', 'Хэрэглэгчийн эрх', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                            <div class="col-sm-2">
                                  
                                  @foreach($groups as $key => $g)
                                  <?php $checked = ($users_group!=null)?(($users_group->group_id == $key)?1:0):0; 
                                  ?>

                                  {{ Form::radio('user_type', $key, $checked) }} {{ $g }}
                                  
                                  @endforeach
                            </div>
                        </div>
                                    
                      </div>
              </section>






              
                      <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                                            <a href="{{ URL::route('admin.users.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                                        </div>
                                    </div>
                            
                          </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop