<?php use App\Models\Category; ?>
      <div class="row">
          <div class="col-lg-12">
            <section class="panel">
                  <header class="panel-heading">
                      Category Attribute
                  </header>
                  <div class="panel-body">
                    <table class="table table-striped table-advance table-hover" style="width: 400px;">
                      <tbody id="catlist">
                        <?php foreach ($category_attributes as $category_attribute): ?>
                        <?php $category = Category::find($category_attribute->category_id);?>
                        <tr>
                          <td><?php echo $category->name?></td>
                          <td class="action">
                            <a href="#" onclick="deleteCategory(this, {{$category_attribute->category_id}}, {{$category_attribute->attribute_id}}); return false;">delete</a>
                          </td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                    <form class="jNice" onsubmit="return addCategory()" id="category_attribute_form" method="post">
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <input type="hidden" id="category_attribute_attribute_id" value="{{$attribute->id}}" name="attribute_id">          <button class="btn btn-round btn-primary" type="submit" name="" id=""><span><span>Нэмэx</span></span></button>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <td>
          <label for="category_attribute_category_id">Category</label>        </td>
        <td>
          <select style="width: 300px;" id="category_id" name="category_id" class="form-control" size="20">
              @foreach ($categorys as $category)
                        <option value="{{$category->id}}">
                        <?php for($i=0; $i<$category->level; $i++){
                          echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        ?>
                        {{$category->name}}</option>
                        @endforeach
          </select>
        </td>
      </tr>
    </tbody>
  </table>
</form>
                    </div>
                  </section>
          </div>
      </div>