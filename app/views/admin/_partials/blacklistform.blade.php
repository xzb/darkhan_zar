@section('main')
      <div class="row">
          <div class="col-lg-12">
              {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
              

              {{ Notification::showAll() }}
              @if ($errors->any())
              <div class="alert alert-error">
                {{ implode('<br>', $errors->all()) }}
              </div>
              @endif
              <section class="panel">
                  <header class="panel-heading">
                      Хар жагсаалт тохиргоо
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                          {{ Form::text('name',($blacklist!=null)?$blacklist->name:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                          <label><input type="radio" name="type" value="1" {{ isset($blacklist->type) == 1 ? 'checked' : '' }} /> Утас</label> <br />
                          <label><input type="radio" name="type" value="2" {{ isset($blacklist->type) == 2 ? 'checked' : '' }}/> Имэйл</label>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.blacklist.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop