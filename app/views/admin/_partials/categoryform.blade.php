@section('main')
      <div class="row">
          <div class="col-lg-12">
              {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
              
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Ангилал тохиргоо
                  </header>
                  <div class="panel-body">
                    @if(!$no_edit)
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Эцэг ангилал</label>
                          <div class="col-lg-6">
                          {{ Form::select('parent_id',$categoriesCombobox, ($category!=null)?$category->parent_id:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                    @endif  
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                          {{ Form::text('name',($category!=null)?$category->name:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">SEO Title</label>
                          <div class="col-lg-6">
                          {{ Form::text('seo_title',($category!=null)?$category->seo_title:'',array('class'=>'form-control', 'maxlength'=>'60'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">SEO Url</label>
                          <div class="col-lg-6">
                          {{ Form::text('seo_url',($category!=null)?$category->seo_url:'',array('class'=>'form-control', 'maxlength'=>'60'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">SEO Description</label>
                          <div class="col-lg-6">
                          {{ Form::text('seo_description',($category!=null)?$category->seo_description:'',array('class'=>'form-control', 'maxlength'=>'160'))}}
                          </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Эрэмбэ</label>
                          <div class="col-lg-6">
                          {{ Form::text('rank',($category!=null)?$category->rank:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Класс нэр</label>
                          <div class="col-lg-6">
                          {{ Form::text('root_css',($category!=null)?$category->root_css:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      {{-- <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Өнгө css</label>
                          <div class="col-lg-6">
                          {{ Form::text('root_color',($category!=null)?$category->root_color:'',array('class'=>'form-control'))}}
                          </div>
                      </div> --}}

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нүүр хуудсанд харагдах</label>
                          <div class="col-lg-6">
                          {{ Form::select('homepage_visible',array(1 =>'Тийм',0 =>'Үгүй'), ($category!=null)?$category->homepage_visible:0,array('class'=>'form-control'))}}
                          </div>
                      </div>

                      {{-- <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Баганы дугаар</label>
                          <div class="col-lg-6">
                           {{ Form::select('homepage_col_no',$category_col, ($category!=null)?$category->hompage_col_no:0,array('class'=>'form-control m-bot15'))}}      
                          </div>
                      </div> --}}

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Линк</label>
                          <div class="col-lg-6">
                          {{ Form::text('link',($category!=null)?$category->link:'',array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Баннер1</label>
                          <div class="col-lg-6">
                          {{ Form::select('banner_id', $combobox, ($category!=null)?$category->banner_id:0,array('class'=>'form-control m-bot15'))}}           
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Гарчигт ашиглах</label>
                          <div class="col-lg-6">
                          {{ Form::select('for_title',array(1 =>'Тийм',0 =>'Үгүй'), ($category!=null)?$category->for_title:0,array('class'=>'form-control'))}}
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Идэвхтэй эсэх</label>
                          <div class="col-lg-6">
                            <label>{{Form::radio('is_visible', 1, $category->is_visible == 1 ? true : false);}} Идэвхтэй</label>
                            <label>{{Form::radio('is_visible', 0, $category->is_visible == 0 ? true : false);}} Идэвхгүй</label>
                          </div>
                      </div>
                      
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.category.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop