@section('main')

<!-- <form action="/file-upload" class="dropzone" id="my-awesome-dropzone"><input type="file" name="file" multiple /></form> -->
    <div class="row">
        <div class="col-lg-12">
          <?php if($event): ?>
            {{ Form::model($event, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'event_form')) }}
          <?php else:?>
            {{ Form::model($event, array('method' => 'put', 'files' => true, 'to' =>'admin/events/store', 'class' => 'form-horizontal','id'=>'event_form')) }}
         <?php endif;?>

            @if ($errors->any())
            <div class="alert alert-error">
                    {{ implode('<br>', $errors->all()) }}
            </div>
            @endif
            <section class="panel">
                <div class="panel-body">
                    <div class="form-group">
                      <div class="col-md-2">
                          <div class="fileupload fileupload-new" data-provides="fileupload">
                              <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                          
                                  <?php if($event != null && $event->image): ?>
                                  <img src="/files/uploads/events/<?php echo $event->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                  <?php else: ?>
                                  <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг" />
                                  <?php endif; ?>
                              </div>
                              <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                              <div>
                               <span class="btn btn-white btn-file">
                               <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                               <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                               {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
                               </span>
                                  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                              </div>
                          </div>
                      </div>

                   </div>                    
                </div>
            </section>

            <section class="panel">
                <div class="panel-body">
                          <div class="form">
                            <div class="form-group">
                            
                              <label class="col-sm-2 control-label col-lg-2">Үйл явдал төрөл</label>
                                <div class="col-lg-6">
                                {{ Form::select('type', $typeArr, ($event!=null)?$event->type:0,array('class'=>'form-control m-bot15'))}}            
                                </div>
                            </div>
                            <div class="form-group">
                            
                              <label class="col-sm-2 control-label col-lg-2">Байгууллагын жагсаалт</label>
                                <div class="col-lg-6">
                                {{ Form::select('company_id', $companyArr, ($event!=null)?$event->company_id:0,array('class'=>'form-control m-bot15'))}}            
                                </div>
                            </div>
                              <div class="form-group">
                                   {{ Form::label('title', 'Гарчиг', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                   <div class="col-sm-10">
                                          {{ Form::text('title', ($event!=null)?$event->title:'', array('class' => 'form-control')) }}
                                   </div>
                              </div>

                              <div class="form-group">
                                  {{ Form::label('body', 'Контент', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                                  <div class="col-sm-10">
                                        {{ Form::textarea('body', ($event!=null)?$event->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '6')) }}
                                  </div>
                              </div>
                          </div>
                        </div>
                      

                  </div>
                </div>
            </section>

            {{ Form::close() }}

<section class="panel">
  <div class="panel-body" align="center">
      <a href="#"><button type="button" class="btn btn-info" onclick="document.getElementById('event_form').submit();">Хадгалах</button></a>
      <a href="{{ URL::route('admin.events.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
  </div>
</section>

            

        </div>
    </div>

@stop