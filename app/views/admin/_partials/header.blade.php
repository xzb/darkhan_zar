  <!--header start-->
      <div class="sidebar-toggle-box">
          <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
      </div>
      <!--logo start-->
      <a href="/admin/dashboard" class="logo" >
        <p>{{\DB::table('settings')->remember(600, 'backend_title')->first()->backend_title}}</p>
      </a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
          
      </div>
      <div class="top-nav ">
          <ul class="nav pull-right top-menu">
              <!-- <li>
                  <input type="text" class="form-control search" placeholder="Search">
              </li> -->
              <!-- user login dropdown start-->
              <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <span class="username">{{ $logged_user->first_name }}</span>
                      <b class="caret"></b>
                  </a>
                  <ul class="dropdown-menu extended logout">
                      <div class="log-arrow-up"></div>
                      <!-- <li><a href="#"><i class=" icon-suitcase"></i>Profile</a></li>
                      <li><a href="#"><i class="icon-cog"></i> Settings</a></li>
                      <li><a href="#"><i class="icon-bell-alt"></i> Notification</a></li>
                       --><li><a href="{{ action('\Admin\AuthController@getLogout') }}"><i class="icon-key"></i> Log Out</a></li>
                  </ul>
              </li>
              <!-- user login dropdown end -->
          </ul>
      </div>
  <!--header end-->