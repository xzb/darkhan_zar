@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($poll): ?>
              {{ Form::model($poll, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($poll, array('method' => 'put', 'files' => true, 'to' =>'admin/polls/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Асуултууд
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Асуулт</label>
                          <div class="col-lg-6">
                          {{ Form::text('body',($poll!=null)?$poll->body:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.polls.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop