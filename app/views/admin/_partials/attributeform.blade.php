
      <div class="row">
          <div class="col-lg-12">
            <?php if($attribute): ?>
              {{ Form::model($attribute, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($attribute, array('method' => 'put', 'files' => true, 'to' =>'admin/attribute/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Ерөнхий тохиргоо
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                            {{ Form::text('name', ($attribute!=null)?$attribute->name:'', array('class' => 'form-control')) }}
                          </div>
                      </div>

                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Type</label>
                          <div class="col-lg-6">
                          {{ Form::select('type', $combobox, ($attribute!=null)?$attribute->type:0,array('class'=>'form-control m-bot15'))}}            
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Is main</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('is_main', '1', ($attribute!=null)?$attribute->is_main:'', array('id' => 'is_main'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Is column</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('is_column', '1', ($attribute!=null)?$attribute->is_column:'', array('id' => 'is_column'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Is filterable</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('is_filterable', '1', ($attribute!=null)?$attribute->is_filterable:'', array('id' => 'is_filterable'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Sort order</label>
                          <div class="col-lg-6">
                            {{ Form::text('sort_order', ($attribute!=null)?$attribute->sort_order:'', array('class' => 'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Is required</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('is_required', '1', ($attribute!=null)?$attribute->is_required:'', array('id' => 'is_required'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Жагсаалтанд харуулах</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('is_stock', '1', ($attribute!=null)?$attribute->is_stock:'', array('id' => 'is_stock'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Гарчигт ашиглах</label>
                          <div class="col-lg-6">
                          {{ Form::checkbox('for_title', '1', ($attribute!=null)?$attribute->for_title:'', array('id' => 'for_title'))}}       
                          </div>
                      </div>
                      <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                                            <a href="{{ URL::route('admin.attribute.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                                        </div>
                                    </div>
                  </div>
              </section>
              
              
              {{ Form::close() }}

          </div>
      </div>
 