@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($bannerloc): ?>
              {{ Form::model($bannerloc, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($bannerloc, array('method' => 'put', 'files' => true, 'to' =>'admin/bannerloc/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Ангилал тохиргоо
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Байрлал</label>
                          <div class="col-lg-6">
                          {{ Form::text('location',($bannerloc!=null)?$bannerloc->location:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Хэмжээ</label>
                          <div class="col-lg-6">
                          {{ Form::text('size',($bannerloc!=null)?$bannerloc->size:'',array('class'=>'form-control'))}}
                          </div>
                      </div>                     
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Үнэ</label>
                          <div class="col-lg-6">
                          {{ Form::text('price',($bannerloc!=null)?$bannerloc->price:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Файлын төрөл</label>
                          <div class="col-lg-6">
                          {{ Form::text('file_type',($bannerloc!=null)?$bannerloc->file_type:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Баннерын тоо(тухайн байрлалд солигдох)</label>
                          <div class="col-lg-6">
                          {{ Form::text('changes',($bannerloc!=null)?$bannerloc->changes:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.bannerloc.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop