@section('main')


      <div class="row">
          <div class="col-lg-12">
            {{ Form::model($contact, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            {{ Notification::showAll() }}
              @if ($errors->any())
              <div class="alert alert-error">
                      {{ implode('<br>', $errors->all()) }}
              </div>
              @endif
                        
              <section class="panel">
                  <header class="panel-heading">
                       Санал хүсэлт
                  </header>
                   <div class="panel-body">
                          <div class="form-group">
                               {{ Form::label('name', 'Нэр', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                               <div class="col-sm-3">
                                      {{ Form::text('name', ($contact!=null)?$contact->name:'', array('class' => 'form-control','disabled')) }}
                               </div>
                          </div>
                          <div class="form-group">
                               {{ Form::label('email', 'Э-шуудан', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                               <div class="col-sm-3">
                                      {{ Form::text('email', ($contact!=null)?$contact->email:'', array('class' => 'form-control','disabled')) }}
                               </div>
                          </div>                
                          <div class="form-group">
                              {{ Form::label('question', 'Санал,хүсэлт', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                              <div class="col-sm-5">
                                    {{ Form::textarea('question', ($contact!=null)?$contact->question:'', array('class' => 'form-control','disabled'  , 'rows' => '6')) }}
                              </div>
                          </div>
                          <div class="form-group">
                              {{ Form::label('answer', 'Хариулт', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                              <div class="col-sm-5">
                                    {{ Form::textarea('answer', ($contact!=null)?$contact->answer:'', array('class' => 'form-control', 'rows' => '6')) }}
                              </div>
                          </div>
                    
                    <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.contact.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                    </div>
                  </div>
                                                      
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop