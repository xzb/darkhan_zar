@section('main')
      <div class="row">
          <div class="col-lg-12">
              {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
              
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Ангилал тохиргоо
                  </header>
                  <div class="panel-body">
                      {{ Form::hidden('category_id', $root_id) }}
                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Баннер1</label>
                          <div class="col-lg-6">
                          {{ Form::select('banner_id', $combobox, ($category!=null)?$category->banner_id:0,array('class'=>'form-control m-bot15'))}}           
                          </div>
                      </div>
                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Баннер2(Босоо хойно байрлах)</label>
                          <div class="col-lg-6">
                          {{ Form::select('banner2_id', $banner2, ($category!=null)?$category->banner2_id:0,array('class'=>'form-control m-bot15'))}}            
                          </div>
                      </div>
                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Баннер3(Босоо урд байрлах)</label>
                          <div class="col-lg-6">
                          {{ Form::select('banner3_id', $banner3, ($category!=null)?$category->banner3_id:0,array('class'=>'form-control m-bot15'))}}
                          </div>
                      </div>
                      
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop