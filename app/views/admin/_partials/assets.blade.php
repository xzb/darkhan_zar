<!-- Bootstrap core CSS -->
<link href="/admin/template/css/bootstrap.min.css" rel="stylesheet">
<link href="/admin/template/css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link href="/admin/template/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!-- Custom styles for this template -->
<link href="/admin/template/css/style.css" rel="stylesheet">
<link href="/admin/template/css/style-responsive.css" rel="stylesheet" />

<link href="/admin/template/assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
<link href="/dropzone/css/dropzone.css" rel="stylesheet" />
<script src="/admin/template/js/jquery.js"></script>
<script src="/admin/template/js/jquery.validate.min.js"></script>
<script src="/admin/template/js/bootstrap.min.js"></script>

 <!-- <link rel="stylesheet" type="text/css" href="/admin/template/assets/bootstrap-datepicker/css/datepicker.css" />
 <link rel="stylesheet" type="text/css" href="/admin/template/assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
 <link rel="stylesheet" type="text/css" href="/admin/template/assets/bootstrap-datetimepicker/css/datetimepicker.css" />
 -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<style>
.first_col {
	height: 735px;
}
.col_lg_2 {
	margin-right: 15px; margin-left: 15px;
}

@media only screen and (max-width: 1200px){
    .first_col { 
    	height: auto !important;
    }
    .news_form .col_lg_2 {
    	display: inline-block !important;
    }
}  
</style>