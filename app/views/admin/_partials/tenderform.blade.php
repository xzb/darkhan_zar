@section('main')


      <div class="row">
          <div class="col-lg-12">
             <?php if($page): ?>
              {{ Form::model($page, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'page_form')) }}
            <?php else:?>
              {{ Form::model($page, array('method' => 'put', 'files' => true, 'to' =>'admin/tender/store', 'class' => 'form-horizontal','id'=>'page_form')) }}
           <?php endif;?>

              @if ($errors->any())
              <div class="alert alert-error">
                      {{ implode('<br>', $errors->all()) }}
              </div>
              @endif

              <section class="panel">
                  <div class="panel-body">
                      <div class="form-group">
                        <div class="col-md-2">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                                    
                                    <?php if($page != null && $page->image): ?>
                                    <img src="/files/uploads/<?php echo $page->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                    <?php else: ?>
                                    <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг" />
                                    <?php endif; ?>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                 <span class="btn btn-white btn-file">
                                 <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                                 <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                 {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
                                 </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                </div>
                            </div>
                        </div>

                                    
                  </div>
              </section>

              <section class="panel">
                  <header class="panel-heading tab-bg-dark-navy-blue ">
                      <ul class="nav nav-tabs">
                          <li class="active">
                              <a href="#home" data-toggle="tab">Монгол</a>
                          </li>
                          <li class="">
                              <a href="#about" data-toggle="tab">English</a>
                          </li>
                      </ul>
                  </header>
                  <div class="panel-body">
                      <div class="tab-content">
                          <div class="tab-pane active" id="home">

                            <div class="form">
                                    <div class="form-group">
                                         {{ Form::label('title', 'Гарчиг', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                         <div class="col-sm-10">
                                                {{ Form::text('title', ($page!=null)?$page->title:'', array('class' => 'form-control')) }}
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('body', 'Контент', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                                        <div class="col-sm-10">
                                              {{ Form::textarea('body', ($page!=null)?$page->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '9')) }}
                                        </div>
                                    </div>

                            </div>

                          </div>
                          <div class="tab-pane" id="about">


                            <div class="form">
                                    <div class="form-group">
                                         {{ Form::label('title_en', 'Title', array('class' => 'col-sm-2 col-sm-2 control-label')) }}
                                         <div class="col-sm-10">
                                                {{ Form::text('title_en', ($page!=null)?$page->title_en:'', array('class' => 'form-control')) }}
                                         </div>
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('body_en', 'Content', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                                        <div class="col-sm-10">
                                              {{ Form::textarea('body_en', ($page!=null)?$page->body_en:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '9')) }}
                                        </div>
                                    </div>
                                   
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.tender.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                    </div>
                  </div>
              </section>

              {{ Form::close() }}
  
          </div>
      </div>
 
@stop