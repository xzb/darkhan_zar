@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($answer): ?>
              {{ Form::model($answer, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($answer, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
           <?php endif;?>
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Хариулт</label>
                          <div class="col-lg-6">
                          {{ Form::text('body',($answer!=null)?$answer->body:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::to('admin/polls/'.$poll_id) }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop