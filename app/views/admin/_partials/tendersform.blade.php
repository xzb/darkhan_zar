@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($tender): ?>
              {{ Form::model($tender, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($tender, array('method' => 'put', 'files' => true, 'to' =>'admin/tender/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Тендер бүртгэл
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                          {{ Form::text('title',($tender!=null)?$tender->title:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      <div class="form-group">
                          {{ Form::label('body', 'Контент', array('class' => 'col-sm-2 control-label col-sm-2')) }}
                          <div class="col-sm-10">
                                {{ Form::textarea('body', ($tender!=null)?$tender->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '6')) }}
                          </div>
                      </div>                   
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.tenders.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
 
@stop