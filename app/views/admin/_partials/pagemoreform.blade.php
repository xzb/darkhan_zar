@section('main')

  {{ Form::model($newsmore, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'pagemore_form')) }}


<div class="row">

@if ($errors->any())
<div class="alert alert-error">
        {{ implode('<br>', $errors->all()) }}
</div>
@endif
<div class="col-lg-9">
  <section class="panel">

      <div class="panel-body">

                <div class="form">
                        <div class="form-group">
                            <div class="col_lg_2">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                                        
                                        <?php if($newsmore != null && $newsmore->image): ?>
                                        <img src="/files/uploads/page/more/<?php echo $newsmore->image?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                        <?php else: ?>
                                        <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+Зураг"/>
                                        <?php endif; ?>
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                     <span class="btn btn-white btn-file">
                                     <span class="fileupload-new"><i class="icon-paper-clip"></i> Select image</span>
                                     <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                     {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
                                     </span>
                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Remove</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                             <div class="col-sm-12">
                                    {{ Form::text('title', ($newsmore!=null)?$newsmore->title:'', array('class' => 'form-control', 'placeholder' => 'Гарчиг')) }}
                             </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                  {{ Form::textarea('body', ($newsmore!=null)?$newsmore->body:'', array('class' => 'form-control ckeditor tinymce1', 'rows' => '15')) }}
                            </div>
                        </div>

                </div>


      </div>
      <div class="panel-body" align="center">
        {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
        <a href="{{ URL::route('admin.pages.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
    </div>
  </section>
</div>



{{ Form::close() }}

@stop