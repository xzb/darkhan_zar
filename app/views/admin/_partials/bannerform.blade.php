@section('main')


      <div class="row">
          <div class="col-lg-12">
            <?php if($banner): ?>
              {{ Form::model($banner, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal')) }}
            <?php else:?>
              {{ Form::model($banner, array('method' => 'put', 'files' => true, 'to' =>'admin/banners/store', 'class' => 'form-horizontal')) }}
           <?php endif;?>
              
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Ерөнхий тохиргоо
                  </header>
                  <div class="panel-body">
                      
                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Баннер байршил</label>
                          <div class="col-lg-6">
                          {{ Form::select('page_id', $combobox, ($banner!=null)?$banner->page_id:0,array('class'=>'form-control m-bot15'))}}            
                          </div>
                      </div>
                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Нэр</label>
                          <div class="col-lg-6">
                            {{ Form::text('name', ($banner!=null)?$banner->name:'', array('class' => 'form-control')) }}
                          </div>
                      </div>

                      <div class="form-group">
                      
                        <label class="col-sm-2 control-label col-lg-2">Тайлбар</label>
                          <div class="col-lg-6">
                            {{ Form::text('name_en', ($banner!=null)?$banner->name_en:'', array('class' => 'form-control')) }}
                          </div>
                      </div>

                      <div class="form-group">
                        <hr>
                        <label class="control-label col-md-1">Баннер</label>
                          <div class="col-md-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
                            
                                    <?php if($banner != null && $banner->path && in_array(substr($banner->path,-3),myHelpers::$imageExt)): ?>
                                    <img src="/files/uploads/banner/<?php echo $banner->path?>" style="max-width: 200px; max-height: 150px; line-height: 20px;"/>
                                    <?php else: ?>
                                    <img src="http://dummyimage.com/122x122/EFEFEF/14151c&text=+File" />
                                    <?php endif; ?>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                 <span class="btn btn-white btn-file">
                                 <span class="fileupload-new"><i class="icon-paper-clip"></i> Select file</span>
                                 <span class="fileupload-exists"><i class="icon-undo"></i> Change</span>
                                 {{ Form::file('file', array('class' => 'default', 'name' => 'path')) }}
                                 </span>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                       <hr>
                        <label class="col-sm-2 control-label col-lg-2">Линк</label>
                          <div class="col-lg-6">
                          {{ Form::text('link', ($banner!=null)?$banner->link:'',array('class'=>'form-control'))}}            
                          <label class="checkbox-inline">{{ Form::checkbox('blank_window', '1', ($banner!=null)?$banner->blank_window:'', array('id' => 'blank_window'))}}Линк дээр дарахад хуудас шинэ цонхон дээр нээгдэх</label>
                          </div>
                      </div>

                      <div class="form-group">
                        <hr>
                          <label class="col-sm-2 control-label col-lg-2">Эхлэх, дуусах огноо</label>
                          <div class="col-lg-6">
                              <div class="input-group input-large"  data-date="2014-07-14" data-date-format="yyyy-mm-dd">
                                  {{ Form::text('from', ($banner!=null)?$banner->start_date:'',array('class'=>'form-control dpd1'))}}   
                                  <span class="input-group-addon">-ээс</span>
                                  {{ Form::text('to', ($banner!=null)?$banner->end_date:'',array('class'=>'form-control dpd2'))}}   
                              </div>
                              <span class="help-block">баннерын хүчинтэй хугацаа  </span>
                          </div>
                      </div>

                      <div class="form-group">
                        <hr>
                          <label class="col-sm-2 control-label col-lg-2">Өргөн, Урт</label>
                          <div class="col-lg-6">
                              <div class="input-group" >
                                  {{ Form::text('width', ($banner!=null)?$banner->width:'',array('class'=>'form-control', 'placeholder' => 'Өргөн'))}}   
                                  <span class="input-group-addon">-</span>
                                  {{ Form::text('height', ($banner!=null)?$banner->height:'',array('class'=>'form-control', 'placeholder' => 'Урт'))}}   
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                       <hr>
                        <label class="col-sm-2 control-label col-lg-2">Хугацаа</label>
                          <div class="col-lg-6">
                          {{ Form::text('second', ($banner!=null)?$banner->second:'',array('class'=>'form-control'))}}            
                          <label class="checkbox-inline">Нэг байрлалд олон баннер орсон тохиолдолд оруулах видео хугацаа</label>
                          </div>
                      </div>
                      <div class="form-group">
                       <hr>
                        <label class="col-sm-2 control-label col-lg-2">Дэс дараалал</label>
                          <div class="col-lg-6">
                          {{ Form::text('sort', ($banner!=null)?$banner->sort:'',array('class'=>'form-control'))}}            
                          <label class="checkbox-inline">{{ Form::checkbox('is_active', '1', ($banner!=null)?$banner->is_active:'', array('id' => 'is_active'))}}Идэвхтэй эсэх </label>
                          </div>
                      </div>
                      <div class="form-group">
                       <hr>
                        <label class="col-sm-2 control-label col-lg-2">Category</label>
                          <div class="col-lg-6">
                          <select style="width: 300px;" id="category_id" name="category_id" class="form-control" size="20">
                              @foreach ($categorys as $category)
                                        <option value="{{$category->id}}" <?php if($banner && $banner->category_id == $category->id){echo 'selected="selected"';}?>>
                                        <?php for($i=0; $i<$category->level; $i++){
                                          echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                                        }
                                        ?>
                                        {{$category->name}}</option>
                                        @endforeach
                          </select>
                          </div>
                      </div>
                      <div class="form-group">
                       <hr>
                        <label class="col-sm-2 control-label col-lg-2">Жижиг зураг</label>
                          <div class="col-lg-6">
                          {{ Form::file('file', array('class' => 'default', 'name' => 'path_en')) }} 
                          <label class="checkbox-inline">Онцлох категориудад компани нэр лого байдлаар нэмэгдэнэ</label>
                          </div>
                      </div>
                      <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                                            <a href="{{ URL::route('admin.banner.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                                        </div>
                                    </div>
                  </div>
              </section>
              
              
              {{ Form::close() }}

          </div>
      </div>
 
@stop