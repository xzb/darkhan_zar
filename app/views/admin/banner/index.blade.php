@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Реклам, Баннерууд

              <div style="float:right;">
                <a href="{{ URL::route('admin.banner.create') }}">
                  <button type="button" class="btn btn-success">Баннер нэмэх</button>
                </a>
                
                <a href="{{ URL::route('admin.bannerloc.index') }}">
                  <button type="button" class="btn btn-success">Баннер төрөл</button>
                </a>
              </div>

          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i> Нэр</th>
                  <th class="hidden-phone"><i class="icon-question-sign"></i> Байрлал</th>
                  <th>Дуусах огноо</th>
                  <th>Статус</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($banners as $banner)  
              <tr>
                  <td><a href="{{ URL::route('admin.banner.edit', $banner->id) }}">{{ $banner->name }}</a></td>
                  <td class="hidden-phone">{{ ($banner->page_id != 0)?$page[$banner->page_id]:'-' }}</td>
                 
                  <td>{{$banner->end_date}}</td>
                  <td>
                    <a href="{{ URL::route('admin.banner.edit', $banner->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.banner.destroy', $banner->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.banner.destroy', $banner->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
            <div align="center">{{ $banners->links() }}</div>
      </section>
  </div>
</div>
 
@stop