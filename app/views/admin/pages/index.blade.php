@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Хуудаснууд
              <a style="float:right;" href="/admin/pages/create">
                <button type="button" class="btn btn-success">Үүсгэх</button>
              </a>
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i> Гарчиг</th>
                  <th>Огноо</th>
                  <th><i class=" icon-edit"></i> Статус</th>
                  <th></th>
              </tr>
              </thead>
              <tbody>
              @foreach ($pages as $page)  
              <tr>
                  <td><a href="{{ URL::route('admin.pages.edit', $page->id) }}">{{ $page->title }}</a></td>
                  <td>{{ $page->created_at }}</td>
                  <td>@if ($page->is_active)
                        <button class="btn btn-success btn-xs"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs"><i class="icon-ok"></i></button>
                    @endif</td>
                  <td>
                    <a href="{{ URL::route('admin.pages.edit', $page->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.pages.destroy', $page->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.pages.destroy', $page->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $pages->links() }}</div>
      </section>
  </div>
</div>
 
@stop