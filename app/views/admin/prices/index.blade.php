@extends('admin._layouts.default')
 
@section('main')
<?php use App\Models\Category; ?>
<div class="row">
  <div class="col-lg-3">
      <section class="panel">
          <header class="panel-heading">
             Онцлох зарын үнэ
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>Үндсэн ангилал</th>
                  <th>Үнэ</th>
                  <th>Засах</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($pricess as $prices)  
              <?php $cate = Category::find($prices->category_id);?>
              <tr>
                  <td>{{ $prices->category_id }} - {{$cate->name;}}</td>
                  <td>{{ $prices->price }}</td>
                  <td>
                    <a href="{{ URL::route('admin.prices.edit', $prices->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
      </section>
  </div>
</div>
 
@stop