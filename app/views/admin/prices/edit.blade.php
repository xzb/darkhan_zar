@extends('admin._layouts.default')
 
@section('main')
 <div class="row">
          <div class="col-lg-12">
            
              {{ Form::model($prices, array('method' => 'put', 'files' => true, 'route' => array('admin.prices.update', $prices->id), 'class' => 'form-horizontal')) }}
           
              {{ Notification::showAll() }}
                        @if ($errors->any())
                        <div class="alert alert-error">
                                {{ implode('<br>', $errors->all()) }}
                        </div>
                        @endif
              <section class="panel">
                  <header class="panel-heading">
                      Онцлох зарыг үнэ засах
                  </header>
                  <div class="panel-body">
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Ангилал</label>
                          <div class="col-lg-6">
                          	<span>{{$category->name;}}</span>
                          {{ Form::hidden('category_id',($prices!=null)?$prices->category_id:'',array('class'=>'form-control'))}}
                          </div>
                      </div>                     
                      <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2">Үнэ</label>
                          <div class="col-lg-6">
                          {{ Form::text('price',($prices!=null)?$prices->price:'',array('class'=>'form-control'))}}
                          </div>
                      </div>
                      
                      <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                              {{ Form::submit('Хадгалах', array('class' => 'btn btn-info')) }}
                              <a href="{{ URL::route('admin.prices.index') }}"><button type="button" class="btn btn-default">Буцах</button></a>
                          </div>
                      </div>
                      </div>
                  </div>
              </section>

              {{ Form::close() }}

          </div>
      </div>
@stop