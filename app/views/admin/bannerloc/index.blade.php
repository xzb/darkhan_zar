@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
             Баннер төрөл<div class="panel-body" style="float:right; padding-top:0px"><a href="{{ URL::route('admin.bannerloc.create') }}" class="btn btn-success btn-sm">Нэмэх</a></div>
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i> Байрлал</th>
                  <th>Хэмжээ</th>
                  <th>Үнэ</th>
                  <th>Засах/Устгах</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($bannerlocs as $bannerloc)  
              <tr>
                  <td><a href="{{ URL::route('admin.bannerloc.edit', $bannerloc->id) }}">{{ $bannerloc->location }}</a></td>
                  <td>{{ $bannerloc->size }}</td>
                  <td>{{ $bannerloc->price }}</td>
                  <td>
                    <a href="{{ URL::route('admin.bannerloc.edit', $bannerloc->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.bannerloc.destroy', $bannerloc->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.bannerloc.destroy', $bannerloc->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
      </section>
  </div>
</div>
 
@stop