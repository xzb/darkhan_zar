@extends('admin._layouts.default')
 
@section('main')
  @include('admin._partials.newsform', array('page' => $page,'page_tag' => $page_tag,'route' => array('admin.news.update',$page->id))) 
@stop