@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Үндсэн мэдээ: {{$news->title}}
              <div align="right" style="padding-right: 20px; position: inherit">
                <a href="{{ URL::to('admin/pagemore/create/'.$news->id) }}">
                  <img src="/files/details_open.png"> Дэлгэрэнгүй нэмэх
                </a>
              </div>
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>Гарчиг</th>
                  <th>Огноо</th>
                  <th>Засах/Устгах</th>
              </tr>
              </thead>
              <tbody>
              @if(count($newsmore)>0)  
              @foreach ($newsmore as $a)  
              <tr>
                  <td>{{ $a->title }}</a></td>
                  <td>{{ $a->created_at }}</a></td>
                  <td>
                    <a href="{{ URL::route('admin.pagemore.edit', $a->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.pagemore.destroy', $a->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.pagemore.destroy', $a->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              @else
              <tr>
                <td colspan ="3" align="center">Одоогоор дэлгэрэнгүй мэдээлэл оруулаагүй байна</td>
              </tr>
              @endif
              </tbody>
          </table>
      </section>
  </div>
</div>

<center><a href="javascript:history.back()"><h5>< Буцах</h5></a></center>
 
@stop