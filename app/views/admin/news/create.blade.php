@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.newsform', array('page'=>$page,'route'=>array('admin.news.store')))

@stop