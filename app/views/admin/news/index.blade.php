@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Хайлт
          </header>
          {{ Form::open(array('class'=>'form-inline', 'method'=>'PUT', 'route' => 'admin.news.index')) }}
          <div class="panel-body">
                
                  <div class="form-group">
                      <label for="title" class="sr-only">Гарчиг</label>
                      <input type="text" placeholder="Гарчиг" id="title" name ="title" class="form-control">
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="page_id" id="page_id" style="width:110px">
                        @foreach($combobox as $key => $comb)
                        <option value="{{$key}}">{{$comb}}</option>
                        @endforeach;
                      </select>
                    </div>    
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="is_featured" id="is_featured" style="width:110px">
                        <option value="3">--Онцлох--</option>
                        <option value="1">Тийм</option>
                        <option value="0">Үгүй</option>
                      </select>
                    </div>    
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="is_emergency" id="is_emergency" style="width:110px">
                        <option value="3">--Шуурхай--</option>
                        <option value="1">Тийм</option>
                        <option value="0">Үгүй</option>
                      </select>
                    </div>    
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="news_type" id="news_type" style="width:110px">
                        <option value="0">--Төрөл--</option>
                        <option value="1">Мэдээ</option>
                        <option value="2">Ярилцлага</option>
                        <option value="3">Бүтээлч хүн</option>
                      </select>
                    </div>    
                  </div>
                  <div class="form-group" style="width: 275px;">
                    <div class="col-lg-3">
                        <div class="input-group input-large"  data-date="2014-07-14" data-date-format="yyyy-mm-dd">
                            <input type="text" placeholder="эхлэх" id="from" name ="from" class="form-control dpd1" style="width: 100px;">
                            <span class="input-group-addon">-ээс</span>
                            <input type="text" placeholder="дуусах" id="to" name ="to" class="form-control dpd2" style="width: 100px;">
                        </div>
                    </div>
                  </div>
                  <button class="btn btn-success" type="submit">Хайх</button>
                  <a href="/admin/news">reset</a>
          </div>
          {{ Form::close()}}
      </section>

                  </div>
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Мэдээ, мэдээлэл
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th width="50%"><i class="icon-bullhorn"></i> Гарчиг</th>
                  <th>Категори</th>
                  <th>Онцлох</th>
                  <th>Шуурхай</th>
                  <th>Огноо</th>
                  <th><i class=" icon-edit"></i> Статус</th>
                  <th>Засах</th>
              </tr>
              </thead>
              <tbody>

              @foreach ($pages as $page)  
              <tr>
                  <td>
                    <a href="{{ URL::route('admin.news.edit', $page->id) }}" {{ $page->is_active ? '' : 'style="color: gray;"' }}>
                      {{ $page->title }}
                    </a>
                  </td>
                  <td>{{ $page->getCategory() }}</td>
                  <td>
                    @if ($page->is_featured)
                        <button class="btn btn-success btn-xs"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs"><i class="icon-ok"></i></button>
                    @endif
                  </td>
                  <td>
                    @if ($page->is_emergency)
                        <button class="btn btn-success btn-xs"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs"><i class="icon-ok"></i></button>
                    @endif
                  </td>                  
                  <td>{{ $page->created_at }}</td>
                  <td>@if ($page->is_active)
                        <button class="btn btn-success btn-xs"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs"><i class="icon-ok"></i></button>
                    @endif
                  </td>
                  <td>
                    <a href="{{ URL::route('admin.news.show', $page->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-plus-sign"></i></button>
                    </a>
                    <a href="{{ URL::route('admin.news.edit', $page->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.news.destroy', $page->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.news.destroy', $page->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $pages->links() }}</div>
      </section>
  </div>
</div>
 
@stop