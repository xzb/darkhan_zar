@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.eventsform', array('event'=>$event,'route'=>array('admin.events.store')))

@stop