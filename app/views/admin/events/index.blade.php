@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Үйл явдлууд
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i> Гарчиг</th>
                  <th>Төрөл</th>
                  <th>Огноо</th>
                  <th></th>
              </tr>
              </thead>
              <tbody>
              @foreach ($events as $event)  
              <tr>
                  <td><a href="{{ URL::route('admin.events.edit', $event->id) }}">{{ $event->title }}</a></td>
                  <td>{{ ($event->type == 1)?'Үйл явдал':'Шинэ байгууллага' }}</td>
                  <td>{{ $event->created_at }}</td>
                  <td><a href="{{ URL::route('admin.events.edit', $event->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.events.destroy', $event->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.events.destroy', $event->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $events->links() }}</div>
      </section>
  </div>
</div>
 
@stop