@extends('admin._layouts.default')

@section('main')
@include('admin._partials.attributeform', array('attribute' => $attribute,'route' => array('admin.attribute.update',$attribute->id)))

<?php if ($attribute->type != 'textarea' && $attribute->type != 'textbox'):?>
  <div class="row">
          <div class="col-lg-12">
            <section class="panel">
                  <header class="panel-heading">
                      Attribute values
                  </header>
                  <div class="panel-body">
                      <input type="submit" value="sort by alpha" onclick="alphaSort(); return false;" />
                      <div id="attribute_value_list">
                        <ul id="sortable" style="width: 300px;">
                          <?php foreach ($attribute_list as $attribute_value): ?>
                          <li class="">
                            {{$attribute_value->value}}
                            <input type="hidden" value="{{$attribute_value->id}}" name="attribute_value_ids[]" />
                            <a href="#" onclick="deleteValue(this, {{$attribute_value->id}})" style="float:right;">delete</a>
                          </li>
                          <?php endforeach; ?>
                        </ul>
                        <form class="jNice" onsubmit="return addValue();" id="att_val_form" method="post" action="#">
                        <table>
                          <tfoot>
                            <tr>
                              <td colspan="2">
                                <input type="hidden" id="attribute_values_attribute_id" value="{{$attribute->id}}" name="attribute_id">          
                                <button class="btn btn-round btn-primary" type="submit" name="" id=""><span><span>Нэмэx</span></span></button>
                              </td>
                            </tr>
                          </tfoot>
                          <tbody>
                                  <tr>
                              <td><label for="attribute_values_value">Value</label></td>
                              <td>
                                          <input type="text" id="attribute_values_value" name="attribute_value">        </td>
                            </tr>
                          </tbody>
                        </table>
                      </form>
                      </div>
                     </div>
                  </section>
          </div>
      </div>
<?php endif;?>
  @include('admin._partials.attributecategory', array('attribute' => $attribute, 'categorys' => $categorys, 'category_attributes'=>$category_attributes))  
  <script type="text/javascript">
  function alphaSort()
  {
    var mylist = $('#sortable');
    var listitems = mylist.children('li').get();
    listitems.sort(function(a, b) {
      var compA = $(a).text().toUpperCase();
      var compB = $(b).text().toUpperCase();
      return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
    })
    $.each(listitems, function(idx, itm) { 
      mylist.append(itm);
    });
    sortValues();
  }
  function sortValues()
  {
    var ids = $("#sortable li :input").map(function(){
      return $(this).val();
    }).get().join(",");
    $.ajax({
      url: "/admin/attribute_values/sort",
      type : "post",
      data: {ids : ids}
    });
  }
  $(function() {
    $("#sortable").sortable({ stop: function() { sortValues(); } });
    $("#sortable").disableSelection();    
  });
  function addValue()
  {
    $.ajax({
      url: "/admin/attribute_values/create",
      type : "post",
      data: $("#att_val_form").serialize(),
      success: function(data){
        $("#sortable").append(data);
        // $("#" + data.update).html(data.content);
        // $("#" + data.update + ' > form').jNice();
        $("#sortable").sortable({ stop: function() { sortValues(); } });
        $("#sortable").disableSelection();
      }
    });
    return false;
  }
  function deleteValue(element, id)
  {
    if(window.confirm('Та итгэлтэй байна уу?'))
    {
      $.ajax({
        url: "/admin/attribute_values/delete",
        type: "post",
        data: {id : id},
        success: function(){
          $(element).parents('li:first').remove();
        }
      });
    }
    return false;
  }

  function addCategory()
  {
    $.ajax({
      url: "/admin/category_attribute/create",
      type : "post",
      data: $("#category_attribute_form").serialize(),
      success: function(data){
        $("#catlist").append(data);
      }
    });
    return false;
  }
  function deleteCategory(element, category_id, attribute_id)
  {
    if(window.confirm('Та итгэлтэй байна уу?'))
    {
      $.ajax({
        url: "/admin/category_attribute/delete",
        type: "post",
        data: {category_id : category_id, attribute_id : attribute_id},
        success: function(){
          $(element).parents('tr:first').remove();
          //window.location.reload();
        }
      });
    }
    return false;
  }

  /*
  $(document).ready(function(){
    $('#att_val_form').bind('submit', function(){
      addValue();
      return false;
    });
    $('#category_attribute_form').bind('submit', function(){
      addCategory();
      return false;
    })
  });
   */
</script>
@stop