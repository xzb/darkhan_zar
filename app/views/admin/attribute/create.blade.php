@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.attributeform', array('attribute'=>$attribute,'route'=>array('admin.attribute.store')))

@stop