<tr>
  <td><?php echo $category->name?></td>
  <td class="action">
    <a href="#" onclick="deleteCategory(this, {{$category_attribute->category_id}}, {{$category_attribute->attribute_id}}); return false;">delete</a>
  </td>
</tr>
<?php foreach ($categories as $key => $child):?>
	<tr>
	  <td><?php echo $child->name?></td>
	  <td class="action">
	    <a href="#" onclick="deleteCategory(this, {{$child->id}}, {{$category_attribute->attribute_id}}); return false;">delete</a>
	  </td>
	</tr>
<?php endforeach;?>