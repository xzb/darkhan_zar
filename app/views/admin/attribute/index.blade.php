@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
        <label for="category_id">Category:</label>
          <select style="width: 300px;" id="category_id" name="category_id" class="form-control" onchange="catChange()">
              @foreach ($categorys as $category)
                        <option value="{{$category->id}}" <?php if($category->id == $category_id){echo 'selected';} ?> style="padding-left:{{ $category->level * 20}}px">{{ $category->name}}</option>
                        @endforeach
          </select>
        
      </section>
      <section class="panel">
          <header class="panel-heading">
              Attribute
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>Нэр</th>
                  <th>Type</th>
                  <th>Is main</th>
                  <th>Is column</th>
                  <th>Is filterable</th>
                  <th>Required</th>
                  <th>Sort order</th>
                  <th>#</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($attributes as $attribute)  


              <tr>
              <td>{{$attribute->name}}</td>
              <td>{{$attribute->type}}</td>
              <td>
                    @if ($attribute->is_main)
                        <button class="btn btn-success btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @endif
              </td>
              <td>
                @if ($attribute->is_column)
                        <button class="btn btn-success btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @endif
              </td>
              <td>
                @if ($attribute->is_filterable)
                        <button class="btn btn-success btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @endif
              </td>
              <td>
                @if ($attribute->is_required)
                        <button class="btn btn-success btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @else    
                        <button class="btn btn-error btn-xs" onclick="return false;"><i class="icon-ok"></i></button>
                    @endif
              </td>

              <td>{{$attribute->sort_order}}</td>
              <td class="action" nowrap>
                  <a href="{{ URL::route('admin.attribute.edit', $attribute->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.attribute.destroy', $attribute->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}

                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.attribute.destroy', $attribute->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>

                    {{ Form::close() }}
              </td>
            </tr>
              @endforeach
              </tbody>

          </table>
      </section>
  </div>
</div>
<script type="text/javascript">
  $('#category_id').bind('change', function(){
    window.location = '/admin/attribute'+'?category_id='+$(this).val();
  });
</script>
@stop
