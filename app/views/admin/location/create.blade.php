@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.locationform', array('location'=>$location,'selectbox'=>$selectbox,'route'=>array('admin.location.store')))

@stop