<?php use App\Models\Location; ?>
@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Байршил
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>Нэр</th>
                  <th>#</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($locations as $parent)
              <tr>
                <td>{{ str_repeat('&nbsp;',$parent->level*8) }}{{$parent->name}}</td>
                <td class="action" nowrap>
                    <a href="{{ URL::route('admin.location.edit', $parent->id) }}">
                        <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                      </a>
                      {{ Form::open(array('route' => array('admin.location.destroy', $parent->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}

                            <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.location.destroy', $parent->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>

                      {{ Form::close() }}
                </td>
              </tr>
              <?php $childs = Location::where('parent_id','=',$parent->id)->orderBy('sort','asc')->get(); ?>
              @if(count($childs)>0)
                @foreach ($childs as $location)
                  <tr>
                    <td>{{ str_repeat('&nbsp;',$location->level*8) }}{{$location->name}}</td>
                    <td class="action" nowrap>
                        <a href="{{ URL::route('admin.location.edit', $location->id) }}">
                            <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                          </a>
                          {{ Form::open(array('route' => array('admin.location.destroy', $location->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}

                                <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.location.destroy', $location->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>

                          {{ Form::close() }}
                    </td>
                  </tr>
                @endforeach
              @endif
              
              @endforeach
              </tbody>

          </table>
      </section>
  </div>
</div>
@stop
