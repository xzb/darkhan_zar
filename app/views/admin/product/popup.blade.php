<script type="text/javascript" src="/admin/template/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/admin/template/js/advanced-form-components.js"></script>
<form class="form-horizontal" accept-charset="UTF-8" action="/admin/product/setfeatured/{{$product->id}}" method="POST">
	<section class="panel"> 
		<header class="panel-heading"> Онцлох болгох </header> 
		<div class="panel-body"> 
			<div class="form-group"> 
				<label class="col-sm-2 control-label col-lg-2">Эхлэх</label> 
				<div class="col-lg-6"> <input type="text" value="" id="from" name="featured_start_date" class="form-control default-date-picker"> </div> 
			</div>
			<div class="form-group"> 
				<label class="col-sm-2 control-label col-lg-2">Дуусах</label> 
				<div class="col-lg-6"> <input type="text" value="" id="to" name="featured_due_date" class="form-control default-date-picker"> </div> 
			</div>
			<!-- <div class="form-group"> 
				<label class="col-sm-2 control-label col-lg-2">Видео</label> 
				<div class="col-lg-6"> <input type="url" value="" name="video" class="form-control"> </div> 
			</div> -->
			<div class="form-group"> 
				<div class="col-lg-offset-2 col-lg-10"> 
					<input type="submit" value="Хадгалах" class="btn btn-info"> 
				</div> 
			</div> 
		</div> 
	</section> 
</form>
