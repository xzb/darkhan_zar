<script src="/admin/template/js/advanced-form-components.js"></script>
<form class="form-horizontal" accept-charset="UTF-8" action="/admin/product/setAutoRenew/{{$product->id}}" method="POST">
	<section class="panel"> 
		<header class="panel-heading"> Автоматаар шинэчлэх </header> 
		<div class="panel-body"> 
			<div class="form-group"> 
				<label class="col-sm-2 control-label col-lg-2">Шинэчлэх хугацаа</label> 
				<div class="col-lg-6"> <input type="number" value="" name="renew_time" class="form-control"> </div> 
			</div>
			<div class="form-group"> 
				<div class="col-lg-offset-2 col-lg-10"> 
					<input type="submit" value="Хадгалах" class="btn btn-info"> 
				</div> 
			</div> 
		</div> 
	</section> 
</form>
