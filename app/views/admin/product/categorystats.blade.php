@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
    <section class="panel">
          <header class="panel-heading">
              Хайлт
          </header>
          {{ Form::open(array('class'=>'form-inline', 'method'=>'GET', 'route' => 'admin.product.categorystats')) }}
          <div class="panel-body">
                  <div class="form-group" style="width: 275px;">
                    <div class="col-lg-3">
                        <div class="input-group input-large"  data-date="2014-07-14" data-date-format="yyyy-mm-dd">
                            <input type="text" placeholder="эхлэх" id="from" name ="from" class="form-control dpd1" style="width: 100px;" value="{{$from}}">
                            <span class="input-group-addon">-ээс</span>
                            <input type="text" placeholder="дуусах" id="to" name ="to" class="form-control dpd2" style="width: 100px;" value="{{$to}}">
                        </div>
                    </div>
                  </div>
                  <button class="btn btn-success" type="submit">Хайх</button>
                  <a href="/admin/product/categorystats?excel=1">Export to Excel</a>
          </div>
          {{ Form::close()}}
      </section>
      <section class="panel">
          <header class="panel-heading">
              Категори хандалт
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th width="50%"><i class="icon-bullhorn"></i> Категори нэр</th>
                  <th>хандсан тоо</th>
              </tr>
              </thead>
              <tbody>

              @foreach ($category_stats as $c)
              <tr>
                  <td>
                    {{ $c->category_name }}
                  </td>
                  <td>{{ $c->count }}</td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $category_stats->links() }}</div>
      </section>
  </div>
</div>
 
@stop