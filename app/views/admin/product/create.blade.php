@extends('admin._layouts.default')
 
@section('main')

@include('admin._partials.productform', array('product'=>$product,'route'=>array('admin.product.store')))

@stop