@extends('admin._layouts.default')
 
@section('main')
{{ HTML::script('js/category_nested.js') }}
{{ HTML::style('css/fancybox/jquery.fancybox.css') }}
{{ HTML::script('js/fancybox/jquery.fancybox.js') }}

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Хайлт
          </header>
          {{ Form::open(array('class'=>'form-inline', 'method'=>'GET', 'route' => 'admin.product.index')) }}
          <div class="panel-body">
                  <div class="form-group">
                      <label for="title" class="sr-only">Дугаар</label>
                      <input type="text" placeholder="Дугаар" id="product_id" name ="product_id" class="form-control" value="{{$product_id}}">
                  </div>
                  <div class="form-group">
                      <label for="title" class="sr-only">Гарчиг</label>
                      <input type="text" placeholder="Гарчиг" id="title" name ="title" class="form-control" value="{{$title}}">
                  </div>
                  <div class="form-group" id="categoryCombos">
                    <input type="hidden" name="category_id" id="category_id"/>
                    <input type="hidden" name="categories" id="categories"/>
                    <div id="categories0">
                    <select class="form-control" name="category0" id="category0" style="width:150px" onchange="drawCategoryCombo($(this).val(),0,1)">
                        <?php
                           for($i= 0; $i<count($cArray); $i++)
                           {
                              echo "<option value=".$cArray[$i]['id'].">".$cArray[$i]['name']."</option>";   
                           }
                        ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="is_featured" id="is_featured" style="width:110px">
                        <option value="3" @if($is_featured == 3){{'selected="selected"'}} @endif>--Онцлох--</option>
                        <option value="1" @if($is_featured == 1){{'selected="selected"'}} @endif>Тийм</option>
                        <option value="0" @if($is_featured == 0){{'selected="selected"'}} @endif>Үгүй</option>
                      </select>
                    </div>    
                  </div>
                  <div class="form-group" style="width: 275px;">
                    <div class="col-lg-3">
                        <div class="input-group input-large"  data-date="2014-07-14" data-date-format="yyyy-mm-dd">
                            <input type="text" placeholder="эхлэх" id="from" name ="from" class="form-control dpd1" style="width: 100px;" value="{{$from}}">
                            <span class="input-group-addon">-ээс</span>
                            <input type="text" placeholder="дуусах" id="to" name ="to" class="form-control dpd2" style="width: 100px;" value="{{$to}}">
                        </div>
                    </div>
                  </div>
                  <button class="btn btn-success" type="submit">Хайх</button>
                  <a href="/admin/productPager">reset</a>
          </div>
          {{ Form::close()}}
      </section>

                  </div>
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Зарууд
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>Дугаар</th>
                  <th width="40%"><i class="icon-bullhorn"></i> Гарчиг</th>
                  <th>Онцлох</th>
                  <th>Онцлох дуусах</th>
                  <th>Авто шинэчлэл</th>
                  <th>IP хаяг</th>
                  <th>Огноо</th>
                  <th>Засах</th>
              </tr>
              </thead>
              <tbody>

              @foreach ($pages as $page)
              <tr>
                  <td>
                    {{ $page->id }}
                  </td>
                  <td>
                    {{ $page->name }}
                  </td>
                  <td>
                    @if ($page->is_featured)
                        <!-- <button id="featured{{$page->id}}" class="btn btn-success btn-xs" onclick="featured({{$page->id}})"><i class="icon-ok"></i></button> -->
                        <a onclick="featured({{$page->id}})" id="featured{{$page->id}}" class="btn btn-success btn-xs"><i class="icon-ok"></i></a>
                    @else
                        <!-- <button id="featured{{$page->id}}" class="btn btn-error btn-xs" onclick="featured({{$page->id}})"><i class="icon-ok"></i></button> -->
                        <a id="featured{{$page->id}}" href="/admin/product/setfeatured/{{$page->id}}" class="btn btn-error btn-xs fancy fancybox.ajax"><i class="icon-ok"></i></a>
                    @endif
                  </td>
                  <td>
                    {{ $page->featured_due_date}}
                  </td>
                   <td>
                    @if ($page->auto_renew_time != 0)
                        <a onclick="autorenew({{$page->id}})" id="renew{{$page->id}}" class="btn btn-success btn-xs"><i class="icon-ok"></i></a>
                    @else
                        <a id="renew{{$page->id}}" href="/admin/product/setAutoRenew/{{$page->id}}" class="btn btn-error btn-xs fancy fancybox.ajax"><i class="icon-ok"></i></a>
                    @endif
                  </td>
                  <td>
                    {{ $page->ip_address }}
                  </td>
                  <td>{{ $page->created_at }}</td>
                  <td>
                    <a href="{{ URL::route('admin.product.edit', $page->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.product.destroy', $page->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.product.destroy', $page->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $pages->appends($url)->links() }}</div>
      </section>
  </div>
</div>
 <script>
   function featured(id){
    $.ajax({
      url: window.location.protocol +'//'+window.location.host+"/admin/product/setfeatured/"+id,
      cache: false,
      dataType: "html",
      success: function(html){
        $("#featured"+id).removeClass('btn-success').addClass('btn-error');
        $("#featured"+id).attr('class','btn btn-error btn-xs fancy fancybox.ajax');
        $("#featured"+id).attr('href','/admin/product/setfeatured/'+id);
        $("#featured"+id).removeAttr('onclick');

      }
    });
   }

   function autorenew(id){
    $.ajax({
      url: window.location.protocol +'//'+window.location.host+"/admin/product/setAutoRenew/"+id,
      cache: false,
      dataType: "html",
      success: function(html){
        $("#renew"+id).removeClass('btn-success').addClass('btn-error');
        $("#renew"+id).attr('class','btn btn-error btn-xs fancy fancybox.ajax');
        $("#renew"+id).attr('href','/admin/product/setAutoRenew/'+id);
        $("#renew"+id).removeAttr('onclick');

      }
    });
   }

   function setCoupon(id){
    $.ajax({
      url: window.location.protocol +'//'+window.location.host+"/admin/product/setCoupon/"+id,
      cache: false,
      dataType: "html",
      success: function(html){
        if(html == 'success')
        {
          $("#coupon"+id).removeClass('btn-error').addClass('btn-success');
          window.location = bm.prefix + '/admin/productPager';
        }
        else
        {
          // $("#coupon"+id).removeClass('btn-success').addClass('btn-error');  
          $("#coupon"+id).removeClass('btn-success').addClass('btn-error');
        $("#coupon"+id).attr('class','btn btn-error btn-xs fancy fancybox.ajax');
        $("#coupon"+id).attr('href','/admin/product/setCoupon/'+id);
        $("#coupon"+id).removeAttr('onclick');
        }

        

      }
    });
   }

  $(document).ready(function() {
    $(".fancy").fancybox({
      'hideOnContentClick': true
    });
  });
 </script>
@stop