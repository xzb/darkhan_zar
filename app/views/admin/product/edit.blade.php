@extends('admin._layouts.default')
 
@section('main')
  @include('admin._partials.productform', array('product' => $product,'cArray'=>$cArray,'parents'=>$parents,'parentcats'=>$parentcats, 'route' => array('admin.product.update',$product->id))) 
@stop