<?php use App\Models\Product; ?>
<?php use App\Models\OrderItems; ?>

@extends('admin._layouts.default')
 
@section('main')
{{ HTML::script('js/category_nested.js') }}
<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Хайлт
          </header>
          {{ Form::open(array('class'=>'form-inline', 'method'=>'GET', 'route' => 'admin.payment.index')) }}
          <div class="panel-body">
                  <div class="form-group">
                      <label for="title" class="sr-only">Дугаар</label>
                      <input type="text" placeholder="Дугаар" id="id" name ="id" class="form-control" value="{{$id}}">
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="status" id="status" style="width:110px">
                        <option value="0" @if($status == 0){{'selected="selected"'}} @endif>--Бүгд--</option>
                        <option value="1" @if($status == 1){{'selected="selected"'}} @endif>New</option>
                        <option value="2" @if($status == 2){{'selected="selected"'}} @endif>Completed</option>
                        <option value="CANCELED" @if($status == 'CANCELED'){{'selected="selected"'}} @endif>CANCELED</option>
                        <option value="Failed" @if($status == 'Failed'){{'selected="selected"'}} @endif>FAILED</option>
                        <option value="DECLINED" @if($status == 'DECLINED'){{'selected="selected"'}} @endif>DECLINED</option>
                      </select>
                    </div>    
                  </div>
                  <div class="form-group">
                    <div class="col-lg-8">
                      <select class="form-control" name="payment_type" id="payment_type" style="width:110px">
                        <?php //echo $payment_type; die;?>
                        <option value="0" @if($payment_type === 0){{'selected="selected"'}} @endif>--Бүгд--</option>
                        <option value="golomt" @if($payment_type === 'golomt'){{'selected="selected"'}} @endif>Голомт банкаар</option>
                        <option value="transfer" @if($payment_type === 'transfer'){{'selected="selected"'}} @endif>Дансаар</option>
                      </select>
                    </div>    
                  </div>
                  <button class="btn btn-success" type="submit">Хайх</button>
                  <a href="/admin/payment/index">reset</a>
          </div>
          {{ Form::close()}}
      </section>

                  </div>
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
              Зарууд
          </header>

          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th>ID</th>
                  <th width="40%"><i class="icon-bullhorn"></i> Гарчиг</th>
                  <th>Төрөл</th>
                  <th>Захиалгын код</th>
                  <th>Төлбөр төлөлт</th>
                  <th>Огноо</th>
                  <th>Төлбөр</th>
                  <th>#</th>
                  <th>#</th>
              </tr>
              </thead>
              <tbody>

              @foreach ($pages as $page)
              <tr>
                <td>
                    {{ $page->id }}
                  </td>
                  <td>
                    <?php $orderItems = OrderItems::getListOfPaymentId($page->id);?>
                    <ul>
                    <?php foreach($orderItems as $item):?>
                      <?php $oProduct = Product::find($item->product_id);?>
                      <?php if($oProduct):?>
                      <li><a href="http://www.dazar.mn/p/{{$oProduct->id}}/{{ str_replace('/', '', $oProduct->name)}}" target="_blank">{{$oProduct->name}} </a>@if($item->is_coupon ==1) Хямдралын купон @endif - {{$item->price}} төг</li>
                      <?php else:?>
                      <p>Зар устсан байна</p>
                    <?php endif;?>
                    <?php endforeach;?>
                    </ul>
                  </td>
                  <td>
                    {{ ($page->type_order == 1)?'Профайл':'Онцлох зар' }}
                  </td>
                   <td>
                    {{ $page->order_code }}
                  </td>
                  <td>
                    {{ $page->payment_type }}
                  </td>
                  <td>{{ $page->created_at }}</td>
                  <td>{{ $page->total }}</td>
                  <td>
                    <?php if($page->payment_type == 'transfer' && $page->status == 1):?>
                    @if($page->type_order == 0)
                    <a href="{{ URL::route('admin.payment.active', $page->id) }}">Онцлох болгох</a>
                    @elseif($page->type_order == 1)
                    <a href="{{ URL::route('admin.payment.profile', $page->id) }}">Бизнес профайл болгох</a>
                    @endif
                    <?php endif;?>
                  </td>
                  <td>
                    <a onclick="if(confirm('Sure')) { return true; } else { return false; }" href="{{ URL::route('admin.payment.destroy', $page->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></a>
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
          <div align="center">{{ $pages->links() }}</div>
      </section>
  </div>
</div>
@stop