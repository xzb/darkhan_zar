<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Singleton Technology LLC">
    <title>{{\DB::table('settings')->remember(600, 'backend_title')->first()->backend_title}}</title>
        @include('admin._partials.assets')
</head>

<body>

<section id="container" class="">

<header class="header white-bg">
    @include('admin._partials.header')
</header>

<aside>
    @include('admin._partials.navigation')
</aside>

<section id="main-content">
  <section class="wrapper">
    {{ Notification::showAll() }}    
    @yield('main')
  </section>
</section>


<footer class="site-footer">
  <div class="text-center">
      {{ date('Y') }} &copy; Developed by <a href="http://singleton.mn" target="_blank" style="color: #fff;">Singleton Technology LLC</a>
      <a href="#" class="go-top">
          <i class="icon-angle-up"></i>
      </a>
  </div>
</footer>

</section>

<!-- js placed at the end of the document so the pages load faster -->

<script class="include" type="text/javascript" src="/admin/template/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="/admin/template/js/jquery.scrollTo.min.js"></script>
<script src="/admin/template/js/respond.min.js" ></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>


<script type="text/javascript" src="/admin/template/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/admin/template/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

<script src="/admin/template/js/advanced-form-components.js"></script>
<script src="/admin/template/assets/bootstrap-fileupload/bootstrap-fileupload.js" ></script>
<script src="/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript">

  $(document).ready(function() {
    tinymce.init({
      theme: "modern",
      templates: [ 
          {title: 'QuoteText', description: 'QuoteText', url: "/assets/js/tinymce/plugins/template/template1.html"},
          {title: 'Twitter', description: 'Twitter', url: "/assets/js/tinymce/plugins/template/template2.html"}  
      ],
      file_browser_callback : elFinderBrowser,
      plugins: [
             "twitterShare quoteText colorpicker advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
      selector: ".tinymce1",
      image_advtab: true,
      style_formats: [
          {title: 'Bold text', inline: 'b'},
          {title: 'Imagem esquerda', inline: 'img', styles: {float: 'left',margin:'0 10px 10px 0'}}
      ],
    });


  });

  function elFinderBrowser (field_name, url, type, win) {
    tinymce.activeEditor.windowManager.open({
    file: '/elfinder/tinymce',// use an absolute path!
    title: 'Arquivos',
    width: 900,
    height: 450,
    resizable: 'yes'
    }, {
    setUrl: function (url) {
      win.document.getElementById(field_name).value = url;
    }
    });
    return false;
  }

</script>


<!--common script for all pages-->
<script src="/admin/template/js/common-scripts.js"></script>

</body>

</html>