@extends('admin._layouts.default')
 
@section('main')

<div class="row">
  <div class="col-lg-12">
      <section class="panel">
          <header class="panel-heading">
               Хар жагсаалт
          </header>
          <table class="table table-striped table-advance table-hover">
              <thead>
              <tr>
                  <th><i class="icon-bullhorn"></i>Нэр</th>
                  <th><i class=" icon-edit"></i> Төрөл</th>
                  <th></th>
              </tr>
              </thead>
              <tbody>
              @foreach ($blacklists as $blacklist)  
              <tr>
                  <td><a href="{{ URL::route('admin.blacklist.edit', $blacklist->id) }}">{{ $blacklist->name }}</a></td>
                  <td>{{ $blacklist->type == 1 ? 'Утас' : 'Имэйл' }}</td>
                  <td>
                    <a href="{{ URL::route('admin.blacklist.edit', $blacklist->id) }}">
                      <button class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>
                    </a>
                    {{ Form::open(array('route' => array('admin.blacklist.destroy', $blacklist->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?', 'style' => 'display:inline;')) }}
                          <button onclick="if(confirm('Sure')) { return true; } else { return false; }  " type="submit" href="{{ URL::route('admin.blacklist.destroy', $blacklist->id) }}" class="btn btn-danger btn-xs delete"><i class="icon-trash "></i></button>
                    {{ Form::close() }}  
                  </td>
              </tr>
              @endforeach
              </tbody>
          </table>
      </section>
  </div>
</div>
 
@stop