@extends('front._layouts.product')

@section('main')

<div id="content">
    <!-- @include('front.profile._left', array('products' => $products, 'profile' => $profile, 'user' => $user, 'from'=>'saw')) -->

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Байгууллага</h4>
          {{ Form::open(array('id'=>'register_form', 'class'=>'form-signin', 'route'=>'profile.company', 'files'=> true)) }}
            <div id="company_div">
              <div class="form-row">
                {{ Form::text('company_name', $profile->company_name, array('placeholder'=>'Байгууллагын нэр')) }}
              </div>
              <div class="form-row">
                {{ Form::text('department', $profile->about_us, array('placeholder'=>'Салбар')) }}
              </div>
              @if($profile->is_company == 0)
              <div class="form-row">
                {{ Form::textarea('about_us', '', array('placeholder'=>'Бидний тухай')) }}
              </div>
              @endif
            </div>
            <div class="form-row">
              <a class="btn btn-red" onclick="submitRegisterForm();" href="#">Хадгалах</a>
            </div>  
          {{ Form::close() }}
        
        <!-- pager -->
        <center>
        {{ $products->links(); }}
        </center>
    </div>

</div>
@stop