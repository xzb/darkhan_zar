<link rel="stylesheet" href="/css/company.css"/>
<?php use App\Models\User; ?>
@extends('front._layouts.product')

@section('main')

<div id="content" class="company-main">
                    <div class="row">
                        <div class="row-same-height">
                          

    @include('front.profile._leftNew', array('profile' => $profile, 'user' => $user, 'from'=>'user'))
                        <div class="col-md-9 col-md-cright col-md-height">
                                <div class="margin-30">
        <?php 
                                              
                    $bottom = '0px';
                    $left = '0px';
                    switch ($profile->cover_style) {
                            case 1:  $bottom = '-10px'; $left = '0px'; break;
                            case 2:  $bottom = '0px'; $left = '0px';  break;
                            case 3:  $bottom = '0px'; $left = '0px'; break;
                          } 
                           
                  ?>
        <!-- company cover -->
        @if(count($banners) > 0 || (Sentry::check() && Sentry::getUser()->id == $user->id))
            @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile,'bottom'=>$bottom, 'left'=>$left))
        @endif
        <div class="row">
            <div class="col-md-12">
        <h4>Байгууллагын оператор</h4>
        <ul class="ads-list search_results" id="comUserList">
          <?php foreach ($users as $companyUser): ?>
                <?php $user = User::find($companyUser->user_id);?>
                @include('front.profile._user', array('user' => $user))
            <?php endforeach;?>
        </ul>
        <br/>
        <div class="user-search">
            <form method="POST" action="#" id="company_user_form">
              <div class="user-search-input">
                <input id="usermail" name="usermail" autocomplete="off" type="text" class="user-search-txt input-1" placeholder="Хэрэглэгчийн и-майл хаяг" value="" />

                <!--  -->
                <div class="user-suggestion">
                    <div class="user-suggestions">
                        <ul id="user-suggestion-list"></ul>
                    </div>
                </div>

              </div>
              <button class="btn btn-red" type="submit" onclick="companyUserSubmit(); return false;">
                  <span>Нэмэх</span>
              </button>
            </form>
          </div>
     </div>
            </div><!-- end product list section-->
            </div>

            
        </div>
        <!-- end right side-->
    </div>
</div>
</div>

@stop