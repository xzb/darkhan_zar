

<!--         <link rel="stylesheet" href="css/base.css" />
        <link rel="stylesheet" href="css/layout.css" />-->    
        <!-- <link rel="stylesheet" href="/css/jquery.bxslider.css"/> -->
        <link rel="stylesheet" href="/css/company.css"/>
        
        <!-- <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script> -->
        <!-- // <script type="text/javascript" src="/js/jquery.bxslider.js"></script> -->

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

@extends('front._layouts.product')

@section('main')
<?php $color = 'gray'; ?>
@if($profile->color_code != '')
<?php $color = '#'.$profile->color_code; ?>
@endif
                <div class="company-main">
                    <div class="row">
                        <div class="row-same-height">
                          

                          <!-- start left side -->
                          @include('front.profile._leftNew', array('products' => $products, 'profile' => $profile, 'user' => $user, 'from'=>'index'))
                            <!-- end left side-->
                            
                            <div class="col-md-9 col-md-cright col-md-height">
                                <div class="margin-30">
                                        <?php 
                                              
                                                $bottom = '0px';
                                                $left = '0px';
                                                switch ($profile->cover_style) {
                                                        case 1:  $bottom = '-10px'; $left = '0px'; break;
                                                        case 2:  $bottom = '0px'; $left = '0px';  break;
                                                        case 3:  $bottom = '0px'; $left = '0px'; break;
                                                      } 
                                                       
                                              ?>
                                    <!-- company cover -->
                                    @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile,'bottom'=>$bottom, 'left'=>$left))
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="company-title" style="display: inline;">Нийт бараа  ({{$counter}})</h1>
                                            <select id="sortType" class="sorter" onchange="this.form.submit()" style="font-size: 11px;width: 170px;" name="sortType">
                                              <option value="0" <?php if($sort_type == '0'){ echo 'selected="selected"';  } ?>>Эрэмбэлэх</option>
                                              <option value="1" <?php if($sort_type == '1'){ echo 'selected="selected"';  } ?>>Үнээр буурахаар</option>
                                              <option value="4" <?php if($sort_type == '4'){ echo 'selected="selected"';  } ?>>Үнээр өсөхөөр</option>
                                              <option value="2" <?php if($sort_type == '2'){ echo 'selected="selected"';  } ?>>Үсгийн дараалал буурахаар</option>
                                              <option value="5" <?php if($sort_type == '5'){ echo 'selected="selected"';  } ?>>Үсгийн дараалал өсөхөөр</option>
                                              <option value="3" <?php if($sort_type == '3'){ echo 'selected="selected"';  } ?>>Нэмэгдсэн огноо буурахаар</option>
                                              <option value="6" <?php if($sort_type == '6'){ echo 'selected="selected"';  } ?>>Нэмэгдсэн огноо өсөхөөр</option>
                                            </select>
                                            <div class="pull-right">
                                                <a href="/profile/{{$user->id}}?viewType=grid<?php if($page>1){echo '&page='.$page;}?>"><i class="fa fa-th-large fa-lg fa-fw" style="color:<?php if ($viewType == "grid") echo $color; else echo 'gray';?>"></i></a>
                                                <a href="/profile/{{$user->id}}?viewType=list<?php if($page>1){echo '&page='.$page;}?>"><i class="fa fa-align-justify fa-lg fa-fw" style="color:<?php if ($viewType == "list") echo $color; else echo 'gray';?>"></i></a>
                                            </div>
                                        </div>



                                        <div class="col-md-12">
                                            <?php $top = '0'; ?>
                                            <div class="productlist">
                                            @if($viewType == "list")
                                            
                                            <style type="text/css">
                                                .ad-box-m .ad-info {
                                                    color: #000;
                                                    float: left;
                                                    font-size: 14px;
                                                    margin-left: -70px !important;
                                                    position: relative;
                                                    width: 290px !important;
                                                }
                                            </style>
                                                
                                                @include('front.product._list2', array('products' => $products,'padding_top'=>$top))
                                            @else
                                                @include('front.product._thumblist', array('products' => $products,'padding_top'=>$top))
                                            @endif    
                                            </div><!-- product list end-->
                                            
                                            <div class="row text-center">
                                                {{ $products->links(); }}
                                            </div>
                                        </div>
                                    </div><!-- end product list section-->
                                </div>

                                
                            </div>
                            <!-- end right side-->
                        </div>
                    </div>
                
          

       

@stop