@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('profile' => $profile, 'user' => $user, 'from'=>'banner'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Баннер нэмэх</h4>
          @if ($errors->any())
            <div style="color: red;">
                    jpg, png, jpeg, gif өргөтгөлтэй зураг оруулна уу.
            </div>
            @endif
        {{ Form::open(array('id'=>'banner_form', 'class'=>'form-signin', 'route'=>'profile.bannernew', 'files'=>true)) }}
            <div class="form-row">
              <span style="float: left; margin-right: 2px;">Баннер :</span>
              {{ Form::file('file', array('class' => 'default', 'name' => 'image')) }}
              <p class="desc">Баннерын хэмжээ 720x300 pixel хэмжээтэй байвал зохимжтой.</p>
            </div>
            <div class="form-row">
                {{ Form::text('link', null, array('placeholder'=>'Линк')) }}
            </div>
            <div class="form-row">
              {{ Form::submit('Хадгалах', array('class' => 'btn btn-red')) }}
            </div>  
        {{ Form::close() }}   
    </div>

</div>
@stop