@extends('front._layouts.product', array('is_coupon',$is_coupon))
<?php use App\Models\Productimage;?>
<link rel="stylesheet" href="/css/coupon.css">
@section('main')
          <div class="coupon-main">
              <div class="row">
                  <div class="col-md-3">
                      <div class="coupon-left-col">
                          <div class="coupon-category">
                              <h4 class="coupon-title">Купон</h4>
                              <ul>
                                  <li><a href="/coupon?pid={{30}}"><span class="coupon red">30%</span><b>хямдралтай</b></a></li>
                                  <li><a href="/coupon?pid={{20}}"><span class="coupon green">20%</span><b>хямдралтай</b></a></li>
                                  <li><a href="/coupon?pid={{10}}"><span class="coupon blue">10%</span><b>хямдралтай</b></a></li>
                                  <li><a href="/coupon?pid={{5}}"><span class="coupon yellow">5%</span><b>хямдралтай</b></a></li>
                              </ul>
                          </div>

                          <div class="coupon-category-2">
                              <h4 class="coupon-title">Ангилал</h4>
                              <ul>
                                @foreach($categories as $c)
                                  <li><a href="/coupon?cid={{$c->id}}"><b>{{$c->name}}</b></a></li>
                                @endforeach  
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-9">
                      <h4 class="coupon-title">Нийт хямдралтай бараа</h4>
                      <div class="row">
                        @if(count($coupons)> 0)
                            @foreach($coupons as $c)
                              <div class="col-md-6">
                                  <div class="product-1">
                                      <div class="product-image">
                                        @if($c->image_filename)
                                          <img src="/uploads/thumb/s_{{$c->image_filename}}" alt="" />
                                        @else
                                        
                                          <img src="/uploads/nophoto.png" alt="" />
                                        
                                        @endif

                                        
                                      </div>

                                      <div class="product-info">
                                          <h1><a href="/p/{{$c->id}}/{{ str_replace('/', '', $c->name)}}">{{$c->name}}</a></h1>
                                          <span class="price">{{$c->coupon_price}} ₮</span>
                                      </div>
                                      <?php 
                                        switch ($c->coupon_percent) {
                                          case '5':
                                            $color = 'yellow';
                                            break;
                                          case '10':
                                            $color = 'blue';
                                            break;
                                          case '20':
                                            $color = 'green';
                                            break;
                                          case '30':
                                            $color = 'red';
                                            break;
                                          
                                          default:
                                            $color = 'red';
                                            break;
                                        }
                                      ?>
                                      <span class="coupon {{$color}}">{{$c->coupon_percent}}%</span>
                                  </div>
                              </div>
                            @endforeach  
                        @else
                        <div id="blank">
                          Хямдралтай бараа байхгүй байна
                        </div> 
                        @endif
                          
                      </div>
                  </div>
              </div>
          </div>
      
@stop