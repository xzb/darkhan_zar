<?php use App\Models\CompanyFollow; ?>
<?php use App\Models\CompanyDetail; ?>
<?php use App\Models\CompanyCategory; ?>
<?php use App\Models\CoverImageType; ?>


<?php $anybodyonline = Sentry::check(); ?>
<?php $onlineUserId = ($anybodyonline)?Sentry::getUser()->id:'0'; ?>
<?php $color = 'gray'; ?>
@if($profile->color_code != '')
<?php $color = '#'.$profile->color_code; ?>
<style type="text/css">
  .profile .left-bar .list ul li.active{
    background-color: #{{$profile->color_code}};
  }
  .pagination .active span {
    background-color: #{{$profile->color_code}} !important;
    border-color: #{{$profile->color_code}} !important;
  }
  .ads-list {
    border-top: 1px solid #{{$profile->color_code}};
  }
</style>
@endif
<div class="left-bar" >
    <div class="user">
      @if($anybodyonline)
        @if($onlineUserId == $user->id)
          @if($profile->is_company == 0)
          <a href="/profile/edit"><img src="/images/icons/edit.png" alt="" title="Засах"> Мэдээлэл засах</a>
          @else
          <a href="/profile/company"><img src="/images/icons/edit.png" alt="" title="Засах"> Мэдээлэл засах</a>
          @endif
        <br/>
        @else
          @if($profile->is_company == 1)
            <?php $is_follow = CompanyFollow::where('company_id', '=', $user->id)->where('user_id', '=', $onlineUserId)->first();?>
            @if($is_follow)
              <a href="#" class="btn btn-red follow" onclick="follow({{$user->id}}); return false;" id="follow"><i class="fa fa-minus-circle" style="padding-right: 4px;"></i>Дагасан</a>
            @else
              <a href="#" class="btn btn-red follow" onclick="follow({{$user->id}}); return false;" id="follow"><i class="fa fa-plus-circle" style="padding-right: 4px;"></i>Дагах</a>
            @endif
          @endif
        @endif
      @endif
      <div class="photo">
      <img src="<?php if($profile && $profile->image):?>/uploads/profile/{{$profile->image}}<?php else:?> /images/profile.png <?php endif;?>">
      
      </div>
      @if($anybodyonline)
      @if($onlineUserId == $user->id)
      <div id="file-uploader" style="position: relative; top: -40px; right: 50px;" class="">
        <button style="border: none; background: none" id="upload">
          <img src="/images/camera-icon.png" id="profileLoader">
        </button>
        <noscript>      
            &lt;p&gt;Please enable JavaScript to use file uploader.&lt;/p&gt;
            &lt;!-- or put I could put an upload form here --&gt;
        </noscript> 
      <input type="file" multiple="multiple" name="file" style=" width: 40px; height: 35px; position: absolute; right: 80px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
      @else
      <p style="margin-top: 30px;"></p>
      @endif
      @else
      <p style="margin-top: 30px;"></p>
      @endif
      <h4>
      @if($profile->is_company == 1)
      {{$profile->company_name}}
      @else
      {{mb_substr($user->last_name, 0, 1)}}.{{ucfirst($user->first_name)}}
      @endif
      </h4>
      <span>@if($profile->is_company == 1)
              {{$profile->about_us}}
            @else
              {{$profile->position}}
            @endif</span>
    </div>
      <hr/>
      
    <div class="list">
      ПРОФАЙЛ
      @if($anybodyonline)
        @if($onlineUserId == $user->id && $profile->is_company == 1)
          <span id="newSubMenu" style ="float:right; cursor: pointer; padding-right: 5px;" alt="Дэд цэс нэмэх" title="Дэд цэс нэмэх" onclick = "createSubMenu($(this),'{{$profile->id}}');">Нэмэх</span>
        @endif
      @endif
      <ul>
        @if($profile->is_company == 1)
        <?php  $menus = CompanyDetail::where('user_id','=',$user->id)->get(); ?>
          @foreach($menus as $menu)
            <li id ="sub_id{{$menu->id}}" <?php if($from == 'content'.$menu->id){echo 'class="active"';}?>><a href="/profile/content/{{$menu->id}}">{{$menu->title}}</a>@if($anybodyonline)@if($onlineUserId == $user->id)<i class="fa fa-remove" style="float:right; padding-top:4px; cursor:pointer; <?php if($from == 'content'.$menu->id){ echo 'margin-right:-4px'; }?>" onclick ="removeSubMenu({{$menu->id}})"></i>@endif @endif</li>
          @endforeach
        @endif
        <li><a href="/add">Бараа оруулах</a></li>
        <li <?php if($from == 'index'){echo 'class="active"';}?>><a href="/profile/{{$user->id}}">Нийт зар</a><em>{{$counter}}</em></li>
        <!-- <li <?php if($from == 'saw'){echo 'class="active"';}?>><a href="/saw/{{$user->id}}">Үзсэн зар</a><em>{{$state}}</em></li> -->
      </ul>
      
      @if($profile->video != '' && $profile->is_company == 1)
      <?php 
        parse_str( parse_url( $profile->video, PHP_URL_QUERY ), $my_array_of_vars );
      ?>
      <div style="margin-left:-20px;padding-top:5px">
       <!-- <video id="vid1" src="" class="video-js vjs-default-skin" 
         preload="auto" 
         width="200" 
         height="160"
         data-setup='{ "techOrder": ["youtube"],"src": "{{$profile->video}}" }'></video> -->
       <iframe width="200" height="160" src="https://www.youtube.com/embed/{{$my_array_of_vars['v']}}?feature=player_detailpage&html5=1&controls=0" frameborder="0" allowfullscreen></iframe>
      </div>
      
      @endif
    </div>
     @if($profile->is_company == 1)
      <div class="category">
        
          <?php $categories = CompanyCategory::where('company_id','=',$profile->user_id)->get(); ?>
          КАТЕГОРИ
          @if($anybodyonline)
            @if($onlineUserId == $user->id)
              <span id="categoryAddInLeft" style="float:right; cursor: pointer; padding-right: 1px; margin-top: -4px">
                <a href="/profile/setCategory" class="fancy fancybox.ajax">Нэмэх</a>
              </span>
            @endif
          @endif  
        <div>
         @if(count($categories)>0)
         <ul id="categoryCompanyList">
            @foreach($categories as $c)
                
                 <li id ="comcat{{$c->id}}">{{$c->category_name}}
                        @if($anybodyonline)
                          @if($onlineUserId == $user->id)
                          <i class="fa fa-remove" style="float:right; padding-top:4px; cursor:pointer" onclick ="removeCompanyCategory({{$c->id}})"></i>
                          @endif
                        @endif
                  </li>
                
            @endforeach
            </ul>
         @else
                <ul id="categoryCompanyList"></ul>
         @endif 
        </div>  
          
      </div>
      @endif

      <div class="list">
      @if($anybodyonline)
        @if($onlineUserId == $user->id)
        <br/>
        ТОХИРГОО
        <ul> 
          <li id="changePassFancy"><a href="/profile/changePass" class="fancy fancybox.ajax">Нууц үг солих</a></li>
          <li><span id="create_video" style ="cursor: pointer; padding-right: 15px;" onclick = "createVideo($(this),'{{$profile->id}}','{{$profile->video}}');">Видео танилцуулга</span></li>
          <li><span style="cursor:pointer" onclick = "$('.color-box').trigger('click');">Өнгө сонгох</span><div class="color-box" style="background-color: {{$color}}"></div></li>
          @if($profile->is_company == 1)
            <li id="cover_style_chooser" style="cursor:pointer">Cover Style
              <div id="cover_style_changer">
                <ul>
                  <li><a href="/profile/setCoverStyle/1">- 1 зурагтай</a></li>
                  <li><a href="/profile/setCoverStyle/2">- 2 зурагтай</a></li>
                  <li><a href="/profile/setCoverStyle/3">- 3 зурагтай</a></li>
                </ul>  
              </div>
            </li>
            <li <?php if($from == 'user'){echo 'class="active"';}?>><a href="/profile/user">Хэрэглэгч</a></li>
            <li <?php if($from == 'expired'){echo 'class="active"';}?>><a href="/profile/expired">Хугацаа дууссан</a></li>
            <li <?php if($from == 'company'){echo 'class="active"';}?>><a href="/profile/companyRemove">Байгууллага устгах</a></li>
          @else
            <li <?php if($from == 'expired'){echo 'class="active"';}?>><a href="/profile/expired">Хугацаа дууссан</a></li>
            <li <?php if($from == 'company'){echo 'class="active"';}?>><a href="/profile/company">Байгууллага үүсгэх</a></li>

          @endif

        </ul>
        @endif
      @endif 
    </div>
    <hr/>
    
    <div class="contacts">
      ХОЛБОО БАРИХ
      <ul id="our-contact">
        @if($profile)<li><i class="fa fa-envelope fa-5 fa-fw" style="margin-left:-40px; padding-right:20px; color:{{$color}}"></i>{{$profile->email}}</li>@endif
        @if($profile)<li><i class="fa fa-phone fa-5 fa-fw" style="margin-left:-40px; padding-right:20px; color:{{$color}}"></i>{{$profile->phone}}</li>@endif
        @if($profile)<li><i class="fa fa-map-marker fa-5 fa-fw" style="margin-left:-40px; padding-right:20px; color:{{$color}}"></i>{{$profile->address}}</li>@endif
      </ul>
    </div>
    <hr>
    @if($profile->is_company == 1)
    <div class="comment">
      САНАЛ ХҮСЭЛТ
    <form id="commentForm" action="/profile/savefeed" method="POST">
        <?php echo Form::token();?>
        <input type="hidden" value="{{$profile->user_id}}" class="objId" name="objId">
        <textarea placeholder="Санал хүсэлт" style="height: 80px; width:162px;border:1px solid #c7c7c7;background:#fff " id="maxcommentbody" name="content" class="commentbox"></textarea>
        <input type="submit" value="Илгээх" class="btn" style="background-color: {{$color}}; width:162px; height: 30px; margin-top:3px">
    </form>
    </div>
    @endif
    <!-- Modal -->
<?php $cover = CoverImageType::where('cover_id','=',$profile->cover_style)->get(); ?>
@foreach($cover as $i)
    <div id="banner_modal{{$i->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Зураг</h3>
    </div>
    <div class="modal-body">
      <img id="thumbnail{{$i->id}}" src="">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/banners" method="post" onsubmit="return checkCoordsBanners({{$i->id}});">
                  <input type="hidden" id="filename{{$i->id}}" name="filename{{$i->id}}" />
                  <input type="hidden" id="pos" name="pos" value="{{$i->id}}"/>
                  <input type="hidden" id="x{{$i->id}}" name="x{{$i->id}}" />
            <input type="hidden" id="y{{$i->id}}" name="y{{$i->id}}" />
            <input type="hidden" id="w{{$i->id}}" name="w{{$i->id}}" />
            <input type="hidden" id="h{{$i->id}}" name="h{{$i->id}}" />
            <input type="submit" value="Хадгалах" class="btn btn-red" />
      </form>
    </div>
    </div>
@endforeach



    <!-- Modal -->
    <div id="myMiga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Зураг</h3>
    </div>
    <div class="modal-body">
      <img id="thumbnail" src="">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/crop" method="post" onsubmit="return checkCoords();">
                  <input type="hidden" id="filename" name="filename" />
                  <input type="hidden" id="x" name="x" />
            <input type="hidden" id="y" name="y" />
            <input type="hidden" id="w" name="w" />
            <input type="hidden" id="h" name="h" />
            <input type="submit" value="Хадгалах" class="btn btn-red" />
      </form>
    </div>
    </div>
    </div>
