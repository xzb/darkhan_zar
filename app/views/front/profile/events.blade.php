@extends('front._layouts.default')
@section('main')

<link rel="stylesheet" href="/css/profile3.css"/>
<script type="text/javascript" src="/js/profile.js"></script>
  
                <div class="profile-main">
                    <div class="row">
                        <div class="row-same-height">
                            <div class="col-md-12">
                                <div class="margin-30">
                                    <ul class="nav tab-menu nav-justified" role="tablist">
                                        <li id="tab1"><a href="/myProduct/{{$id}}">Миний зарууд</a></li>
                                        <li id="tab2"><a href="/followedCompanies/{{$id}}">Дагасан дэлгүүрүүд</a></li>
                                        <li id="tab3" class="active"><a href="/events/{{$id}}">Мэдээлэл</a></li>
                                    </ul>
                                    
                                    <div style="padding-top:20px">
                                        <h1 class="profile-title">Мэдээлэл</h1>
                                    </div>
                                    <div id="myEvents" user_id = "{{$id}}"></div>
                                            
                                </div>
                            </div>
                            <!-- end right side-->
                        </div>
                    </div>
                </div>
            </div>
        
            
        
@stop