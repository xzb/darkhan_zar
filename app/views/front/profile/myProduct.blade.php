@extends('front._layouts.default')
@section('main')

<link rel="stylesheet" href="/css/profile1.css"/>
<script type="text/javascript" src="/js/profile.js"></script>
  
                <div class="profile-main">
                    <div class="row">
                        <div class="row-same-height">
                            <div class="col-md-12">
                                <div class="margin-30">
                                    <ul class="nav tab-menu nav-justified" role="tablist">
                                        <li id="tab1" class="active"><a href="/myProduct/{{$id}}">Миний зарууд</a></li>
                                        <li id="tab2"><a href="/followedCompanies/{{$id}}">Дагасан дэлгүүрүүд</a></li>
                                        <li id="tab3"><a href="/events/{{$id}}">Мэдээлэл</a></li>
                                    </ul>

                                    <div class="row padding-20">
                                        <h1 class="profile-title">
                                            Миний зарууд  ({{ $countProducts }})
                                        </h1>
                                    </div>
                                    <div id="myProducts" my_id="{{$id}}">
                                    </div>    
                                    
                                </div>
                            </div>
                            <!-- end right side-->
                        </div>
                    </div>
                </div>
            </div>
        
            
        
@stop