@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('profile' => $profile, 'user' => $user, 'from'=>'banner'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Миний баннерууд</h4>
        <ul class="ads-list search_results">
            <?php foreach ($banners as $banner): ?>
                <li class="ad ad-box-m">
                    <div class="clearfix">
                        <div class="ad-info" style="width: 280px;">
                    <img src="/uploads/company/{{$banner->image}}" style="max-height: 100px;">
                    </div>
                    <div style="width: 300px; float:left;">
                        <a href="http://{{$banner->link}}" target="_blank">{{$banner->link}}</a>
                    </div>
                    <span>
                        <span class="ad-action">
                            <a href="/profile/banner/delete/{{ $banner->id }}" onclick="return confirm('Та итгэлтэй байна уу?');"><img src="/images/icons/delete.png" alt="" title="Устгах"></a>
                        </span>
                    </span>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
        <br/>
        <!-- Modal -->
        <div id="myBanner" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Баннер</h3>
        </div>
        <div class="modal-body">
          <img id="thumbnailbanner" src="">
                <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
                <br/>
                <form action="/profile/savebanner" method="post" onsubmit="return checkCoordsBanner();">
                  <input type="hidden" id="bannername" name="bannername" />
                  <input type="hidden" id="bx" name="bx" />
                    <input type="hidden" id="by" name="by" />
                    <input type="hidden" id="bw" name="bw" />
                    <input type="hidden" id="bh" name="bh" />
                <input type="submit" value="Хадгалах" class="btn btn-red" />
          </form>
        </div>
        </div>
        <div id="banner-uploader" style="position: relative; overflow: hidden; direction: ltr;" class="">
                <button class="btn btn-red" id="upload_banner">Шинээр нэмэх</button>
                <noscript>          
                        &lt;p&gt;Please enable JavaScript to use file uploader.&lt;/p&gt;
                        &lt;!-- or put I could put an upload form here --&gt;
                </noscript> 
            <input type="file" multiple="multiple" name="banner" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
            <div style="margin-top: 5px; display:none;" id="loading_bar">
                <img src="/images/loading_bar.gif"/>
            </div>
    </div>

</div>
@stop