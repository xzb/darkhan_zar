@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('profile' => $profile, 'user' => $user, 'from'=>'info'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Бидний тухай</h4>
        <div>
            {{$profile->about_us;}}
        </div>
    </div>

</div>
@stop