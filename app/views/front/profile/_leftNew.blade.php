<?php use App\Models\CompanyFollow; ?>
<?php use App\Models\CompanyDetail; ?>
<?php use App\Models\CompanyCategory; ?>
<?php use App\Models\CoverImageType; ?>


<?php $anybodyonline = Sentry::check(); ?>
<?php $onlineUserId = ($anybodyonline)?Sentry::getUser()->id:'0'; ?>
<?php $color = '#E31132'; ?>
@if($profile->color_code != '')
<?php $color = '#'.$profile->color_code; ?>
<style type="text/css">
  .nav-profile > li.active > a,
  .nav-profile > li.active > a:hover,
  .nav-profile > li.active > a:focus {
      color: #fff !important;
      background-color: #{{$profile->color_code}} !important;
  }
  .pagination .active span {
    background-color: #{{$profile->color_code}} !important;
    border-color: #{{$profile->color_code}} !important;
  }
  .ads-list {
    border-top: 1px solid #{{$profile->color_code}};
  }
</style>
@endif

<!-- new style -->

<div class="col-md-2 col-md-cleft col-md-height col-top">
                                <div class="margin-30">
                                  @if($anybodyonline)
                                    @if($onlineUserId != $user->id)
                                      @if($profile->is_company == 1)
                                        <?php $is_follow = CompanyFollow::where('company_id', '=', $user->id)->where('user_id', '=', $onlineUserId)->first();?>
                                        @if($is_follow)
                                          <a href="#" class="btn btn-red btn-block" onclick="follow({{$user->id}}); return false;" id="follow"><i class="fa fa-minus-circle"></i>Дагасан</a>
                                        @else
                                          <a href="#" class="btn btn-red btn-block" onclick="follow({{$user->id}}); return false;" id="follow"><i class="fa fa-plus-circle"></i>Дагах</a>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                      

                                  <div class="banner-section">
                                    <a href="" class="img">
                                      <img src="<?php if($profile && $profile->image):?>/uploads/profile/{{$profile->image}}<?php else:?> /images/profile.png <?php endif;?>"/>
                                    </a>
                                    @if($anybodyonline)
                                      @if($onlineUserId == $user->id)
                                      <div id="file-uploader" style="position: relative; top: -40px; right: 0px;" class="">
                                        <button style="border: none; background: none" id="upload">
                                          <img src="/images/camera-icon.png" id="profileLoader">
                                        </button>
                                        <noscript>      
                                            &lt;p&gt;Please enable JavaScript to use file uploader.&lt;/p&gt;
                                            &lt;!-- or put I could put an upload form here --&gt;
                                        </noscript> 
                                      <input type="file" multiple="multiple" name="file" style=" width: 40px; height: 35px; position: absolute; right: 118px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                                      @else
                                      <p style="margin-top: 30px;"></p>
                                      @endif
                                    @else
                                      <p style="margin-top: 30px;"></p>
                                    @endif
                                    <a href="/profile/{{$profile->user_id}}" class="link">
                                        <span>{{$profile->company_name}}</span>
                                        <span>{{$profile->about_us}}</span>
                                    </a>
                                  </div>


                                </div>
                                <div class="profile left-block">
                                  @if($anybodyonline)
                                    @if($onlineUserId == $user->id && $profile->is_company == 1)
                                      <span  id="newSubMenu"  class="btn-edit pull-right" style="cursor:pointer;" onclick = "createSubMenu($(this),'{{$profile->id}}');"><i class="fa fa-plus"></i></span>
                                    @endif
                                  @endif
                                    
                                    <div class="margin-30">
                                        <h4 class="company-title"><i class="fa fa-user"></i> Профайл</h4>
                                        <ul class="nav nav-profile">
                                          <?php  $menus = CompanyDetail::where('user_id','=',$user->id)->get(); ?>
                                          @foreach($menus as $menu)
                                          <?php $content_color=($from == 'content'.$menu->id)?'color:white':'color:#666'; ?> 
                                          <li id ="sub_id{{$menu->id}}" <?php if($from == 'content'.$menu->id){echo 'class="active"';}?>><a href="/profile/content/{{$menu->id}}">{{$menu->title}}</a>@if($anybodyonline)@if($onlineUserId == $user->id)<i class="fa fa-remove" style="position: inherit;  float:right; margin-top:-20px; cursor:pointer; {{$content_color}}; margin-right:5px" onclick ="removeSubMenu({{$menu->id}})"></i>@endif @endif</li>
                                         
                                          @endforeach
                                            <li <?php if($from == 'index'){echo 'class="active"';}?>><a href="/profile/{{$user->id}}">Нийт бараа<span class="pull-right" style="margin-right:4px">{{$counter}}</span></a></li>
                                            <li <?php if($from == 'feedback'){echo 'class="active"';}?>><a href="/profile/feedback/{{$profile->user_id}}">Санал хүсэлт</a></li>
                                            @if($anybodyonline)
                                                @if($onlineUserId == $user->id)
                                                <li><a href="/add">Бараа оруулах</a></li>
                                                @endif
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                                @if($profile->video != '' && $profile->is_company == 1)
                                <?php 
                                  parse_str( parse_url( $profile->video, PHP_URL_QUERY ), $my_array_of_vars );
                                ?>
                                <div class="left-block video-block">
                                    <iframe width="220" height="160" src="https://www.youtube.com/embed/{{$my_array_of_vars['v']}}?feature=player_detailpage&html5=1&controls=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                @endif

                                <div class="profile left-block">
                                  @if($anybodyonline)
                                    @if($onlineUserId == $user->id)
                                      <span id="categoryAddInLeft" class="btn-edit pull-right" style="cursor:pointer;"><i class="fa fa-plus"></i></span>
                                    
                                    @endif
                                  @endif  
                                  <?php $categories = CompanyCategory::where('company_id','=',$profile->user_id)->get(); ?>
                                    <div class="margin-30">
                                        <h4 class="company-title"><i class="fa fa-book"></i> Категори</h4>
                                        @if(count($categories)>0)
                                         <ul class="nav-cat" id="categoryCompanyList">
                                            @foreach($categories as $c)
                                                
                                                 <li id ="comcat{{$c->category_id}}"><a href="/profile/{{$profile->user_id}}?category={{$c->category_id}}">{{$c->category_name}}</a>
                                                        @if($anybodyonline)
                                                          @if($onlineUserId == $user->id)
                                                          <i class="fa fa-remove" style="float:right; padding-top:4px; cursor:pointer" onclick ="removeCompanyCategory({{$c->category_id}})"></i>
                                                          @endif
                                                        @endif
                                                  </li>
                                                
                                            @endforeach
                                            </ul>
                                         @else
                                                <span id="non_category" style="color:gray">категори сонгоогүй байна</span>
                                                <ul id="categoryCompanyList" class="nav-cat"></ul>
                                         @endif 
                                        
                                    </div>
                                </div>

                                
                                @if($anybodyonline)
                                  @if($onlineUserId == $user->id)
                                  <div class="profile left-block">
                                      <div class="margin-30">
                                        <h4 class="company-title"><i class="fa fa-cog"></i> Тохиргоо</h4>
                                        
                                         <ul class="nav-cat">
                                              <li id="changePassFancy"><a href="/profile/changePass" class="fancy fancybox.ajax">Нууц үг солих</a></li>
                                              <li><span id="create_video" style ="cursor: pointer; padding-right: 15px;" onclick = "createVideo($(this),'{{$profile->id}}','{{$profile->video}}');">Видео танилцуулга</span></li>
                                              <li><span style="cursor:pointer" onclick = "$('.color-box').trigger('click');">Өнгө сонгох</span><div class="color-box" style="background-color: {{$color}}"></div></li>
                                              @if($profile->is_company == 1)
                                                <li id="cover_style_chooser" style="cursor:pointer; color:#666">Cover Style
                                                  <div id="cover_style_changer">
                                                    <ul class="covers">
                                                      <li><a href="/profile/setCoverStyle/1">- 1 зурагтай</a></li>
                                                      <li><a href="/profile/setCoverStyle/2">- 2 зурагтай</a></li>
                                                      <li><a href="/profile/setCoverStyle/3">- 3 зурагтай</a></li>
                                                    </ul>  
                                                  </div>
                                                </li>
                                                <li <?php if($from == 'user'){echo 'class="active"';}?>><a href="/profile/user">Хэрэглэгч</a></li>
                                                <li <?php if($from == 'expired'){echo 'class="active"';}?>><a href="/profile/expired">Хугацаа дууссан</a></li>
                                              @else
                                                <li <?php if($from == 'expired'){echo 'class="active"';}?>><a href="/profile/expired">Хугацаа дууссан</a></li>
                                              @endif
                                            </ul>
                                      </div>
                                      </div>
                                      @endif 
                                    @endif 
                                
                                @if($profile)  
                                <div class="contact-block left-block">
                                    <div class="margin-30">
                                        <h4 class="company-title"><i class="fa fa-location-arrow"></i> Холбоо барих</h4>
                                        <div class="row cc">
                                            <div class="col-md-2"><i class="fa fa-envelope" style="color:{{$color}}"></i></div>
                                            <div class="col-md-10">{{$profile->email}}</div>
                                        </div>
                                        <div class="row cc">
                                            <div class="col-md-2"><i class="fa fa-phone" style="color:{{$color}}"></i></div>
                                            <div class="col-md-10">{{$profile->phone}}</div>
                                        </div>
                                        <div class="row cc">
                                            <div class="col-md-2"><i class="fa fa-map-marker" style="color:{{$color}}"></i></div>
                                            <div class="col-md-10">{{$profile->address}}</div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>

<!-- end newstyle -->

<!-- Modal -->
<?php $cover = CoverImageType::where('cover_id','=',$profile->cover_style)->get(); ?>
@foreach($cover as $i)
    <div id="banner_modal{{$i->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Зураг</h3>
    </div>
    <div class="modal-body">
      <img id="thumbnail{{$i->id}}" src="">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/banners" method="post" onsubmit="return checkCoordsBanners({{$i->id}});">
                  <input type="hidden" id="filename{{$i->id}}" name="filename{{$i->id}}" />
                  <input type="hidden" id="pos" name="pos" value="{{$i->id}}"/>
                  <input type="hidden" id="x{{$i->id}}" name="x{{$i->id}}" />
            <input type="hidden" id="y{{$i->id}}" name="y{{$i->id}}" />
            <input type="hidden" id="w{{$i->id}}" name="w{{$i->id}}" />
            <input type="hidden" id="h{{$i->id}}" name="h{{$i->id}}" />
            <input type="submit" value="Хадгалах" class="btn btn-red" />
      </form>
    </div>
    </div>
@endforeach



    <!-- Modal -->
    <div id="myMiga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Зураг</h3>
    </div>
    <div class="modal-body">
      <img id="thumbnail" src="">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/crop" method="post" onsubmit="return checkCoords();">
                  <input type="hidden" id="filename" name="filename" />
                  <input type="hidden" id="x" name="x" />
            <input type="hidden" id="y" name="y" />
            <input type="hidden" id="w" name="w" />
            <input type="hidden" id="h" name="h" />
            <input type="submit" value="Хадгалах" class="btn btn-red" />
      </form>
    </div>
    </div>
    
