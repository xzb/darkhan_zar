@extends('front._layouts.default')
@section('main')
<div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="index.html">Нүүр</a></li>
                  <li><a href="#">Профайл</a></li>
               </ul>
            </div>
         </div>
      </div>

      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
                     <!-- Sidebar Widgets -->
                     <div class="user-profile">
                        <a href="/home/{{$user->id}}">
                            <img src="<?php if($profile && $profile->image):?>/uploads/profile/{{$profile->image}}<?php else:?> /images/profile.png <?php endif;?>">
                        </a>
                        <div class="profile-detail">
                           <h6>{{$user->first_name}}</h6>
                           <ul class="contact-details">
                              <li>
                                 <i class="fa fa-envelope"></i>{{$user->email}}
                              </li>
                           </ul>
                           <ul class="contact-details">
                              <li>
                                 <i class="fa fa-list"></i> <a href="/home/{{$id}}" >Миний зарууд </a><span>{{ $countProducts }}</span>
                              </li>
                              <li>
                                 <i class="fa fa-pencil-square-o"></i><a href="/profile/edit" >Мэдээллээ засах </a>
                              </li>
                              <li>
                                 <i class="fa fa-sign-out "></i> <a href="/logout" >Гарах</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                     <div class="row">
                        <!-- Sorting Filters -->
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                           <!-- Sorting Filters Breadcrumb -->
                           <h3 class="main-title text-left">
                           Миний зарууд
                           </h3>
                           <!-- Sorting Filters Breadcrumb End -->
                        </div>
                        <!-- Sorting Filters End-->
                        <div class="clearfix"></div>
                        <!-- Ads Archive -->
                        <div class="posts-masonry">
                           <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
                                @foreach($products as $product)
                                        <div class="ads-list-archive">
                                             <!-- Image Block -->
                                             <div class="col-lg-5 col-md-5 col-sm-5 no-padding">
                                                <!-- Img Block -->
                                                <div class="ad-archive-img">
                                                   <a href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}">
                                                      @if($product->image_filename)
                                                        <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}">
                                                      @else
                                                        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
                                                      @endif
                                                   </a>
                                                </div>
                                                <!-- Img Block -->   
                                             </div>
                                             <!-- Ads Listing -->    
                                             <div class="clearfix visible-xs-block"></div>
                                             <!-- Content Block -->     
                                             <div class="col-lg-7 col-md-7 col-sm-7 no-padding">
                                                <!-- Ad Desc -->     
                                                <div class="ad-archive-desc">
                                                   <!-- Price -->    
                                                   <div class="ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</div>
                                                   <!-- Title -->    
                                                   <a href="/p/{{$product->id}}"><h3>{{  mb_substr($product->name, 0, 20, 'UTF-8') }}</h3></a>
                                                   <!-- Category -->
                                                   <!-- Short Description -->
                                                   <div class="clearfix visible-xs-block"></div>
                                                   <p class="hidden-sm">{{  mb_substr($product->description, 0, 200, 'UTF-8') }}...</p>
                                                   <!-- Ad History -->
                                                   <div class="clearfix archive-history">
                                                      <div class="last-updated">оруулсан: {{ time_ago($product->created_at) }}</div>
                                                      <div class="ad-meta">

<script type="text/javascript">
//product show.action: checkdelete
function renewajax(pid) {
    $.ajax({
      url: '/renewajax/' + pid,
      success: function(html) {
         alert(html);
      },
      beforeSend: function(html) {
      }
    });
    return false;
}
</script>

                                                         <a class="btn save-ad" onclick="renewajax(<?php echo $product->id?>);" href="#"><i class="fa fa-plus"></i> Шинэчилэх</a>
                                                         <a class="btn btn-success" target="_blank" href="/e/<?php echo $product->id ?>"><i class="fa fa-pencil"></i> Засах</a>
                                                         <a class="btn btn-danger" href="/del/<?php echo $product->id ?>?s=1"><i class="fa fa-times"></i> Устгах</a>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- Ad Desc End -->     
                                             </div>
                                             <!-- Content Block End --> 
                                        </div>
                                  @endforeach

                                 </div>
                        </div>
                        <!-- Ads Archive End -->  
                        <div class="clearfix"></div>
                        <!-- Pagination -->  
                        <div class="col-md-12 col-xs-12 col-sm-12">
                           {{ $products->links();}}
                        </div>
                        <!-- Pagination End -->   
                     </div>
                     <!-- Row End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
@stop