
<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/category_nested.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.fancybox.js"></script>

<script>
$(document).ready(function() {
	
	$( "#saveCategory" ).click(function() {
		
		if($('#category0').val() != 0)
		{
		  	$.ajax({
              url: bm.prefix + '/profile/setCat',
              type: 'POST',
              dataType: 'json',
              data: {id: $('#category_id').val()},
              success: function(data) {

                if(data.same == ''){
                  $('#categoryCompanyList').append('<li id ="comcat'+data.id+'"><a href="/profile/'+data.profile_id+'?category='+data.id+'">'+data.name+'</a><i class="fa fa-remove" style="float:right; padding-top:4px; cursor:pointer" onclick ="removeCompanyCategory('+data.id+')"></i></li>');
                }
                
              }
          	});
        $('#category0').attr("class","form-control");
        if($('#non_category').length)
          $('#non_category').remove();
		}
		else
		{
			$('#category0').attr("class","input-error");
		}	


	});
	
});
</script>
<div id="dialogCategory" title="Категори сонгох">

 <div class="form-group" id="categoryCombos" >
 	<!-- <form id="categoryAddForm" class="form-horizontal" accept-charset="UTF-8" action="/profile/setCategory" method="POST"> -->

    <input type="hidden" name="category_id" id="category_id"/>
    <input type="hidden" name="categories" id="categories"/>
    <div id="categories0" style="height:350px; width:160px">
    <select class="form-control" name="category0" id="category0" style="width:150px" onchange="drawCategoryComboInFront($(this).val(),0,1)">
        <?php
           for($i= 0; $i<count($cArray); $i++)
           {
              echo "<option value=".$cArray[$i]['id'].">".$cArray[$i]['name']."</option>";   
           }
        ?>
    </select>
    </div>
    <div class="form-group"> 
		<div class="col-lg-offset-2 col-lg-10"> 
			<input id="saveCategory" type="button" value="Хадгалах" class="btn btn-info"> 
		</div> 
	</div>
	<!-- </form> -->
  </div>

</div>
