@extends('front._layouts.product')

@section('main')
<div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="index.html">Нүүр</a></li>
                  <li><a href="#">Профайл</a></li>
               </ul>
            </div>
         </div>
      </div>

      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
                     <!-- Sidebar Widgets -->
                     <div class="user-profile">
                        <a href="profile.html"><img src="images/product/profile_360x362_1.jpg" alt=""></a>
                        <div class="profile-detail">
                           <h6>{{$user->first_name}}</h6>
                           <ul class="contact-details">
                              <li>
                                 <i class="fa fa-envelope"></i>{{$user->email}}
                              </li>
                           </ul>
                           <ul class="contact-details">
                              <li>
                                 <i class="fa fa-list"></i> <a href="/home/{{$id}}" >Миний зарууд </a><span>{{ $countProducts }}</span>
                              </li>
                              <li>
                                 <i class="fa fa-pencil-square-o"></i><a href="/profile/edit" >Мэдээллээ засах </a>
                              </li>
                              <li>
                                 <i class="fa fa-sign-out "></i> <a href="/logout" >Гарах</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                           <div class="heading-panel">
                              <h3 class="main-title text-left">
                                 Мэдээллээ засах
                              </h3>
                            </div>
                           <div class="content-info" style="background-color: white;">
                           <!--  Form -->
                             <div class="form-grid">
                                {{ Form::open(array('id'=>'register_form', 'class'=>'form-signin', 'route'=>'profile.edit', 'files'=> true)) }}
                                  @if($errors->any())
                                      <div class="error">
                                        @if ($errors->has('login'))

                                        @else
                                          {{ implode('<br>', $errors->all()) }}
                                        @endif
                                      </div>
                                      @endif
                                   <div class="row">
                                      <div class="col-md-12">
                                         <div class="form-group">
                                            <div style="width:60px">
                                              <div class="photo">
                                              <img src="<?php if($profile && $profile->image):?>/uploads/profile/{{$profile->image}}<?php else:?> /images/profile.png <?php endif;?>" width="150px">
                                              </div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="row">
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>Овог</label>
                                            {{ Form::text('last_name', $user->last_name, array('class'=>'form-control','placeholder'=>'Таны овог')) }}
                                         </div>
                                      </div>
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>Нэр</label>
                                            {{ Form::text('first_name', $user->first_name, array('class'=>'form-control','placeholder'=>'Таны нэр')) }}
                                         </div>
                                      </div>
                                   </div>
                                   <div class="row">
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>Төрсөн он</label>
                                            <?php 
                                                $year = array(0 => 'Төрсөн он');
                                                for ($i = 1920; $i < 2010; $i++) {
                                                  $year[$i] = $i;
                                                }
                                                ?>
                                                {{ Form::select('year', $year, $user->year, array('class'=>'form-control')) }}
                                         </div>
                                      </div>
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>Хүйс</label>
                                            {{ Form::select('gender', array('0'=>'Хүйс', '1'=>'Эр','2'=>'Эм'), $user->gender, array('class'=>'form-control')) }}
                                         </div>
                                      </div>
                                   </div>
                                   <div class="row">
                                      <div class="col-md-12">
                                         <div class="form-group">
                                            <label>Имэйл хаяг</label>
                                            {{ Form::text('email', $user->email, array('class'=>'form-control', 'placeholder'=>'Имэйл')) }}
                                         </div>
                                      </div>
                                   </div>
                                   <?php $place = 'Мэргэжил';?>
                                  <?php if($profile && $profile->is_company):?>
                                  <h3>Байгууллагын мэдээлэл</h3>
                                  <?php $place = 'Салбар'?>
                                  <?php else:?>
                                  <h3>Хэрэглэгчийн мэдээлэл</h3>
                                  <?php endif?>

                                   <div class="row">
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>{{$place}}</label>
                                            {{ Form::text('position', $profile->position, array('class'=>'form-control','placeholder'=>$place)) }}
                                         </div>
                                      </div>
                                      <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                         <div class="form-group">
                                            <label>Утасны дугаар</label>
                                            {{ Form::text('phone', $profile->phone, array('class'=>'form-control','placeholder'=>'Утас')) }}
                                         </div>
                                      </div>
                                   </div>
                                   <div class="row">
                                      <div class="col-md-12">
                                         <div class="form-group">
                                            <label>Хаяг</label>
                                            {{ Form::text('address', $profile->address, array('class'=>'form-control','placeholder'=>'Хаяг')) }}
                                         </div>
                                      </div>
                                   </div>


                                   <a class="btn btn-theme btn-lg btn-block" onclick="submitRegisterForm(); return false;" href="#">Хадгалах</a>
                                   
                              
                                </form>
                             </div>
                       
                           </div>
                 
                  <!-- Middle Content Area  End -->
                  </div>
               <!-- Row End -->
               </div>
            <!-- Main Container End -->
            </div>
         </section>
                  

<div id="myMiga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%; height: 100%; background-color: white;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Зураг</h3>
    </div>
    <div class="modal-body">
      <img id="thumbnail" src="">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/crop" method="post" onsubmit="return checkCoords();">
                  <input type="hidden" id="filename" name="filename" />
                  <input type="hidden" id="x" name="x" />
            <input type="hidden" id="y" name="y" />
            <input type="hidden" id="w" name="w" />
            <input type="hidden" id="h" name="h" />
            <input type="submit" value="Хадгалах" class="btn btn-red" />
      </form>
    </div>
</div>
@stop