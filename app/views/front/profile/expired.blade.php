<link rel="stylesheet" href="/css/company.css"/>
<?php use App\Models\User; ?>
@extends('front._layouts.product')

@section('main')
 <style type="text/css">
      .ad-box-m .ad-info {
          color: #000;
          float: left;
          font-size: 14px;
          margin-left: -70px !important;
          position: relative;
          width: 290px !important;
      }
  </style>
<div id="content" class="company-main">
                    <div class="row">
                        <div class="row-same-height">
    @include('front.profile._leftNew', array('profile' => $profile, 'user' => $user, 'from'=>'expired'))

     <div class="col-md-9 col-md-cright col-md-height">
                                <div class="margin-30">
        <?php 
                                              
                    $bottom = '0px';
                    $left = '0px';
                    switch ($profile->cover_style) {
                            case 1:  $bottom = '-10px'; $left = '0px'; break;
                            case 2:  $bottom = '0px'; $left = '0px';  break;
                            case 3:  $bottom = '0px'; $left = '0px'; break;
                          } 
                           
                  ?>
        <!-- company cover -->
        @if(count($banners) > 0 || (Sentry::check() && Sentry::getUser()->id == $user->id))
            @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile,'bottom'=>$bottom, 'left'=>$left))
        @endif
        <div class="row">
            <div class="col-md-12">
        <h4>Хугацаа дууссан зарууд</h4>

     
          <?php 
              $top = '6px';          
          ?>

     
    <div class="ads-list-head1" style="padding-top:{{$top}}">
      <span class="ad-info">
        <span class="ad-title">Гарчиг</span>
      </span>

      <span class="ad-flds">
        <span class="ad-date">Огноо</span>
        <span class="ad-price1 ad-price">Үнэ</span>
        <span class="ad-contact1 ad-contact">Xолбоо бариx</span>
      </span>
    </div>


    <ul class="ads-list">
    @foreach($exProducts as $product)
    <li class="ad ad-box-m {{ $product->image_filename ? 'hasPhoto' : '' }}">

    <div class="clearfix">
      <div class="ad-info">
          @if($product->image_filename)
            <span class="ad-photo">
              <span class="photo">
                <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" alt="">
              </span>
            </span>
          @endif
          <h4 class="ad-title">
            <a target="_blank" href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</a>
          </h4>
      </div>
      <span class="ad-flds">
        <span class="fld fld-2 ad-phone">{{ mb_substr($product->contact, 0, 17, 'UTF-8'); }}</span>
        <span class="fld fld-1 ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</span>
        <span class="ad-date">{{ time_ago($product->created_at) }}</span>
        <span class="ad-action">
          
            <a href="/renew/{{ $product->id }}"><img src="/images/icons/renew.png" alt="" title="Шинэчилэх"></a>
            <a href="/del/{{ $product->id }}" onclick="return confirm('Та итгэлтэй байна уу?');"><img src="/images/icons/delete.png" alt="" title="Устгах"></a>
          
        </span>
      </span>
    </div>
    </li>
    @endforeach
    </ul>
    </div>

</div><!-- end product list section-->
            </div>

            
        </div>
        <!-- end right side-->
    </div>
</div>
</div>

@stop