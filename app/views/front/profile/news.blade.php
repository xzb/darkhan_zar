@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('profile' => $profile, 'user' => $user, 'from'=>'news'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Үйлчилгээний нөхцөл</h4>
        <div>
            {{$profile->body;}}
        </div>
    </div>

</div>
@stop