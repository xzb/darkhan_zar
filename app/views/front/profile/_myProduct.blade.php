
    <?php $j = 0; ?>
    @foreach($products as $product)
    <?php $j++; ?>
    @if($j%2!=0)
    <div class="row">
    @endif
        <div class="col-md-6 pitem">
            <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" class="img pull-left">
                <?php if($product->image_filename):?>
                    <img src="{{ Croppa::url('/uploads/thumb/s_'.$product->image_filename, 163, 138) }}" class="">                                 
                <?php else:?>
                <img src="/images/nophoto.png" class="">                 
                <?php endif;?>
            </a>
            <div class="details">
                <span class="date">{{$product->created_at}}</span>
                <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" class="name">{{  mb_substr($product->name, 0, 40, 'UTF-8') }}</a>
                <strong class="price">{{ $product->price>999999999?"-":format_price($product->price) }}</strong>
            </div>
            
            <div class="pctrl text-right">
                @if($is_mine)
                <a href="/renew/{{ $product->id }}" class="btn-refresh"><i class="fa fa-refresh"></i></a>
                <a href="/e/{{ $product->id }}" class="btn-edit"><i class="fa fa-pencil"></i></a>
                <a href="/del/{{ $product->id }}" class="btn-remove" onclick="return confirm('Та итгэлтэй байна уу?');"><i class="fa fa-times"></i></a>
                @else
                <div style="padding-top:25px"></div>
                @endif
            </div>
            
        </div>
    @if($j%2==0 || $j == $countProducts)
    </div>
    @endif
    @endforeach
        
    @if(ceil($countProducts/$paginate) > $page)
    <div class="row">
        <div class="col-md-12 text-center">
            <button class="btn btn-more1" onclick="moreMyProducts({{$user_id}},{{$page+1}},$(this).parent().parent())"><i class="fa fa-refresh"></i>&nbsp;&nbsp;&nbsp;Үргэлжлүүлэх</button>
        </div>
    </div>
    @endif