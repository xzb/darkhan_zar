<?php use App\Models\Profile; ?>

@foreach($events as $e)
    @if($e->type == 1)
        <div class="eitem">
            <h4 class="event-title">Зар.мн <span>{{$e->title}}</span></h4>
            <span class="date">{{$e->created_at}}</span>
            <div class="row coupon-added">
                @if($e->image)
                <div class="col-md-6">
                    <img src="/files/uploads/events/{{$e->image}}" alt="">
                </div>
                @endif
                <div class="col-md-6">
                      {{$e->body}}
                    
                </div>
            </div>
        </div>
    @else($e->type == 2 )
    <?php $company = Profile::where('user_id','=',$e->company_id)->first(); ?>
        <div class="eitem">
            <h4 class="event-title">{{$company->company_name}} <span>дэлгүүр нэмэгдлээ.</span></h4>
            <span class="date">{{$company->created_at}}</span>
            <div class="row store-added">
                <div class="col-md-6">
                    <div class="eventc">
                        <div class="img">
                            <img src="/uploads/profile/{{$company->image}}" alt=""/>
                        </div>
                        <div class="text">
                            <a href="/profile/{{$company->user_id}}" class="link">
                                <span>{{$company->company_name}}</span>
                                <span>{{$company->about_us}}</span>
                                
                            </a>
                            <ul class="cbtn">
                                <li><a href="#" class="fa fa-envelope"></a></li>
                                <li><a href="#" class="fa fa-phone"></a></li>
                                <li><a href="#" class="fa fa-link"></a></li>
                                <li><a href="#" class="fa fa-map-marker"></a></li>
                            </ul>
                        </div>
                        <a href="/profile/{{$company->user_id}}" class="btn btn-more">Дэлгэрэнгүй &nbsp;<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                  
                </div>
            </div>
        </div>
    @endif                                        
@endforeach                                            
                                    
@if(ceil($eventsCount/$paginate) > $page)
    <div class="row">
        <div class="col-md-12 text-center">
            <button class="btn btn-more1" onclick="moreEvents({{$id}},{{$page+1}},$(this).parent().parent())"><i class="fa fa-refresh"></i>&nbsp;&nbsp;&nbsp;Үргэлжлүүлэх</button>
        </div>
    </div>
    @endif