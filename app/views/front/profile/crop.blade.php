@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('profile' => $profile, 'user' => $user, 'from'=>'index'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        <h4>Зураг тохируулах</h4>
        <div class="form-row">
        	
            <img id="profileImage" src="<?php if($profile && $profile->image):?>/uploads/profile/{{$profile->image}}<?php else:?> /images/profile.png <?php endif;?>">
            <p class="desc">Зураг дээр хулганаар хэмжээг тохируулна уу.</p>
            <br/>
            <form action="/profile/crop" method="post" onsubmit="return checkCoords();">
            	<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input type="submit" value="Хадгалах" class="btn btn-red" />
			</form>
        </div>
    </div>

</div>
@stop