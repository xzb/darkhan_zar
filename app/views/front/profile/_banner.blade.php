<?php
    use App\Models\CompanyBanner;
    use App\Models\CoverImageType;

?>
<?php if($profile->is_company):?>

    <div id="profileBanner" style="margin-bottom:{{$bottom}}; margin-left:{{$left}}" >
    
        @if($profile->cover_style == 1)

        <?php $coverType = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',1)->first(); ?>
        <?php $banner = CompanyBanner::getBannerInPosition($profile->user_id, $coverType->id); ?>
            @if($banner)
            <div>
                <img src="/uploads/company/{{ $banner->image }}" style="height: {{$coverType->height}}px;width: {{$coverType->width}}px;">
            </div>
            @elseif(Sentry::check())
            <div style="height: {{$coverType->height}}px;width: {{$coverType->width}}px; background-color: #c7c7c7">
                <div style="color:#737373; padding-top:{{intval($coverType->height/2)}}px" align="center">cover</div>
            </div>    
            @endif

            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-37', 'left' => '675','coverType'=>$coverType,'pos'=>1))
              @endif
            @endif

        @elseif($profile->cover_style == 2)

            <?php $coverType1 = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',1)->first(); ?>
            <?php $banner1 = CompanyBanner::getBannerInPosition($profile->user_id, $coverType1->id); ?>

            @if($banner1)
            <div style="float: left">
                <img src="/uploads/company/{{ $banner1->image }}" style="height: {{$coverType1->height}}px;width: {{$coverType1->width}}px;">
            
            @elseif(Sentry::check())
            <div style="float: left;">
                <div style="height: {{$coverType1->height}}px;width: {{$coverType1->width}}px; background-color: #c7c7c7; color:#737373; padding-top:{{intval($coverType1->height/2)}}px" align="center">cover</div>

            @endif

            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-33', 'left' => '464','coverType'=>$coverType1,'pos'=>2))
              @endif
            @endif
            
            </div>

            <?php $coverType2 = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',2)->first(); ?>
            <?php $banner2 = CompanyBanner::getBannerInPosition($profile->user_id, $coverType2->id); ?>
            <?php $top_s =(Sentry::check() && Sentry::getUser()->id == $user->id)?'-335':'0'; ?>
            @if($banner2)
            <div style="float:right;margin-top:{{$top_s}}px;">
                <img src="/uploads/company/{{ $banner2->image }}" style="height: {{$coverType2->height}}px;width: {{$coverType2->width}}px;">
            
            @elseif(Sentry::check())
            <div style="float: right;">
                <div style="margin-top:{{$top_s}}px; height: {{$coverType2->height}}px;width: {{$coverType2->width}}px; background-color: #c7c7c7; color:#737373; padding-top:{{intval($coverType2->height/2)}}px" align="center">cover</div>
            
            @endif

            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-32', 'left' => '168','coverType'=>$coverType2,'pos'=>3))
              @endif
            @endif
            </div>
        @elseif($profile->cover_style == 3)

            <?php $coverType1 = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',1)->first(); ?>
            <?php $banner1 = CompanyBanner::getBannerInPosition($profile->user_id, $coverType1->id); ?>

            @if($banner1)
            <div style="float: left">
                <img src="/uploads/company/{{ $banner1->image }}" style="height: {{$coverType1->height}}px;width: {{$coverType1->width}}px;">
            
            @elseif(Sentry::check())
                @if(Sentry::getUser()->id == $user->id)
                <div style="float: left;">
                    <div style=" height: {{$coverType1->height}}px;width: {{$coverType1->width}}px; background-color: #c7c7c7;color:#737373; padding-top:{{intval($coverType1->height/2)}}px" align="center">cover</div>
                @endif
            @endif
            
            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-37', 'left' => '385','coverType'=>$coverType1,'pos'=>4))
              @endif
            @endif
            
            </div>
            
            <?php $coverType2 = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',2)->first(); ?>
            <?php $banner2 = CompanyBanner::getBannerInPosition($profile->user_id, $coverType2->id); ?>
            <?php $top_s =(Sentry::check() && Sentry::getUser()->id == $user->id)?'-335':'0'; ?>
            <?php $right_s =(Sentry::check() && Sentry::getUser()->id == $user->id)?'-5':'0'; ?>
            @if($banner2)
            
            <div style="float:right;margin-top:{{$top_s}}px;margin-right:{{$right_s}}px">
                <img src="/uploads/company/{{ $banner2->image }}" style="height: {{$coverType2->height}}px;width: {{$coverType2->width}}px;">
            
            @elseif(Sentry::check())
            @if(Sentry::getUser()->id == $user->id)
            <div style="float: right;margin-top:{{$top_s}}px;">
                <div style="height: {{$coverType2->height}}px;width: {{$coverType2->width}}px; background-color: #c7c7c7; color:#737373; padding-top:{{intval($coverType2->height/2)}}px" align="center">cover</div>
            
            @endif
            @endif

            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-36', 'left' => '246','coverType'=>$coverType2,'pos'=>5))
              @endif
            @endif

            </div>

            <?php $coverType3 = CoverImageType::where('cover_id','=',$profile->cover_style)->where('position','=',3)->first(); ?>
            <?php $banner3 = CompanyBanner::getBannerInPosition($profile->user_id, $coverType3->id); ?>
            <?php $top_s =(Sentry::check() && Sentry::getUser()->id == $user->id)?'-185':'0'; ?>
            <?php $right_s =(Sentry::check() && Sentry::getUser()->id == $user->id)?'-5':'0'; ?>
            @if($banner3)
            <div style="float:right;margin-top:{{$top_s}}px;margin-right:{{$right_s}}px">
                <img src="/uploads/company/{{ $banner3->image }}" style="height: {{$coverType3->height}}px;width: {{$coverType3->width}}px;">
            
            @elseif(Sentry::check())
                @if(Sentry::getUser()->id == $user->id)
                <div style="float: right;margin-top:{{$top_s}}px;">
                    <div style="height: {{$coverType3->height}}px;width: {{$coverType3->width}}px; background-color: #c7c7c7; color:#737373; padding-top:{{intval($coverType3->height/2)}}px" align="center">cover</div>
                
                @endif
            @endif

            @if(Sentry::check())
              @if(Sentry::getUser()->id == $user->id)
                @include('front.profile._banner_button', array('top' => '-37', 'left' => '246','coverType'=>$coverType3,'pos'=>6))
              @endif
            @endif
            </div>

        @endif

    </div>


<?php else:?>

<?php if($profile && $profile->banner):?>
  <img src="/uploads/profile/{{$profile->banner}}" class="profile_banner">
<?php endif;?>

<?php endif;?>
