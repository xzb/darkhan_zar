@extends('front._layouts.product')

@section('main')

<div class="other-content-body container sitecontent  login-user-body" style="margin-top:25px; padding:20px" >
  <h4 class="product-list-title">Та Голомт банкны онлайн системээр төлөх сонголтыг сонгосон байна</h4>
<table width="98%" align="center" style="margin-bottom: 10px;">
<tbody>
<tr>
  <td>
<table cellpadding="3" width="100%">
          <tbody>
          <tr>
            <td align="right"></td>
            <td>
          <p>
          Ашиглаж болох картууд:
          </p><ul>
            <li> - Голомт банкны дотоод, гадаадын мастер болон виза карт</li>
            <li> - Капитрон банкны дотоод, гадаадын мастер карт</li>
            <li> - Хас банкны дотоод, гадаадын мастер карт</li>
            <li> - Зоос банкны дотоод, гадаадын мастер карт</li>
            <li> - Бусад банкнуудын олон улсын эрхтэй бүх төрлийн төлбөрийн картууд</li>
          </ul>
          
                                  Үргэлжлүүлэх гэсэн товчийг дарснаар та Голомт Банкны онлайн төлбөр тооцооны вэб сайт руу шилжих бөгөөд тэнд онлайн төлбөр төлөлт маш аюулгүй орчинд явагдах болно.
          <p>Харин та төлбөрөө аль болох хурдан хугацаанд хийх, төлбөр хийх явцдаа интернэт холболтоо салгахгүй байх зэргийг анхаарч үзнэ үү!</p>
          </td>
          </tr>
          </tbody></table>
  </td>
</tr>

<tr>
  <td>

    <div align="center">
<form name="pForm" action="https://m.egolomt.mn/billingnew/cardinfo.aspx" method="post" >
<!--   <form name="pForm" action="https://m.egolomt.mn/billing/cardinfo.aspx" method="post" > -->
    <input type="hidden" name="key_number" value="<?php echo $key_number ?>" >
    <input type="hidden" name="trans_number" value="<?php echo $order_code ?>" >
    <input type="hidden" name="trans_amount" value="<?php echo $amount ?>">
    <input type="hidden" name="lang" value="0">
    <input type="hidden" name="customer_id" value="">
    <input type="hidden" name="subID" value="1">
    
    <table cellpadding="10">
    <tr>
      <th></th>
      <td>
        <span class=""><a href="javascript:this.document.forms['pForm'].submit()" class="btn btn-red" title="Төлбөр хийх"><span>Төлбөр хийх </span></a></span>
        <div class="clear"></div>
      </td>
    </tr>
    </table>
</form>
      </div>
  </td>
</tr>
</tbody>
</table>
</div>

@stop