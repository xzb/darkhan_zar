@extends('front._layouts.product')

@section('main')

<div id="content" class="profile">
    @include('front.profile._left', array('products' => $products, 'profile' => $profile, 'user' => $user, 'from'=>'saw'))

    <div class="content-body hasRightBar">
        @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile))
        @if(Sentry::check() && Sentry::getUser()->id == $profile->user_id)
            <?php $top = '0px'; ?>
        @else    
            <?php $top = '6px'; ?>
        @endif
        <h4 style="margin-top:{{$top}}">Миний үзсэн зарууд ({{$state}})</h4>
          @include('front.product._list2', array('products' => $products,'padding_top'=>'6px'))
        
        <!-- pager -->
        <center>
        {{ $products->links(); }}
        </center>
    </div>

</div>
@stop