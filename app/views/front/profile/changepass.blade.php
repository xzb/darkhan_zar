
<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/category_nested.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.fancybox.js"></script>

<script>
$(document).ready(function() {
	
	$( "#changePass" ).click(function() {
		  	$.ajax({
              url: bm.prefix + '/profile/setPass',
              type: 'POST',
              dataType: 'json',
              data: $( "#changeForm" ).serialize(),
              success: function(data) {
                if(data == 'success'){
                  $('#categories').html('<span style="color:green;" id="s_error">Нууц үг амжилттай солилоо</span>');
                }else{
                  $('#s_error').show();
                }
                
              }
          	});
  });
	
});
</script>
<div id="dialogCategory" title="Нууц үг солих">

 <div class="form-group" id="categoryCombos" >
 	<form id="changeForm" class="" accept-charset="UTF-8" action="#" method="POST">

    <div id="categories" style="height:220px; width:150px">
      <span style="display:none; color:red;" id="s_error">Алдаа гарлаа</span>
      <label>Хуучин нууц үг</label>
      <input type="password" id="password" name="password">
      <label style="margin-top: 10px;">Шинэ нууц үг</label>
      <input type="password" id="new_password" name="new_password">
      <label style="margin-top: 10px;">Шинэ нууц үг давтах</label>
      <input type="password" id="comfirm_password" name="comfirm_password">
    </div>
    <div class="form-group"> 
		<div class="col-lg-offset-2 col-lg-10"> 
			<input id="changePass" type="button" value="Хадгалах" class="btn btn-info"> 
		</div> 
	</div>
	</form>
  </div>

</div>
