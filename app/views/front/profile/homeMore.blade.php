@extends('front._layouts.default')
@section('main')

<link rel="stylesheet" href="/css/profile1.css"/>
<script type="text/javascript" src="/js/profile.js"></script>
  
                <div class="profile-main">
                    <div class="row">
                        <div class="row-same-height">
                            <div class="col-md-12">
                                <div class="margin-30">
                                    <ul class="nav tab-menu nav-justified" role="tablist">
                                        <li id="tab1" class="active"><a href="#" onclick="homeMore({{$id}},1)">Миний зарууд</a></li>
                                        <li id="tab2"><a href="#" onclick="homeMore({{$id}},2)">Дагасан дэлгүүрүүд</a></li>
                                        <li id="tab3"><a href="#" onclick="homeMore({{$id}},3)">Мэдээлэл</a></li>
                                    </ul>

                                    <div class="row padding-20">
                                        <h1 class="profile-title">
                                            Миний зарууд  (8)
                                        </h1>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6 pitem">
                                            <a href="#" class="img pull-left">
                                                <img src="/images1/product/product(1).jpg" alt=""/>
                                            </a>
                                            <div class="details">
                                                <span class="date">2013.01.26    12:45</span>
                                                <a href="#" class="name">Hyundai Equus</a>
                                                <strong class="price">59,000 ₮</strong>
                                            </div>
                                            <div class="pctrl text-right">
                                                <a href="#" class="btn-refresh"><i class="fa fa-refresh"></i></a>
                                                <a href="#" class="btn-edit"><i class="fa fa-pencil"></i></a>
                                                <a href="#" class="btn-remove"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-md-6 pitem">
                                            <a href="#" class="img pull-left">
                                                <img src="/images1/product/product(2).jpg" alt=""/>
                                            </a>
                                            <div class="details">
                                                <span class="date">2013.01.26    12:45</span>
                                                <a href="#" class="name">Hyundai Equus</a>
                                                <strong class="price">59,000 ₮</strong>
                                            </div>
                                            <div class="pctrl text-right">
                                                <a href="#" class="btn-refresh"><i class="fa fa-refresh"></i></a>
                                                <a href="#" class="btn-edit"><i class="fa fa-pencil"></i></a>
                                                <a href="#" class="btn-remove"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div> <!--end row -->
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button class="btn btn-more1"><i class="fa fa-refresh"></i>&nbsp;&nbsp;&nbsp;Үргэлжлүүлэх</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end right side-->
                        </div>
                    </div>
                </div>
            </div>
        
            
        
@stop