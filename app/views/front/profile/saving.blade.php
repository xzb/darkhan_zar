@extends('front._layouts.product')

@section('main')

<div class="other-content-body container sitecontent  login-user-body" style="margin-top:25px; padding:20px" >
  <h4 class="product-list-title">Та Төрийн Банкны картаар төлөх сонголтыг сонгосон байна</h4>
<table width="98%" align="center" style="margin-bottom: 10px;">
<tbody>
<tr>
  <td>
<table cellpadding="3" width="100%">
          <tbody>
          <tr>
            <td align="right"></td>
            <td>
          <p>
          Ашиглаж болох картууд:
          </p><ul>
            <li> - Төрийн Банкны дотоодын VISA карт</li>
          </ul>
          Үргэлжлүүлэх гэсэн товчийг дарснаар та Төрийн Банкны онлайн төлбөр тооцооны вэб сайт руу шилжих бөгөөд тэнд онлайн төлбөр төлөлт маш аюулгүй орчинд явагдах болно.
          <p>Харин та төлбөрөө аль болох хурдан хугацаанд хийх, төлбөр хийх явцдаа интернэт холболтоо салгахгүй байх зэргийг анхаарч үзнэ үү!</p>

          </td>
          </tr>
          </tbody></table>
  </td>
</tr>

<tr>
  <td>

    <div align="center">
    <form id="pForm" name="pForm" action="/shopping/postsaving" method="post">
              <input type="hidden" name="orderId" value="<?php echo $order_code?>">
              <input type="hidden" name="amount" value="1<?php //echo $amount?>00">
                <table cellpadding="10">
                <tr>
                  <th></th>
                  <td>  
                      <span class="button">
                          <input type="submit" value="Төлбөр хийх" class="button big button-gray"/>
                      </span>
                  <div class="clear"></div>
                  </td>
                </tr>
                </table>
              </form>
      </div>
  </td>
</tr>
</tbody>
</table>
</div>

@stop