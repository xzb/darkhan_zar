<link rel="stylesheet" href="/css/company.css"/>
<?php use App\Models\User; ?>
@extends('front._layouts.product')

@section('main')
<div id="content" class="company-main">
    <div class="row">
        <div class="row-same-height">
          @include('front.profile._leftNew', array('profile' => $profile, 'user' => $user, 'from'=>'feedback'))
    
    <div class="col-md-9 col-md-cright col-md-height">
                                <div class="margin-30">
        <?php 
                                              
                    $bottom = '0px';
                    $left = '0px';
                    switch ($profile->cover_style) {
                            case 1:  $bottom = '-10px'; $left = '0px'; break;
                            case 2:  $bottom = '0px'; $left = '0px';  break;
                            case 3:  $bottom = '0px'; $left = '0px'; break;
                          } 
                           
                  ?>
        <!-- company cover -->
        @if(count($banners) > 0 || (Sentry::check() && Sentry::getUser()->id == $user->id))
            @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile,'bottom'=>$bottom, 'left'=>$left))
        @endif
        <?php $top_padding = '6px'; ?>
        @if(count($banners) > 0)
          <?php $top_padding = '26px'; ?>
        @endif
        @if(count($banners) > 0 && Sentry::check() && Sentry::getUser()->id == $user->id)
                <?php $top_padding = '6px'; ?>
        @endif
        
      <div class="row">
        <div class="col-md-12">
                <h4 style="margin-top:{{$top_padding}}">Санал хүсэлт</h4>
                  <div style="margin-bottom:40px;" class="commentm">
            
                  <div class="com-lists">
                  <ul id="maxcommentlist">
                      <?php foreach($feedbacks as $feed):?>
                      <li class="liclass" id="commentid{{$feed->id}}">
                          <input type="hidden" value="{{$feed->id}}" class="lastid" id="{{$feed->id}}">
                          <div class="comment">
                            <div class="box">
                              <h4>
                                <b>{{$feed->user_name}}</b>                 
                                <span>{{time_ago($feed->created_at)}}</span>
                                @if(Sentry::check())
                                @if(Sentry::getUser()->id == $user->id)
                                <a href="#" onclick="deleteFeedback({{$feed->id}}) ; return false;" style="float:right;" title="Устгах" class="">x</a>  
                                @endif
                                @endif
                              </h4>
                              
                              <p>{{{$feed->content}}}
                              </p>
                            </div>
                          </div>

                        </li>
                    <?php endforeach;?>
                    </ul>
                </div>
          <br clear="all">
            <div class="com-post">
              <form id="commentForm" action="/profile/savefeed" method="POST">
                <?php echo Form::token();?>
                <input type="hidden" value="{{$profile->user_id}}" class="objId" name="objId">
                <textarea style="height: 80px; width:90%" id="maxcommentbody" name="content" class="commentbox"></textarea>
                <input type="submit" value="Илгээх" class="btn btn-red">
              </form>
            </div>
  
  
    </div>
  
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
   


@stop