@extends('front._layouts.default')
@section('main')

<link rel="stylesheet" href="/css/profile1.css"/>
<script type="text/javascript" src="/js/profile.js"></script>
  
                <div class="profile-main">
                    <div class="row">
                        <div class="row-same-height">
                            <div class="col-md-12">
                                <div class="margin-30">
                                    <ul class="nav tab-menu nav-justified" role="tablist">
                                        <li id="tab1" class="active"><a href="/othersProduct/{{$id}}">{{$user->first_name}}</a></li>
                                        <li id="tab2"><a href="/followedCompanies/{{$id}}">Дагасан дэлгүүрүүд</a></li>
                                        <li id="tab3"><a href="/events/{{$id}}">Мэдээлэл</a></li>
                                    </ul>
                                    <div class="row padding-20">
                                        <div style = "padding: 10px; align:center; valign:top; display: inline-block;">
                                          <div style="width:150px;float:left">
                                            <img src="<?php if($user && $user->image):?>/uploads/profile/{{$user->image}}<?php else:?> /images/profile.png <?php endif;?>" width="150px">
                                          </div>  
                                          <div style="width:350px;padding-left:30px;float:left;font-size: 15px;color: #6E6767;">
                                            <ul style="list-style: none">
                                                <li style="padding-top:10px;border-bottom: 1px solid gray">{{$user->last_name}} овогтой {{$user->first_name}}  <a href="/chats/{{$user->id}}" style="padding-left:30px"><i class="fa fa-comments"></i>Чатлах</a></li>
                                                <li style="padding-top:10px;border-bottom: 1px solid gray">Албан тушаал:  {{$user->position}}</li>
                                                <li style="padding-top:10px;border-bottom: 1px solid gray">Имэйл хаяг: {{$user->email}}</li>
                                                <li style="padding-top:10px;border-bottom: 1px solid gray">Утас: {{$user->phone}}</li>
                                            </ul>    
                                          </div>
                                         
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="row padding-20">
                                        <h1 class="profile-title">
                                            Зарууд  ({{ $countProducts }})
                                        </h1>
                                    </div>
                                    <div id="myProducts" my_id="{{$id}}">
                                    </div>    
                                    
                                </div>
                            </div>
                            <!-- end right side-->
                        </div>
                    </div>
                </div>
            </div>
        
            
        
@stop