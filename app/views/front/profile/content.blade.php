<link rel="stylesheet" href="/css/company.css"/>
@extends('front._layouts.product')
@section('main')
<link rel="stylesheet" href="/js/fileManager/wysiwyg.fileManager.css" type="text/css"/>


    <div id="content" class="company-main">
                    <div class="row">
                        <div class="row-same-height">
                          

    @include('front.profile._leftNew', array('profile' => $profile, 'user' => $user, 'from'=>'content'.$detail->id))
                        <div class="col-md-9 col-md-cright col-md-height">
                                <div class="margin-30">
    	  <?php 
                                              
                    $bottom = '0px';
                    $left = '0px';
                    switch ($profile->cover_style) {
                            case 1:  $bottom = '-10px'; $left = '0px'; break;
                            case 2:  $bottom = '0px'; $left = '0px';  break;
                            case 3:  $bottom = '0px'; $left = '0px'; break;
                          } 
                           
                  ?>
            <?php 
                $style = (count($banners) > 0) ? "margin-top:20px" : "margin-top:0px"; 
            ?>            
        <!-- company cover -->
        @if(count($banners) > 0 || (Sentry::check() && Sentry::getUser()->id == $user->id))
            @include('front.profile._banner', array('banners' => $banners, 'profile' => $profile,'bottom'=>$bottom, 'left'=>$left))
            <?php 
                $style = "margin-top:0px"; 
            ?>
        @endif
        @if(count($banners) > 0 && Sentry::check() && Sentry::getUser()->id != $user->id)
                                        <?php 
                                            $style = "margin-top:26px";
                                        ?>    
                                    @endif
        
        <div class="row">
            <div class="col-md-12" style="{{$style}}">
                    @if(Sentry::check() && Sentry::getUser()->id == $profile->user_id)
                        
                        <h4><div class="edit" detail-id="{{$detail->id}}">{{$detail->title}}</div></h4><span style="float:right; margin-top:-30px;cursor:pointer" onclick="$('.edit').trigger('click');"><i class="fa fa-edit"></i> засах</span>
                        <div style="border-bottom: 1px solid; border-color:{{($profile->color_code =='')?'gray':$profile->color_code}}"></div>
                        <div class="editarea" detail-id="{{$detail->id}}" style="padding-top:5px">
                        	@if($detail->value == '')
                        		 Агуулга оруулна уу
                        	@else
                                {{$detail->value}}
                            @endif
                        </div>
                        <span style="float:right; margin-top:-20px;cursor:pointer" onclick="$('.editarea').trigger('click');"><i class="fa fa-edit"></i> засах</span>
                        
                    @else
                        <h4><div>{{$detail->title}}</div></h4>
                        <div style="border-bottom: 1px solid; border-color:{{($profile->color_code =='')?'gray':$profile->color_code}}"></div>
                        <div style="padding-top:5px">
                            @if($detail->value == '')
                                 Мэдээлэл байхгүй байна
                            @else
                                {{$detail->value}}
                            @endif
                        </div>
                        
                    @endif
            </div>
        </div><!-- end product list section-->
            </div>

            
        </div>
        <!-- end right side-->
    </div>
</div>
</div>

@stop