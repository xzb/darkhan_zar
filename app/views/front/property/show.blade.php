<?php use App\Models\Category; ?>
<?php use App\Models\Location; ?>
<?php use App\Models\sfUser; ?>
<?php use App\Models\AttributeValues; ?>
<?php use App\Models\ProductAttribute; ?>

@extends('front._layouts.property')

@section('main')
<?php $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

  <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Нүүр хуудас</a></li>
        @foreach($parents as $parentCategory)
            <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
        @endforeach
        <li class="active">{{$category->name}}</li>
    </ol>
    </div>
    <div class="container bdy_main_content">
      <div class="row auto_detail bottom_shadow">
          <div class="col-md-12">
            <?php if ($product->image_filename): ?>
              <div class="col-md-6 col-sm-6 col-xs-12 product_full_img">
    
                  <style type="text/css">
                    .photo-view {text-align:center;vertical-align:middle;background:#eee;width:100%;position:static;display:table-cell;float:none;width:520px;overflow:hidden;}
                    .photo-view img {max-width:100%;}
                  </style>

                  <div class="ad-photo-view" id="ad-photo-container">
                    <div id="photo-slider-container">
                      <div style="position:relative;">
                        <div class="btn-fullscreen"><a href="/images/{{ $product->id }}" class="fancybox"><i class="fa fa-arrows-alt"></i></a></div>  
                        <span href="#" id="slider-prev"></span>
                        <span href="#" id="slider-next"></span>

                        <ul id="photo-slider">
                          <?php foreach ($images as $idx => $image): ?>
                            <li>
                              <div class="photo-view"><img src="{{ $base_img_url.'/uploads/orig/'.$image->filename }}" {{ $idx == 0 ? 'itemprop="image"' : "" }} alt=""></div>
                            </li>
                          <?php endforeach; ?>
                        </ul>
                      </div>
                      <div class="photo-slider-thumbnail">
                        <div id="ad-photo-pager" class="ad-thumnails clearfix">
                          <?php foreach ($images as $idx => $image): ?>
                            <a href="#" data-slide-index="<?php echo $idx; ?>"><img src="{{ $base_img_url.'/uploads/thumb/'.$image->filename }}" alt=""></a>
                          <?php endforeach; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                
                </div>
                <?php endif; ?>
                
                <div class="col-md-<?php if ($product->image_filename): ?>6<?php else:?>12<?php endif;?> col-sm-6 col-xs-12 product_det">
                  <div class="heading_div">
                      <div class="heading_div_left">
                        <h3><?php echo $product->name; ?></h3>
                          <small><?php echo date("Y-m-d", strtotime($product->created_at)) ?></small>
                        </div>
                        <div class="heading_div_right" onclick="autoToggle({{ $product->id }}, this); return false;" style="cursor: pointer;"> 
                          <span class="round {{ sfUser::hasAuto($product->id) ? 'activeround' : ''; }}"></span>
                            <b>Дугуйлах</b>
                        </div>
                    </div>
                        <div class="price_div">
                          <small>Утас: <span><?php echo $product->contact ?></span></small>
                            <small>Үнэ: <span class="coloured"><?php echo $product->price.' ₮'; ?></span></small>
                        </div>
                        <div class="sh_pr">
                          <a href="#" onclick="window.print()" title="Хэвлэх" class="soc"><i class="fa fa-print"></i></a>
                            <a href="http://www.biznetwork.mn/share?u=<?php echo urlencode($url); ?>" class="soc" title="Biznetwork-ийн танилууддаа түгээх"><i class="fa fa-user"></i></a>
                            <a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($url); ?>" class="soc" title="Facebook найзууддаа түгээх"><i class="fa fa-facebook"></i></a>
                            <a href="http://twitter.com/home/?status=<?php echo urlencode($product->name) ?>:%20<?php echo urlencode($url); ?>" title="Twitter дагагч нартаа түгээх" class="soc"><i class="fa fa-twitter"></i></a>
                            <div style="margin-left:10px; margin-left: 100px; margin-top: -20px;">
                            <div class="fb-like fb_iframe_widget" data-href="<?php echo $url ?>" data-send="true" data-layout="button_count" data-width="100" data-show-faces="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;href=<?php echo urlencode($url); ?>&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=true&amp;show_faces=false&amp;width=100">
                              <span style="vertical-align: bottom; width: 124px; height: 20px;">
                                <iframe name="f18f1d2f58" width="100px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like Facebook Social Plugin" src="http://www.facebook.com/plugins/like.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FV80PAcvrynR.js%3Fversion%3D41%23cb%3Df15a77857%26domain%3D<?php echo $_SERVER['SERVER_NAME'] ?>%26origin%3Dhttp%253A%252F%252F<?php echo $_SERVER['SERVER_NAME'] ?>%252Ff28ba522d%26relation%3Dparent.parent&amp;href=<?php echo urlencode($url); ?>&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=true&amp;show_faces=false&amp;width=100" style="border: none; visibility: visible; width: 124px; height: 20px;" class=""></iframe>
                              </span>
                            </div>
                            <li style="margin-left:10px;">
                              <div class="fb-send" data-href="<?php echo $url ?>"></div>
                              <div id="fb-root"></div>
                              <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=1489911667927619";
                                fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));</script>
                              </li>
                            </div>
                            <p>{{ nl2br($product->description) }}</p>
                            <?php if($product->location_id):?>
                            <?php 
                                  $parentLocation = null;
                                  $location = Location::find($product->location_id);
                                  if($location->parent_id !=0)
                                  {
                                    $parentLocation = Location::find($location->parent_id);
                                  }
                            ?>
                            <p><b>Байршил : </b>@if($parentLocation){{$parentLocation->name}}, @endif {{$location->name}}</p>
                            <?php endif;?>
                            <span><b>Зарын дугаар:</b> {{ intval($product->id) }}</span>
                        </div>
                        <?php if($user && $profile && false):?>
                        <div class="addr_panel">
                            <div class="addr_panel_top">
                                <div class="addr_panel_top_left">
                                    <?php if($profile->is_company == 1):?>
                                      <?php if($profile->image):?>
                                        <img alt="" src="/uploads/profile/{{$profile->image}}" style="width: 40px; height: 40px;">
                                      <?php endif;?>
                                    <h3>{{$profile->company_name;}}</h3>
                                    <?php else:?>
                                    <h3>{{ucfirst($user->last_name)}} {{ucfirst($user->first_name)}}</h3>
                                    <?php endif;?>
                                  </div>
                                  <div class="addr_panel_top_right">
                                    <!-- <a class="fb" href="#"><i class="fa fa-facebook"></i></a>
                                      <a class="twit" href="#"><i class="fa fa-twitter"></i></a> -->
                                  </div>
                              </div>
                                  <div class="addr_panel_top_middle">
                                    <ul>
                                        @if($profile && $profile->phone)<li><i class="fa fa-phone"></i>{{$profile->phone}}</li>@endif
                                        @if($profile && $profile->email)<li><i class="fa fa-envelope"></i>{{$profile->email}}</li>@endif
                                        @if($profile && $profile->address)<li><i class="fa fa-map-marker"></i>{{$profile->address}}</li>@endif
                                      </ul>
                                  </div>
                                  <div class="addr_panel_top_bottom">
                                    <a href="/profile/{{$user->id}}"><i class="fa fa-user"></i>Профайл</a>
                                    <a href="/profile/feedback/{{$user->id}}"><i class="fa fa-comments"></i>Санал хүсэлт</a>
                                  </div>
                              </div>
                          <?php endif;?>
                        </div>
                        
                    </div>
                    <div class="col-md-12 h_r"><hr /></div>
                    <div class="col-md-12 tab_le">
                      <h2>Үзүүлэлт</h2>
                        <div class="row list_ing">
                          <?php $attrCount = count($attributes);?>
                          @foreach($attributes as $key=>$attribute)
                          <?php $attributeValue = ProductAttribute::getProductAttrValues($product->id, $attribute->id, $attribute->type);?>
                          <?php if($key%3 == 0):?>
                          <div class="col-md-12">
                          <?php endif;?>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <h4>{{$attribute->name}}</h4>
                                    <span><i class="fa fa-tag"></i>{{$attributeValue}}</span>
                                </div>
                            <?php if($key%3 == 2 || $attrCount == $key+1):?>
                            </div>
                            <?php endif;?>
                            @endforeach
                        </div>
                    </div>
                    <?php if($product->latlng):?>
                    <div class="col-md-12 h_r"><hr /></div>
                    <div class="col-md-12 tab_le">
                      <h2>Газрын зураг дээр</h2>
                        <div class="row list_ing">
                          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
                          <script>

                          var ub = new google.maps.LatLng({{$product->latlng}});
                          var marker;
                          var map;

                          function initialize() {
                            var mapOptions = {
                              zoom: 14,
                              center: ub
                            };

                            map = new google.maps.Map(document.getElementById('map-canvas'),
                                    mapOptions);

                            marker = new google.maps.Marker({
                              map:map,
                              draggable:false,
                              animation: google.maps.Animation.DROP,
                              position: ub
                            });
                            
                          }

                          google.maps.event.addDomListener(window, 'load', initialize);

                            

                          </script>
                          <div id="map-canvas" style="width:800px; height:400px"></div>
                        </div>
                    </div>
                    <?php endif;?>

                    <div style="text-align: right; padding-right:12px" class="form-row">
                      <?php if (!sfUser::hasPid($product->id)): ?>
                      <input id="product_id" type="hidden" name="pid" value="<?php echo $product->id ?>" />
                      <input id="product_password" type="text" autocomplete="off" name="password" placeholder="Нууц код" style="width: 80px;" />
                      <a href="#" id="product_edit" onclick="pochdt({{ $product->id }}); return false;" class="btn btn-red">Засаx</a>&nbsp
                      <a href="#ad-photo-pager" id="product_edit" onclick="chds({{ $product->id }}); return false;" class="btn btn-red">Онцлох болгох</a>&nbsp
                      <a href="#" id="product_delete" onclick="chdl({{ $product->id }}); return false;" class="btn btn-red">Устгаx</a>
                      <?php else: ?>
                      <a href="/propertyEdit/<?php echo $product->id ?>" id="product_edit" class="btn btn-red">Засаx</a>&nbsp
                      <a href="/addBasket/<?php echo $product->id ?>" id="product_edit" class="btn btn-red">Онцлох болгох</a>&nbsp
                      <a href="/del/<?php echo $product->id ?>?s=1" id="product_delete" class="btn btn-red">Устгаx</a>
                      <?php endif; ?>
                      <div id="dialog" title="Анхааруулга!" style="display: none;">
                        <p>Та нууц үгээ оруулах эсвэл нэвтэрч орж байж онцлох болгоно уу, дэлгэрэнгүй <a href="http://www.dazar.mn/page/featuredAd" target="_blank">энд</a> дарж уншина уу</p>
                      </div>
                    </div>
                </div>
        <?php $co = count($other);?>
        @if($co>0)
        <div class="row auto_detail bottom_shadow">
          <div class="col-md-12">
              <div class="row sl_1">
                  <h2>Төстэй зарууд</h2>
                    
                </div>
              <div id="amazingcarousel-container-2">
    <div id="amazingcarousel-2" style="display:block;position:relative;width:100%;max-width:825px;margin:0px auto 0px;">
        <div class="amazingcarousel-list-container" style="overflow:hidden;">
            <ul class="amazingcarousel-list">
                @foreach($other as $oProduct)
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
                      <a href="/p/{{$oProduct->id}}/{{ str_replace('/', '', $oProduct->name) }}">
                      @if($oProduct->image_filename)
                      <div class="amazingcarousel-image"><img src="{{ $base_img_url.'/uploads/thumb/s_'.$oProduct->image_filename }}" alt="" ></div>
                      @else
                      <div class="amazingcarousel-image"><img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt=""></div>
                      @endif

                      <div class="amazingcarousel-title"><p>{{$oProduct->name}}</p><b>{{$oProduct->price.' ₮'}}</b></div>                    
                      </a>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="amazingcarousel-prev"></div>
        <div class="amazingcarousel-next"></div>
        <div class="amazingcarousel-nav"></div>
    </div>
</div>
            </div>
        </div>
        @endif
    </div>
    </div>
    </div>

@include('front.product.nextprev', array('product' => $product))
@stop