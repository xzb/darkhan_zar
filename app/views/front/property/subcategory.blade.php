
<div class="email-subscribe">
  <form id="subscribe-form" action="/subscribe/popupCategory/{{$category->id}}?rand={{md5(uniqid(rand(), true))}}" method="post">
    <div class="form-flds-col">
      <div class="form-flds-group clearfix">
        <div class="form-row">
          <div class="form-col">
            <label>Ангилал: </label> <?php echo $category->name; ?>
          </div>
        </div>
      </div>
      <div class="form-flds-group clearfix">
        <div class="form-row inline">
          <p style="color:#666;">Имэйл хүлээн авах хугацаа</p>
          <div class="form-cols-2 clearfix">
            <div class="form-col">
              <input name="is_daily" type="radio" value="1" id="subscribe_is_daily_1" checked="checked" /><label for="subscribe_is_daily_1">Өдөр бүр</label>
            </div>
            <div>
              <input name="is_daily" type="radio" value="0" id="subscribe_is_daily_0" /><label for="subscribe_is_daily_0">Орсон даруйд нь</label>
            </div>
          </div>
        </div>
      </div>

      <div class="form-flds-group clearfix">
        <div class="form-row">
          <?php //echo $form['email']->render(array('class' => ($form['email']->hasError() ? 'input-error input-email' : 'input-email'), 'placeholder' => 'Имэйл хаягаа оруулна уу!')) ?>
          <input type="text" id="subscribe_email" placeholder="Имэйл хаягаа оруулна уу!" value="" name="email" autocomplete="off" class="{{$class}}">
        </div>
      </div>

      <div class="form-flds-group clearfix">
        <div class="form-row">
          <button class="btn btn-red" id="subscribe-form-button">Бүртгүүл</button>
        </div>
      </div>
    </div>
  </form>
</div>