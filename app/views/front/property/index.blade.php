<?php 
    use App\Models\Category;
	use App\Models\Mycache;
?>
@extends('front._layouts.property')

@section('main')
	
	<div class="container">
		<div class="BannerArea">
			@if(Active::route('front.autohomepage'))
                {{ Mycache::autohomebanner() }}
            @endif
			@include('front._partials.auto_quicksearch',array('attr2'=>$attr2))
		</div>
	</div>
    <div class="container bdy_main_content">
    	<div class="row">
        	<div class="col-md-8 col-sm-8 col-xs-12 bottom_shadow uramshulal">
            	<h2>Хамтрагчид</h2>
                <div class="logo_slider">
                	<div id="amazingcarousel-container-1">
    					<div id="amazingcarousel-1" style="display:block;position:relative;width:100%;max-width:680px;margin:0px auto 0px;">
        					<div class="amazingcarousel-list-container" style="overflow:hidden;">
            					<ul class="amazingcarousel-list">
                                    <?php $i = 0; ?>
                                    @foreach($coworkers as $banner)
                                    <?php $i++; $style = ''; ?>
                                    @if($i%4 == 1)
                					<li class="amazingcarousel-item">
                                        <div class="amazingcarousel-item-container">
                                    @endif
                    					    @if($i%4 == 1)
                                                <?php $style = "border-right: 1px solid #dcdcdc; border-bottom: solid 1px #DCDCDC;"; ?>
                                            @elseif($i%4 == 2)    
                                                <?php $style = "border-bottom: solid 1px #DCDCDC;"; ?>
                                            @elseif($i%4 == 3)
                                                <?php $style = "border-right: 1px solid #dcdcdc;"; ?>
                                            @endif
                                        	<div class="amaz_holder" style="{{$style}}">
												<div class="amazingcarousel-image">
                                            		<div class="amaz_img"><a href="{{ $banner->link }}" target="_blank"><img src="/files/uploads/banner/{{ $banner->path }}"  alt="{{$banner->name}}" /></a></div>
                                            	</div>
												<div class="amazingcarousel-title">{{$banner->name}}</div>
                                            </div>
                                            
                                        
                                    @if($i%4 == 0) 
                                        </div>
                					</li>
                                    @endif
                                    @endforeach
                                </ul>
        					</div>
        					<div class="amazingcarousel-nav"></div>
    					</div>
					</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-8 col-xs-12 bottom_shadow">
            	<h2>Бусад ангилал</h2>
                <div class="tag_content">
                	<a href="/c/23"><span class="icon_moto">&nbsp;</span>Мотоцикл</a>
                    <a href="/c/25"><span class="icon_parts">&nbsp;</span>Сэлбэг</a>
                    <a href="/c/22"><span class="icon_truck">&nbsp;</span>Ачааны машин</a>
                    <a href="/c/218"><span class="icon_bus">&nbsp;</span>Автобус/Микро</a>
                    <a href="/c/24"><span class="icon_machine">&nbsp;</span>Механизм</a>
                    <a href="/c/2151"><span class="icon_solino">&nbsp;</span>Солино</a>
                    <a href="/c/2156"><span class="icon_tureeslene">&nbsp;</span>Түрээслэнэ</a>
                    <a href="/c/2155"><span class="icon_tureesluulne">&nbsp;</span>Түрээслүүлнэ</a>
                    <a href="/c/2152"><span class="icon_car">&nbsp;</span>Авна</a>
                    <a href="/c/2333"><span class="icon_tire">&nbsp;</span>Дугуй</a>
                </div>
            </div>
        </div>
    </div>
	
@stop