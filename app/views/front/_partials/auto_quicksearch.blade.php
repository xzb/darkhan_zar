<div class="BannerRight col-sm-3 col-xs-12 bottom_shadow">
            	<h2>Суудлын тэрэг, Жийп</h2>
            {{ Form::open(array('method' => 'post', 'files' => true, 'route' =>'front.autohome.search', 'class' => 'form-horizontal','role'=>'form')) }}
            <input type="hidden" name="category_id" id="category_id" value=""/>
            <input type="hidden" name="attr_1" id="attr_1" value=""/>
            <div class="right_panel_tab">
                	<ul id="myTab" class="nav nav-tabs">
   						<li class="active"><a href="#home" data-toggle="tab" onclick="$('#attr_1').val(7)">Шинэ</a></li>
   						<li class=""><a href="#ios" data-toggle="tab" onclick="$('#attr_1').val(6)">Орж ирсэн</a></li>
   						<li><a href="#Сараар" data-toggle="tab" onclick="$('#attr_1').val(5)">Монголд явсан</a></li>
                	</ul>
                	<div id="myTabContent" class="tab-content">
       						<div class="tab-pane active" id="home">
                  		<div class="right_select_box">
                    	<select name="type_category" id="type_category" class="form-control-home" onchange="autoFactoryLoad(this.value,'factory_category',1)">
                        <option value="0">Төрөл</option>
                        <option value="21">Суудлын</option>
                          <option value="151">Жийп</option>
                      </select>
                      </div>
                                <div class="right_select_box">
                                	<select class="form-control-home" name="factory_category" id="factory_category" onchange="autoMarkLoad(this.value,'mark_category',1)">
                                      <option value="0">Үйлдвэрлэгч</option>
                                    </select>
                                </div>
                                <div class="right_select_box">
                                	<select class="form-control-home"  name="mark_category" id="mark_category" onchange="setMarkCategory(this.value)">
                                      <option value="0">Марк</option>
                                    </select>
                                </div>
                                <div class="right_select_box">
                                	<select class="form-control-home"  name="attr_2" id="attr_2">
                                      <option value="0">Хөдөлгүүр төрөл</option>
                                      @foreach($attr2 as $a)
                                        <option value="{{$a->id}}">{{$a->value}}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <input type="submit" value="Хайх" />
                	</div>
                    	
                </div>
            </div>
            </form>