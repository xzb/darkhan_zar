<?php use App\Models\Page; ?>
<?php $pages = Page::where('page_id','=', 0)->where('is_footer', '=', 1)->whereIn('type',array(0,2))->get(); ?>

</section>
<section class="footer">
  <div class="container">
      <div class="row">

             @foreach($pages as $page)
                <?php $pages = Page::where('page_id','=', $page->id)->whereIn('type',array(0,2))->get(); ?>
                <div class="col-md-2 col-sm-2 col-xs-2">
                  <h3>{{ $page->title }}</h3>
                  <ul>
                    @foreach($pages as $page)
                    <li><a href="/page/{{ $page->slug }}">{{ $page->title }}</a></li>
                    @endforeach
                  </ul>
                </div>
                @endforeach
            <div class="col-md-2 col-sm-2 col-xs-2">
              <h3>Холбоо барих</h3>
              <ul>
                <li> СБ Дүүрэг 8-р хороо  Ерөнхий сайд Амарын гудамж. Адмон компаний байр 2 давхар 204 тоот</li>
                <li>Утас: 70116300</li>
              </ul>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="social_holder">
                  
                    <a href="https://www.facebook.com/pages/%D0%97%D0%B0%D1%80%D0%BC%D0%BD/1454482421493571"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/zar_mn"><i class="fa fa-twitter"></i></a>
                  
                </div>
                <div class="store_holder">
                  <a href="https://play.google.com/store/apps/details?id=mn.singleton.enkuso.zar.app"><img src="/auto_images/google_play.jpg" alt="Google Play" /></a>
                    <a href="https://itunes.apple.com/mn/app/dazar.mn/id913155026?mt=8"><img src="/auto_images/app_store.jpg" alt="App Store" /></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer_bottom">
  <div class="container">
       &copy; 2002-2015 ЗарЭмЭн ХХК
    </div>
</section>

<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
</body>

</html>