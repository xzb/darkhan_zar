<?php use App\Models\AttributeValues;?>
<?php if(count($attributes)>0):?>

<div class="row">

    <?php foreach($attributes as $a):?>
    <?php 
    
                      $checkArray = array();

                      $textValues = '';

                      if($product)
                      {
                        if(array_key_exists($a->id, $arrayPA))
                        {
                          if(array_key_exists('id',$arrayPA[$a->id]))
                          {
                            if(array_key_exists($arrayPA[$a->id]['id'],$arrayPAV))
                            {
                              $checkArray = $arrayPAV;      
                            } 
                          }

                          if(array_key_exists('value',$arrayPA[$a->id]))
                          {
                              $textValues = $arrayPA[$a->id]['value']; 
                          }
                        }
                        
                      }  
                      
                ?>
    <?php 
        $avalues = AttributeValues::where('attribute_id','=',$a->id)->orderBy('sort_order')->get();
    ?>
       <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
           <label class="control-label"><?php echo $a->name?></label>
           <?php if($a->type == 'checkbox'):?>
             <div class="radio_box">
                  <?php foreach($avalues as $v): ?>
                      <span><input id="checkbox<?php echo $a->id;?>" type="checkbox" name="checkbox<?php echo $a->id;?>[]" value="<?php echo $v->id;?>" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'checked="checked"'; } ?>><label for="checkbox<?php echo $v->id;?>"><?php echo $v->value; ?></label></span>
                  <?php endforeach; ?>
              </div>
             <?php endif?>
              
              <?php if($a->type == 'selectbox'):?>
              <select class="form-control" id="selectbox<?php echo $a->id;?>" name="selectbox<?php echo $a->id;?>">
                  <?php foreach($avalues as $v): ?>
                      <option value="<?php echo $v->id;?>" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'selected="selected"'; }?>><?php echo $v->value;?></option>
                  <?php endforeach; ?>
              </select>
              <?php endif?>

              <?php if($a->type == 'textbox'):?>
                <input type ="text" name="textbox<?php echo $a->id;?>" id="textbox<?php echo $a->id;?>" class="form-control" value="<?php echo $textValues;?>" data-placement="bottom"/>
              <?php endif?>

              <?php if($a->type == 'textarea'):?>
                <textarea name="textarea<?php echo $a->id;?>" id="textarea<?php echo $a->id;?>" class="form-control"><?php echo $textValues?></textarea>
              <?php endif?>

        </div>
        
     <?php endforeach;?>
      
</div>  
<?php endif;?>