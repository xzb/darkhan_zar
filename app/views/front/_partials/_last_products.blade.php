<!-- =-=-=-=-=-=-= Featured Listing =-=-=-=-=-=-= -->
<section class="custom-padding">
   <!-- Main Container -->
   <div class="container">
      <!-- Row -->
      <div class="row">
         <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                     <img class="img-responsive" alt="" src="/images/product/home_list_313x234_1.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Автомашин > Зарна</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">2017 оны "0" гүйлттэй приус </a></h3>
                     <!-- Price -->
                     <div class="price">27.000.000₮ <span class="negotiable">(үнэ тохиролцоно)</span></div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li><i class="fa fa-map-marker"></i>Дархан</li>
                        <li><i class="fa fa-clock-o"></i> 28 минутын өмнө</li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                  <img class="img-responsive" alt="" src="/images/product/home_list_313x234_2.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Компьютер & Лаптоп</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">MacBook худалдаж авна</a></h3>
                     <!-- Price -->
                     <div class="price">1.500.000₮</div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li></li>
                        <li><i class="fa fa-clock-o"></i> 20 минутын өмнө </li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                     <img class="img-responsive" alt="" src="/images/product/home_list_313x234_3.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Үл хөдлөх</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">16д байр зарна</a></h3>
                     <!-- Price -->
                     <div class="price">95.500.000₮</div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li><i class="fa fa-map-marker"></i>Дархан</li>
                        <li><i class="fa fa-clock-o"></i> 45 минутын өмнө </li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                  <img class="img-responsive" alt="" src="/images/product/home_list_313x234_4.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Үл хөдлөх > Хашаа байшин</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">Хашаа байшин зарна</a></h3>
                     <!-- Price -->
                     <div class="price">3.500.000₮ <span class="negotiable">(үнэ тохиролцоно)</span></div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li><i class="fa fa-map-marker"></i>Дархан</li>
                        <li><i class="fa fa-clock-o"></i> 1 цагийн өмнө </li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                     <img class="img-responsive" alt="" src="/images/product/home_list_313x234_5.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Сургалт</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">Англи хэлний сургалт</a></h3>
                     <!-- Price -->
                     <div class="price">350.000₮</div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li><i class="fa fa-map-marker"></i>Дархан</li>
                        <li><i class="fa fa-clock-o"></i> 1 цаг 15 минутын өмнө </li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6">
               <!-- Ad Box -->
               <div class="category-grid-box">
                  <!-- Ad Img -->
                  <div class="category-grid-img">
                     <img class="img-responsive" alt="" src="/images/product/home_list_313x234_6.jpg">
                     <!-- View Details --><a href="" class="view-details">Дэлгэрэнгүй</a>
                  </div>
                  <!-- Ad Img End -->
                  <div class="short-description">
                     <!-- Ad Category -->
                     <div class="category-title"> <span><a href="#">Гар утас</a></span> </div>
                     <!-- Ad Title -->
                     <h3><a title="" href="single-page-listing.html">iPhone 6s зарна</a></h3>
                     <!-- Price -->
                     <div class="price">850.000₮</div>
                  </div>
                  <!-- Addition Info -->
                  <div class="ad-info">
                     <ul>
                        <li><i class="fa fa-map-marker"></i>Дархан</li>
                        <li><i class="fa fa-clock-o"></i> 1 цаг 30 минутын өмнө </li>
                     </ul>
                  </div>
               </div>
               <!-- Ad Box End -->
            </div>
         </div>
         <!-- Middle Content Box End -->
      </div>
      <!-- Row End -->
   </div>
   <!-- Main Container End -->
</section>
<!-- =-=-=-=-=-=-= Featured Listing End =-=-=-=-=-=-= -->