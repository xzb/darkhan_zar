<!-- =-=-=-=-=-=-= App Download Section  =-=-=-=-=-=-= --> 
<div class="app-download-section parallex">
   <!-- app-download-section-wrapper -->
   <div class="app-download-section-wrapper">
      <!-- app-download-section-container -->
      <div class="app-download-section-container">
         <!-- container -->
         <div class="container">
            <!-- row -->
            <div class="row">
               <!-- col-md-4 -->
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <!-- Windows Store -->
                  <a href="#" title="Windows Store" class="btn app-download-button"> <span class="app-store-btn">
                  <i class="fa fa-windows"></i>
                  <span>
                  <span>Download From</span> <span>Windows Store </span> </span>
                  </span>
                  </a>
                  <!-- /Windows Store -->
               </div>
               <!-- /col-md-4 -->
               <!-- col-md-4 -->
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <!-- Google Store -->
                  <a href="#" title="Google Store" class="btn app-download-button"> <span class="app-store-btn">
                  <i class="fa fa-android"></i>
                  <span>
                  <span>Download From</span> <span>Google Store </span> </span>
                  </span>
                  </a>
                  <!-- /Google Store -->
               </div>
               <!-- /col-md-4 -->
               <!-- col-md-4 -->
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <!-- Apple Store -->
                  <a href="#" title="Windows Store" class="btn app-download-button"> <span class="app-store-btn">
                  <i class="fa fa-apple"></i>
                  <span>
                  <span>Download From</span> <span>Apple Store </span> </span>
                  </span>
                  </a>
                  <!-- /Apple Store -->
               </div>
               <!-- /col-md-4 -->
            </div>
            <!-- /row -->
         </div>
         <!-- /container -->
      </div>
      <!-- /app-download-section-container -->
   </div>
   <!-- /download-section-wrapper -->
</div>