<?php use App\Models\Chats; ?>
<?php use App\Models\User; ?>
<?php use App\Models\Events; ?>
<?php use App\Models\Product; ?>

<?php $body = ''; ?>
@if(count($notification) > 0)
@foreach($notification as $not)
<?php
  if($not->object_type == 1)
  {
    $chat_profile = User::join('profile', function($join) {
        $join->on('users.id', '=', 'profile.user_id');
    })->select('users.id','users.last_login','profile.image', 'users.first_name')->where('users.id','=',$not->from_id)->first();
    $imagewithpath =($chat_profile->image != '')?'/uploads/profile/'.$chat_profile->image:'/images/profile.png'; 
    $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                  <a class="notify_taga" href="/chats/'.$chat_profile->id.'">
                      <img src="'.$imagewithpath.'" alt="" width="42px" />
                      <p><strong>'.$chat_profile->first_name.'</strong><span class="date">'.$not->created_at.'</span></p>
                      <p>Танд шинэ зурвас ирсэн байна. <span class="paragraph-end"></span></p>
                  </a>
              </li>';
  }
  else if($not->object_type == 2)
  {
    $company_profile = User::join('profile', function($join) {
        $join->on('users.id', '=', 'profile.user_id');
    })->select('users.id','users.last_login','profile.image', 'users.first_name','profile.company_name')->where('users.id','=',$not->from_id)->first();
    $imagewithpath =($company_profile->image != '')?'/uploads/profile/'.$company_profile->image:'/images/profile.png'; 
    $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                  <a class="notify_taga" href="/p/'.$not->object_id.'">
                      <img src="'.$imagewithpath.'" alt="" width="42px" />
                      <p><strong>'.$company_profile->company_name.'</strong><span class="date">'.$not->created_at.'</span></p>
                      <p>Таны дагсан компани шинэ бараа оруулсан байна.<span class="paragraph-end"></span></p>
                  </a>
              </li>';
  }
  else if($not->object_type == 3)
  {
    if($not->from_id == 2)
    {
      $company_profile = User::join('profile', function($join) {
        $join->on('users.id', '=', 'profile.user_id');
      })->select('users.id','users.last_login','profile.image', 'users.first_name','profile.company_name')->where('users.id','=',$not->object_id)->first();

      $imagewithpath =($company_profile->image != '')?'/uploads/profile/'.$company_profile->image:'/images/profile.png'; 
      $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                    <a class="notify_taga" href="/profile/'.$not->object_id.'">
                        <img src="'.$imagewithpath.'" alt="" width="42px" />
                        <p><strong>'.$company_profile->company_name.'</strong><span class="date">'.$not->created_at.'</span></p>
                        <p>Шинэ компани нэмэгдлээ.<span class="paragraph-end"></span></p>
                    </a>
                </li>';

    }
    else if($not->from_id == 1)
    {
          $imagewithpath = '/images/zar_events.png'; 
          $event = Events::where('id','=',$not->object_id)->first();
          if($event->image != '')
          {
              $imagewithpath = '/files/uploads/events/'.$event->image;
          }

          $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                        <a class="notify_taga" href="/events/'.Sentry::getUser()->id.'">
                            <img src="'.$imagewithpath.'" alt="" width="42px" />
                            <p><strong>'.$event->title.'</strong><span class="date">'.$not->created_at.'</span></p>
                            <p>'.$event->body.'<span class="paragraph-end"></span></p>
                        </a>
                    </li>';
    }           
  
  }else if($not->object_type == 4)
  {
      $product = Product::find($not->object_id);
      $imagewithpath =($product->image_filename)?'/uploads/thumb/s_'.$product->image_filename:'/uploads/nophoto.png';
      
      $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                    <a class="notify_taga" href="/p/'.$not->object_id.'">
                        <img src="'.$imagewithpath.'" alt="" width="42px" height="42px"/>
                        <p><strong>'.$product->name.'</strong><span class="date">'.$not->created_at.'</span></p>
                        <p>Бүтээгдэхүүнд сэтгэгдэл бичсэн байна.<span class="paragraph-end"></span></p>
                    </a>
                </li>';

  }else if($not->object_type == 5)
  {
    $body .= '<li class="notify_detail" not_id="'.$not->id.'">
                  <a class="notify_taga" href="/profile/feedback/'.$not->object_id.'">
                      <p><strong>Шинэ санал хүсэлт</strong><span class="date">'.$not->created_at.'</span></p>
                      <p>Таны бизнес профайлд санал хүсэлт нэмэгдсэн байна. </p>
                  </a>
              </li>';

  }
    
?>
  
@endforeach
<?php echo $body; ?>
@else
<li id="blank_notification">
     Шинэ мэдээлэл байхгүй байна.
</li>
@endif