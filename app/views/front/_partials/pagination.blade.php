
<?php $presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);?>

@if ($paginator->getLastPage() > 1)
<ul class="pagination">
	<li><a href="{{ $paginator->getUrl(1) }}" ><i class="fa fa-chevron-left"></i></a></li>
	@for ($i = 1; $i <= $paginator->getLastPage(); $i++)
		@if($paginator->getCurrentPage() === $i)
		<li class="active"><a href="#">{{ $i }}</a></li>
		@else
		<li><a href="{{ $paginator->getUrl($i) }}">{{ $i }}</a></li>
		@endif
	@endfor
	<li><a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}"><i class="fa fa-chevron-right"></i></a></li>
</ul>
@endif