<!doctype html>
<html>
<head>
   @include('front._partials.meta')
   @include('front._partials.assets1')
</head>
   <body>
   @include('front._partials._coloured_header')
   <div class="container">{{ Notification::showAll() }}</div>