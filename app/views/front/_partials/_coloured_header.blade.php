<!-- =-=-=-=-=-=-= Light Header =-=-=-=-=-=-= -->
<div class="colored-header">
   <!-- Navigation Menu -->
   <nav id="menu-1" class="mega-menu">
         <!-- menu list items container -->
         <section class="menu-list-items">
            <div class="container">
               <div class="row">
                     
                     <div class="col-lg-1 col-md-1 col-xs-2">
                     <ul class="menu-logo">
                        <li>
                           <a href="/"><img src="/images/logo_d.png" alt="logo"> </a>
                        </li>
                     </ul>
                     </div>

                    <div class="col-lg-8 col-md-8 col-xs-6">
                    <form  class="form-group" id="searchForm" name ="searchForm" method="GET" action="/search">
                     <div class="">
                           <div class="col-md-1 col-sm-2 no-padding search_menu hidden-xs">
                              <div class="form-group">
                                 <select class="select2 category form-control" name="category_id" placeholder="asdf">
                                    <option label="Select Option"></option>
                                    <option value="2158">Автомашин</option>
                                    <option value="1">Үл хөдлөх</option>
                                    <option value="2">Үйлчилгээ</option>
                                    <option value="3">Ажлын байр</option>
                                    <option value="4">Электрон бараа</option>
                                    <option value="5">Хувцас</option>
                                    <option value="6">Спорт</option>
                                    <option value="7">Бусад</option>
                                 </select>
                              </div>
                           </div>

                           <div class="col-md-11 col-sm-6 col-xs-12 no-padding">
                                 <div class="row">
                                    <div class="col-lg-11 col-md-11 col-xs-8 npr">
                                    <input type="text" class="form-control banner-icon-search" name="keyword" placeholder="Та хайх үгээ бичнэ үү" value="{{ Input::get('keyword') }}"> 
                                    </div>

                                    <div class="col-lg-1 col-md-1 col-xs-2 header_search_icon">
                                    <a href="#" onclick="$('#searchForm').submit();"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </div>
                                 </div>
                           </div>
                     </div>
                     </form>
                     </div>

                     <div class="col-lg-2 col-md-2 col-xs-4">
                     <ul class="menu-search-bar">
                        <li>
                           <a href="/add" class="btn btn-light add_button">
                              <span class="hidden-xs">ЗАР</span>
                              <i class="fa fa-plus" aria-hidden="true"></i>
                           </a>
                        </li>

                        <li>
                           <ul class="user_menu">
                              <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                 <i class="fa fa-user-o" aria-hidden="true"></i>
                                 <i class="fa fa-angle-down"></i>
                                 </a>
                                 <ul class="dropdown-menu user_menu_sub">
                                    @if(Sentry::check())
                                      <?php 
                                          $user = Sentry::getUser();
                                          $profile = \App\Models\Profile::where('user_id','=',$user->id)->first();
                                          if($profile)
                                          {
                                            $link = ($profile->is_company == 1)?'/profile/':'/home/';
                                          }
                                      ?>
                                      <li><a href="/home/{{$user->id}}">Миний зарууд</a></li>
                                      <li><a href="/profile/edit">Профайл засах</a></li>
                                      <li><a href="/logout">Гарах</a></li>
                                    @else
                                       <li><a href="/login?new=1">Бүртгүүлэх </a></li>
                                       <li><a href="/login">Нэвтрэх</a></li>
                                    @endif
                                 </ul>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/i" class="my-checked-ads loading-bar2">
                             <i class="fa fa-bookmark-o"></i><em class="checked-ads-count">{{ count(sfUser::getInterest()) }}</em>
                           </a>
                        </li>

                     </ul>
                     </div>

               </div>
            </div>
         </section>
      </nav>
</div>