<?php 
use App\Models\Banner; 
$banners = Banner::getBannersByLocation(2);
?>

<!-- Master Slider -->
<div class="master-slider ms-skin-default" id="masterslider">
  <?php foreach($banners as $ban => $banner):?>
   <div class="ms-slide slide-2" data-delay="4">
      <!-- slide background --> 
      <img src="/files/uploads/banner/{{ $banner->path }}" data-src="/files/uploads/banner/{{ $banner->path }}" alt="{{$banner->name}}"/> 
      <h3 class="ms-layer title3 font-white font-thin uppercase"
         style="left: 90px;top: 160px;"
         data-type="text"
         data-delay="2000"
         data-duration="2500"
         data-ease="easeOutExpo"
         data-effect="skewright(30,80)">{{ $banner->name }}</h3>
      <a href="{{ $banner->link }}" target="{{$banner->blank_window == 1 ? '_blank' : ''}}" class="ms-layer btn3 uppercase"
         style="left:95px; top: 330px;"
         data-type="text"
         data-delay="3500"
         data-ease="easeOutExpo"
         data-duration="2000"
         data-effect="scale(1.5,1.6)"> Дэлгэрэнгүй</a>
   </div>
  <?php endforeach; ?>
   
</div>
<!-- end Master Slider -->