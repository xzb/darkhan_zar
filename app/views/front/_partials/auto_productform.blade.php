<?php use App\Models\AttributeValues; ?>
    <div class="container bdy_main_content" id = "automain">
    	<div class="row">
        	<div class="col-md-12 auto_add bottom_shadow">
            	<h2>Зар үнэгүй нэмэх</h2>
                 <script>
                  $(document).ready(function(){
                   $( "[title]" ).tooltip({'container':'body'});
                   $('textarea[name=description]').tooltip();
                  });
                  </script>
                <?php $tooltipText = ''; ?>
                <?php if($product): ?>
                  {{ Form::model($product, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'autoproduct-form', 'role'=>'form')) }}
                <?php else:?>
                  {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'autoproduct-form', 'role'=>'form')) }}
                <?php endif;?>

                <input type="hidden" name="product_id" id="product_id" value="{{ ($product)?$product->id:0 }}"/>
                <input type="hidden" name="duplicate" id="duplicate">
                <input type="hidden" name="is_edit" id="is_edit" value="{{ $is_edit }}">

                <input type="hidden" name="category_id" id="category_id" value="{{ ($product)?$product->category_id:20 }}"/>

                  <div class="col-md-8 col-xs-8 form_main">
            				<div class="form-group form-group-main full_form">
                        <label for="type_category">Ангилал</label>
                        <select name="type_category" id="type_category" class="form-control input" onchange="autoCategory(this.value)">
                              <option value="20" >Автомашин</option>
                              <option value="23" >Мотоцикл</option>
                              <option value="25" >Сэлбэг</option>
                              <option value="22" >Ачааны машин</option>
                              <option value="218" >Автобус/Микро</option>
                              <option value="24" >Механизм</option>
                              <option value="2151" >Солино</option>
                              <option value="2156" >Түрээслэнэ</option>
                              <option value="2155" >Түрээслүүлнэ</option>
                              <option value="2152" >Машин авна</option>
                              <option value="2333" >Дугуй</option>
                        </select>
                    </div>
                    <div class="form-group form-group-main full_form">
              					<label for="email">Зарын гарчиг <sup>*</sup></label>
              					{{ Form::text('name', ($product!=null)?$product->name:'', array('id' => 'name', 'class'=>'form-control')) }}
                    </div>
          				</div>
                <hr />
                <div class="col-md-8 form_main" id="attributesForCar">
                  <div class="form-group form-group-main">
                          <div class="col-md-6 col-sm-6 col-xs-6">
                              <label for="type_category">Төрөл <sup>*</sup></label>
                      <select name="type_category" id="type_category" class="form-control input" onchange="autoFactoryLoad(this.value,'factory_category')">
                                  <option value="0">Сонгоно уу</option>
                                  <option value="21" <?php if($type){ if($type->id == 21) echo 'selected="selected"'; } ?>>Суудлын</option>
                                    <option value="151" <?php if($type){ if($type->id == 151) echo 'selected="selected"'; } ?>>Жийп</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                              <label for="factory_category">Үйлдвэрлэгч  <sup>*</sup></label>
                      <select class="form-control input" name="factory_category" id="factory_category" onchange="autoMarkLoad(this.value,'mark_category')">
                                  <option value="0">Сонгоно уу</option>
                                  <?php if($factory)
                                      echo '<option value="'.$factory->id.'" selected="selected">'.$factory->name.'</option>';
                                  ?>
                                  
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-group-main">
                          <div class="col-md-6 col-sm-6 col-xs-6">
                              <label for="mark_category">Марк</label>
                      <select class="form-control input"  name="mark_category" id="mark_category" onchange="setMarkCategory(this.value)">
                                  <option value="0">Сонгоно уу</option>
                                  <?php if($mark)
                                      echo '<option value="'.$mark->id.'" selected="selected">'.$mark->name.'</option>';
                                  ?>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                              
                            </div>
                        </div>
                        <?php $j = 2; ?>
                        @foreach($attributes as $a)
                        <?php $j++; 
                              $checkArray = array();

                              $textValues = '';

                              if($product)
                              {
                                if(array_key_exists($a->id, $arrayPA))
                                {
                                  if(array_key_exists('id',$arrayPA[$a->id]))
                                  {
                                    if(array_key_exists($arrayPA[$a->id]['id'],$arrayPAV))
                                    {
                                      $checkArray = $arrayPAV;      
                                    } 
                                  }

                                  if(array_key_exists('value',$arrayPA[$a->id]))
                                  {
                                      $textValues = $arrayPA[$a->id]['value']; 
                                  }
                                }
                                
                              }  
                              
                        ?>
                            @if($j%2 == 1)
                                <div class="form-group form-group-main">
                            @endif    
                                
                                <div class="col-md-6 col-sm-6 col-xs-6 full_form">
                                @if($a->type == 'checkbox')
                                <div class="checkbox" for="email">
                                @endif    
                                    <label class="ch_lab">{{ $a->name }}  <sup>*</sup></label>
                                    <?php 
                                        $avalues = AttributeValues::where('attribute_id','=',$a->id)->get();
                                    ?>

                                    @if($a->type == 'radio')

                                    <div class="radio_box">
                                        @foreach($avalues as $v)
                                        <span><input id="radio{{$v->id}}" type="radio" name="radio{{$a->id}}" value="{{ $v->id }}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'checked="checked"';} ?>><label for="radio{{$v->id}}">{{ $v->value }}</label></span>
                                        @endforeach
                                    </div>
                                    @endif

                                    @if($a->type == 'checkbox')
                                        <div class="radio_box">
                                            @foreach($avalues as $v)
                                                <span><input id="checkbox{{$a->id}}" type="checkbox" name="checkbox{{$a->id}}[]" value="{{ $v->id }}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'checked="checked"'; } ?>><label for="checkbox{{$v->id}}">{{ $v->value }}</label></span>
                                            @endforeach
                                        </div>
                                    @endif

                                    @if($a->type == 'selectbox')
                                        <select class="form-control input" id="selectbox{{$a->id}}" name="selectbox{{$a->id}}">
                                            @foreach($avalues as $v)
                                                <option value="{{$v->id}}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'selected="selected"'; }?>>{{ $v->value }}</option>
                                            @endforeach
                                        </select>
                                    @endif

                                    @if($a->type == 'textbox')
                                    @if($a->id == 102)
                                    <?php $tooltipText = 'Та хөдөлгүүрийн багтаамжаа дараах байдлаар оруулна уу! 2.0, 3.0, 4.5 гэх мэт'; ?>
                                    @endif
                                      <input type ="text" name="textbox{{$a->id}}" id="textbox{{$a->id}}" class="form-control" value="{{$textValues}}" title="{{$tooltipText}}" data-placement="bottom"/>
                                        
                                    @endif


                              

                                </div>

                            @if($j%2==0)
                                </div>
                            @endif

                        @endforeach
                  
                </div>
                </div>


                <div class="col-md-8 form_main">
        				<div class="form-group form-group-main">
          					<label for="pwd">Зар <sup>*</sup></label>
          					{{ Form::textarea('description', ($product!=null)?$product->description:'', array('id' => 'description', 'rows' => '4','class'=>'form-control',)) }}
        				</div>
                        <div class="form-group form-group-main">
                        	<div class="col-md-6 col-sm-6 col-xs-6 full_form">
                            	<label id="forPrice" for="price">Үнэ <sup>*</sup>(сая)</label>
          						{{ Form::text('price', ($product!=null)?$product->price : 0, array('id' => 'priceauto','class'=>'form-control','maxlength'=>'5',"title"=>'Та бодит үнийн дүн оруулна уу! Таны оруулсан дүн саяар бичигдэнэ Жиш: 12.5 сая гэх мэт','data-placement'=>'bottom')) }}
                      {{ Form::text('priceother', ($product!=null)?$product->price : 0, array('id' => 'priceother','class'=>'form-control','style'=>'display:none','maxlength'=>'8',"title"=>'Та бодит үнийн дүн оруулна уу!','data-placement'=>'bottom')) }}
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 full_form">
                            	<label for="email">Холбоо барих утас <sup>*</sup></label>
          						{{ Form::text('contact', ($product!=null)?$product->contact:'', array('id' => 'contact','class'=>'form-control',)) }}
                            </div>
                        </div>
                        <div class="form-group full_form form-group-main full_form">
          					<label for="pwd">Нууц үг <sup>*</sup></label>
          					 {{ Form::text('password', ($product!=null)?$product->password:'', array('id' => 'password','class'=>'form-control',)) }}
                            <span class="infor">Та нууц кодоо өөрөө зохиож энд оруулна уу. Дараа уг кодоо ашиглан зараа засаж, устгана уу</span>
        				</div>
                </div>
                
                <hr />
                <div class="col-md-8 form_main">
                	<div class="form-group form-group-main"  id="image_container">
                    	<?php foreach ($images as $image): ?>
                       <div class="image-upload-2 {{ $image->filename ? 'uploaded' : '' }}">
                         <div class="image-select"></div>
                         <div class="image-uploaded">
                           <?php
                           if (is_file('/uploads/tmp/' . $image->filename)) {
                             echo Form::image('/uploads/tmp/' . $image->filename);
                           } else if($image->filename) {
                             echo Form::image('/uploads/thumb/' . $image->filename);
                           } else {
                             echo Form::image('');
                           }
                           ?>
                         </div>
                         <div class="image-delete"></div>
                         <div class="image-uploading"></div>
                         <input type="hidden" class="image-filename" value="{{ $image->filename }}" name="images[]">
                       </div>
                     <?php endforeach; ?>
                    </div>
                    <div id="prog-bars"></div>    
                </div>
                

                <div class="col-md-8 form_main infor_red no_padding">
                	<div class="form-group form-group-main">
                    	<span>Бодит зурагтай зар 2 дахин илүү олон хүнд хүрдэг гэдгийг анхаарна уу!<br />ЖИЧ: Хуурамч эсвэл зохиомол зурагтай зар устана.</span>
                    </div>
                </div>
                <div class="col-md-8 form_main infor_red no_padding">
                	<div class="form-group form-group-main form-group-unique">
                    	<span>
                            <input type="checkbox" name="vnagree" id="vnagree" class="auto_more_chk" {{ $is_edit ? 'checked="checked"' : '' }}/> 
                        	<label for="vnagree" class="auto_more_chk_lab">Үйлчилгээний нөхцөлийг зөвшөөрч байна. <a href="http://www.dazar.mn/page/terms" target="_blank"><b>ЭНД</b></a>  дарж уншина уу.</label>
                        </span>
                    </div>
                </div>
                <hr />
                <div class="col-md-8 form_main submit_section">
                	<div class="form-group form-group-main form-group-unique">
                    	<input type="submit" id="autoproduct-submit" value="<?php echo ($product === null)? 'Нэмэх' : 'Xадгалаx' ?>" />
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    

    

   