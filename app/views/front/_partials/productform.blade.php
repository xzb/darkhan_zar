<?php use App\Models\Category; ?>

 <!-- Small Breadcrumb -->
  <div class="small-breadcrumb">
     <div class="container">
        <div class=" breadcrumb-link">
           <ul>
              <li><a href="/">Нүүр</a></li>
              <li><a class="active" href="/add">Зар нэмэх</a></li>
           </ul>
        </div>
     </div>
  </div>
  <!-- Small Breadcrumb -->

<div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding  gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
                <?php if($product): ?>
                  {{ Form::model($product, array('method' => 'put', 'files' => true, 'route' =>$route, 'name' => 'addzar','id'=>'product-form','class'=>'submit-form','enctype'=>'multipart/form-data')) }}
                <?php else:?>
                  {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'name' => 'addzar','id'=>'product-form','class'=>'submit-form')) }}
                <?php endif;?>
              
                @if ($errors->any())
                <div style="color: red;">
                        {{ implode('<br>', $errors->all()) }}
                </div>
                @endif
                <input type="hidden" name="product_id" id="product_id" value="{{ ($product)?$product->id:0 }}"/>
                <input type="hidden" name="duplicate" id="duplicate">
                <input type="hidden" name="is_edit" id="is_edit" value="{{ $is_edit }}">
                 <?php if($product): ?>
                      <?php $cat_id = $product->category_id; ?>
                      <?php $loc_id = $product->location_id; ?>
                    <?php elseif(isset($cidd)): ?>
                      <?php $category = Category::find($cidd); ?>
                      <?php if($category->isLeaf()): ?>
                        <?php $cat_id = $cidd; ?>
                      <?php else: ?>
                        <?php $cat_id = ''; ?>
                      <?php endif; ?>
                    <?php else: ?>
                      <?php $cat_id = ''; ?>
                      <?php $loc_id = ''; ?>
                    <?php endif; ?>
                    <div>
                    <input type="hidden" name="category_id" id="category_id" value="{{ $cat_id }}"/>
                    <input type="hidden" name="location_id" id="location_id" value="{{ $loc_id }}"/>


               <div class="row">
                  <div class="col-md-12">
                     <!-- end post-padding -->
                     <div class="post-ad-form extra-padding postdetails">
                        <div class="heading-panel">
                           <h3 class="main-title text-left">
                              Зар нэмэх
                           </h3>
                        </div>
                        <form  class="submit-form">
                           <!-- Title  -->
                              
                           <div class="row" id="categorySelectRow">
                              <!-- Category  -->
                              <?php $hide = false; ?>

                              <?php foreach ($cArray as $idx => $categoryList): ?>
                                <?php $selected_id = isset($breadcrumb[$idx]) ? $breadcrumb[$idx]->id : 0; ?>
                                  @if(isset($breadcrumb[$idx]))
                                  <?php if(2 == $breadcrumb[$idx]->id || $breadcrumb[$idx]->id == 3): ?>
                                    <?php $hide = true; ?>
                                  <?php endif; ?>
                                  @endif
                                @include('front._partials.ajaxList', array('categories' => $categoryList, 'selected_id' => $selected_id))
                              <?php endforeach; ?>
                           </div>
                           <!-- end row -->
                           <?php if($hide): ?>   
                           <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="display: none">
                                 <label class="control-label">Зарын гарчиг <small>Enter a short title for your project</small></label>
                                 {{ Form::hidden('name', ($product!=null)?$product->name:'', array('id' => 'name', 'class'=>'form-control','title'=>'Та гарчигаа кирилл үсгээр бичсэнээр хайлтаар гарах магадлал ихэснэ.','data-placement'=>'bottom')) }}
                              </div>
                           </div>
                           <?php else: ?>
                           <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <label class="control-label">Зарын гарчиг <small>Enter a short title for your project</small></label>
                                 {{ Form::text('name', ($product!=null)?$product->name:'', array('id' => 'name', 'class'=>'form-control','title'=>'Та гарчигаа кирилл үсгээр бичсэнээр хайлтаар гарах магадлал ихэснэ.','data-placement'=>'bottom')) }}
                              </div>
                           </div>
                           <?php endif; ?>
                           
                           <div id="attributeform">
                              @include('front._partials.attribute', array('product'=>$product, 'attributes'=>$attributes))
                           </div>
                           
                            <label class="control-label">Зураг <small>Зургийн хэмжээ. (350x450)</small></label>
                            <div class="imageuploader" id="image_container">
                               <!-- Image Upload  -->
                               <div class="row">
                                 <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
                                   <link href="/css/uploadfile.css" rel="stylesheet">
                                   <div id="fileuploader">Upload</div>
                                 </div>
                               </div>
                            </div>

                           <!-- end row -->
                           <!-- Ad Description  -->
                           <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
                                 <label class="control-label">Дэлгэрэнгүй </label>
                                 {{ Form::textarea('description', ($product!=null)?$product->description:'', array('id' => 'description', 'onKeyDown' => 'limitText(this.form.description,this.form.countdown,1000);', 'class'=>'form-control zartext','title'=>'Та тайлбараа кирилл үсгээр бичсэнээр хайлтаар гарах магадлал ихэснэ.','onKeyUp'=>'limitText(this.form.description,this.form.countdown,1000);')) }}
                
                              </div>
                           </div>
                           <!-- end row -->
                           <div class="row">
                           
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <label for="owner_type"><span class="red">#</span> Хувь хүн / Компаний зар</label>
                                <select id="owner_type" name="owner_type" class="form-control">
                                  <option value="0" <?php if($product && $product->owner_type == 1){echo 'selected="selected"';}?>>Хувь хүн</option>
                                  <option value="1" <?php if($product && $product->owner_type == 2){echo 'selected="selected"';}?>>Компаний зар</option>
                               </select>    
                              </div>
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <label> Үнэ:</label>
                                 {{ Form::text('price', ($product!=null)?$product->price : 0, array('id' => 'price', 'class'=>'form-control','title'=>'Та үнэн, бодит үнэ оруулна уу.','data-placement'=>'bottom')) }}
                              </div>
                           </div>
                           <!-- end row -->
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <label for="lizen"><span class="red">#</span> Холбоо барих утас:</label>
                                    {{ Form::text('contact', ($product!=null)?$product->contact:'', array('id' => 'contact', 'class'=>'form-control','title'=>'Тантай холбоо барихад шаардлагатай дугааруудаа оруулна уу.','data-placement'=>'bottom')) }}
                              </div>
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                  @if(!Sentry::check())
                                  
                                   <label for="lizen"><span class="red">#</span> Нууц дугаар:</label>
                                   {{ Form::text('password', ($product!=null)?$product->password:'', array('id' => 'password', 'class'=>'form-control','title'=>'Та уг нууц кодоор оруулсан зараа засах, устгах, сэргээх тул мартагдхааргүй код ашиглана уу','data-placement'=>'bottom')) }}
                                  
                                  @else
                                    <input type="hidden" name="password" id="password" value="1c2514b67bc22476c198dc2c1937b822"/>
                                  @endif
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                              <label for="changes"> Бартер солилцоо:</label>
                                    {{ Form::text('changes', ($product!=null)?$product->changes:'', array('id' => 'changes', 'class'=>'form-control','title'=>'Солих зүйл','data-placement'=>'bottom')) }}
                              </div>
                           </div>
                           <div class="row">
                              @include('front._partials.ajaxLocation', array('parentLocations' => $parentLocations, 'selected_id' => $selected_id))
                           </div>
                           <?php $latlng = '47.91990271551152,106.91825866699219';
                                if($product){
                                     $latlng = ($product->latlng !== '')?$product->latlng:'47.91990271551152,106.91825866699219';
                                            } 
                           ?>
                              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKGHDGh5HEeEN9ediwKxFhAUbQoYolVb4"></script>
                              <script>

                              var ub = new google.maps.LatLng(<?php echo $latlng ?>);
                              var marker;
                              var map;

                              function initialize() {
                                var mapOptions = {
                                  zoom: 14,
                                  center: ub
                                };

                                map = new google.maps.Map(document.getElementById('map-canvas'),
                                        mapOptions);

                                marker = new google.maps.Marker({
                                  map:map,
                                  draggable:true,
                                  animation: google.maps.Animation.DROP,
                                  position: ub
                                });
                                google.maps.event.addListener(map, 'click', function (event) {
                                    marker.setPosition(event.latLng);
                                    $('#latlng').val(event.latLng.lat()+','+event.latLng.lng());
                                });

                              }

                              google.maps.event.addDomListener(window, 'load', initialize);

                                

                              </script>
                              <div class="row">
                                  <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
                                      <div class="form-group form-group-main form-group-unique">
                                        <div id="map-canvas" style="width:600px; height:400px"></div>
                                        <input id="latlng" name="latlng" value="<?php echo $latlng; ?>" type="hidden" />
                                      </div>
                                  </div>
                              </div>

                           <!-- end row -->
                           <button class="btn btn-theme pull-center" id="product-submit">Нэмэх</button>
                        </form>
                     </div>
                     <!-- end post-ad-form-->
                  </div>
                  <!-- end col -->
               </div>
               <!-- Row End -->

               </form>
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->