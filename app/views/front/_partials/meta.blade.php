<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="language" content="mn" />
<meta name="robots" content="index, follow" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="csrf-token" content="{{ csrf_token() }}">
{{SEOMeta::generate()}}
{{OpenGraph::generate()}}