<script src="/js/modernizr.js"></script>
<!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
<script src="/js/jquery.min.js"></script>
<!-- Jquery Select Options  -->
<script src="/js/select2.min.js"></script>
<!-- Bootstrap Core Css  -->
<script src="/js/bootstrap.min.js"></script>
<!-- Menu Hover  -->
<script src="/js/forest-megamenu.js"></script>
<!-- Jquery Appear Plugin -->
<script src="/js/jquery.appear.min.js"></script>
<!-- Numbers Animation   -->
<script src="/js/jquery.countTo.js"></script>
<!-- noUiSlider -->
<script src="/js/nouislider.all.min.js"></script>
<!-- Carousel Slider  -->
<script src="/js/carousel.min.js"></script>
<script src="/js/slide.js"></script>
<!-- Image Loaded  -->
<script src="/js/imagesloaded.js"></script>
<script src="/js/isotope.min.js"></script>
<!-- CheckBoxes  -->
<script src="/js/icheck.min.js"></script>
<!-- Jquery Migration  -->
<script src="/js/jquery-migrate.min.js"></script>
<!-- Sticky Bar  -->
<script src="/js/theia-sticky-sidebar.js"></script>
<!-- Template Core JS -->
<script src="/js/custom.js"></script>
<!-- For This Page Only -->
<!-- MasterSlider --> 
<script src="/js/masterslider/masterslider.min.js"></script> 

<!-- DROPZONE JS  -->
<script src="/js/dropzone.js" ></script>
<script src="/js/form-dropzone.js" ></script>
<script  src="/js/jquery.Jcrop.min.js" type="text/javascript" ></script>
<script  src="/js/ajaxupload.js" type="text/javascript" ></script>
<script  src="/js/fileuploader.js" type="text/javascript" ></script>
<script src="/js/modernizr.js"></script>
<script  src="/js/fancybox/jquery.fancybox.js" type="text/javascript" ></script>
<script src="/js/old_main.js"></script>
<script src="/js/old_added.js"></script>

<script type="text/javascript">
//disabling other select2 js
$('.main-content-area .select2').hide();
</script>

<script type="text/javascript"> 
(function($) {
 "use strict";  
    var slider = new MasterSlider();

    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('bullets');

     slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:450,   // slider standard height
         space:1,
         layout:'fullwidth',
         loop:true,
         preload:0,
         instantStartLayers:true,
         autoplay:true
    });
})(jQuery);
</script> 

<script type="text/javascript">
   $('.number').click(function() {
    $(this).find('span').text( $(this).data('last') );
   });
</script>

<?php if(Active::route('product_add') || Active::route('product_edit')): ?>
<script src="/js/jquery.uploadfile.js"></script>
<script>
$(document).ready(function()
{
    $("#fileuploader").uploadFile({
        url:"/image/upload",
        multiple:true,
        dragDrop:true,
        fileName:"userfile",
        allowedTypes: "jpg,png,gif",
        maxFileSize:1572864,
        showPreview:true,
        previewHeight: "100px",
        previewWidth: "100px",
        showFileSize: false,
        maxFileCount:10,
        // extraHTML:function()
        // {
        //         var html = "<label class='is_default'><input type='radio' name='is_default' value='' /><small>Нүүр</small></label>";
        //         return html;            
        // },
        onSuccess:function(files,data,xhr,pd)
        {
            $('<input/>').attr({type:'hidden',name:'images[]', value: JSON.parse(data).filename}).appendTo('#product-form');
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        showDelete: true,
        <?php if(Active::route('product_edit') && isset($product)):?>
        onLoad:function(obj)
        {
            $.ajax({
                    cache: false,
                    url: "/image/load/" + <?php echo $product->id?>,
                    dataType: "json",
                    success: function(data) 
                    {
                        for(var i=0;i<data.length;i++)
                    { 
                        obj.createProgress(data[i]["name"],data[i]["path"],data[i]["size"]);
                    }
                    }
                });
        },
        deleteCallback: function (data, pd) {
            $.post("/image/delete", {pid: <?php echo $product->id?>, filename: JSON.parse(data).filename },
                function (resp,textStatus, jqXHR) {
                    //Show Message  
                    //alert("Зураг устгагдлаа");
            });
            pd.statusbar.hide(); //You choice.

        },
        <?php else:?>
        deleteCallback: function (data, pd) {
            $.post("/image/delete", {pid: '-1', filename: JSON.parse(data).filename },
                function (resp,textStatus, jqXHR) {
                    //Show Message  
                    //alert("Зураг устгагдлаа");
            });
            pd.statusbar.hide(); //You choice.

        },
        <?php endif;?>

    });
});
</script>
<?php endif;?>


<!-- 
<script src="/js/jquery.min.js" type="text/javascript" ></script>
<script src="/js/bootstrap.min.js" type="text/javascript" ></script>
<script  src="/js/zarmn.js" type="text/javascript" ></script>
<script src="/js/Chart.min.js" type="text/javascript" ></script>

<script  src="/js/jquery.number.min.js" type="text/javascript" ></script>
<script  src="/js/fancybox/jquery.fancybox.js" type="text/javascript" ></script>
<script  src="/js/jquery.loadingbar.js" type="text/javascript" ></script>
<script  src="/js/ajaxupload.js" type="text/javascript" ></script>
<script  src="/js/modernizr.js" type="text/javascript" ></script>
<script  src="/js/arg-min.js" type="text/javascript" ></script>
<script  src="/js/main.js" type="text/javascript" ></script>


<script  src="/js/fileuploader.js" type="text/javascript" ></script>
<script  src="/js/jquery.bm.js" type="text/javascript" ></script>

<script  src="/js/added.js" type="text/javascript" ></script> -->