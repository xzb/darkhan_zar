<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="fb:app_id" content="1489911667927619"/>
@include('front._partials.meta')
@include('front._partials.auto_assets2')
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900&subset=cyrillic-ext,latin,cyrillic' rel='stylesheet' type='text/css'>
@include('front._partials.auto_assets1')
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!--[if IE]>
<link rel="stylesheet" href="/auto_bootstrap/ie.css" type="text/css" />
<![endif]-->

</head>
<body id="property">
<section class="header_top">
<div class="container">
  <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <?php use App\Models\Banner; 
          $headerBanner = Banner::where('page_id','=',15)->first();
      ?>
      @if(count($headerBanner)>0)
      <img src="/files/uploads/banner/{{$headerBanner->path}}" />
      @else
      <img src="/files/uploads/banner/property.jpg" />
      @endif
    </div>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      @if(Sentry::check())
              <?php $user = Sentry::getUser(); ?>
              <ul class="nav navbar-nav navbar-right">
                  
                  <li>
                    <a href="/profile/{{$user->id}}" style="text-transform: none">{{ $user->first_name }}</a>
                  </li>
                  
                  <li>
                    <a href="/my" style="text-transform: none">Миний зарууд ({{ $user->myProductsCount() }})</a>
                  </li>
                  <li>
                    <img src="{{ $user->image }}" alt="" style="max-height: 35px;">
                    <a href="/logout" style="text-transform: none">Гарах</a>
                  </li>
              </ul>
              @else
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login">Нэвтрэх <span class="glyphicon glyphicon-user"></span></a></li>
                    <li><a href="/login">Бүртгүүлэх <span class="glyphicon glyphicon-plus"></span></a></li>
                  </ul>        
              @endif
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
</section>
<section class="logo">
  <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <a class="navbar-brand" href="/"><img src="/images/property_logo.png" alt="Logo" /></a>
          </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    </div>
</section>
<section id="SearchContent">
  <div class="container">
      <div class="search_content_left">

      <a href="/addproperty"><div class="LinkTab"><span class="glyphicon glyphicon-plus-sign"></span><span class="zar_add_text">&nbsp;&nbsp;Зар нэмэх</span></div></a>
      <form class="navbar-form navbar-left" role="search" action="/search" method="GET">

          <div class="form-group ad-search-input">
            <input name="keyword" type="text" class="form-control ad-search-txt input-1" autocomplete="off" placeholder="Зар хайх: Байр зарна, Viva city, түрээслэнэ гэх мэт" id="keyword" value="{{ Input::get('keyword') }}">
            <!--  -->
            <div class="search-suggestion"><div class="suggestions"><ul id="suggestion-list"></ul></div></div>
          </div>
          <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span><span class="zar_search_text">&nbsp;&nbsp;Хайх</span></button>
      </form>
    </div>
        <div class="search_content_right">
          <a href="/i">
          <span class="count_square">
              {{ count(sfUser::getProperty()) }}
            </span>
            <span class="count_square_det">дугуйлсан<br />зарууд</span>
          </a>
        </div>  
    </div>
</section>
@if(!Request::is('profile/*'))
<span class="fixedspan"><a href="/page/feedback"><img border="0" src="/images/mail_yellow.png"></a></span>
@endif
<section id="BodyContent">