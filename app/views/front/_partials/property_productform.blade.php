<?php use App\Models\AttributeValues; ?>
<?php use App\Models\Category; ?>


    <div class="container bdy_main_content" id = "propertymain">
    	<div class="row">
        	<div class="col-md-12 auto_add bottom_shadow">
            	<h2>Зар үнэгүй нэмэх</h2>
                 <script>
                  $(document).ready(function(){
                   $( "[title]" ).tooltip({'container':'body'});
                   $('textarea[name=description]').tooltip();
                  });
                  </script>
                <?php $tooltipText = ''; ?>
                <?php if($product): ?>
                  {{ Form::model($product, array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'propertyproduct-form', 'role'=>'form')) }}
                <?php else:?>
                  {{ Form::open(array('method' => 'put', 'files' => true, 'route' =>$route, 'class' => 'form-horizontal','id'=>'propertyproduct-form', 'role'=>'form')) }}
                <?php endif;?>

                <input type="hidden" name="product_id" id="product_id" value="{{ ($product)?$product->id:0 }}"/>
                <input type="hidden" name="duplicate" id="duplicate">
                <input type="hidden" name="is_edit" id="is_edit" value="{{ $is_edit }}">

                <input type="hidden" name="category_id" id="category_id" value="{{ ($product)?$product->category_id:28 }}"/>
                <input type="hidden" name="location_id" id="location_id" value="{{ ($product)?$product->location_id:0 }}"/>

                  <div class="col-md-8 col-xs-8 form_main">
            				<div class="form-group form-group-main full_form">
                              <label for="type_category">Ангилал <sup>*</sup></label>
                      <select name="type_property" id="type_property" class="form-control input" onchange="propertyCategory(this.value,'property_child')">
                        <option value="0">Сонгоно уу</option>
                        @foreach($select1 as $select)
                        <?php if($product) { $selectedValue = ($parentId != 0) ? $parentId : $product->category_id; } ?>
                        <option value="{{$select->id}}" <?php if($product){ if($selectedValue == $select->id) echo 'selected="selected"'; } ?>>{{$select->name}}</option>
                        @endforeach
                      </select>
                      
                        <select class="form-control input" name="property_child" id="property_child" onchange="setMarkCategory(this.value)" style="display:none">
                          <option value="0">Сонгоно уу</option>
                          @if($product)
                            @if($select2)
                              @foreach($select2 as $select)
                              <option value="{{$select->id}}" <?php if($product){ if($product->category_id == $select->id) echo 'selected="selected"'; } ?>>{{$select->name}}</option>
                              @endforeach
                            @endif
                          @endif 
                        </select>
                      
                    </div>
                    <div class="form-group form-group-main full_form">
              					<label for="email">Зарын гарчиг <sup>*</sup></label>
              					{{ Form::text('name', ($product!=null)?$product->name:'', array('id' => 'name', 'class'=>'form-control')) }}
                    </div>
          				</div>
                <hr />
                <div class="col-md-8 form_main" id="attributesForProperty">
                  <div class="form-group form-group-main">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <label for="location">Байршил <sup>*</sup></label>
                          <select name="location" id="location" class="form-control input" onchange="loadLocation(this.value,'sub_location')">
                              <option value="0">Сонгоно уу</option>
                              @foreach($locations as $select)
                              <?php if($product) { $selectedLocValue = ($parentLoc != 0) ? $parentLoc : $product->location_id; } ?>
                              <option value="{{$select->id}}" <?php if($product){ if($selectedLocValue == $select->id) echo 'selected="selected"'; } ?>>{{$select->name}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <label for="sub_location" style="color:white">Байршил</label>
                          <select class="form-control input" name="sub_location" id="sub_location" onchange="setLocation(this.value)">
                            <option value="0">Сонгоно уу</option>
                            @if($locations2)
                              @foreach($locations2 as $select)
                              <option value="{{$select->id}}" <?php if($product){ if($product->location_id == $select->id) echo 'selected="selected"'; } ?>>{{$select->name}}</option>
                              @endforeach
                            @endif
                          </select>
                      </div>
                  </div>
                        
                        <?php $j = 2; ?>
                        @foreach($attributes as $a)
                        <?php $j++; 
                              $checkArray = array();

                              $textValues = '';

                              if($product)
                              {
                                if(array_key_exists($a->id, $arrayPA))
                                {
                                  if(array_key_exists('id',$arrayPA[$a->id]))
                                  {
                                    if(array_key_exists($arrayPA[$a->id]['id'],$arrayPAV))
                                    {
                                      $checkArray = $arrayPAV;      
                                    } 
                                  }

                                  if(array_key_exists('value',$arrayPA[$a->id]))
                                  {
                                      $textValues = $arrayPA[$a->id]['value']; 
                                  }
                                }
                                
                              }  
                              
                        ?>
                            @if($j%2 == 1)
                                <div class="form-group form-group-main">
                            @endif    
                                
                                <div class="col-md-6 col-sm-6 col-xs-6 full_form">
                                @if($a->type == 'checkbox')
                                <div class="checkbox" for="email">
                                @endif    
                                    <label class="ch_lab">{{ $a->name }}  <sup>*</sup></label>
                                    <?php 
                                        $avalues = AttributeValues::where('attribute_id','=',$a->id)->get();
                                    ?>

                                    @if($a->type == 'radio')

                                    <div class="radio_box">
                                        @foreach($avalues as $v)
                                        <span><input id="radio{{$v->id}}" type="radio" name="radio{{$a->id}}" value="{{ $v->id }}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'checked="checked"';} ?>><label for="radio{{$v->id}}">{{ $v->value }}</label></span>
                                        @endforeach
                                    </div>
                                    @endif

                                    @if($a->type == 'checkbox')
                                        <div class="radio_box">
                                            @foreach($avalues as $v)
                                                <span><input id="checkbox{{$a->id}}" type="checkbox" name="checkbox{{$a->id}}[]" value="{{ $v->id }}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'checked="checked"'; } ?>><label for="checkbox{{$v->id}}">{{ $v->value }}</label></span>
                                            @endforeach
                                        </div>
                                    @endif

                                    @if($a->type == 'selectbox')
                                        <select class="form-control input" id="selectbox{{$a->id}}" name="selectbox{{$a->id}}">
                                            @foreach($avalues as $v)
                                                <option value="{{$v->id}}" <?php if(count($checkArray)>0){ if(in_array($v->id, $checkArray)) echo 'selected="selected"'; }?>>{{ $v->value }}</option>
                                            @endforeach
                                        </select>
                                    @endif

                                    @if($a->type == 'textbox')
                                    @if($a->id == 102)
                                    <?php $tooltipText = 'Та хөдөлгүүрийн багтаамжаа дараах байдлаар оруулна уу! 2.0, 3.0, 4.5 гэх мэт'; ?>
                                    @endif
                                      <input type ="text" name="textbox{{$a->id}}" id="textbox{{$a->id}}" class="form-control" value="{{$textValues}}" title="{{$tooltipText}}" data-placement="bottom"/>
                                        
                                    @endif


                              

                                </div>

                            @if($j%2==0)
                                </div>
                            @endif

                        @endforeach
                  
                </div>
                <!-- </div> -->


                <div class="col-md-8 form_main">
        				<div class="form-group form-group-main">
          					<label for="pwd">Зар <sup>*</sup></label>
          					{{ Form::textarea('description', ($product!=null)?$product->description:'', array('id' => 'description', 'rows' => '4','class'=>'form-control',)) }}
        				</div>
                        <div class="form-group form-group-main">
                        	<div class="col-md-6 col-sm-6 col-xs-6 full_form">
                          	<label id="forPrice" for="price">Үнэ <sup>*</sup></label>
          						      {{ Form::text('price', ($product!=null)?$product->price : 0, array('id' => 'price','class'=>'form-control','maxlength'=>'20',"title"=>'Та бодит үнийн дүн оруулна уу!','data-placement'=>'bottom')) }}
                            <select class="form-control input" name="price_type" id="price_type" style="display:block; width:30%; float:right">
                              <option value="0">нийт</option>
                              <option value="1">мкв</option>
                            </select>  
                          </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 full_form">
                            	<label for="email">Холбоо барих утас <sup>*</sup></label>
          						{{ Form::text('contact', ($product!=null)?$product->contact:'', array('id' => 'contact','class'=>'form-control',)) }}
                            </div>
                        </div>
                        <div class="form-group full_form form-group-main full_form">
          					<label for="pwd">Нууц үг <sup>*</sup></label>
          					 {{ Form::text('password', ($product!=null)?$product->password:'', array('id' => 'password','class'=>'form-control',)) }}
                            <span class="infor">Та нууц кодоо өөрөө зохиож энд оруулна уу. Дараа уг кодоо ашиглан зараа засаж, устгана уу</span>
        				</div>
                </div>
                
                <hr />
                <div class="col-md-8 form_main">
                	<div class="form-group form-group-main"  id="image_container">
                    	<?php foreach ($images as $image): ?>
                       <div class="image-upload-2 {{ $image->filename ? 'uploaded' : '' }}">
                         <div class="image-select"></div>
                         <div class="image-uploaded">
                           <?php
                           if (is_file('/uploads/tmp/' . $image->filename)) {
                             echo Form::image('/uploads/tmp/' . $image->filename);
                           } else if($image->filename) {
                             echo Form::image('/uploads/thumb/' . $image->filename);
                           } else {
                             echo Form::image('');
                           }
                           ?>
                         </div>
                         <div class="image-delete"></div>
                         <div class="image-uploading"></div>
                         <input type="hidden" class="image-filename" value="{{ $image->filename }}" name="images[]">
                       </div>
                     <?php endforeach; ?>
                    </div>
                    <div id="prog-bars"></div>    
                </div>
                

                <div class="col-md-8 form_main infor_red no_padding">
                	<div class="form-group form-group-main">
                    	<span>Бодит зурагтай зар 2 дахин илүү олон хүнд хүрдэг гэдгийг анхаарна уу!<br />ЖИЧ: Хуурамч эсвэл зохиомол зурагтай зар устана.</span>
                    </div>
                </div>

                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
                <script>

                var ub = new google.maps.LatLng(47.918281100053804,106.88172234179683);
                var marker;
                var map;

                function initialize() {
                  var mapOptions = {
                    zoom: 14,
                    center: ub
                  };

                  map = new google.maps.Map(document.getElementById('map-canvas'),
                          mapOptions);

                  marker = new google.maps.Marker({
                    map:map,
                    draggable:true,
                    animation: google.maps.Animation.DROP,
                    position: ub
                  });
                  google.maps.event.addListener(map, 'click', function (event) {
                      marker.setPosition(event.latLng);
                      $('#latlng').val(event.latLng.A+','+event.latLng.F);
                  });

                }

                google.maps.event.addDomListener(window, 'load', initialize);

                  

                </script>

                <div class="col-md-8 form_main infor_red no_padding">
                    <div class="form-group form-group-main form-group-unique">
                      <div id="map-canvas" style="width:600px; height:400px"></div>
                      <input id="latlng" name="latlng" type="hidden" />
                    </div>
                </div>
                <div class="col-md-8 form_main infor_red no_padding">
          	        <div class="form-group form-group-main form-group-unique">
                      
                      <div>
                        <label>Үйлчилгээний нөхцөл</label>
                      </div>
                      <div style="width:686px;overflow-x:hidden; overflow-y: scroll; height: 190px;border:1px solid #ccc; padding: 10px;">
                        Зар.МН нь хэрэглэгчдэд олон төрлийн үйлчилгээг дараах нөхцлийн дагуу үзүүлж байгаа бөгөөд бүх хэрэглэгчид эдгээр нөхцөлд захирагдан үйлчлүүлэх болно. Та Зар.МН сайтаар зочилж, манайхаар үйлчлүүлж байгаа нь энэхүү үйлчилгээний нөхцлийг хүлээн зөвшөөрсний илрэл юм.

                        Садар самуун, айлган сүрдүүлэх, хүчирхийлэл, хууль бус сурталчилгаа, бусдын нэр хүндэд халдсан, гүтгэн доромжилсон зар оруулахыг хориглоно.

                        Хэрэглэгч зар оруулахдаа МУ-н Зар сурталчилгааны тухай хуулийн 2-р бүлгийн 6.5.5; 6.5.7; 6.5.8-р заалтуудыг дагаж мөрдөнө.

                        Хэрэглэгчийн оруулсан зар, мэдээ мэдээлэл нь тухайн хэрэглэгчийн өөрийн хариуцлага байна. Хэрэглэгчийн оруулсан зард Зар.МН ямар нэгэн хариуцлага хүлээхгүй. 
                      </div>
                    </div>
                </div>
                <hr />
                <div class="col-md-8 form_main submit_section">
                	<div class="form-group form-group-main form-group-unique">
                    	<input type="submit" id="propertyproduct-submit" value="<?php echo ($product === null)? 'Нэмэх' : 'Xадгалаx' ?>" />
                      <p class="" style="color: #888;font-size: 11px;line-height: 16px;margin-bottom: 0;margin-left: 0;margin-right: 0;margin-top: 8px;">Та дээрх үйлчилгээний нөхцлийг зөвшөөрч байвал үргэлжлүүлнэ үү</p>  
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    

    

   