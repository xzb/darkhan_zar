 <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
 <footer class="footer-area no-bg">
    <!--Footer Bottom-->
    <div class="footer-copyright">
       <div class="container clearfix">
          <!--Copyright-->
          <div class="copyright text-center">Copyright 2017 © <a href="#">dzar.mn</a> All Rights Reserved</div>
       </div>
    </div>
 </footer>
 <!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->