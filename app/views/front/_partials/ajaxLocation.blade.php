<?php
if (!isset($selected_id)) {
  $selected_id = 0;
}
?>
<div class="col-md-4 col-lg-6 col-xs-12 col-sm-12">
   <label class="control-label">Байршил сонгох</label>
   <select class="location form-control">
    <option value="">Сонгоно уу</option>
    <?php foreach ($parentLocations as $id => $loc): ?>
      <option value="{{$id}}">{{ $loc }}</option>
    <?php endforeach; ?>   
   </select>
</div>
