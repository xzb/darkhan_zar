<?php 
use App\Models\Category;
$roots = Category::where('parent_id','=',null)->where('homepage_visible','=',1)->orderBy('rank')->get();
?>

<!-- =-=-=-=-=-=-= Categories =-=-=-=-=-=-= -->
 <section class="custom-padding categories gray">
    <!-- Main Container -->
    <div class="container">
       <!-- Row -->
       <div class="row">
          <!-- Middle Content Box -->
          <div class="col-md-12 col-xs-12 col-sm-12">
            @if(isset($roots))
            <?php foreach($roots as $root): ?>
            <!-- Category Grid List -->
            <div class="col-md-3 col-sm-6">
                <div class="category-list">
                   <div class="category-list-icon">
                        <i class="{{$root->root_css;}} home_flaticon"></i>
                        <div class="category-list-title">
                           <h5><a href="/c/{{$root->id}}/{{$root->getSeoUrl()}}" target="_blank">{{$root->name}}</a></h5>
                        </div>
                      
                   </div>


                   <?php 
                        $childs = Category::where('category.id','!=',$root->id)
                        ->where('category.lft','>=', $root->lft)
                        ->where('category.lft','<', $root->rgt)
                        ->where('category.homepage_visible','=',1)
                        ->where('category.level','<',2)
                        ->limit(6)
                        ->orderBy('category.rank')
                        ->orderBy('category.lft')
                        ->get();
                    ?> 

                   <ul class="category-list-data">
                    <?php foreach($childs as $child): ?>
                      <li><a href="/c/{{$child->id}}/{{$child->getSeoUrl()}}" target="_blank">{{ $child->name }} </a></li>
                    <?php endforeach; ?>
                   </ul>
                   <div class="view-more"> <a href="/c/{{$root->id}}/{{$root->name}}">Бүгдийг үзэх</a> </div>
                </div>
            </div>
            <?php endforeach; ?> 
            @endif
            

          </div>
          <!-- Middle Content Box End -->
       </div>
       <!-- Row End -->
    </div>
    <!-- Main Container End -->
 </section>
 <!-- =-=-=-=-=-=-= Categories End =-=-=-=-=-=-= -->