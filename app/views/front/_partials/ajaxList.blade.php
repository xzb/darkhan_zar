<?php
if (!isset($selected_id)) {
  $selected_id = 0;
}
$is_leaf = false;
?>

<div class="col-md-4 col-lg-6 col-xs-12 col-sm-12 product-category-main">
   <label class="control-label">Ангилал сонгох</label>
   <select class="category form-control">
    <option value="">Сонгоно уу</option>
    <?php foreach ($categories as $category): ?>
      <?php
      if ($category->id == 0) {
        continue;
      }
      if ($category->id == $selected_id && $category->isLeaf()) {
        $is_leaf = true;
      }
      ?>
      <option <?php echo $category->id == $selected_id ? "selected" : "" ?> value="<?php echo $category->id ?>" data-leaf="<?php echo $category->isLeaf() ? "true" : "false" ?>"
      data-for-title ="<?php echo $category->for_title; ?>"><?php echo $category->name . ($category->isLeaf() ? "" : " &raquo;") ?></option>
    <?php endforeach; ?>   
   </select>
</div>
<?php if ($is_leaf): ?>
  <div class="form-col product-category-main"><img src="/images/icons/tick.png" style="padding-top: 5px"></div>
<?php endif; ?>