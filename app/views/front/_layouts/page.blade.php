<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        {{SEOMeta::generate()}}
        {{OpenGraph::generate()}}
        @include('front._partials.assets')
    </head>
    <body>
        @include('front._partials.menumobile')

        <div id="site">

            @include('front._partials.header', array('cate' => isset($cate) ? $cate : null ))

            <div id="main">
                <div class="section fliud">
                    <div class="container">
                        @include('front.banner.show3',array('loc'=> 5))
                        <div class="wrap">
                            <div class="row">
                            @yield('main')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('front._partials.footer')
        </div>
        
</body>
</html>