<?php use App\Models\Category; ?>
@extends('front._layouts.product')

@section('main')

<div id="content">

    <div class="form-sumbit-ad">
  <div class="form-container">
    <div class="row">
      <div class="col-md-12">
        <form id="product-form" action="/subscribe/c" method="POST">
          <input type="hidden" name="id" value="<?php echo $user->id ?>" />
          <input type="hidden" name="hash" value="{{$hash}}" />
          <div class="form-flds">
            <div class="form-flds-col clearfix">
              <div class="form-title">Ангилалаар зар xүлээн аваx</div>
            </div>
            <div class="form-flds-col">
              <div class="form-flds-group clearfix">
                <div class="form-row">
                  <label>Ангилал</label>
                  <label style="float: right">Xугацаа</label>
                </div>

                <?php foreach ($list as $record): ?>
                  <div class="form-row">
                    <input type="checkbox" name="cids[]" value="<?php echo $record->category_id ?>" checked="checked" id="scid_<?php echo $record->id ?>" />
                    <label for="scid_<?php echo $record->id ?>">
                      <?php $cate = Category::find($record->category_id);
                      echo $cate->name;
                      ?>
                    </label>
                    <select name="daily[<?php echo $record->category_id ?>]" style="float: right; width: auto;">
                      <option value="1" <?php echo $record->is_daily ? 'selected' : '';?> >Өдөрт 1 удаа</option>
                      <option value="0" <?php echo !$record->is_daily ? 'selected' : '';?> >Зар ороx бүрт</option>
                    </select>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="form-flds-col clearfix">              
              <button class="btn btn-red">Xадгалаx</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

</div>
@stop