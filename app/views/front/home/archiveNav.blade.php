<ul class="days-pagination">
    <li class="day-label visible-lg">
        <b>7</b>
        <span>ӨМНӨХ<br/>ХОНОГТ:</span>
    </li>

	@for ($i = 0; $i <= 7; $i++)
	<?php $date = date('Y-m-d',strtotime("-".$i." days")); ?>

	<?php $class = ''; ?>
	<?php $day   =  getMglNamesForDays(date('w', strtotime($date)))?>

	<?php if($date == date('Y-m-d')): ?>
		<?php $class = 'day-today visible-lg' ?>
		<?php $day   = 'Өнөөдөр' ?>
	<?php endif; ?>


    <li class="archiveday {{ $class }}" rel="{{$date}}">
        <a href="/archive/{{ $date }}">
            <span class="day-name">{{ $day }}</span>
            <span class="day-date">{{ date('m.d', strtotime($date)) }}</span>
        </a>
    </li>
	@endfor
</ul>