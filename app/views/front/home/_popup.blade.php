<?php
use App\Models\Page;
$popup = Page::where('is_homepopup',1)->where('is_active',1)->orderBy('created_at','DESC')->first();
?>
@if($popup)
@if(!Cookie::get('is_homepopup_'.$popup->id))
<!-- Modal -->
<div class="modal fade" id="homepopup_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">

        <button style="margin-bottom: 5px;margin-top: -15px;" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      {{ $popup->body }}
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(window).load(function(){
        //$('#homepopup_modal').modal('show');
    });
</script>
<style type="text/css">
#searchbar {
  z-index: 2 !important;
}
#homepopup_modal .modal-body img {
  height: auto !important;
  width: auto !important;
  max-width: 100%;
}
#homepopup_modal .modal-dialog {
  width: auto;
  max-width: 1000px;
}
</style>
<?php Cookie::queue('is_homepopup_'.$popup->id, 1, 10080);?>
@endif
@endif