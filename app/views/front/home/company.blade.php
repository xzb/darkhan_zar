<?php use App\Models\Product; ?>
@extends('front._layouts.product')

@section('main')
<link rel="stylesheet" href="/css/jquery.bxslider.css" />
<link rel="stylesheet" href="/css/profile2.css"/>
<!-- <script type="text/javascript" src="/js/profile.js"></script> -->

<div id="content">
	<div class="margin-30">  <div class="row padding-20"> <h1 class="profile-title"> Байгууллагууд </h1> </div> 
	<script>
$( document ).ready(function() {
    $('.followslider').bxSlider({
        infiniteLoop: false,
        hideControlOnEnd: true,
        pager: false,
        slideWidth: 140,
        minSlides: 2,
        maxSlides: 3,
        slideMargin: 10
    });
});
</script>
            
	 @foreach($companies as $f)
        <div class="row eventrow">
            <div class="col-md-5">
                <div class="eventc">
                    <div class="img">
                        <a href="/profile/{{$f->user_id}}"><img src="/uploads/profile/{{$f->image}}" alt=""/></a>
                    </div>
                    <div class="text">
                        <a href="#" class="link">
                            <span>{{$f->company_name}}</span>
                            <span>{{$f->about_us}}</span>
                        </a>
                        <ul class="cbtn">
                            <li><a href="#" class="fa fa-envelope"></a></li>
                            <li><a href="#" class="fa fa-phone"></a></li>
                            <li><a href="#" class="fa fa-link"></a></li>
                            <li><a href="#" class="fa fa-map-marker"></a></li>
                        </ul>
                    </div>
                    <a href="/profile/{{$f->user_id}}" class="btn btn-more">Дэлгэрэнгүй &nbsp;<i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-7">
                <div class="pslide">
                    <ul class="followslider">
                        <?php $products  = Product::where('user_id', $f->user_id)->orderBy('created_at', 'DESC')->take(9)->get(); ?>
                        @if($products)
                            @foreach($products as $product)
                            <li>
                                <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" class="img">
                                    <?php if($product->image_filename):?>
                                        <img src="{{ Croppa::url('/uploads/thumb/s_'.$product->image_filename, 163, 138) }}" class="">                                 
                                    <?php else:?>
                                    <img src="/images/nophoto.png" class="">                 
                                    <?php endif;?>
                                    <span>{{ $product->price>999999999?"-":format_price($product->price) }}</span>
                                </a>
                                <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" class="link">{{  mb_substr($product->name, 0, 40, 'UTF-8') }}</a>
                            </li>
                            @endforeach
                        @else
                            <li>
                                Бүтээгдэхүүн оруулаагүй байна
                            </li>
                        @endif    
                    </ul>
                </div>
            </div>
        </div>
        @endforeach

            </div>
</div>

<!-- pager -->
<center>
{{ $companies->links(); }}
</center>
@stop