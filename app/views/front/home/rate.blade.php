<?php
$stars = array('0' => 'no', '1' => 'one', '2' => 'two', '3' => 'three', '4' => 'four', '5' => 'five');
$star = 0;
if ($product->nb_vote > 1)
{
  $star = $product->nb_totalvote / ($product->nb_vote-1);
}
$product_id  = $product->id;
?>
<ul class="rating <?php echo $stars[$star]?>star" id="rate_cont">
  <?php if (!$is_rate):?>
  <li class="one"><a href="#" title="1 Star" onclick="doRate(<?php echo $product_id?>, 1);return false;">1</a></li>
  <li class="two"><a href="#" title="2 Stars" onclick="doRate(<?php echo $product_id?>,2);return false;">2</a></li>
  <li class="three"><a href="#" title="3 Stars" onclick="doRate(<?php echo $product_id?>,3); return false;">3</a></li>
  <li class="four"><a href="#" title="4 Stars" onclick="doRate(<?php echo $product_id?>,4); return false;">4</a></li>
  <li class="five"><a href="#" title="5 Stars" onclick="doRate(<?php echo $product_id?>,5); return false;">5</a></li>
  <?php endif;?>
</ul>