@extends('front._layouts.default')

@section('main')

@include('front._partials._master_slider')

<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
	@include('front._partials._home_categories')
	@include('front._partials._last_products')
	@include('front._partials._app_download')
	@include('front._partials._footer1')
</div>
<!-- =-=-=-=-=-=-= Main Content Area END =-=-=-=-=-=-= -->

@stop