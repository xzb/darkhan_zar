@extends($layout)

@section('main')

<!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="/">Нүүр</a></li>
                  <li><a href="#">Нэвтрэх,Бүртгүүлэх</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">

                  <!-- Middle Content Area -->
                  <div class="col-md-6  col-md-6  col-xs-12 col-sm-6">
                     <div class="heading-panel">
                        <h3 class="main-title text-left">
                           Бүртгүүлэх
                        </h3>
                     </div>
                     <div class="content-info">
                     <!--  Form -->
                     <div class="form-grid">
                        {{ Form::open(array('id'=>'register_form', 'role'=>'form', 'route'=>'front.user.register')) }}
                          @if ($errors->any())
                          <div class="error">
                            @if ($errors->has('login'))

                            @else
                              {{ implode('<br>', $errors->all()) }}
                            @endif
                          </div>
                          @endif
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Овог</label>
                                    {{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder'=>'Таны овог')) }}
                                 </div>
                              </div>
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Нэр</label>
                                    {{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder'=>'Таны нэр')) }}
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Төрсөн он</label>
                                    <?php 
                                      $year = array(0 => 'Төрсөн он');
                                      for ($i = 1920; $i < 2010; $i++) {
                                        $year[$i] = $i;
                                      }
                                      ?>
                                    {{ Form::select('year', $year, 1990, array('class'=>'form-control')) }}
                                 </div>
                              </div>
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Хүйс</label>
                                    {{ Form::select('gender', array('0'=>'Хүйс', '1'=>'Эр','2'=>'Эм'), null, array('class'=>'form-control')) }}
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label>Имэйл хаяг</label>
                                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Имэйл')) }}
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Нууц үг</label>
                                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Нууц үг')) }}
                                 </div>
                              </div>
                              <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Нууц үг давтах</label>
                                    {{ Form::password('confirm_password', array('class'=>'form-control', 'placeholder'=>'Нууц үг давтах', 'onkeydown'=>'if (event.keyCode == 13) { this.form.submit(); return false; }')) }}
                                 </div>
                              </div>
                           </div>
                           <br>
                           <a class="btn btn-theme btn-lg btn-block" onclick="submitRegisterForm(); return false;" href="#">Бүртгүүлэх</a>
                        </form>
                     </div>

                     <!-- Form -->
                     </div>
                  </div>
                  <!-- Middle Content Area  End -->

                  <!-- Middle Content Area -->
                  <div class="col-md-6  col-md-6  col-xs-12 col-sm-6">
                     <div class="heading-panel">
                        <h3 class="main-title text-left">
                           Нэвтрэх
                        </h3>
                     </div>
                     <div class="content-info">
                     <!--  Form -->
                     <div class="form-grid">

                       <div class="row">
                          <div class="col-md-6  col-md-6  col-xs-12 col-sm-12">
                             <a href="/oauth/facebook" class="btn btn-theme btn-lg btn-block" style="background:#3b5998">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                Facebook
                             </a>
                          </div>
                          <div class="col-md-6  col-md-6  col-xs-12 col-sm-12">
                             <a href="/oauth/twitter" class="btn btn-theme btn-lg btn-block" style="background:#1da1f2">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                Twitter
                             </a>
                          </div>
                       </div>


                        <br>
                        <center>  - эсвэл -  </center>
                        <br>


                        {{ Form::open(array('id'=>'login_form', 'role'=>'form', 'route'=>'front.user.login')) }}
                          @if ($errors->has('login'))
                                <div class="error">{{ $errors->first('login', ':message') }}</div>
                                @endif
                           <div class="form-group">
                              <label>Имэйл хаяг</label>
                              {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Имэйл')) }}
                           </div>
                           <div class="form-group">
                              <label>Нууц үг</label>
                              {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Нууц үг', 'onkeydown'=>'if (event.keyCode == 13) { this.form.submit(); return false; }')) }}
                           </div>
                            <a class="btn btn-theme btn-lg btn-block" onclick="submitForm(); return false;" href="#">Нэвтрэх</a>
                            <a href="/profile/forgotPass" class="fancy fancybox.ajax"> Нууц дугаараа мартсан</a> </div>
                        {{ Form::close() }}


                     </div>
                     <!-- Form -->
                     </div>
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>

@stop