@extends($layout)

@section('main')

<div id="content" class="container">
    <div class="column2 clearfix">
      <div class="column">
        <div class="column_wrap">
          <h2>Нууц үг сэргээх</h2>
          
          <form action="/resetPass/{{$code}}" method="POST" class="form-signin">
          @if ($error)
                <div class="error">{{ $error}}</div>
                @endif
            <span class="error_list"></span>
            <div class="form-row">
              {{ Form::password('password', array('placeholder'=>'Шинэ нууц үг')) }}
            </div>
            <div class="form-row">
              {{ Form::password('password_com', array('placeholder'=>'Нууц үг давтах', 'onkeydown'=>'if (event.keyCode == 13) { this.form.submit(); return false; }')) }}
            </div>
            <div class="form-row" id="forgotPassword">
              <input class="btn btn-red" type="submit" value="Хадгалах">
            </div>  
          </form>
        </div>
      </div>
    </div>
  </div>
  <br clear="all"/>
  <br clear="all"/>
  <br clear="all"/>
  <br clear="all"/>
  <br clear="all"/>
  <br clear="all"/>
  <br clear="all"/>
@stop