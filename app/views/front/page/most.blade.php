<?php
use App\Models\Page;
$pages1 = Page::mpages1();
$pages2 = Page::mpages2();
$pages3 = Page::mpages3();
?>

<div class="bottomMargin {{ $from == 'homepage' ? '' : 'category-tab-1' }}">

<div class="tab-1">
    <ul class="nav">
        <li class="active"><a href="#tab1" data-toggle="tab">Шинэ мэдээ</a></li>
        <li class=""><a href="#tab2" data-toggle="tab">Их үзсэн</a></li>
        <li class=""><a href="#tab3" data-toggle="tab">Их сэтгэгдэлтэй</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab1">

            <div class="scroll-pane">
                <ul class="news-list-1">
                    @foreach ($pages1 as $page)
                    <li>
                        <a href="/n/{{ $page->pid }}" class="news-image">

                            @if($page->youtube_link)
                            <span class="icon-video"></span>
                            @endif

                            @if($page->is_uploads)
                            <span class="icon-gallery"></span>
                            @endif

                            <?php if($page->image_old): ?>
                                <img src="{{ $_ENV['baseurl'].'/pic/'.$page->image_old }}" alt="" />
                            <?php else: ?>
                                <img src="{{ '/files/uploads/'.$page->image }}" alt="" />
                            <?php endif; ?>
                        </a>
                        <div class="news-content">
                            <h4>
                                {{ $page->is_emergency ? '<span class="live">Шуурхай</span>' : '' }}
                                <a href="/n/{{ $page->id }}">{{ $page->title }}</a>
                            </h4>
                            <time>{{ time_ago($page->created_at) }}</time>
                            <ul class="news-meta">
                                <li><i class="fa fa-comments-o"></i><span>{{ $page->comment_count }}</span></li>
                                <li><i class="fa fa-share-square-o"></i><span>{{ $page->total }}</span></li>
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="tab-pane" id="tab2">
            <div class="scroll-pane">
                <ul class="news-list-1">

                    @foreach($pages2 as $page)
                    <li>
                        <a href="/n/{{ $page->pid }}" class="news-image">

                            @if($page->youtube_link)
                            <span class="icon-video"></span>
                            @endif

                            @if($page->is_uploads)
                            <span class="icon-gallery"></span>
                            @endif

                            <?php if($page->pimageold): ?>
                                <img src="{{ $_ENV['baseurl'].'/pic/'.$page->pimageold }}" alt="" />
                            <?php else: ?>
                                <img src="{{ '/files/uploads/'.$page->pimage }}" alt="" />
                            <?php endif; ?>
                        </a>
                        <div class="news-content">
                            <h4>
                                {{ $page->pis_emergency ? '<span class="live">Шуурхай</span>' : '' }}
                                <a href="/n/{{ $page->pid }}">{{ $page->ptitle }}</a>
                            </h4>
                            <time>{{ time_ago($page->pcreated_at) }}</time>
                            <ul class="news-meta">
                                <li><i class="fa fa-comments-o"></i><span>{{ $page->comment_count }}</span></li>
                                <li><i class="fa fa-share-square-o"></i><span>{{ $page->total }}</span></li>
                            </ul>
                        </div>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="tab-pane" id="tab3">
            <div class="scroll-pane">
                <ul class="news-list-1">
                    @foreach($pages3 as $page)
                    <li>
                        <a href="/n/{{ $page->pid }}" class="news-image">

                            @if($page->youtube_link)
                            <span class="icon-video"></span>
                            @endif

                            @if($page->is_uploads)
                            <span class="icon-gallery"></span>
                            @endif

                            <?php if($page->pimageold): ?>
                                <img src="{{ $_ENV['baseurl'].'/pic/'.$page->pimageold }}" alt="" />
                            <?php else: ?>
                                <img src="{{ '/files/uploads/'.$page->pimage }}" alt="" />
                            <?php endif; ?>
                        </a>
                        <div class="news-content">
                            <h4>
                                {{ $page->pis_emergency ? '<span class="live">Шуурхай</span>' : '' }}
                                <a href="/n/{{ $page->pid }}">{{ $page->ptitle }}</a>
                            </h4>
                            <time>{{ time_ago($page->pcreated_at) }}</time>
                            <ul class="news-meta">
                                <li><i class="fa fa-comments-o"></i><span>{{ $page->comment_count }}</span></li>
                                <li><i class="fa fa-share-square-o"></i><span>{{ $page->total }}</span></li>
                            </ul>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

</div>