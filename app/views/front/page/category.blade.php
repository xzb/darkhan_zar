@extends('front._layouts.page', array('cate' => $cate))

@section('main')


<div class="col col-1">

@if (sizeOf($fpages))
<div style="margin-bottom:30px;">
    <div class="cf-slider-container">

        <ul id="cf-slider">
            @foreach ($fpages as $fpage)
            <li>
                <div class="category-featured">
                    <article style="height: 360px; overflow: hidden;">
                        <a href="/n/{{$fpage->id}}" class="news-image">
                            <img src="{{ imagePath($fpage) }}" alt="" style="width: 680px"/>
                        </a>
                        <aside>
                            <h3><a href="/n/{{$fpage->id}}">{{$fpage->title}}</a></h3>
                        </aside>
                    </article>
                </div>
            </li>
            @endforeach;
        </ul>

        <script>
            $(document).ready(function() {
                $('#cf-slider').bxSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    slideMargin: 0,
                    useCSS: false,
                    hideControlOnEnd: true,
                    infiniteLoop: false,
                    controls: false,
                    adaptiveHeight: true
                });
            });
        </script>
    </div>
</div>
@endif


<div class="category-news-list-container">

    <div class="block-title green">
        <h4>{{ $category->name }}</h4>
    </div>
    @if (sizeOf($pages))
    <ul class="category-news-list">
        @foreach ($pages as $page)
        <li class="clearfix">
            <div class="news-image" style="overflow: hidden;">
                <a href="/n/{{$page->id}}">
                    <img src="{{ imagePath($page) }}" alt="" style="height: 150px; width: auto !important; max-width: none !important;"/>
                </a>
            </div>
            <div class="news-content">
                <h3><a href="/n/{{$page->id}}">{{ $page->title }}</a></h3>
                <time>{{ time_ago($page->created_at) }}</time>
                <p>{{ $page->description }}</p>
            </div>
        </li>
        @endforeach
    </ul>
    @else
    Мэдээ ороогүй байна.
    @endif

    {{ $pages->links(); }}
</div>    


</div>

<div class="col col-2">

    <div style="margin-bottom:10px;">
        <div class="banner-slider-container">
            @include('front.banner.show3',array('loc'=> 1))
        </div>
    </div>
    <div class="bottomMargin">
        @include('front.banner.show3',array('loc' => 2))
    </div>
    @include('front.page.most', array('from' => 'category'))
</div>

@stop