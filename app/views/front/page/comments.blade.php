<div class="comment-warning">
    <p>АНХААР! Та сэтгэгдэл бичихдээ хууль зүйн болон ёс суртахууныг баримтална уу. Ёс бус сэтгэгдлийг админ устгах
        эрхтэй. Мэдээний сэтгэгдэлд MEDEE.mn хариуцлага хүлээхгүй.</p>
</div>

<div class="news-comment-container">
<div class="news-comment-list-container">
<div class="block-title">
    <h4><b>17</b> Сэтгэгдэл</h4>
</div>
<ul class="comment-list">
<li>
    <div class="comment-box clearfix">
        <figure>
            <img alt="" src="/images/avatar.png" class="avatar">
        </figure>
        <aside>
            <div class="comment-head">
                <b class="name">Зочин</b>
                <time>17 минутын өмнө</time>
                <span class="ip">203.194.115.91</span>
            </div>
            <div class="comment-txt">
                <p>
                    solongos kinog odoo boli boli, hog...............
                </p>
            </div>
            <ul class="actions clearfix">
                <li>
                    <a class="action_btn plus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-up"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li>
                    <a class="action_btn minus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-down"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li><a href="#">Хариулах</a></li>
            </ul>
        </aside>
    </div>
</li>
<li>
    <div class="comment-box clearfix">
        <figure>
            <img alt="" src="/images/avatar.png" class="avatar">
        </figure>
        <aside>
            <div class="comment-head">
                <b class="name">Зочин</b>
                <time>17 минутын өмнө</time>
                <span class="ip">203.194.115.91</span>
            </div>
            <div class="comment-txt">
                <p>
                    Za ene harin bolj baina. Nomopol togtoogood baisan Mobicom ingewel l harin hereglegchee tatah
                    zorilgoor harin uilchilgee ni sajirah baih da
                </p>
            </div>
            <ul class="actions clearfix">
                <li>
                    <a class="action_btn plus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-up"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li>
                    <a class="action_btn minus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-down"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li><a href="#">Хариулах</a></li>
            </ul>
        </aside>
    </div>
</li>
<li class="level-2">
    <div class="comment-box clearfix">
        <figure>
            <img alt="" src="/images/avatar.png" class="avatar">
        </figure>
        <aside>
            <div class="comment-head">
                <b class="name">Зочин</b>
                <time>17 минутын өмнө</time>
                <span class="ip">203.194.115.91</span>
            </div>
            <div class="comment-txt">
                <p>
                    Za ene harin bolj baina. Nomopol togtoogood baisan Mobicom ingewel l harin hereglegchee tatah
                    zorilgoor harin uilchilgee ni sajirah baih da
                </p>
            </div>
            <ul class="actions clearfix">
                <li>
                    <a class="action_btn plus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-up"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li>
                    <a class="action_btn minus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-down"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li><a href="#">Хариулах</a></li>
            </ul>
        </aside>
    </div>

</li>
<li class="level-3">
    <div class="comment-box clearfix">
        <figure>
            <img alt="" src="/images/avatar.png" class="avatar">
        </figure>
        <aside>
            <div class="comment-head">
                <b class="name">Зочин</b>
                <time>17 минутын өмнө</time>
                <span class="ip">203.194.115.91</span>
            </div>
            <div class="comment-txt">
                <p>
                    Za ene harin bolj baina. Nomopol togtoogood baisan Mobicom ingewel l harin hereglegchee tatah
                    zorilgoor harin uilchilgee ni sajirah baih da
                </p>
            </div>
            <ul class="actions clearfix">
                <li>
                    <a class="action_btn plus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-up"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li>
                    <a class="action_btn minus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-down"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li><a href="#">Хариулах</a></li>
            </ul>
        </aside>
    </div>

    <div class="comment-form">
        <form>
            <div class="comment-form-box clearfix">
                <figure>
                    <span>
                        <img alt="" src="/images/avatar.png"
                             class="avatar">
                    </span>
                    <a class="btn-ref" href="#">
                        <em>Avatar Refresh</em>
                    </a>
                </figure>
                <aside>
                    <div class="cf-row">
                        <textarea placeholder="Таны сэтгэгдэл" class="comment-body"></textarea>
                    </div>
                    <div class="cf-row clearfix">
                        <div class="cfni">
                            <input type="text" placeholder="Таны нэр" class="comment-name">
                        </div>
                        <a class="btn btn-dark btn-cpost" href="#">Нийтлэх</a>
                    </div>
                </aside>
            </div>
        </form>
    </div>
</li>
<li>
    <div class="comment-box clearfix">
        <figure>
            <img alt="" src="/images/avatar.png" class="avatar">
        </figure>
        <aside>
            <div class="comment-head">
                <b class="name">Зочин</b>
                <time>17 минутын өмнө</time>
                <span class="ip">203.194.115.91</span>
            </div>
            <div class="comment-txt">
                <p>
                    Mongol ulsad hariltsaa holboonii uilchilgee uneguideed bgaa,,,, HHZH -oos ene baidliig
                    zogsoono... tsaash n uneguiduulehgui geed bga mortloo iim huuliin tosol demjuulchihdeg n sonin
                    yumaa.... odoo hereglegchee dugaartai n tatahiin tuld uneeree l orsoldonoshd
                </p>
            </div>
            <ul class="actions clearfix">
                <li>
                    <a class="action_btn plus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-up"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li>
                    <a class="action_btn minus" href="#">
                        <span class="icon"><i class="fa fa-thumbs-down"></i></span>
                        <span id="">3</span>
                    </a>
                </li>
                <li><a href="#">Хариулах</a></li>
            </ul>
        </aside>
    </div>
</li>
</ul>
</div>

<div class="comment-form-container">
    <div class="comment-form">
        <form>
            <div class="comment-form-box clearfix">
                <figure>
                    <span>
                        <img alt="" src="/images/avatar.png" class="avatar">
                    </span>
                    <a class="btn-ref" href="#">
                        <em>Avatar Refresh</em>
                    </a>
                </figure>
                <aside>
                    <div class="cf-row">
                        <textarea placeholder="Таны сэтгэгдэл" class="comment-body"></textarea>
                    </div>
                    <div class="cf-row clearfix">
                        <div class="cfni">
                            <input type="text" placeholder="Таны нэр" class="comment-name">
                        </div>
                        <a class="btn btn-dark btn-cpost" href="#">Нийтлэх</a>
                    </div>
                </aside>
            </div>
        </form>
    </div>
</div>
</div>