@extends('front._layouts.page', array('cate' => $cate))

@section('main')



<div class="col col-1">
<article class="news news-read">

    <div class="news-fixed-left" style="position: absolute;">
    <div class="author">
        <span class="author-img">
            <img alt="" src="/images/logo-avatar.png">
        </span>
        @if($user)
            <span class="author-name">{{$user->author}}</span>
        @else
            <span class="author-name">{{$page->author}}</span>
        @endif
    </div>
    </div>

    <div class="news-fixed-left" style="margin-top: 100px;">
        @include('front._partials.share', array('page_status' => $page_status))
    </div>

    <header class="news-header">
        <h1 class="news-title">{{ $page->title}}</h1>
        <time>{{ time_ago($page->created_at) }}</time>
        @if($page->image)
        <div class="news-featured-image">
            <img alt="" src="/files/uploads/{{ $page->image }}" style="max-width: 680px;">
            <!--
            <span class="imgDesc">
                Хурдан морь
            </span>
            -->
        </div>
        @endif
    </header>

    <script>
    $(document).ready(function( $ ) {
        var contentimg = $('.news_text_body img');
        if(contentimg.length) {
            $.each( contentimg, function( key, value ) {
                var src = this.src;
                var filename = src.split("/").pop();
                $(this).attr('src', '<?php echo $_ENV["baseurl"]?>/pic/' + filename);
                $(this).attr('width', '');
                $(this).attr('height', '');
            });
        }
        <?php if($page->image_old): ?>
            var author = $(".news_text_body strong").last().text();
            if(author.length < 15) {
                $(".author-name").text(author);
            }
        <?php endif; ?>

        $( ".shareable-twitter" ).click(function() {
            var shareText = $(this).text();
            var sharer = "https://twitter.com/intent/tweet?text=" + shareText + ' ';
            window.open(sharer + location.href, 'sharer', 'width=626,height=236');
        });


    });
    </script>

    <div class="news-entry news-container">
        @if(sizeof($more_news))
            <ul class="timeline">
                @foreach($more_news as $n)
                <li>
                    <span class="tl-time">
                        {{ date('H : i', strtotime($n->created_at)) }} <br />
                        {{ time_ago2($n->created_at) }}
                    </span>
                    <div class="tl-text">
                        @if($n->image)
                        <img src="/files/uploads/page/more/{{ $n->image }}" alt="" style="max-width: 50% !important;">
                        @endif
                        {{ $n->body }}
                    </div>
                </li>
                @endforeach
                <li>
                    <span class="tl-time">
                        {{ date('H : i', strtotime($page->created_at)) }} <br />
                        {{ time_ago2($page->created_at) }}
                    </span>
                    <div class="tl-text news_text_body">
                        {{ $page->body }}
                    </div>
                </li>
            </ul>
        @else
            <span class="news_text_body">
                {{ $page->body }}
            </span>
        @endif
        @include('front.page.photos', array('photos' => $photos))
    </div>
</article>

@include('front.page.relatednews', array('related_news' => $related_news))

@include('front._partials.share2', array('page_status' => $page_status))

@if($page->type == 1 && $page->is_comment)
@include('front.comment.show', array('page' => $page))
@endif

</div>

<div class="col col-2">

    <div style="margin-bottom:10px;">
        <div class="banner-slider-container">
            @include('front.banner.show3',array('loc'=> 1))
        </div>
    </div>
    <div class="bottomMargin">
        @include('front.banner.show3',array('loc' => 2))
    </div>

    @include('front.page.most', array('from' => 'page'))
</div>

@stop