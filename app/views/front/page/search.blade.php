@extends('front._layouts.page')

@section('main')


<div class="col col-1">

<div class="category-news-list-container">

    <div class="block-title green">
        <h4>Мэдээ хайх &raquo; {{{ $q }}}</h4>
    </div>
    @if (sizeOf($pages))
    <ul class="category-news-list">
        @foreach ($pages as $page)
        <li class="clearfix">
            <div class="news-image"><a href="/n/{{$page->id}}"><img src="{{ imagePath($page) }}" alt=""/></a></div>
            <div class="news-content">
                <h3><a href="/n/{{$page->id}}">{{ $page->title }}</a></h3>
                <time>{{ time_ago($page->created_at) }}</time>
                <p>{{ $page->description }}</p>
            </div>
        </li>
        @endforeach
    </ul>
    @else
    Мэдээ ороогүй байна.
    @endif

    {{ $pages->links(); }}
</div>    


</div>

<div class="col col-2">
    <div style="margin-bottom:10px;">
        <div class="banner-slider-container">
            @include('front.banner.show3',array('loc'=> 1))
        </div>
    </div>
    <div class="bottomMargin">
        @include('front.banner.show3',array('loc' => 2))
    </div>
    @include('front.page.most', array('from' => 'category'))
</div>

@stop