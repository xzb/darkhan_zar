@if(sizeof($photos))
<div class="news-photo-slide">
    <div class="news-photo-slider-container">

        <div class="slidesCntr">
            <span id="currentSlide">1</span>
            <span>/</span>
            <span id="totalSlides"></span>
        </div>

        <ul class="newsPhotoSlider">
            @foreach($photos as $photo)
            <li>
                <img src="/files/uploads/page/pictures/{{ $photo->filename }}" alt="" title="" />
            </li>
            @endforeach
        </ul>

        <script>
            $(document).ready(function() {
                slider = $('.newsPhotoSlider').bxSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    slideMargin: 0,
                    useCSS: false,
                    hideControlOnEnd: true,
                    infiniteLoop: false,
                    controls: true,
                    adaptiveHeight: true,
                    captions: true,
                    pager: false,
                    onSlideAfter: function($slideElement, oldIndex, newIndex) {
                        $('#currentSlide').html(newIndex + 1);
                    }
                });
                $('#totalSlides').html(slider.getSlideCount());
            });
        </script>
    </div>
</div>
@endif