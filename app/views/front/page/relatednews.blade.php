<?php use App\Models\Category; ?>

@if(sizeof($related_news))
<div class="related-news bottomMargin">
    <div class="block-title">
        <h4>Холбообой мэдээ</h4>
    </div>

    <div class="row">

        @foreach($related_news as $news)
        <?php $category = Category::find($news->page_id) ?>
        <div class="col-md-6 col-sm-6">
            <div class="news-item rel-news">
                <article>
                    @if($category)
                    <span class="news_cat">{{ $category->name }}</span>
                    @endif
                    <a href="/n/{{ $news->id }}" class="news_img gr-overlay">
                        <?php if($page->image_old): ?>
                            <img src="{{ $_ENV['baseurl'].'/pic/'.$news->image_old }}" alt="" />
                        <?php elseif($page->image): ?>
                            <img src="{{ '/files/uploads/'.$news->image }}" alt="" />
                        <?php else: ?>
                            <img src="http://dummyimage.com/300x185/EFEFEF/14151c&text=Зураггүй" alt="" />
                        <?php endif; ?>
                    </a>
                    <h3><a href="/n/{{ $news->id }}">{{ $news->title }}</a></h3>
                </article>
            </div>
        </div>
        @endforeach

    </div>
</div>
@endif

<style>
    .news-item {
        height: 185px !important;
        overflow: hidden;
    }
    .news-item article h3 {
        bottom: auto !important;
        top: 112px !important;
    }
</style>