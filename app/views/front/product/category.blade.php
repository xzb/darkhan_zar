<?php use App\Models\Category; ?>
<?php use App\Models\sfUser; ?>
<?php use App\Models\AttributeValues; ?>
<?php use App\Models\Banner; ?>
@extends('front._layouts.product', array('banner_id' => $banner_id))

@section('main')


<?php 
$cid = $category->id;
$breadcrumb = $category->getAncestorsAndSelf();
?>
<?php $headBanner = Banner::getCategoryBanner($category, 1);
      $banner_path = '/images/breadcrumb.jpg';

    if(count($headBanner)>0){
      $banner_path = '/files/uploads/banner/'.$headBanner->path;      
    }
      
?>      

<!-- =-=-=-=-=-=-= Transparent Breadcrumb =-=-=-=-=-=-= -->
  <!-- Small Breadcrumb -->
  @if(count($headBanner))
  <div class="bread-2 page-header-area" style ="background: rgba(159, 159, 159, 0) url({{$banner_path}}) no-repeat scroll center center / cover;">
     <div class="container">
        <div class="row">
           <div class="col-md-4 col-sm-7 col-xs-12">
              <div class="small-breadcrumb">
                    <div class=" breadcrumb-link">
                       <ul>
                          <li><a href="/">Нүүр хуудас</a></li>
                            @foreach($parents as $parentCategory)
                                <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
                            @endforeach
                            <li><a href="#">{{$category->name}}</a></li>
                       </ul>
                    </div>
              </div>
           </div>
        </div>
     </div>
  </div>
  @else

  <div class="small-breadcrumb">
     <div class="container">
        <div class=" breadcrumb-link">
           <ul>
              <li><a href="/">Нүүр хуудас</a></li>
                @foreach($parents as $parentCategory)
                    <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
                @endforeach
                <li><a href="#">{{$category->name}}</a></li>
           </ul>
        </div>
     </div>
  </div>

  @endif
  <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
  
 <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">


<section class="section-padding gray">
   <div class="container">
      <div class="row">


    <!-- Left Sidebar -->
    <div class="col-md-4 col-sm-12 col-sx-12">
       <!-- Sidebar Widgets -->
       <div class="sidebar">
          <!-- Panel group -->
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
             <!-- Categories Panel -->
                <!-- Heading -->
                <?php $i = 0; ?>
                @foreach($breadcrumb as $ca => $category)
                
                    <?php $i++; ?>
                    <?php if($ca == 0): else :?>
                        <?php $childs  = $category->getSiblingsAndSelf();?>
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="heading_{{$i}}">
                             <!-- Title -->
                             <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                <i class="more-less glyphicon glyphicon-plus"></i>
                                Ангилал
                                </a>
                             </h4>
                             <!-- Title End -->
                          </div>
                          <!-- Content -->
                          <div id="collapse{{$i}}" class="panel-collapse" role="tabpanel" aria-labelledby="heading_{{$i}}">
                             <div class="panel-body categories">
                                <ul>
                                   <?php foreach ($childs as $c): ?>
                                   <li><a href="/c/{{$c->id}}" <?php if($c->id == $category->id) ?> style="font-weight: 300; ">» &nbsp; <?php echo $c->name; ?></a></li> 
                                   <?php endforeach; ?>
                                </ul>
                             </div>
                          </div>
                        </div>
                    <?php endif;?>
                    @if($category->id == $cid && !$category->isLeaf())
                      <?php $leaves = Category::find($category->id)->descendants()->limitDepth(1)->get(); ?>
                      <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="heading_{{$i}}">
                         <!-- Title -->
                         <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                            <i class="more-less glyphicon glyphicon-plus"></i>
                            Ангилал
                            </a>
                         </h4>
                         <!-- Title End -->
                      </div>
                      <!-- Content -->
                      <div id="collapse{{$i}}" class="panel-collapse" role="tabpanel" aria-labelledby="heading_{{$i}}">
                         <div class="panel-body categories">
                            <ul>
                               <?php foreach ($leaves as $c): ?>
                               <li><a href="/c/{{$c->id}}">» &nbsp; <?php echo $c->name; ?></a></li> 
                               <?php endforeach; ?>
                            </ul>
                         </div>
                      </div>
                      </div>
                    @endif
                    
                @endforeach
            
             <!-- Categories Panel End -->
            <?php if(count($attributes)>0):?>
            <div class="panel panel-default" onclick="resetAll({{$category->id}}); return false;" id="resetAll" style="display: <?php if(count($avalids)>0){echo 'block';}else{echo 'none';}?>">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingfour_">
                   <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                      <i class="more-less glyphicon glyphicon-plus"></i>
                      Шүүлтүүр арилгах
                      </a>
                   </h4>
                </div>
            </div>

             <!-- Pricing Panel -->
             <div class="panel panel-default">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingfive_">
                   <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                      <i class="more-less glyphicon glyphicon-plus"></i>
                      Үнэ
                      </a>
                   </h4>
                </div>
                <!-- Content -->
                <div id="collapsefour" class="panel-collapse" role="tabpanel" aria-labelledby="headingfive">
                   <div class="panel-body">
                      <input id="price_min" type="text" name="price_min" value="" style="width: 70px;">
                      -
                      <input id="price_max" type="text" name="price_max" value="" style="width: 70px;">
                      <button onclick="gotoPriceFilter({{$category->id}});" style="height: 30px;"> <span class="glyphicon glyphicon-search"></span></button>
                   </div>
                </div>
             </div>
             <!-- Pricing Panel End -->
             @foreach($attributes as $attribute) 
             <!-- Brands Panel -->    
             <div class="panel panel-default">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="heading{{$attribute->id}}">
                   <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$attribute->id}}" aria-expanded="false" aria-controls="collapse{{$attribute->id}}">
                      <i class="more-less glyphicon glyphicon-plus"></i>
                      {{$attribute->name}}
                      </a>
                   </h4>
                </div>
                <!-- Content -->
                <?php $aValues = AttributeValues::where('attribute_id', '=', $attribute->id)->orderBy('sort_order', 'ASC')->get();?>
                <div id="collapse{{$attribute->id}}" class="panel-collapse" role="tabpanel" aria-labelledby="heading{{$attribute->id}}">
                   <div class="panel-body">
                      <!-- Brands List -->                              
                      <div class="skin-minimal">
                        <ul class="list">
                        @foreach($aValues as $aValue)
                          @if($aValue->value != 'Сонгоно уу')
                          <li><input id="checkbox{{$aValue->id}}" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttribute({{$category->id}},{{$aValue->id}}); return false;" <?php if(in_array($aValue->id, $avalids)){echo 'checked="checked"';}?>><label for="checkbox{{$aValue->id}}" class="auto_more_chk_lab">{{$aValue->value}}</label></li>
                          @endif
                        @endforeach
                        </ul>
                      </div>
                      <!-- Brands List End -->                 
                   </div>
                </div>
             </div>
             <!-- Brands Panel End -->
             @endforeach
          <?php endif;?>
             
          
                  
                  </div>
              <!-- panel-group end -->
           </div>
           <!-- Sidebar Widgets End -->
        </div>
        <!-- Left Sidebar End -->

          <!-- Middle Content Area -->
          <div class="col-md-8 col-lg-8 col-sx-12 white-bg">
             <!-- Row -->
              <div class="auto_more_heading mobile-hide" turul="category">
                  <h2 class="pull-left">{{$category->name}}</h2>
                  <div class="sorting pull-right">
                    <select id="sortType" name="sortType" style="font-size: 11px;width: 170px;" class="form-control-sorting" onchange="orderList({{$category->id}});">
                                  <option <?php if($sortType == 'price_desc'){echo 'selected="selected"';}?> value="price_desc">Үнэ буурахаар</option>
                                  <option <?php if($sortType == 'price_asc'){echo 'selected="selected"';}?> value="price_asc">Үнэ өсөхөөр</option>
                                  <option <?php if($sortType == 'date_asc'){echo 'selected="selected"';}?> value="date_asc">Нэмэгдсэн огноо өсөхөөр</option>
                                  <option <?php if($sortType == 'date_desc'){echo 'selected="selected"';}?> value="date_desc">Нэмэгдсэн огноо буурахаар</option>
                      <!-- <option value="year_asc">Орж ирсэн он өсөхөөр</option>
                      <option value="year_desc">Орж ирсэн он буурахаар</option> -->
                    </select>
                  </div>
                  <ul class="compare-ul list-inline pull-right">
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li><a href="/productCompare?ids=" class="btn  btn-haritsuulah compare-link">Харьцуулах</a></li>
                  </ul>
                  <input type="hidden" id="compare_ids" name="compare_ids" value="">
                  <div class="clearfix"></div>
            </div>
            <!-- shuuultuur end --> 
            
             <div class="row">
                <div class="clearfix"></div>
                <!-- Ads Archive -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                   <ul id="autolist">
                      @include('front.product._list', array('products' => $products, 'featuredProducts' => $featuredProducts, 'category' => $category, 'keyword'=>null, 'parent'=> $parents[0]))
                   </ul>
                   <!-- Pagination -->  
                   <div class="text-center margin-top-30 margin-bottom-30">
                      {{ $products->links(); }}
                   </div>
                   <!-- Pagination End -->   
                </div>
             </div>
             <!-- Row End -->
          </div>
          <!-- Middle Content Area  End -->
        </div>
      </div>
    </section>
@stop