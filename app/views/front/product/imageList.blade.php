<div class="popup-top-bar">
  <div class="fancybox-margin">
    <div class="container popup-top-bar-inner">
      <div class="row">
        <div class="popup-close">
          <span><a href="#" class="btn-popup-close" onclick="return false;">✖</a></span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div style="padding-top:50px;">
      <div class="ad-photos-container">
        <?php foreach ($images as $image): ?>
          <div>
            <img src="{{ $base_img_url.'/uploads/orig/'.$image->filename }}" alt="">
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>