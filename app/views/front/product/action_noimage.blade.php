<?php use App\Models\sfUser; ?>
<div class="row zar-editing">
<?php if (!sfUser::hasPid($product->id)): ?>
        <input id="product_id" type="hidden" name="pid" value="<?php echo $product->id ?>" />
        <div class="col-md-2 col-xs-4">
          <input class="form-control" id="product_password" type="text" autocomplete="off" name="password" placeholder="Нууц код"/>
          <div class="tooltip-btn-arrow first" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn first">Зарын нууц кодоо оруулж Засах, Устгах, Хэвлэх, Шинэчлэх, Онцлох болгох үйлдлийг хийх боломжтой.</div>
        </div>
        <div class="col-md-1 col-xs-2 "> <a href="#" id="product_edit" onclick="chdt({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-edit"></i> Засах </a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Та зарын утга, утсаа засах боломжтой.</div>
        </div>
        <div class="col-md-1 col-xs-2 "> <a href="#" id="product_delete" onclick="chdl({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-trash" ></i> Устгах</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn" >Өөрийн зараа Устгах боломжтой.</div>
        </div>
        <div class="col-md-1 col-xs-2  "> <a href="#" onclick="window.print()" class="btn btn-zar-editing" ><i class="fa fa-print" ></i> Хэвлэх</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Та зараа Хэвлэх боломжтой.</div>
        </div>
        <div class="col-md-1 col-xs-2"> <a href="#" id="product_renew" onclick="chrenew({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-refresh" ></i> Шинэчлэх </a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn" >Та зараа өдөрт 1 удаа сэргээх боломжтой.</div>
        </div>
        <div class="col-md-1 col-xs-2 "> <a href="#" id="product_edit" onclick="chds({{ $product->id }}); return false;" class="btn btn-zar-editing" >Онцлох болгох</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn" >Өөрийн зараа Онцлох зар болгоорой.</div>
        </div>
        <div class="col-md-5 col-xs-7"></div>
        <?php else: ?>
        <div class="col-md-2 col-xs-4 "> <a href="/e/<?php echo $product->id ?>" id="product_edit" class="btn btn-zar-editing" ><i class="fa fa-edit" ></i> Засах </a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Та зарын утга, утсаа засах боломжтой.</div>
        </div>
        <div class="col-md-2 col-xs-4 "> <a href="/del/<?php echo $product->id ?>?s=1" id="product_delete" class="btn btn-zar-editing" ><i class="fa fa-trash" ></i> Устгах</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Өөрийн зараа Устгах боломжтой.</div>
        </div>
        <div class="col-md-2 col-xs-4  "> <a href="#" onclick="window.print()" class="btn btn-zar-editing" ><i class="fa fa-print" ></i> Хэвлэх</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Та зараа Хэвлэх боломжтой.</div>
        </div>
        <div class="col-md-2 col-xs-4"> <a href="/renew/<?php echo $product->id ?>" class="btn btn-zar-editing" ><i class="fa fa-refresh" ></i> Шинэчлэх </a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn">Та зараа өдөрт 1 удаа сэргээх боломжтой.</div>
        </div>
        <div class="col-md-2 col-xs-4 "> <a href="/addBasket/<?php echo $product->id ?>" id="product_edit" class="btn btn-zar-editing" >Онцлох болгох</a>
          <div class="tooltip-btn-arrow" ><i class="glyphicon glyphicon-triangle-top"></i></div>
          <div class="tooltip-btn" >Өөрийн зараа Онцлох зар болгоорой.</div>
        </div>
        <div class="col-md-2"></div>
        <?php endif; ?>
        <div id="dialog" title="Анхааруулга!" style="display: none;">
          <p>Та нууц үгээ оруулах эсвэл нэвтэрч орж байж онцлох болгоно уу, дэлгэрэнгүй <a href="http://www.dazar.mn/page/featuredAd" target="_blank">энд</a> дарж уншина уу</p>
        </div>
      </div>
