<?php use App\Models\sfUser; ?>
<div class="popup-top-bar">
  <div class="fancybox-margin">
    <div class="container popup-top-bar-inner">
      <div class="row">
        <div class="popup-close">
          <span><a href="#" class="btn-popup-close" onclick="return false;">✖</a></span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <form class="form-signin" action="/product/salesave" method="POST">
    <input type="hidden" name="product_id" value="{{$product_id}}" id="product_id">
    <br/>
    <br/>
    <br/>
  <div class="form-row"> 
    <select name="percent" style="width: 60px;">
      <option value="5">5%</option>
      <option value="10">10%</option>
      <option value="10">20%</option>
      <option value="10">30%</option>
      </select>
  </div>
  <div class="form-row"> <input placeholder="Зарын гарчигаар хайх" name="keyword" type="text" style="width: 200px;" class="zar-search-txt"> </div>
    
  <div class="row">
    <div>
      <div class="ad-photos-container">
        <div class="ads-list-head1" style="margin-top: 120px;border-bottom: 1px solid #E31132;">
          <span class="ad-info">
            <span class="ad-title">Гарчиг</span>
          </span>

          <span class="ad-flds">
            <span class="ad-date">Огноо</span>
            <span class="ad-price1 ad-price">Үнэ</span>
            <span class="ad-contact1 ad-contact">Xолбоо бариx</span>
          </span>
        </div>


        <ul class="ads-list" style="border: none;">
          @include('front.product._list3', array('products' => $products))
        </ul>
        <input type="submit" value="Хямдруулах" class="btn btn-red" style="float: left;margin-left: 80px;margin-top: 10px;"/>
      </div>
    </div>
  </div>
  </form>
</div>
<script>
var saleTimer = null;
  var _salevalue = '';
  $('.zar-search-txt').keyup(function() {
    if (_salevalue === $(this).val().trim()) {
      return;
    }
    if (saleTimer !== null) {
      clearTimeout(saleTimer);
    }

    // value
    _salevalue = $(this).val().trim();

    saleTimer = setTimeout(function() {
      $.ajax({
        url: bm.prefix + '/product/salesearch',
        data: {keyword: _salevalue, product_id: $('#product_id').val()},
        type: 'POST',
        success: function(data) {
          $('.ads-list').html(data);
        }
      });
    }, 800);
  });
</script>