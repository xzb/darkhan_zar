@extends($layout)

@section('main')

@include('front._partials.auto_productform', array('cid' => ($category != null)?$category->id:0, 'cArray'=>$cArray, 'breadcrumb'=>$breadcrumb, 'product'=>$product,'is_edit'=>$is_edit, 'route'=>'front.product.storeauto', 'whos'=>$whos, 'attributes'=>$attributes, 'arrayPA'=>$arrayPA,'arrayPAV'=>$arrayPAV,'type'=>$type,'factory'=>$factory,'mark'=>$mark))
                      

@stop