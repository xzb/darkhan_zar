@extends('front._layouts.product')

@section('main')
<div id="content">

<div style="clear: both" class="ad-list-container">
  @include('front.product._list', array('products' => $products, 'category'=> null, 'keyword'=>null))
</div>


<!-- pager -->
<center>
{{ $products->appends(Input::except(array('page')))->links(); }}
</center>

</div>
@stop