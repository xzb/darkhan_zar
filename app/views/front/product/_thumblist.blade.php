<?php $i = 0;?>
@foreach($products as $product)
<?php $i ++;?>
@if($i%4 == 1)
  <div class="row">
@endif
        <div class="col-md-3">
            <div class="thumbnail">
                <a href="/p/{{ $product->id }}/{{  str_replace('/', '', $product->name) }}" class="img">
                  <?php if($product->image_filename):?>
                    <img alt="" src="{{ Croppa::url('/uploads/thumb/s_'.$product->image_filename, 163, 138) }}">
                  <?php else:?>
                    <img src="/images/nophoto.png" class="">                 
                  <?php endif;?>
                </a>
                <div class="details">
                    <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 40, 'UTF-8') }}</a>
                    <strong>{{ $product->price>999999999?"-":format_price($product->price) }}</strong>
                </div>
            </div>
        </div>
@if($i%4 == 0 || count($products) == $i)
   </div>
@endif
@endforeach

         