<?php use App\Models\sfUser; ?>

<div class="ads-list-head1" style="padding-top:{{$padding_top}}">
  <span class="ad-info">
    <span class="ad-title">Гарчиг</span>
  </span>

  <span class="ad-flds">
    <span class="ad-date">Огноо</span>
    <span class="ad-price1 ad-price">Үнэ</span>
    <span class="ad-contact1 ad-contact">Xолбоо бариx</span>
  </span>
</div>


<ul class="ads-list">
@foreach($products as $product)
<li class="ad ad-box-m {{ $product->image_filename ? 'hasPhoto' : '' }}">

<div class="clearfix">
  <div class="ad-info">
      @if($product->image_filename)
        <span class="ad-photo">
          <span class="photo">
            <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" alt="">
          </span>
        </span>
      @endif
      <h4 class="ad-title">
        <a target="_blank" href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</a>
      </h4>
  </div>
  <span class="ad-flds">
    <span class="fld fld-2 ad-phone">{{ mb_substr($product->contact, 0, 17, 'UTF-8'); }}</span>
    <span class="fld fld-1 ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</span>
    <span class="ad-date">{{ time_ago($product->created_at) }}</span>
    <span class="ad-action">
      <?php if (sfUser::hasPid($product->id)): ?>
        <a href="/renew/{{ $product->id }}"><img src="/images/icons/renew.png" alt="" title="Шинэчилэх"></a>
        <a href="/e/{{ $product->id }}"><img src="/images/icons/edit.png" alt="" title="Засах"></a>
        <a href="/del/{{ $product->id }}" onclick="return confirm('Та итгэлтэй байна уу?');"><img src="/images/icons/delete.png" alt="" title="Устгах"></a>
      <?php endif ?>
    </span>
  </span>
</div>
</li>
@endforeach
</ul>
