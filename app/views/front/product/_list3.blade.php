@foreach($products as $product)
        <li class="ad ad-box-m {{ $product->image_filename ? 'hasPhoto' : '' }}">
          <input type="radio" class="sale_check" style="margin-left: -45px;" name="sale_id" value="{{$product->id}}">
        <div class="clearfix">

          <div class="ad-info">
              @if($product->image_filename)
                <span class="ad-photo">
                  <span class="photo">
                    <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" alt="">
                  </span>
                </span>
              @endif
              <h4 class="ad-title">
                <a target="_blank" href="/p/{{ $product->id }}/{{  str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</a>
              </h4>
          </div>
          <span class="ad-flds">
            <span class="fld fld-2 ad-phone">{{ mb_substr($product->contact, 0, 17, 'UTF-8'); }}</span>
            <span class="fld fld-1 ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</span>
            <span class="ad-date">{{ time_ago($product->created_at) }}</span>
          </span>
        </div>
        </li>
        @endforeach