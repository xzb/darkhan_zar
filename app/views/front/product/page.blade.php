@extends($layout)
@section('main')

<div class="container">
  <h4 class="pd-l-15">{{ $page->title }}</h4>
</div>

<!------------ content body start -------------->
<div class="other-content-body container sitecontent">
  {{ $page->body }}
</div>
<!------------ content body end -------------->
@stop