<?php use App\Models\sfUser; ?>
<?php use App\Models\Profile; ?>
<?php use App\Models\Image; ?>

<!------------------ zar list start ---------------->
@foreach($featuredProducts as $product)
      <li class="ad" id="zar-id-{{$product->id}}">
         <div class="content-zone">
            <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="img-zone">
                  <div class="ribbon popular"></div>
                  @if($product->image_filename)
                    <img class="img-responsive" src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" alt="">
                  @else
                    <img class="img-responsive" src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
                  @endif
                  <div class="quick-view"> <a href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
               </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
               <div class="short-description-1 ">
                  <!-- Ad Title -->
                  <h3>
                     <a title="" href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}">

                      @if($parent && $parent->id == 19)
                        {{ ($product->name_more != '') ? $product->name_more : mb_substr($product->name, 0, 20, 'UTF-8'); }}
                      @else
                        {{mb_substr($product->name, 0, 40, 'UTF-8');}}
                      @endif
                     </a>
                  </h3>
                  <!-- Category Title -->
                  <div class="category-title"> <span>
                    <a href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}">
                      <?php $strs = explode('|', $product->is_stock_str);?>
                      <?php foreach($strs as $st):?>
                        <i class="glyphicon glyphicon-tag"></i><?php echo $st;?>&nbsp;
                      <?php endforeach;?>
                      <?php unset($strs, $st);?>
                    </a></span> 

                    <div class="right-side toggle-check-trigger"> 
                      <a class="{{ sfUser::hasInterest($product->id) ? 'saved' : 'btn-haritsuulah-circle'; }}" data-pid="{{ $product->id }}" onclick="return false;"> 
                      <i class="fa fa-bookmark-o"></i>
                        Хадгалах </a> 
                    </div>
                  </div>
                  <!-- Location -->
                  <p class="location"><i class="fa fa-map-marker"></i> {{ $product->getLocationName() }} </p>
                  <div class="zar-list-haritsuulah-form mobile-hide text-right">
                        <input id="compare_id_{{$product->id}}" class="auto_more_chk compare-select" type="checkbox" name="compareId[]" value="{{$product->id}}">
                        <label for="compare_id_{{$product->id}}">Харьцуулах</label>
                  </div>
                  <!-- Rating -->
                  <div class="rating"><p class="location"> {{ $product->created_at }}</p></div>
                  <!-- Price --><span class="ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</span> 
               </div>
            </div>
         </div>
      </li>
      @endforeach
      <!------------------ zar list end ----------------> 
      
      <!------------------ zar list start ---------------->
      @foreach($products as $product)
      <li class="ad" id="zar-id-{{$product->id}}">
         <div class="content-zone">
            <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="img-zone">
                  @if($product->image_filename)
                    <img class="img-responsive" src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" alt="">
                  @else
                    <img class="img-responsive" src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
                  @endif
                  <div class="quick-view"> <a href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
               </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
               <div class="short-description-1 ">
                 <!-- Ad Title -->
                 <h3>
                    <a title="" href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}">

                     @if($parent && $parent->id == 19)
                       {{ ($product->name_more != '') ? $product->name_more : mb_substr($product->name, 0, 20, 'UTF-8'); }}
                     @else
                       {{mb_substr($product->name, 0, 40, 'UTF-8');}}
                     @endif
                    </a>
                 </h3>
                  <!-- Category Title -->
                  <div class="category-title"> <span>
                    <a href="/p/{{ $product->id }}/{{   str_replace('%','хувь',str_replace('/', '', $product->name))  }}">
                      <?php $strs = explode('|', $product->is_stock_str);?>
                      <?php foreach($strs as $st):?>
                        <i class="glyphicon glyphicon-tag"></i><?php echo $st;?>&nbsp;
                      <?php endforeach;?>
                      <?php unset($strs, $st);?>
                    </a></span> 
                    <div class="right-side toggle-check-trigger"> 
                      <a class="{{ sfUser::hasInterest($product->id) ? 'saved' : 'btn-haritsuulah-circle'; }}" data-pid="{{ $product->id }}" onclick="return false;"> 
                      <i class="fa fa-bookmark-o"></i>
                        Хадгалах </a> 
                    </div>
                  </div>
                  <!-- Location -->
                  <p class="location"><i class="fa fa-map-marker"></i> {{ $product->getLocationName() }} </p>
                  <div class="zar-list-haritsuulah-form mobile-hide text-right">
                        <input id="compare_id_{{$product->id}}" class="auto_more_chk compare-select" type="checkbox" name="compareId[]" value="{{$product->id}}">
                        <label for="compare_id_{{$product->id}}">Харьцуулах</label>
                  </div>
                  <!-- Rating -->
                  <div class="rating"><p class="location"> {{ $product->created_at }}</p></div>
                  <!-- Price --><span class="ad-price">{{ $product->price>999999999?"-":format_price($product->price) }}</span> 
               </div>
            </div>
         </div>
      </li>
      @endforeach
      <!------------------ zar list end ----------------> 
