@extends($layout)

@section('main')

@include('front._partials.property_productform', array('cid' => ($category != null)?$category->id:0, 'cArray'=>$cArray, 'breadcrumb'=>$breadcrumb, 'product'=>$product,'is_edit'=>$is_edit, 'route'=>'front.product.storeproperty', 'whos'=>$whos, 'attributes'=>$attributes, 'arrayPA'=>$arrayPA,'arrayPAV'=>$arrayPAV,'parentLoc'=>$parentLoc,'parentId'=>$parentId,'locations'=>$locations,'locations2'=>$locations2,'select1'=>$select1,'select2'=>$select2,))
                      

@stop