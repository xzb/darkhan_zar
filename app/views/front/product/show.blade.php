<?php use App\Models\Category; ?>
<?php use App\Models\AttributeValues; ?>
<?php use App\Models\ProductAttribute; ?>
<?php 
use App\Models\sfUser;  
use App\Models\ProductSale; 
use App\Models\ProductComment;?>

@extends('front._layouts.product')

@section('main')

<?php $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

<div class="small-breadcrumb">
   <div class="container">
      <div class=" breadcrumb-link">
         <ul>
            <li><a href="/">Нүүр хуудас</a></li>
            @foreach($parents as $parentCategory)
                <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
            @endforeach
            <li><a>{{$category->name}}</a></li>
         </ul>
      </div>
   </div>
</div>


 <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bgs gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-8 col-xs-12 col-sm-12">
                     <!-- Single Ad -->
                     <div class="single-ad">
                        <!-- Title -->
                        <div class="ad-box">
                           <h1><?php echo $product->name; ?></h1>
                           <div class="short-history">
                              <ul>
                                 <li>Нийтэлсэн: <b>{{ date("Y-m-d", strtotime($product->created_at)) }}</b></li>
                                 <li>Ангилал: <b><a href="/{{$category->id}}">{{$category->name}}</a></b></li>
                                 <li>Байрлал: <b>{{$product->getLocationName()}}</b></li>
                              </ul>
                           </div>
                        </div>
                        

                        @if($images)
                        <!-- Listing Slider  --> 
                        <div class="flexslider single-page-slider">
                           <div class="flex-viewport">
                              <ul class="slides slide-main">
                                <?php foreach ($images as $idx => $image): ?>
                                  <li><img alt="" src="{{ $base_img_url.'/uploads/orig/'.$image->filename }}" title=""></li>
                                <?php endforeach; ?>
                              </ul>
                           </div>
                        </div>
                        <!-- Listing Slider Thumb --> 
                        <div class="flexslider" id="carousels">
                           <div class="flex-viewport">
                              <ul class="slides slide-thumbnail">
                                <?php foreach ($images as $idx => $image): ?>
                                   <li><img alt="" draggable="false" src="{{ $base_img_url.'/uploads/thumb/'.$image->filename }}" title=""></li>
                                <?php endforeach; ?>
                              </ul>
                           </div>
                        </div>
                        @endif

                        @include('front.product.action', array('product' => $product))
                        <!-- Share Ad  --> 
                        
                        
                        <!-- Short Description  --> 
                        <div class="ad-box">
                           <!-- Short Features  --> 
                           <div class="short-features">
                                 <strong>Дэлгэрэнгүй</strong>
                                 </hr>
                           </div>
                           <div class="desc-points">
                               @foreach($attributes as $key=>$attribute)
                                <?php $attributeValue = ProductAttribute::getProductAttrValues($product->id, $attribute->id, $attribute->type);?>
                                @if($attributeValue)
                                  @if($attributeValue != 'Сонгоно уу')
                                    <div class="col-xs-5">{{$attribute->name}}</div>
                                    <div class="col-xs-7"><i class="glyphicon glyphicon-tag"></i>{{$attributeValue}}</div>
                                    <div class="clearfix"></div>
                                  @endif
                                @endif
                                @endforeach
                           </div>
                           <!-- Ad Specifications --> 
                           <div class="specification">
                              <p>
                                 {{ $product->description }}
                              </p>
                           </div>
                           
                        </div>
                     </div>
                     <!-- Single Ad End --> 
                  </div>
                  <!-- Right Sidebar -->
                  <div class="col-md-4 col-xs-12 col-sm-12">
                     <!-- Sidebar Widgets -->
                     <div class="sidebar">
                        <!-- Contact info -->
                        <div class="contact white-bg">
                           <!-- Email Modal -->
                           <button class="btn-block btn-contact contactPhone number" data-last="{{substr($product->contact,4,strlen($product->contact)-4)}}" >{{substr($product->contact,0,4)}}<span> .. дугаар харах</span></button>
                        </div>
                        <!-- Price info block -->   
                        <div class="ad-listing-price">
                           <p>{{ format_price($product->price) }}</p>
                        </div>

                        @if($product->getUser())
                        <!-- User Info -->
                        <div class="white-bg user-contact-info">
                           <div class="user-info-card">
                              <div class="user-photo col-md-4 col-sm-3  col-xs-4">
                                 <img src="/images/profile.png" alt="">
                              </div>
                              <div class="user-information no-padding col-md-8 col-sm-9 col-xs-8">
                                 <span class="user-name"><a class="hover-color" href="profile.html">{{$product->getUser()->name}}</a></span>
                                 <div class="item-date">
                                    <a href="#" class="link">Бусад зарууд</a>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                        @endif

                        <!-- Recent Ads --> 
                        <div class="widget">
                           <div class="widget-heading">
                              <h4 class="panel-title"><a>Төстэй зарууд</a></h4>
                           </div>
                           <div class="widget-content recent-ads">
                              
                              <!-- Ads -->
                              @foreach($other as $o=>$oProduct)
                              <div class="recent-ads-list">
                                 <div class="recent-ads-container">
                                    <div class="recent-ads-list-image">
                                       <a href="/p/{{$oProduct->id}}/{{str_replace('/', '', $oProduct->name)}}" class="recent-ads-list-image-inner">
                                        @if($oProduct->image_filename)
                                        <img src="{{ $base_img_url.'/uploads/thumb/'.$oProduct->image_filename }}" alt="">
                                        @else
                                        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
                                        @endif
                                       </a><!-- /.recent-ads-list-image-inner -->
                                    </div>
                                    <!-- /.recent-ads-list-image -->
                                    <div class="recent-ads-list-content">
                                       <h3 class="recent-ads-list-title">
                                          <a href="/p/{{$oProduct->id}}/{{str_replace('/', '', $oProduct->name)}}">{{  mb_substr($oProduct->name, 0, 20, 'UTF-8') }}</a>
                                       </h3>
                                       <div class="recent-ads-list-price">
                                          {{ $oProduct->price>999999999?"-":format_price($oProduct->price) }}
                                       </div>
                                       <!-- /.recent-ads-list-price -->
                                    </div>
                                    <!-- /.recent-ads-list-content -->
                                 </div>
                                 <!-- /.recent-ads-container -->
                              </div>
                              @endforeach
                              <!-- ads end -->

                           </div>
                        </div>
                     </div>
                     <!-- Sidebar Widgets End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>

@stop