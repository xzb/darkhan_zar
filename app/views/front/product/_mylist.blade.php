<?php use App\Models\sfUser; ?>
@if($products)  
<table class="table table-bordered table-striped"  >
            <thead>
                 <tr>
                   <th class="col-xs-1 ">Зураг</th>
                   <th class="col-xs-10 " colspan="1">Гарчиг</th>
                   <th class="col-xs-1 ">Үйлдэл</th>
 
                 </tr>
            </thead>
       
                 <tbody>
<?php $tot = 0; ?>                 
@foreach($products as $product)
                    <tr>
                      <td>
                      
                      @if($product->image_filename)
                        <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" class="w100" alt="">
                      @else
                        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" class="w100" alt="">
                      @endif
                      <td  class="vert-align">
                      <h4><a target="_blank" href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</a></h4>
                      <time>{{ $product->created_at }}</time> 
                      </td>
                        <td class="vert-align text-center col-xs-1">
                        @if (sfUser::hasPid($product->id))
                          <a href="/renew/{{ $product->id }}"><img src="/images/icons/renew.png" alt="" title="Шинэчилэх"></a>
                          <a href="/e/{{ $product->id }}"><img src="/images/icons/edit.png" alt="" title="Засах"></a>
                          <a href="/del/{{ $product->id }}" onclick="return confirm('Та итгэлтэй байна уу?');"><img src="/images/icons/delete.png" alt="" title="Устгах"></a>
                        @endif
                        </td>
                    </tr>
                    
@endforeach                  
                 </tbody>
           </table>
@endif   

@if($product->is_featured == 1) 
  <div class ="bottom_shadow"> </div>
@endif
