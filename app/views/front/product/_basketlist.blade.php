<?php use App\Models\sfUser; use App\Models\Category;?>
@if($products)  
<table class="table table-bordered table-striped"  >
            <thead>
                 <tr>
                   <th class="col-xs-1 ">Зураг</th>
                   <th class="col-xs-10 " colspan="3">Гарчиг</th>
 
                 </tr>
            </thead>
       
                 <tbody>
<?php $tot = 0; ?>                 
@foreach($products as $product)
  <?php $grand_id = Category::getGrandParentId($product->category_id);?>                 
                    <tr>
                      <td>
                      
                      @if($product->image_filename)
                        <img src="{{ $base_img_url.'/uploads/thumb/'.$product->image_filename }}" class="w100" alt="">
                      @else
                        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" class="w100" alt="">
                      @endif
                      <td  class="vert-align">
                      <h4><a target="_blank" href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}">{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</a></h4>
                      <time>{{ $product->created_at }}</time> 
                      </td>
                      <td class="vert-align col-xs-5">
                       <select class="form-control" rel="{{$product->id}}" name="pVal{{$product->id;}}">
                          <?php $thisPrice = $prices[$grand_id];?>
                          <?php for($i=1; $i<=4; $i++):?>
                          <option value="{{$i*$thisPrice}}">{{$i}} долоо хоног {{$i*$thisPrice}}төг</option>
                          <?php endfor;?>
                      </select>
                      </td>
                        <td class="vert-align text-center col-xs-1">
                        <a href="/removeBasket/{{$product->id}}"  onclick="return confirm('Та итгэлтэй байна уу?');" class="btn btn-xs"  data-toggle="tooltip"  title="хасах"  ><span class="  fa fa-trash"></span> </a>
                        </td>
                    </tr>
                    <?php $tot += $thisPrice;?>
@endforeach                  
                     <tr >
                       <td  colspan="4" class=" text-right"> <h4><b> Нийт төлбөр : <span class="red">{{ $tot }}</span></b></h4> </td>
                     </tr>
                    <tr >
                       <td  colspan="4" >
                         
                              <fieldset style="padding-left: 80px;">
                                      <h4 style="padding-bottom:20px">Төлбөр xийx төрөл</h4>
                                      <div style="width: 400px;float: left">
                                        <div class="field">
                                <input type="radio" name="payment_type" value="golomt" id="golomt" checked> <label for="golomt">Голомт банкны картаар</label> <br />
                                <!-- <input type="radio" name="payment_type" value="tdbm" id="tdbm"> <label for="tdbm">Худалдаа хөгжлийн банкны картаар</label> <br /> -->
                                <!-- <input type="radio" name="payment_type" value="saving" id="saving"> <label for="saving">Төрийн банкны картаар</label> <br /> -->
                                <input type="radio" name="payment_type" value="transfer" id="transfer"> <label for="transfer">Дансаар шилжүүлэх </label>
                                        </div>
                                      </div>
                                      <div style="float: right; margin-right: 114px; margin-top: 40px;">
                            <button class="btn btn-red">Төлбөр төлөх</button>
                          </div>   
                                    </fieldset>

                       </td>
                     </tr>
                 </tbody>
           </table>
@endif           
@if($coupons)
    @foreach($coupons as $coupon)
    <li class="ad ad-box-m hasPhoto">

    <div class="clearfix">
      <div class="ad-info">
          @if($coupon->image_filename)
            <span class="ad-photo">
              <span class="photo">
                <img src="{{ $base_img_url.'/uploads/thumb/'.$coupon->image_filename }}" alt="">
              </span>
            </span>
          @else
              <span class="ad-photo">
              <span class="photo">
                <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
              </span>
            </span>
          @endif
          <h4 class="ad-title">
            <a target="_blank" href="/p/{{ $coupon->id }}/{{ str_replace('/', '', $coupon->name) }}">{{  mb_substr($coupon->name, 0, 100, 'UTF-8') }}</a>
            
          </h4>
          <span style="margin-left:-100px;margin-top:30px;color:gray">{{$coupon->coupon_percent}}% ийн хямдралын купон</span>
      </div>
      <span class="ad-action" style="float: right; margin-top: 10px; margin-right: 10px;">
        <a href="/removeBasket/{{$coupon->id}}?is_coupon=1" onclick="return confirm('Та итгэлтэй байна уу?');"><img src="/images/icons/delete.png" alt="" title="Устгах"></a>
      </span>
      <span class="ad-flds" style="width: 300px !important;">
        <span class="ad-date" style="float:right;width:50px;text-align:left;">
          <div class="form-row"><div class="form-cols-4 clearfix "><div class="form-col product-category-main"> 
             {{$coupon->coupon_price}}төг
          <input type="hidden" class="weekprice" value="{{$coupon->coupon_price}}"/>
          </div>  </div> </div>
          <?php $tot += $coupon->coupon_price;?>
        </span>
      </span>
      
    </div>
    </li>
    @endforeach
@endif
