@extends('front._layouts.product')

@section('main')


<!-- =-=-=-=-=-=-= Transparent Breadcrumb =-=-=-=-=-=-= -->
  <!-- Small Breadcrumb -->
  <div class="bread-2 page-header-area" style ="background: rgba(159, 159, 159, 0) url(/images/breadcrumb.jpg) no-repeat scroll center center / cover;">
     <div class="container">
        <div class="row">
           <div class="col-md-4 col-sm-7 col-xs-12" > 
              <div class="small-breadcrumb">
                    <div class=" breadcrumb-link">
                       <ul>
                          <li><a href="/">Нүүр хуудас</a></li>
                          <li><a href="#">Хайлтын үр дүн</a></li>
                       </ul>
                    </div>
              </div>
           </div>
        </div>
     </div>
  </div>
  <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">


<section class="section-padding gray">
   <div class="container">
      <div class="row">

          <!-- Middle Content Area -->
          <div class="col-md-12 col-lg-12 col-sx-12 white-bg">
             <!-- Row -->
             <div class="row">
                <div class="clearfix"></div>
                <!-- Ads Archive -->
                <div class="col-md-8 col-sm-8 col-xs-12">
                
                @if(count($featuredProducts)>0)

                   <ul id="autolist">
                      @include('front.product._list', array('featuredProducts' => $featuredProducts, 'category' => null, 'keyword'=>$keyword,'parent'=>null))
                   </ul>
                   <!-- Pagination -->  
                   <div class="text-center margin-top-30 margin-bottom-30">
                      {{ $pager->appends(Input::except(array('page')))->links(); }}
                   </div>

                @elseif(count($products)>0)
                
                   <ul id="autolist">
                      @include('front.product._list', array('products' => $products, 'category' => null, 'keyword'=>$keyword,'parent'=>null))
                   </ul>
                   <!-- Pagination -->  
                   <div class="text-center margin-top-30 margin-bottom-30">
                      {{ $pager->appends(Input::except(array('page')))->links(); }}
                   </div>
                @else
                <center style="padding:30px 0 40px 0"><span class="blank_result">Хайлт илэрцгүй. Зөвлөмж: Үгийг зөв бичсэн эсэхийг шалгана уу мөн өөр үгээр хайж үзнэ үү</span></center>
                @endif





                   <!-- Pagination End -->   
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12">
                  
                </div>
             </div>
             <!-- Row End -->
          </div>
          <!-- Middle Content Area  End -->
        </div>
      </div>
    </section>




@stop