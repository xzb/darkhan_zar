@extends($layout)

@section('main')

@include('front._partials.productform', array('cid' => ($category != null)?$category->id:0, 'cArray'=>$cArray, 'breadcrumb'=>$breadcrumb, 'product'=>$product,'is_edit'=>$is_edit, 'route'=>'front.product.store', 'whos'=>$whos, 'attributes'=>$attributes, 'arrayPA'=>$arrayPA,'referrer'=>$referrer, 'parentLocations'=>$parentLocations))

@stop
