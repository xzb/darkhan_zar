<?php use App\Models\sfUser; ?>
<div class="ad-share text-center">
<?php if (!sfUser::hasPid($product->id)): ?>
  <input id="product_id" type="hidden" name="pid" value="<?php echo $product->id ?>" />
   <div class="ad-box-action col-md-4 col-sm-3 col-xs-6">
      <input class="form-control" id="product_password" type="text" autocomplete="off" name="password" placeholder="Нууц код"/>
   </div>
   <div class="ad-box-action col-md-2 col-sm-3 col-xs-6">
      <a href="#" id="product_edit" onclick="chdt({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-edit"></i> Засах </a>
   </div>
   <div class="ad-box-action col-md-2 col-sm-3 col-xs-6">
       <a href="#" id="product_delete" onclick="chdl({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-trash" ></i> Устгах</a>
   </div>
   <div class="ad-box-action col-md-2 col-sm-3 col-xs-6">
       <a href="#" id="product_renew" onclick="chrenew({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-refresh" ></i> Шинэчлэх </a>
   </div>
   <div class="ad-box-action col-md-2 col-sm-3 col-xs-6">
       <a href="#" id="product_edit" onclick="chds({{ $product->id }}); return false;" class="btn btn-zar-editing" ><i class="fa fa-star" ></i>Онцлох</a>
   </div>
   <?php else: ?>
    <div class="ad-box-action col-md-3 col-sm-3 col-xs-6">
      <a href="/e/<?php echo $product->id ?>" id="product_edit" class="btn btn-zar-editing" ><i class="fa fa-edit" ></i> Засах </a>
   </div>
   <div class="ad-box-action col-md-3 col-sm-3 col-xs-6">
      <a href="/del/<?php echo $product->id ?>?s=1" id="product_delete" class="btn btn-zar-editing" ><i class="fa fa-trash" ></i> Устгах</a>
   </div>
   <div class="ad-box-action col-md-3 col-sm-3 col-xs-6">
       <a href="/renew/<?php echo $product->id ?>" class="btn btn-zar-editing" ><i class="fa fa-refresh" ></i> Шинэчлэх </a>
   </div>
   <div class="ad-box-action col-md-3 col-sm-3 col-xs-6">
       <a href="/addBasket/<?php echo $product->id ?>" id="product_edit" class="btn btn-zar-editing" >Онцлох</a>
   </div>
 <?php endif; ?>
</div>
<div id="dialog" title="Анхааруулга!" style="display: none;">
  <p>Та нууц үгээ оруулах эсвэл нэвтэрч орж байж онцлох болгоно уу</p>
</div>
