<?php use App\Models\Product; ?>
<?php use App\Models\Category; ?>
<?php $next = false;
  $prev = false;?>
@if(Cookie::get('show_profile_id'))
<?php

  $p_cate_id  = Cookie::get('show_profile_id');
  
  if(Cache::has('profile_id'.$p_cate_id)){

    $productArr = Cache::get('profile_id'.$p_cate_id);
    $myIndex = array_search($product->id,$productArr);

    $prev = array_key_exists($myIndex+1,$productArr) ;
    if($prev)
      $prevId = $productArr[$myIndex+1];

    $next = array_key_exists($myIndex-1,$productArr) ;
    if($next)
      $nextId = $productArr[$myIndex-1];
  }
?>
@elseif(Cookie::get('cate_id'))
<?php
  $cate_id  = Cookie::get('cate_id');
  $category = Category::find($cate_id);

  if(Cache::has('id'.$cate_id)){
    
    $productArr = Cache::get('id'.$cate_id);
    $myIndex = array_search($product->id,$productArr);


    $prev = array_key_exists($myIndex+1,$productArr) ;
    if($prev)
      $prevId = $productArr[$myIndex+1];

    $next = array_key_exists($myIndex-1,$productArr) ;
    if($next)
      $nextId = $productArr[$myIndex-1];

  }

?>
@elseif(Cookie::get('search'))
<?php 
$keyword = Cookie::get('search');

if(Cache::has($keyword)){
    
    $productArr = Cache::get($keyword);
    $myIndex = array_search($product->id,$productArr);


    $prev = array_key_exists($myIndex+1,$productArr) ;
    if($prev)
      $prevId = $productArr[$myIndex+1];

    $next = array_key_exists($myIndex-1,$productArr) ;
    if($next)
      $nextId = $productArr[$myIndex-1];

  }
?>
@endif

@if(Cookie::get('cate_id')||Cookie::get('search')||Cookie::get('show_profile_id'))
<div class="arrows overlay-arrows" style="transition: all 0s ease 0s; opacity: 1;">
  @if($next)
  <a href="/p/{{ $nextId }}" rel="prev" class="overlay-content-arrows overlay-content-arrows-previous-wrap">
    <div class="overlay-arrow-previous overlay-arrows-anchor">
      <div class="overlay-arrows-previous-anchor-image"></div>
    </div>
  </a>
  @endif
  @if($prev)
  <a href="/p/{{ $prevId }}" rel="next" class="overlay-content-arrows overlay-content-arrows-next-wrap">
    <div class="overlay-arrow-next overlay-arrows-anchor">
      <div class="overlay-arrows-next-anchor-image"></div>
    </div>
  </a>
  @endif
</div>

<script type="text/javascript">
var pnext = {{ $next ? $nextId : 0 }};
var pprev  = {{ $prev ? $prevId : 0 }};
</script>
@endif