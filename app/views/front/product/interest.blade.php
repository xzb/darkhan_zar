@extends('front._layouts.product')

@section('main')


<!-- =-=-=-=-=-=-= Transparent Breadcrumb =-=-=-=-=-=-= -->
<div class="small-breadcrumb">
   <div class="container">
      <div class=" breadcrumb-link">
         <ul>
            <li><a href="/">Нүүр хуудас</a></li>
            <li><a href="#">Хадгалсан зарууд</a></li>
         </ul>
      </div>
   </div>
</div>
  <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">


<section class="section-padding gray">
   <div class="container">
      <div class="row">

          <!-- Middle Content Area -->
          <div class="col-md-12 col-lg-12 col-sx-12 white-bg">
             <!-- Row -->
             <div class="row">
                <div class="clearfix"></div>
                <!-- Ads Archive -->
                <div class="col-md-8 col-sm-8 col-xs-12">
                
                @if(count($products)>0)
                
                   <ul id="autolist">
                      @include('front.product._list', array('products' => $products, 'featuredProducts' => array(), 'category' => null, 'keyword'=>null,'parent'=>null))
                   </ul>
                   <!-- Pagination -->  
                   <div class="text-center margin-top-30 margin-bottom-30">
                      {{ $products->appends(Input::except(array('page')))->links(); }}
                   </div>
                @else
                <center style="padding:30px 0 40px 0"><span class="blank_result">Хадгалсан зар байхгүй байна</span></center>
                @endif

                   <!-- Pagination End -->   
                </div>
                
             </div>
             <!-- Row End -->
          </div>
          <!-- Middle Content Area  End -->
        </div>
      </div>
    </section>






 

@stop