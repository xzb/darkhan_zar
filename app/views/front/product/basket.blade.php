@extends('front._layouts.product')

@section('main')

<div class="other-content-body container sitecontent  login-user-body" style="margin-top:25px;" >
<form action="/basket" method="post">
   
  @include('front.product._basketlist', array('products' => $products, 'coupons'=>$coupons, 'prices'=> $prices))     
           
</form>   
</div>
@stop