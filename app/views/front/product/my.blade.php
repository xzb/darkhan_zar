@extends('front._layouts.product')

@section('main')

<div class="other-content-body container sitecontent  login-user-body" style="margin-top:25px;" id="content">
<h2>Миний оруулсан зарууд</h2>
<br />

<div style="clear: both" class="ad-list-container">
  @include('front.product._mylist', array('products' => $products, 'category'=> null, 'keyword'=>null))
</div>
<br />
<form action="/basket">
<button class="btn btn-red">Онцлох болгох</button>
</form>
</div>
@stop