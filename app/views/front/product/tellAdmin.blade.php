@extends($layout)

@section('main')
@include('front.product._css', array('category'=>null))
@include('front.product._search', array('keyword'=>null))

<div class="other-content-body container sitecontent admind-medegdeh" >
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h3>Та энэхүү зарыг яагаад Админд мэдэгдэх болсон бэ?</h3>
    <form action="/tellAdmin/{{$product_id}}" method="POST">
            <input type="hidden" name="id" id ="id" value="{{$product_id}}"/>
		 	  <div class="form-group">
			    <label for="phone_number">Утасны дугаар</label>
			    <input type="text" class="form-control"  id="phone_number" name="phone_number" placeholder="Утасны дугаар">
			  </div>
			   <p class="antispam">Leave this empty: <input type="text" name="url" /></p>
			  <div class="form-group">
			    <label for="msgs">Шалтгаан</label>
			    <textarea class="form-control" id="msgs" name="msgs"></textarea>
			  </div>
			  

			<div class="text-right">  <button type="submit" class="btn ">Илгээх</button></div>

	</form>
    </div>
  </div>
</div>

@stop
