<?php
use App\Models\Category;
$category = Category::find($cid);


$breadcrumb = $category->getAncestorsAndSelf();
?>
<div class="bm-bread-crumb">
<ul class="clearfix">
  <li><a class="loading-bar2" href="/">Нүүр xуудас</a></li>

  @foreach($breadcrumb as $category)

  <?php $childs  = $category->getSiblingsAndSelf();?>

  <li><span><i class="fa fa-angle-right"></i></span></li>
  <li>
      <?php if ($category->id):?>
        <a href="/c/<?php echo $category->id ?>/<?php echo $category->name; ?>" class="loading-bar2"><?php echo $category->name; ?><?php if (!$category->isLeaf()): ?><i class="fa fa-angle-down"></i><?php endif; ?></a>
      <?php else:?>
        <b><?php echo $category->name; ?><?php if (!$category->isLeaf()): ?><i class="fa fa-angle-down"></i><?php endif; ?></b>
      <?php endif?>
      <div class="bm-dropdown">
        <ul>
          <?php foreach ($childs as $c): ?>
            <li>
              <a href="/c/<?php echo $c->id?>/<?php echo $c->name; ?>"><?php echo $c->name; ?></a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
  </li>

  @if($category->id == $cid && !$category->isLeaf())
  <?php $leaves = Category::find($category->id)->descendants()->limitDepth(1)->get(); ?>

  <li><span><i class="fa fa-angle-right"></i></span></li>
  <li>
  <a href="#" class="loading-bar">Сонгоно уу</a>
  <div class="bm-dropdown">
    <ul>
    @foreach($leaves as $l)
      <li>
        <a href="/c/<?php echo $l->id?>/<?php echo $l->name; ?>"><?php echo $l->name; ?></a>
      </li>
    @endforeach
    </ul>
  </div>
  </li>
  @endif

  

  @endforeach
  @if($non_subscribe === 'false')
  @if($category)
  <?php $scids = Session::get('scids');
  if(!isset($scids[$category->id])):
  ?>
<li><span><a id="openBtn" href="/subscribe/popupCategory/{{Route::Input('id')}}?rand={{md5(uniqid(rand(), true))}}"
    alt="Энэ ангилал дахь заруудыг имэйлээр хүлээж авах бол энд дарна уу!"
    title="Энэ ангилал дахь заруудыг имэйлээр хүлээж авах бол энд дарна уу!"

  ><i class="subscribe_od"></i></a></span></li>
  
  <?php endif;?>
  @endif
@endif  

</ul>

</div>


