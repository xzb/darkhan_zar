<?php use App\Models\Category; ?>
<?php use App\Models\BannerLocation; ?>

<?php $speed    = 0;  $temp_second = array();  ?>
<?php $location      = BannerLocation::find($loc); ?>

<?php $banners = null ?>
@if($loc == 2 && $loc)
    <?php $banners  = Banner::getBannersByLocation($location->id);?>
@endif

@if(sizeOf($banners))
<div class="section-banner">
    <div id="banner-carousel-container">
        <ul id="banner-carousel">
            @foreach($banners as $banner)
            <li>
                <div class="container">
                    <a href="{{ $banner->link ? $banner->link : '#' }}" target="_blank">
                    <div class="banner" style="background: url(/files/uploads/banner/{{ $banner->path }}) no-repeat center bottom;">
                        <!-- <aside>
                            <h4></h4>
                            <p class="bd"></p>
                            <p class="bm" style="position: absolute; top: 230px;"><a href="{{ $banner->link ? $banner->link : '#' }}" target="_blank" class="btn-tour">Дэлгэрэнгүй</a></p>
                        </aside> -->
                    </div>
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif