<?php use App\Models\BannerLocation; ?>
<?php use App\Models\Category; ?>
<?php   
$category = Category::find($category_id);
if($category->banner_id != 0)
$banner = Banner::find($category->banner_id);
?>
@if($banner)
<div class="section-banner">
    <div id="banner-carousel-container">
        <ul id="banner-carousel">
            
            <li>
                <div class="container">
                    <div class="banner" style="background: url(/files/uploads/banner/{{ $banner->path }}) no-repeat center bottom;">
                        
                    </div>
                </div>
            </li>
            
        </ul>
    </div>
</div>
@endif