<link rel="stylesheet" href="/css/banner2.css" />

<div class="banner-top"></div>
<div id="tabs">
    <ul id="list_banners">
        @foreach($banner1 as $b)
        <li><a href="#tabs-{{$b->sort}}" onclick="$(this).children().trigger('click');"><div id="tab_click_{{$b->sort}}" class="li" onclick="changeBg($(this), '{{'/files/uploads/banner/'.$b->path_en}}')">{{$b->name}}</div></a><div class="space"></div></li>
        @endforeach
    </ul>
@foreach($banner1 as $b)    
<div id="tabs-{{$b->sort}}">
    <a href="{{ $b->link }}" target="_blank"><img src="/files/uploads/banner/{{ $b->path }}" target="_blank" alt="banner"></a>
</div>
@endforeach
</div>
<div class="banner-bottom"></div>