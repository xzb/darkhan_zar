@if($banner1)
        @if($banner1->link !='')
            <a href="{{ $banner1->link }}" target="_blank">
        @endif

        @if($loc == 1)
            <div style="background: url(/files/uploads/banner/{{ $banner1->path }}) no-repeat top; margin:-32px 0 20px 0px;
                         top:0px; 
                         position:relative;
                         width:100%; 
                         height:{{$banner1->height}}px;">
                
            </div>
        @elseif($loc == 3)
            <div id="banner2_fixed_div" 
                    style="background: url(/files/uploads/banner/{{ $banner1->path }}) no-repeat top right;  position: fixed;
                                    width: {{$banner1->width}}px;
                                    height: {{$banner1->height}}px;
                                    top:172px;
                                    z-index: 100;
                                    display:none;
                                    ">
            </div>
        @elseif($loc == 4)
            <div id="banner3_fixed_div" 
                style="background: url(/files/uploads/banner/{{ $banner1->path }}) no-repeat top left;  position: fixed;
                                width: {{$banner1->width}}px;
                                height: {{$banner1->height}}px;
                                top:172px;
                                z-index: 100;
                                display:none;
                                ">
            </div>
        @endif

        @if($banner1->link !='')
            </a>
        @endif
    
@endif
    
