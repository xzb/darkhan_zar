<?php use App\Models\Category; ?>
<?php use App\Models\sfUser; ?>
@extends('front._layouts.auto')

@section('main')

    <div class="container bdy_main_content">
      <div class="row auto_more">
          <div class="col-md-2 col-sm-2 col-xs-2 auto_more_left">
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-10 auto_more_middle bottom_shadow">
              <div class="auto_more_heading">
                  <h2>Дугуйлсан зарууд</h2>
                </div>
                <div class="row auto_more_row">
                        @include('front.auto._list', array('products' => $products, 'attributes'=>$stockAttributes))
                        <nav>
                        {{ $products->links('front.auto.pagination'); }}
                        </nav>
                </div>
            </div>
            @include('front.auto._banner', array('banners'=>array()))
        </div>
    </div>


@stop