<?php use App\Models\ProductAttribute; ?>
@if(count($products) > 0 || count($featuredProducts) > 0)
  @foreach($featuredProducts as $product)
  <div class="col-md-12 {{($product->is_featured == 1) ? 'auto_big_img':'small_img'}}">
    <div class="col-md-3 col-sm-3 col-xs-3 product-photo" style="background-color:{{($product->is_featured == 1) ? '#ecf8ff':'#fff'}}">
      <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" target="_blank">
      <span class="sphoto">
      @if($product->image_filename)
        <img src="{{ $base_img_url.'/uploads/thumb/s_'.$product->image_filename }}" alt="" />
        @else
        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
        @endif
      </span>
      </a>
      </div>
      <div class="col-md-7 col-sm-7 col-xs-7">
        <a href="/p/{{ $product->id }}/{{  str_replace('%','хувь',str_replace('/', '', $product->name))  }}" target="_blank"><h3>{{($product->is_featured == 1) ? '<code>Онцлох</code>':''}}{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</h3></a>
          <small>{{ time_ago($product->created_at) }}</small>
          <p>{{  mb_substr($product->description, 0, 300, 'UTF-8') }}</p>
          <div class="tags">
            {{$product->getStockStr()}}
          </div>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-2">
                      <?php $arrayCat = array("23","25","22","218","24","2151","2156","2155","2152","2333","2154");?>
                      @if(in_array($product->category_id, $arrayCat))
                        <?php $say=''; ?>
                      @else  
                        <?php $say='сая'; ?>
                      @endif
        <h3>{{ $product->price>999999999?"-":$product->price.$say.' ₮' }}</h3>
          <a href="#" onclick="autoToggle({{ $product->id }}, this); return false;">
              <span class="round {{ sfUser::hasAuto($product->id) ? 'activeround' : ''; }}"></span>
              <small>Дугуйлах</small>
          </a>
          <span class="compare-hide" style="display: block">
            <input id="compare_id_{{$product->id}}" class="auto_more_chk compare-select" type="checkbox" name="compareId[]" value="{{$product->id}}">
            <label for="compare_id_{{$product->id}}" class="auto_more_chk_lab">Xарьцуулаx</label>
              @if($product->image_filename)
              <img src="{{ $base_img_url.'/uploads/thumb/s_'.$product->image_filename }}" alt="" style="display: none;" >
              @else
              <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="" style="display: none;">
              @endif
          </span>
      </div>
  </div>
  @endforeach
  @foreach($products as $product)
  <div class="col-md-12 {{($product->is_featured == 1) ? 'auto_big_img':'small_img'}}">
    <div class="col-md-3 col-sm-3 col-xs-3 product-photo" style="background-color:{{($product->is_featured == 1) ? '#ecf8ff':'#fff'}}">
      <a href="/p/{{ $product->id }}/{{ str_replace('/', '', $product->name) }}" target="_blank">
      <span class="sphoto">
      @if($product->image_filename)
        <img src="{{ $base_img_url.'/uploads/thumb/s_'.$product->image_filename }}" alt="" />
        @else
        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="">
        @endif
      </span>
      </a>
      </div>
      <div class="col-md-7 col-sm-7 col-xs-7">
        <a href="/p/{{ $product->id }}/{{  str_replace('%','хувь',str_replace('/', '', $product->name))  }}" target="_blank"><h3>{{($product->is_featured == 1) ? '<code>Онцлох</code>':''}}{{  mb_substr($product->name, 0, 100, 'UTF-8') }}</h3></a>
          <small>{{ time_ago($product->created_at) }}</small>
          <p>{{  mb_substr($product->description, 0, 300, 'UTF-8') }}</p>
          <div class="tags">
            {{$product->getStockStr()}}
          </div>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-2">
                      <?php $arrayCat = array("23","25","22","218","24","2151","2156","2155","2152","2333","2154");?>
                      @if(in_array($product->category_id, $arrayCat))
                        <?php $say=''; ?>
                      @else  
                        <?php $say='сая'; ?>
                      @endif
        <h3>{{ $product->price>999999999?"-":$product->price.$say.' ₮' }}</h3>
          <a href="#" onclick="autoToggle({{ $product->id }}, this); return false;">
              <span class="round {{ sfUser::hasAuto($product->id) ? 'activeround' : ''; }}"></span>
              <small>Дугуйлах</small>
          </a>
          <span class="compare-hide" style="display: block">
            <input id="compare_id_{{$product->id}}" class="auto_more_chk compare-select" type="checkbox" name="compareId[]" value="{{$product->id}}"><label for="compare_id_{{$product->id}}" class="auto_more_chk_lab">Xарьцуулаx</label>
              @if($product->image_filename)
              <img src="{{ $base_img_url.'/uploads/thumb/s_'.$product->image_filename }}" alt="" style="display: none;" >
              @else
              <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}" alt="" style="display: none;">
              @endif
          </span>
      </div>
  </div>
  @endforeach
@else
  <div class="col-md-12" style="text-align:center">
      Таны хайлтанд тохирох автомашин олдсонгүй  
  </div>
@endif