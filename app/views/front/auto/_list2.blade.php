@include('front.auto._list', array('products' => $products, 'featuredProducts' => $featuredProducts, 'attributes'=>$stockAttributes, 'ajax'=>true))
<nav>
{{ $products->links('front.auto.pagination'); }}
</nav>

<script>
$(document).ready( function() {
  $(".compare-link").click(function(){
    if ($('#compare_ids').val().split("-", 5).length < 2) {
      alert('Та 2-оос болон түүнээс дээш бүтээгдxүүн сонгоно уу');
    } else {
      var url ='/productCompare?ids='+$('#compare_ids').val();
      window.location.href = url;
    }    
    return false;
  });
//filter PAGE compare
    productCompare.categoryId = 20;
    eval("var compareValues = "+(cookieManager.getCookie("compareValues") || '{categoryId:0,items:{}}')+";");

    //var compareItems = new compareProductItem();
    if (productCompare.categoryId == compareValues.categoryId){
      productCompare.compareItems.setItems(compareValues.items);
    } else {
      cookieManager.setCookie('compareValues', "{categoryId:0,items:{}}");
    }

    //binding
    $('.compare-select').click(function(){
      var element = $(this);
      if (element.attr('checked')) {
        element.removeAttr('checked');
      }else{
        element.attr('checked', 'checked');
      }
      if (element.attr('checked')) {
        
        if (productCompare.compareItems.getLength() < 4){
          productCompare.compareItems.addItem(element.val(), element.siblings('img').attr('src'));
          element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -27px');
        } else {
          alert('Та дээд тал нь 4 бүтээгдэxүүн харьцуулж чадна');
          element.attr('checked', false);
        }
      } else { //removing
        element.parent().find('.auto_more_chk_lab').css('background-position', '-4px -3px');
        productCompare.compareItems.deleteItem(element.val());
      }
      cookieManager.setCookie('compareValues', "{categoryId:"+productCompare.categoryId+",items:"+productCompare.compareItems.serialize()+"}");
      productCompare.bindCompareItems();
    });
    productCompare.bindCompareItems();
});
</script>