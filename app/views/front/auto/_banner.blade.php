<?php use App\Models\Banner; ?>
 <?php $banners  = Banner::getBannersByLocationLimit(5, 1);?>

<div class="col-md-2 col-sm-2 col-xs-2 auto_more_right advertisement_main" id="auto_banner_fixed_div" style="position:fixed">
  @foreach($banners as $banner) 
  <?php if(strtolower(substr(strrchr($banner->path, '.'), 1)) == 'swf'):?>
    <object width="<?php echo $banner->width;?>" height="<?php echo $banner->height;?>">
              <param name="movie" value="/files/uploads/banner/<?php echo $banner->path?>">
              <param name="quality" value="high">
              <param name="wmode" value="transparent">
              <embed width="<?php echo $banner->width;?>" height="<?php echo $banner->height?>" wmode="transparent" src="/files/uploads/banner/<?php echo $banner->path?>" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>
            </object>
    <br/ clear="all">
    <br/ clear="all">
    <?php else:?>
    <a href="{{$banner->link}}">
      <img src="/files/uploads/banner/{{ $banner->path }}">
    </a>
    <br/ clear="all">
    <br/ clear="all">
    <?php endif;?>
    @endforeach
</div>