<?php use App\Models\Category; ?>
<?php use App\Models\ProductAttribute; ?>
<?php use App\Models\sfUser; ?>
@extends('front._layouts.auto')

@section('main')

  
  <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Нүүр хуудас</a></li>
        @foreach($parents as $parentCategory)
            <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
        @endforeach
        <li class="active">{{$category->name}}</li>
    </ol>
    </div>
    <div class="container bdy_main_content bottom_shadow">
      <div class="row auto_compare">
          <div class="col-md-12 top_img common_col_md_12">
              <div class="col-md-2 col-sm-2 col-xs-2 text_bold text_left">
                  Харьцуулалт
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                  @foreach($products as $product)
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div>
                        @if($product->image_filename)
                        <img src="{{ $base_img_url.'/uploads/thumb/s_'.$product->image_filename }}" alt="" />
                        @else
                        <img src="{{ $base_img_url.'/uploads/nophoto.png'; }}"  alt="">
                        @endif
                      </div>    
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 common_col_md_12">
              <div class="col-md-2 col-sm-2 col-xs-2 text_bold">
                  Гарчиг
                </div>
                @foreach($products as $product)
                <div class="col-md-2 col-sm-2 col-xs-2 text_coloured_bold">
                  <a href="#">{{$product->name}}</a>
                </div>
                @endforeach
            </div>
            <div class="col-md-12 common_col_md_12">
              <div class="col-md-2 col-sm-2 col-xs-2 text_bold">
                  Огноо
                </div>
                @foreach($products as $product)
                <div class="col-md-3 col-sm-3 col-xs-3 text_regular">
                  {{ time_ago($product->created_at) }}
                </div>
                @endforeach
            </div>
            <div class="col-md-12 common_col_md_12">
              <div class="col-md-2 col-sm-2 col-xs-2 text_bold">
                  Үнэ
                </div>
                @foreach($products as $product)
                <div class="col-md-3 col-sm-3 col-xs-3 text_bold text_align_left">
                  {{ $product->price>999999999?"-":format_price($product->price) }}
                </div>
                @endforeach
            </div>
            @foreach($attributes as $attribute)
            <div class="col-md-12 common_col_md_12">
              <div class="col-md-2 col-sm-2 col-xs-2 text_bold">
                  {{$attribute->name}}
                </div>
                @foreach($products as $product)
                <?php $attributeValue = ProductAttribute::getProductAttrValues($product->id, $attribute->id, $attribute->type);?>
                <div class="col-md-3 col-sm-3 col-xs-3 text_tag">
                  <i class="fa fa-tag"></i>{{$attributeValue}}
                </div>
                @endforeach
            </div>
            @endforeach
            <div class="col-md-12 common_col_md_12">
            </div>
        </div>
    </div>


@stop