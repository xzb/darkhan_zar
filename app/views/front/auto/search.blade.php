<?php use App\Models\Category; ?>
<?php use App\Models\AttributeValues; ?>
<?php use App\Models\sfUser; ?>
@extends('front._layouts.auto')

@section('main')

    <div class="container bdy_main_content">
      <div class="row auto_more">
           <div class="col-md-2 col-sm-2 col-xs-2 auto_more_left">
                <div onclick="resetSearchFilter('{{$keyword}}'); return false;" id="resetSearchFilter" style="display: <?php if(count($avalids)>0){echo 'block';}else{echo 'none';}?>">Шүүлтүүр арилгах</div>
                <div class="auto_more_box">
                  <h2>Үнэ</h2>
                      <span><input id="price1" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',1,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 1){echo 'checked="checked"';}}?>><label for="price1" class="auto_more_chk_lab2">5 саяас бага</label></span>
                      <span><input id="price2" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',2,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 2){echo 'checked="checked"';}}?>><label for="price2" class="auto_more_chk_lab2">5 - 10 сая</label></span>
                      <span><input id="price3" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',3,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 3){echo 'checked="checked"';}}?>><label for="price3" class="auto_more_chk_lab2">10 - 20 сая</label></span>
                      <span><input id="price4" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',4,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 4){echo 'checked="checked"';}}?>><label for="price4" class="auto_more_chk_lab2">20 - 30 сая</label></span>
                      <span><input id="price5" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',5,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 5){echo 'checked="checked"';}}?>><label for="price5" class="auto_more_chk_lab2">30 - 50 сая</label></span>
                      <span><input id="price6" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',6,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 6){echo 'checked="checked"';}}?>><label for="price6" class="auto_more_chk_lab2">50 саяас дээш</label></span>
                </div>
                <div class="auto_more_box">
                  <h2>Хөдөлгүүрийн багтаамж</h2>
                      <span><input id="cc1" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',1,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 1){echo 'checked="checked"';}}?>><label for="cc1" class="auto_more_chk_lab2">1.0 cc - бага</label></span>
                      <span><input id="cc2" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',2,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 2){echo 'checked="checked"';}}?>><label for="cc2" class="auto_more_chk_lab2">1.0 - 1.5 cc</label></span>
                      <span><input id="cc3" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',3,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 3){echo 'checked="checked"';}}?>><label for="cc3" class="auto_more_chk_lab2">1.6 - 2.0 сc</label></span>
                      <span><input id="cc4" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',4,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 4){echo 'checked="checked"';}}?>><label for="cc4" class="auto_more_chk_lab2">2.1 - 3.0 сc</label></span>
                      <span><input id="cc5" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',5,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 5){echo 'checked="checked"';}}?>><label for="cc5" class="auto_more_chk_lab2">3.1 - 4.0 сc</label></span>
                      <span><input id="cc6" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="searchAttributeText('{{$keyword}}',6,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 6){echo 'checked="checked"';}}?>><label for="cc6" class="auto_more_chk_lab2">4.0 сc - дээш</label></span>
                </div>
              @foreach($attributes as $attribute)
                <div class="auto_more_box">
                  <h2>{{$attribute->name}}</h2>
                      <?php $aValues = AttributeValues::where('attribute_id', '=', $attribute->id)->orderBy('sort_order', 'ASC')->get();?>
                      @foreach($aValues as $aValue)
                      <span><input id="checkbox{{$aValue->id}}" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="searchAttribute('{{$keyword}}',{{$aValue->id}}); return false;" <?php if(in_array($aValue->id, $avalids)){echo 'checked="checked"';}?>><label for="checkbox{{$aValue->id}}" class="auto_more_chk_lab">{{$aValue->value}}</label></span>
                      @endforeach
                  </div>
              @endforeach
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-10 auto_more_middle bottom_shadow">
                <div class="auto_more_heading" turul="search">
                  <h2>Хайлтын үр дүн</h2>
                  <div style="float: left; width: 287px; margin-top: 20px;">
                <ul class="compare-ul">
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li><a style="padding-top: 2px; height: 27px;" href="/productCompare?ids=" class="btn btn-default compare-link">Xарьцуулаx</a></li>
                  </ul>
                </div>
                </div>

              <input type="hidden" id="compare_ids" name="compare_ids" value="" />
              <div style="margin-top:70px;overflow:hidden; border-bottom:solid 1px #dcdcdc;"></div>
                <div class="row auto_more_row" id="autolist">
                  @if(count($products)>0)
                    @include('front.auto._list', array('products' => $products, 'featuredProducts' => $featuredProducts, 'attributes'=>$stockAttributes))
                        <nav>
                        {{ $products->appends(Input::except(array('page')))->links('front.auto.pagination'); }}
                        </nav>
                  @else
                    <center style="padding:30px 0 40px 0"><span class="blank_result">Хайлт илэрцгүй. Зөвлөмж: Үгийг зөв бичсэн эсэхийг шалгана уу мөн өөр үгээр хайж үзнэ үү</span></center>
                  @endif      
                </div>
            </div>
            @include('front.auto._banner', array('banners'=>array()))
        </div>
    </div>


@stop