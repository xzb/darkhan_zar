  <?php use App\Models\Category; ?>
<?php use App\Models\sfUser; ?>
<?php use App\Models\AttributeValues; ?>

@extends('front._layouts.auto')

@section('main')
<?php $producerId = 0;?>
<?php $modelId = 0;?>
  <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Нүүр хуудас</a></li>
        @foreach($parents as $parentCategory)
            <li><a href="/c/{{$parentCategory->id}}">{{$parentCategory->name}}</a></li>
        @endforeach
        <li class="active">{{$category->name}}</li>
    </ol>
    </div>
    <div class="container bdy_main_content">
      <div class="row auto_more">
          <div class="col-md-2 col-sm-2 col-xs-2 auto_more_left">
              <div class="auto_more_box">
                <h2>Төрөл</h2>
                  <select size="7" class="leftSelect">
                    @foreach($parentCates as $id=>$name)
                    <option onclick="gotoCategory({{$id}});" <?php if(in_array($id, $parentsIds)){echo 'selected="selected"';$producerId = $id;}?>>{{$name}}</option>
                    @endforeach
                  </select>
                </div>
                @if($producerId>0)
                <?php $producers = Category::getChildren($producerId);?>
                @if(count($producers)>0)
                <div class="auto_more_box">
                <h2>Үйлдвэрлэгч</h2>
                    <select size="10" class="leftSelect">
                    @foreach($producers as $producer)
                    <option <?php if($producer->rank == 0){echo 'class="featured"';}?> onclick="gotoCategory({{$producer->id;}});" <?php if(in_array($producer->id, $parentsIds)){echo 'selected="selected"';$modelId = $producer->id;}?>>{{$producer->name}}</option>
                    @endforeach
                  </select>
                </div>
                @endif
                @endif
                @if($modelId>0)
                <?php $models = Category::getChildren($modelId);?>
                @if(count($models)>0)
                <div class="auto_more_box">
                <h2>Марк</h2>
                    <select size="10" class="leftSelect">
                    @foreach($models as $model)
                    <option <?php if($model->rank == 0){echo 'class="featured"';}?> onclick="gotoCategory({{$model->id;}});" <?php if(in_array($model->id, $parentsIds)){echo 'selected="selected"';}?>>{{$model->name}}</option>
                    @endforeach
                  </select>
                </div>
                @endif
                @endif
                <?php if(in_array(21, $parentsIds) || in_array(151, $parentsIds)):?>
                <div onclick="resetAll({{$category->id}}); return false;" id="resetAll" style="display: <?php if(count($avalids)>0){echo 'block';}else{echo 'none';}?>">Шүүлтүүр арилгах</div>
                <!-- <div class="auto_more_box">
                  <h2>Үйлдвэрлэсэн он</h2>
                      <span><input id="make_year1" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttributeText({{$category->id}},1,'make_year'); return false;" <?php if(array_key_exists('make_year',$avtexts)){if($avtexts['make_year'] == 1){echo 'checked="checked"';}}?>><label for="make_year1" class="auto_more_chk_lab">1990 оноос өмнө</label></span>
                      <span><input id="make_year2" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttributeText({{$category->id}},2,'make_year'); return false;" <?php if(array_key_exists('make_year',$avtexts)){if($avtexts['make_year'] == 2){echo 'checked="checked"';}}?>><label for="make_year2" class="auto_more_chk_lab">1990-2000 он</label></span>
                      <span><input id="make_year3" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttributeText({{$category->id}},3,'make_year'); return false;" <?php if(array_key_exists('make_year',$avtexts)){if($avtexts['make_year'] == 3){echo 'checked="checked"';}}?>><label for="make_year3" class="auto_more_chk_lab">2000-2010 он</label></span>
                      <span><input id="make_year4" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttributeText({{$category->id}},4,'make_year'); return false;" <?php if(array_key_exists('make_year',$avtexts)){if($avtexts['make_year'] == 4){echo 'checked="checked"';}}?>><label for="make_year4" class="auto_more_chk_lab">2010 оноос хойш</label></span>
                </div> -->
                <div class="auto_more_box">
                  <h2>Үнэ</h2>
                      <span><input id="price1" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},1,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 1){echo 'checked="checked"';}}?>><label for="price1" class="auto_more_chk_lab2">5 саяас бага</label></span>
                      <span><input id="price2" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},2,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 2){echo 'checked="checked"';}}?>><label for="price2" class="auto_more_chk_lab2">5 - 10 сая</label></span>
                      <span><input id="price3" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},3,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 3){echo 'checked="checked"';}}?>><label for="price3" class="auto_more_chk_lab2">10 - 20 сая</label></span>
                      <span><input id="price4" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},4,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 4){echo 'checked="checked"';}}?>><label for="price4" class="auto_more_chk_lab2">20 - 30 сая</label></span>
                      <span><input id="price5" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},5,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 5){echo 'checked="checked"';}}?>><label for="price5" class="auto_more_chk_lab2">30 - 50 сая</label></span>
                      <span><input id="price6" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},6,'price'); return false;" <?php if(array_key_exists('price',$avtexts)){if($avtexts['price'] == 6){echo 'checked="checked"';}}?>><label for="price6" class="auto_more_chk_lab2">50 саяас дээш</label></span>
                </div>
                <div class="auto_more_box">
                  <h2>Хөдөлгүүрийн багтаамж</h2>
                      <span><input id="cc1" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},1,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 1){echo 'checked="checked"';}}?>><label for="cc1" class="auto_more_chk_lab2">1.0 cc - бага</label></span>
                      <span><input id="cc2" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},2,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 2){echo 'checked="checked"';}}?>><label for="cc2" class="auto_more_chk_lab2">1.0 - 1.5 cc</label></span>
                      <span><input id="cc3" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},3,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 3){echo 'checked="checked"';}}?>><label for="cc3" class="auto_more_chk_lab2">1.6 - 2.0 сc</label></span>
                      <span><input id="cc4" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},4,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 4){echo 'checked="checked"';}}?>><label for="cc4" class="auto_more_chk_lab2">2.1 - 3.0 сc</label></span>
                      <span><input id="cc5" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},5,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 5){echo 'checked="checked"';}}?>><label for="cc5" class="auto_more_chk_lab2">3.1 - 4.0 сc</label></span>
                      <span><input id="cc6" type="checkbox" name="checkbox" value="1" class="auto_more_chk2 autoselecterselect" onclick="gotoAttributeText({{$category->id}},6,'cc'); return false;" <?php if(array_key_exists('cc',$avtexts)){if($avtexts['cc'] == 6){echo 'checked="checked"';}}?>><label for="cc6" class="auto_more_chk_lab2">4.0 сc - дээш</label></span>
                </div>
              @foreach($attributes as $attribute)
                <div class="auto_more_box">
                  <h2>{{$attribute->name}}</h2>
                      <?php $aValues = AttributeValues::where('attribute_id', '=', $attribute->id)->orderBy('sort_order', 'ASC')->get();?>
                      @foreach($aValues as $aValue)
                      <span><input id="checkbox{{$aValue->id}}" type="checkbox" name="checkbox" value="1" class="auto_more_chk autoselecterselect" onclick="gotoAttribute({{$category->id}},{{$aValue->id}}); return false;" <?php if(in_array($aValue->id, $avalids)){echo 'checked="checked"';}?>><label for="checkbox{{$aValue->id}}" class="auto_more_chk_lab">{{$aValue->value}}</label></span>
                      @endforeach
                  </div>
              @endforeach
              <?php endif;?>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-10 auto_more_middle bottom_shadow">
              <div class="auto_more_heading" turul="category">
                  <h2>{{ $category->name}}</h2>
                  <?php //if($category->isComparable()):?>
                <div style="float: left;width: 287px; margin-top: 20px;">
                <ul class="compare-ul">
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li class="compare-li"></li>
                    <li><a style="padding-top: 2px; height: 27px;" href="/productCompare?ids=" class="btn btn-default compare-link">Xарьцуулаx</a></li>
                  </ul>
                </div>
              <input type="hidden" id="compare_ids" name="compare_ids" value="" />
              <?php //endif?>
                    <div class="sorting">
                      <select id="sortType" name="sortType" style="font-size: 11px;width: 170px;" class="form-control-home" onchange="orderList({{$category->id}});">
                        <option <?php if($sortType == 'price_desc'){echo 'selected="selected"';}?> value="price_desc">Үнэ буурахаар</option>
                        <option <?php if($sortType == 'price_asc'){echo 'selected="selected"';}?> value="price_asc">Үнэ өсөхөөр</option>
                        <option <?php if($sortType == 'date_asc'){echo 'selected="selected"';}?> value="date_asc">Нэмэгдсэн огноо өсөхөөр</option>
                        <option <?php if($sortType == 'date_desc'){echo 'selected="selected"';}?> value="date_desc">Нэмэгдсэн огноо буурахаар</option>
                        <option <?php if($sortType == 'year_asc'){echo 'selected="selected"';}?> value="year_asc">Орж ирсэн он өсөхөөр</option>
                        <option <?php if($sortType == 'year_desc'){echo 'selected="selected"';}?> value="year_desc">Орж ирсэн он буурахаар</option>

                      </select>
                    </div>
                </div>
                <div style="margin-top:70px;overflow:hidden; border-bottom:solid 1px #dcdcdc;"></div>
                <div class="row auto_more_row" id="autolist">
                    @include('front.auto._list', array('products' => $products, 'featuredProducts' => $featuredProducts, 'attributes'=>$stockAttributes, 'ajax'=>false))
                    <nav>
                    <!-- {{ $products->links('front.auto.pagination'); }} -->
                    {{ $products->links(); }}
                    </nav>
                </div>
            </div>
            @include('front.auto._banner', array('banners'=>array()))
        </div>
    </div>
@stop