<link rel="stylesheet" href="/css/perfect-scrollbar.min.css" />
<link rel="stylesheet" href="/css/chats.css" />

<?php use App\Models\Chats; ?>
@extends('front._layouts.product')

@section('main')
<script type="text/javascript" src="/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/js/jquery.main.js"></script>
                <div class="chat-main">
                    <div class="row">
                        <div class="row-same-height">
                            <div class="col-md-4  col-chat col-md-height">
                                <h4 class="chat-title" id="message{{$me->id}}">Мессеж</h4>
                                <div class="msg-scroll">
                                    <div class="content">
                                        <ul class="msg-list" id="profiles-msg">
                                            <?php $k = 0;?>
                                            @if(count($profiles)>0)
                                                @foreach($profiles as $p)
                                                    <?php $k++; ?>
                                                    <?php $imagewithpath =($p->image != '')?'/uploads/profile/'.$p->image:'/images/profile.png'; ?>
                                                    @if($chat_user_id == 0)
                                                        @if($k == 1)
                                                            <?php $class = 'active'; ?>
                                                            <script>
                                                                $(function(){
                                                                    chatLoadingMessages('{{$p->id}}',$('#li_a{{$p->id}}'));
                                                                });
                                                            </script> 

                                                        @else
                                                            <?php $class = ''; ?>
                                                        @endif
                                                    @else
                                                        @if($chat_user_id == $p->id)
                                                            <?php $class = 'active'; ?>
                                                            <script>
                                                                $(function(){
                                                                    chatLoadingMessages('{{$p->id}}',$('#li_a{{$p->id}}'));
                                                                });
                                                            </script> 
                                                        @else
                                                        <?php $class = ''; ?>
                                                        @endif
                                                    @endif

                                                    <li><a id ="li_a{{$p->id}}" href="#" onclick = "chatLoadingMessages({{$p->id}},$(this))">
                                                        <img src="{{$imagewithpath}}" alt="" width="42px" height="42px"/>    
                                                        <p><strong>{{$p->first_name}} @if(in_array($p->id,$onlineUsers)) <span style="color:green">online</span> @endif</strong></p>
                                                         <p><span class="paragraph-end"></span>{{$p->last_login}}</p>
                                                         <span class="badge" id="badge{{$p->id}}"></span>
                                                    </a></li>

                                                @endforeach
                                            @endif
                                            
                                        </ul>
                                        <input type="hidden" id="user_id" value="{{$me->id}}"/>
                                        <input type="hidden" id="to_user_id"/>
                                        <input type="hidden" id="my_profile_pic" value="{{$my_profile->image}}"/>
                                        <input type="hidden" id="my_profile_name" value="{{$me->first_name}}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- end left side-->
                            <div class="col-md-8 col-chat col-md-height" id="chats_body">
                                
                            </div><!-- end right side-->
                               
                                
                        </div>
                    </div>
                </div><!-- end chatmain-->



<script>

    $(function(){
        window.setInterval(function() {
            var count2 = 0;
            $('#chats_body').find('li').each(function(){
                count2++; 
            });
            if(count2 > $('#conversationCount').val())
            {
                $(".chat-scroll").scrollTop( $( ".chat-scroll" ).prop( "scrollHeight" ) );
                $(".chat-scroll").perfectScrollbar('update');    
                $('#conversationCount').val(count2);
            }
        }, 1000);

        app.BrainSocket.Event.listen('generic.event',function(msg){
            if(msg.client.data.user_id == $('#user_id').val()){
                if($('#my_profile_pic').val() == '')
                var image = '/images/profile.png';
                else
                var image = '/uploads/profile/'+$('#my_profile_pic').val();
                $('#chat-log'+$('#user_id').val()+'_'+$('#to_user_id').val()).append('<li><a href="" class="img"><img src="'+image+'" width="42px" alt="" /></a><dl class="timeline-series"><dt><a href="#" class="name"><strong>{{$me->first_name}}</strong><span class="date">{{date("Y-m-d H:i:s",time())}}</span></a></dt><dd class="timeline-event-content" id="event3EX"><p>'+msg.client.data.message+'</p></dd></dl></li>');
            }else{
                if(msg.client.data.to_user_id == $('#user_id').val())
                {
                    var str_test='<li><a href="" class="img"><img src="'+msg.client.data.user_portrait+'" width="42px" alt="" /></a><dl class="timeline-series"><dt><a href="#" class="name"><strong>'+msg.client.data.user_name+'</strong><span class="date">'+msg.client.data.created_at+'</span></a></dt><dd class="timeline-event-content" id="event3EX"><p>'+msg.client.data.message+'</p></dd></dl></li>';
                    if($('#li_a'+msg.client.data.user_id).parent().attr("class") == 'active')
                    {
                        $('#chat-log'+$('#user_id').val()+'_'+$('#to_user_id').val()).append(str_test);   
                    }
                }
            }
        });

        $('#chat_input').keypress(function(event) {
             
            if(event.keyCode == 13){
                 var image = '';
                  if($('#my_profile_pic').val() == '')
                    image = '/images/profile.png';
                  else
                    image = '/uploads/profile/'+$('#my_profile_pic').val();
                  
                app.BrainSocket.message('generic.event',
                    {
                        'message':$(this).val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':'/uploads/profile/{{$me->first_name}}',
                        'created_at':'{{date("Y-m-d H:i:s",time())}}',
                    }
                );
                chating($('#user_id').val(), $('#to_user_id').val(),$(this).val());
                $(this).val('');
 
            }
 
            return event.keyCode != 13; 
        });

        $('#chat_button').click(function() {
                 var image = '';
                  if($('#my_profile_pic').val() == '')
                    image = '/images/profile.png';
                  else
                    image = '/uploads/profile/'+$('#my_profile_pic').val();
                  app.BrainSocket.message('generic.event',
                    {
                        'message':$('#chat_input').val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':image,
                        'user_name':$('#my_profile_name').val(),
                        'created_at':getDateTime()
                    }
                  );
                chating($('#user_id').val(), $('#to_user_id').val(),$('#chat_input').val());
                $('#chat_input').val('');
        });
    });
</script>
@stop

