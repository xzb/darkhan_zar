<?php use App\Models\Chats; ?>
@extends('front._layouts.default')

@section('main')
    <style>
    .ui-tabs-vertical { width: 55em; }
    .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
    </style>
<div class="chat_body">
    @if($profiles)
    <div id="tabs">
        <ul>
            @foreach($profiles as $p)
                <li><a href="#tabs-{{$p->id}}" onclick = "$('#to_user_id').val({{$p->id}});"> {{$p->first_name}} @if(in_array($p->id,$onlineUsers)) {{'online'}} @endif</a></li>
            @endforeach
        </ul>
        <!-- SELECT * FROM chats WHERE (user_id = 12 and to_user_id = 11)or(user_id = 11 and to_user_id = 12) order by id asc  -->
        <?php $arrUsers = array(); ?>
        @foreach($profiles as $p)
        <?php
            $arrUsers[] = $p->id; 
            $countUnread = array();
            $conversations = Chats::where(function($query)use ($p,$me) {
                    return $query->where('user_id','=',$p->id)->where('to_user_id','=',$me->id);
                })->orWhere(function($query)use ($p,$me) {
                    return $query->where('user_id','=',$me->id)->where('to_user_id','=',$p->id);
                })->orderBy('id')->get();
        ?>

        <div id="tabs-{{$p->id}}" style="min-height: 469px; overflow-y:scroll">
            <ul class="chat-messages" id="chat-log{{$me->id}}_{{$p->id}}">
            @foreach($conversations as $c)
            <?php
                if($c->is_read == 0)
                {
                    if (array_key_exists($p->id,$countUnread))
                    $countUnread[$p->id] = $countUnread[$p->id] + 1;
                }
                $image =($c->user_id == $my_profile->user_id)?$my_profile->image:$p->image;
                
            ?>
            <li><img src="/uploads/profile/{{$image}}" class="img-circle" width="26"><div class="message">{{$c->text}}</div></li>
            @endforeach
            </ul>
            @if (array_key_exists($p->id,$countUnread))
            <div id="countUnread_{{$countUnread[$p->id]}}" count="{{$countUnread[$p->id]}}"></div>
            @endif                
        </div>
        @endforeach
    </div>
    <input type="hidden" id="user_id" value="{{$me->id}}"/>
    <input type="hidden" id="to_user_id" value="{{$arrUsers[0]}}"/>
    <input type="text" id="chat_button" class="form-input" style="float: left;  width:714px"/>
    @else
    <div style="padding-top:200px">
    Танд зурвас байхгүй байна
    </div>
    @endif
</div>
<script>
    $(function(){
        
        // var fake_user_id = $('#user_id').val();
        // var to_user_id = $('#to_user_id').val();
        
        app.BrainSocket.Event.listen('generic.event',function(msg){
            console.log(msg);
            if(msg.client.data.user_id == $('#user_id').val()){
                $('#chat-log'+$('#user_id').val()+'_'+$('#to_user_id').val()).append('<li><img src="/uploads/profile/{{$my_profile->image}}" class="img-circle" width="26"><div class="message">'+msg.client.data.message+'</div></li>');
                console.log('a---------'+$('#user_id').val()+'_'+$('#to_user_id').val());
            }else{
                if(msg.client.data.to_user_id == $('#user_id').val())
                {
                    var str_test='<li class="right"><img src="'+msg.client.data.user_portrait+'" class="img-circle" width="26"><div class="message">'+msg.client.data.message+'</div></li>';
                    $('#chat-log'+$('#user_id').val()+'_'+$('#to_user_id').val()).append(str_test);
                    console.log('b---------'+$('#user_id').val()+'_'+$('#to_user_id').val());
                }
            }
        });
 
        $('#chat_button').keypress(function(event) {
 
            if(event.keyCode == 13){
 
                app.BrainSocket.message('generic.event',
                    {
                        'message':$(this).val(),
                        'user_id':$('#user_id').val(),
                        'to_user_id':$('#to_user_id').val(),
                        'user_portrait':'/uploads/profile/{{$my_profile->image}}'
                    }
                );
                $(this).val('');
 
            }
 
            return event.keyCode != 13; }
        );
    });
</script>
@stop

