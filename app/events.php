<?php
	// use App\Models\Chats;

	Event::listen('event.user.login', function($user)
	{
		
	    return BrainSocket::success(array('There was a Laravel App Success Event!'));
	});

	Event::listen('generic.event',function($client_data){
		return BrainSocket::message('generic.event',array('message'=>'A message from a generic event fired in Laravel!'));
	});

	Event::listen('event.notify.chat',function($client_data){
		return BrainSocket::message('event.notify.chat',array('message'=>'A message from a event notifification form chat fired in Laravel!'));
	});

	 
	Event::listen('app.success',function($client_data){
	    return BrainSocket::success(array('There was a Laravel App Success Event!'));
	});
	 
	Event::listen('app.error',function($client_data){
	    return BrainSocket::error(array('There was a Laravel App Error!'));
	});
