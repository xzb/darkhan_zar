<?php

function time_ago($timestamp, $format = '%number% %word%') {
  if (!is_numeric($timestamp)) {
    $timestamp = strtotime($timestamp);
  }

  $now_timestamp = time();

  if ($timestamp > $now_timestamp) {
    $timestamp = $now_timestamp;
  }
  // 
  $distance_in_hour = ceil(abs($now_timestamp - $timestamp) / 3600) + 1;

  if ($distance_in_hour <= 23) {
    $parameters['%word%'] = '';
    $parameters['%number%'] = 'Өнөөдөр';
  } else if ($distance_in_hour <= 48) {
    $parameters['%word%'] = '';
    $parameters['%number%'] = 'Өчигдөр';
  } else {
    $parameters['%word%'] = '';
    $parameters['%number%'] = date("Y-m-d", $timestamp);
  }
  return strtr($format, $parameters);
}

function time_ago2($from_time, $to_time = null, $include_seconds = false)
{
  if (!is_numeric($from_time))
  {
    $from_time = strtotime($from_time);
  }

  $to_time = $to_time ? $to_time : time();

  if ($from_time > $to_time)
    $from_time = $to_time;

  $distance_in_minutes = floor(abs($to_time - $from_time) / 60);
  $distance_in_seconds = floor(abs($to_time - $from_time));

  $string = '';
  $parameters = array();


  if ($distance_in_minutes >= 1 && $distance_in_minutes <= 1439)
  {
    $string = 'Өнөөдөр';
  }
  else if($distance_in_seconds <= 60) {
    $string = 'Өнөөдөр';
  }
  else if ($distance_in_minutes >= 1440 && $distance_in_minutes <= 2879)
  {
    $string = 'Өчигдөр';
  }
  else
  {
    return date("Y-m-d", $from_time);
  }

  return strtr($string, $parameters);
}

function getMglNamesForDays($day)
{

  switch ($day) {
      case 0:
          return 'Ням';
          break;
      case 1:
          return 'Даваа';
          break;
      case 2:
          return 'Мягмар';
          break;
      case 3:
          return 'Лхагва';
          break;
      case 4:
          return 'Пүрэв';
          break;
      case 5:
          return 'Баасан';
          break;
      case 6:
          return 'Бямба';
          break;
      default:
         return 'Даваа.';
  }  
}

function imagePath($page) {

  if($page->image_old) {
    return $_ENV['baseurl'].'/pic/'.$page->image_old;
  } else {
    return $_ENV['tmpurl'].'/files/uploads/'.$page->image;
  }

}

function mbsubstr($string, $start, $len) {
  return mb_substr ($string, 0, $len, 'utf-8');  
}

function format_price($price) {
  return number_format($price, 0, '.', ',') . ' ₮';
}

function textAnalyze($text) {
  $text = preg_replace('/[\'`´"]/u', '', $text);
  $text = preg_replace('/[^\p{L}\p{N}]+/u', ' ', $text);
  $text = str_replace('  ', ' ', $text);
  
  $terms = explode(' ', $text);

  $ret = array();
  if (!empty($terms)) {
    foreach ($terms as $i => $term) {
      if (empty($term)) {
        continue;
      }

      $lower = mb_strtolower(trim($term), 'UTF-8');

      if (!isset($ret[$lower])) {
        $ret[$lower] = 0;
      }
      $ret[$lower] ++;
    }
  }
  return $ret;
}


// tusgai temdegt tuuhgui
function textAnalyze2($text) {
  $text = preg_replace('/[\'`´"]/u', '', $text);
  $text = str_replace('  ', ' ', $text);
  
  $terms = explode(' ', $text);

  $ret = array();
  if (!empty($terms)) {
    foreach ($terms as $i => $term) {
      if (empty($term)) {
        continue;
      }

      $lower = mb_strtolower(trim($term), 'UTF-8');

      if (!isset($ret[$lower])) {
        $ret[$lower] = 0;
      }
      $ret[$lower] ++;
    }
  }
  return $ret;
}

function translateText($text) {
  $text = mb_strtolower($text);

  $chars_cyr = array('й', 'Ө','ө', 'Ү','ү', 'ш', 'е', 'ю', 'ё', 'я', 'ч', 'э', 'р', 'т', 'ы', 'у', 'и', 'о', 'п', 'а', 'с', 'д', 'ф', 'г', 'х', 'ж', 'к', 'л', 'з', 'ц', 'в', 'б', 'н', 'м', 'х', 'Х', 'У');
  $chars_lat = array('i', 'O','o', 'U','u', 'sh', 'ye', 'yu', 'yo', 'ya', 'ch', 'e', 'r', 't', 'ii', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'ts', 'v', 'b', 'n', 'm', 'h', 'H', 'U');

  $chars_cyr2lat = array_combine($chars_cyr, $chars_lat);
  $chars_lat2cyr = array_combine($chars_lat, $chars_cyr);

  $chars_mix = array_merge($chars_cyr2lat, $chars_lat2cyr);

  return strtr($text, $chars_mix);
}

function cp1251_utf8($word)  {
    $cyr_lower_chars = array(
        'е', 'щ', 'ф', 'ц', 'у', 'ж', 'э',
        'н', 'г', 'ш', 'ү', 'з', 'к', 'ъ',
        'й', 'ы', 'б', 'ө', 'а', 'х', 'р',
        'о', 'л', 'д', 'п', 'я', 'ч', 'ё',
        'с', 'м', 'и', 'т', 'ь', 'в', 'ю',);

    $latin_lower_chars = array(
        'å', 'ù', 'ô', 'ö', 'ó', 'æ', 'ý',
        'í', 'ã', 'ø', '¿', 'ç', 'ê', 'ú',
        'é', 'û', 'á', 'º', 'à', 'õ', 'ð',
        'î', 'ë', 'ä', 'ï', 'ÿ', '÷', '¸',
        'ñ', 'ì', 'è', 'ò', 'ü', 'â', 'þ',);

    $cyr_upper_chars = array(
        'Е', 'Щ', 'Ф', 'Ц', 'У', 'Ж', 'Э',
        'Н', 'Г', 'Ш', 'Ү', 'З', 'К', 'Ъ',
        'Й', 'Ы', 'Б', 'Ө', 'А', 'Х', 'Р',
        'О', 'Л', 'Д', 'П', 'Я', 'Ч', 'Ё',
        'С', 'М', 'И', 'Т', 'Ь', 'В', 'Ю',);

    $latin_upper_chars = array(
        'Å', 'Ù', 'Ô', 'Ö', 'Ó', 'Æ', 'Ý',
        'Í', 'Ã', 'Ø', '¯', 'Ç', 'Ê', 'Ú',
        'É', 'Û', 'Á', 'ª', 'À', 'Õ', 'Ð',
        'Î', 'Ë', 'Ä', 'Ï', 'ß', '×', '¨',
        'Ñ', 'Ì', 'È', 'Ò', 'Ü', 'Â', 'Þ',);

    //replacing lower cyrillic
    $word = str_replace($latin_lower_chars, $cyr_lower_chars, $word);
    //replacing upper cyrillic
    $word = str_replace($latin_upper_chars, $cyr_upper_chars, $word);
    return $word;
  }

  function randomPassword() {
      $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()_+";
      $pass = array(); //remember to declare $pass as an array
      $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
      for ($i = 0; $i < 10; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }
      return implode($pass); //turn the array into a string
  }

function cleanInput($input) {

  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );

    $output = preg_replace($search, '', $input);
    return $output;
  }
  
function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        //$output = mysql_real_escape_string($input);
        $output = $input;
    }
    return $output;
}

function getFileExtension($fileName){
    return strtolower(substr(strrchr($fileName, '.'), 1));
}

// 1: cryll to lat
// 2: lat to cryll
function stringConverter($str, $type = 1)
{
  $cyr  = array('а','б','в','г','д','e','ж','з','и','й','к','л','м','н','о','п','р','с','т','у', 
  'ф','х','ц','ч','ш','щ','ъ','ь', 'ю','я','А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У',
  'Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ь', 'Ю','Я','Ё','Ө','Ү','ё','ө','ү','Ы','ы','Э','э','Е','е' );
  $lat = array( 'a','b','v','g','d','e','j','z','i','i','k','l','m','n','o','p','r','s','t','u',
  'f' ,'h' ,'ts' ,'ch','sh' ,'sht' ,'a' ,'y' ,'yu' ,'ya','A','B','V','G','D','E','J',
  'Z','I','I','K','L','M','N','O','P','R','S','T','U',
  'F' ,'H' ,'Ts' ,'Ch','Sh' ,'Sht' ,'A' ,'Y' ,'Yu' ,'Ya','Yo','U','U','yo','u','u','I','i','E','e','E','e' );

  if($type == 1) {
    return str_replace($lat, $cyr, $str);
  } else {
    return str_replace($lat, $cyr, $str);
  }

}
?>