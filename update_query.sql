ALTER TABLE `location` ADD `sort` INT NOT NULL DEFAULT '0' AFTER `level`;

ALTER TABLE `product` ADD `auto_renew_time` INT NOT NULL DEFAULT '0' AFTER `owner_type`;

ALTER TABLE `category` ADD `for_title` TINYINT NOT NULL DEFAULT '0' AFTER `link`, ADD `for_url` TINYINT NOT NULL DEFAULT '0' AFTER `for_title`;

ALTER TABLE `attribute` ADD `for_title` TINYINT NOT NULL DEFAULT '0' AFTER `sort_order`; 


