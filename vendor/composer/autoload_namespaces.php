<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Whoops' => array($vendorDir . '/filp/whoops/src'),
    'Way\\Generators' => array($vendorDir . '/way/generators/src'),
    'Vinicius73\\SEO' => array($vendorDir . '/vinicius73/seotools/src'),
    'System' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Security\\Core\\' => array($vendorDir . '/symfony/security-core'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Stack' => array($vendorDir . '/stack/builder/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Predis' => array($vendorDir . '/predis/predis/lib'),
    'PHPParser' => array($vendorDir . '/nikic/php-parser/lib'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'OAuth\\Unit' => array($vendorDir . '/lusitanian/oauth/tests'),
    'OAuth' => array($vendorDir . '/lusitanian/oauth/src'),
    'Net' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Mustache' => array($vendorDir . '/mustache/mustache/src'),
    'Math' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'Krucas\\Notification' => array($vendorDir . '/edvinaskrucas/notification/src'),
    'Jeremeamia\\SuperClosure' => array($vendorDir . '/jeremeamia/SuperClosure/src'),
    'JShrink' => array($vendorDir . '/tedivm/jshrink/src'),
    'Illuminate' => array($vendorDir . '/laravel/framework/src'),
    'Guzzle\\Stream' => array($vendorDir . '/guzzle/stream'),
    'Guzzle\\Parser' => array($vendorDir . '/guzzle/parser'),
    'Guzzle\\Http' => array($vendorDir . '/guzzle/http'),
    'Guzzle\\Common' => array($vendorDir . '/guzzle/common'),
    'Fitztrev\\LaravelHtmlMinify' => array($vendorDir . '/fitztrev/laravel-html-minify/src'),
    'File' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'DebugBar' => array($vendorDir . '/maximebf/debugbar/src'),
    'Crypt' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'ClassPreloader' => array($vendorDir . '/classpreloader/classpreloader/src'),
    'CeesVanEgmond\\Minify' => array($vendorDir . '/ceesvanegmond/minify/src'),
    'Cartalyst\\Sentry' => array($vendorDir . '/cartalyst/sentry/src'),
    'Carbon' => array($vendorDir . '/nesbot/carbon/src'),
    'Calotype\\SEO' => array($vendorDir . '/calotype/seo/src'),
    'BrainSocket' => array($vendorDir . '/brainboxlabs/brain-socket/src'),
    'Boris' => array($vendorDir . '/d11wtq/boris/lib'),
    'Bkwld\\Croppa' => array($vendorDir . '/bkwld/croppa/src'),
    'Baum' => array($vendorDir . '/baum/baum/src'),
    'Artisaninweb\\SoapWrapper' => array($vendorDir . '/artisaninweb/laravel-soap/src'),
    'Artdarek\\OAuth' => array($vendorDir . '/artdarek/oauth-4-laravel/src'),
);
